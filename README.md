# Lottery Analyzer

Analisador de sorteios de loteria que apresenta estatísticas com base em alguns modelos matemáticos.

Requer DevExpress 16.2.5+ e [delphi-units](https://gitlab.com/d3adf1sh/delphi-units).

Depois de compilar, copiar o conteúdo da pasta `resources` para `build/bin`.
