//=============================================================================
// U_MainForm
// Lottery Analyzer
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_MainForm;

interface

uses
  Windows,
  SysUtils,
  Classes,
  Graphics,
  Controls,
  Forms,
  StdCtrls,
  ExtCtrls,
  ComCtrls,
  ImageList,
  ImgList,
  Menus,
  DB,
  DBClient,
  cxButtons,
  cxCalc,
  cxCheckBox,
  cxCheckComboBox,
  cxClasses,
  cxContainer,
  cxControls,
  cxCurrencyEdit,
  cxCustomData,
  cxData,
  cxDataStorage,
  cxDBData,
  cxDropDownEdit,
  cxEdit,
  cxFilter,
  cxGraphics,
  cxGrid,
  cxGridCustomPopupMenu,
  cxGridCustomTableView,
  cxGridCustomView,
  cxGridDBTableView,
  cxGridLevel,
  cxGridPopupMenu,
  cxGridTableView,
  cxGroupBox,
  cxInplaceContainer,
  cxLabel,
  cxListView,
  cxLookAndFeelPainters,
  cxLookAndFeels,
  cxMaskEdit,
  cxMemo,
  cxNavigator,
  cxPC,
  cxSplitter,
  cxStyles,
  cxTL,
  cxTLdxBarBuiltInMenu,
  cxTextEdit,
  cxTreeView,
  dxBarBuiltInMenu,
  dxSkinOffice2010Silver,
  dxSkinsCore,
  dxSkinscxPCPainter,
  dxSkinsForm,
  U_FormType,
  U_LotteryAnalyzer,
  U_TableFrame,
  U_TextFrame,
  U_TextAndTableFrame;

type
  // TfrmPrincipal
  TMainForm = class(TRLFormType)
    scSkin: TdxSkinController;
    ilImagens: TcxImageList;
    gpmHistorico: TcxGridPopupMenu;
    gpmJogos: TcxGridPopupMenu;
    gpmSimulador: TcxGridPopupMenu;
    dsHistorico: TDataSource;
    dsJogos: TDataSource;
    dsSimulador: TDataSource;
    dsComparativo: TDataSource;
    dsAcertos: TDataSource;
    pnlLoteria: TPanel;
    lblLoteria: TcxLabel;
    cbLoteria: TcxComboBox;
    pcOpcoes: TcxPageControl;
    tsHome: TcxTabSheet;
    lblLoteriaSelecionada: TcxLabel;
    lblTarefasHistorico: TcxLabel;
    btnImportarHistorico: TcxButton;
    btnExportarHistorico: TcxButton;
    btnAtualizarEstatisticasHistorico: TcxButton;
    lblTarefasEstatisticas: TcxLabel;
    btnRecalcularEstatisticas: TcxButton;
    lblDesenvolvidoPor: TcxLabel;
    lblEmail: TcxLabel;
    tsHistorico: TcxTabSheet;
    grdHistorico: TcxGrid;
    grdHistoricoView: TcxGridDBTableView;
    grdHistoricoLevel: TcxGridLevel;
    tsEstatisticas: TcxTabSheet;
    pnlEstatisticas: TPanel;
    tvEstatisticas: TcxTreeView;
    splEstatisticas: TcxSplitter;
    lblEstatisticas: TcxLabel;
    tsSimulador: TcxTabSheet;
    pnlSimulador: TPanel;
    pnlParametrosSimulador: TPanel;
    lblQuantidadeJogosSimulador: TcxLabel;
    edtQuantidadeJogosSimulador: TcxCurrencyEdit;
    lblParesSimulador: TcxLabel;
    chkParesSimulador: TcxCheckBox;
    edtMinimoParesSimulador: TcxCurrencyEdit;
    edtMaximoParesSimulador: TcxCurrencyEdit;
    edtMargemParesSimulador: TcxCurrencyEdit;
    lblPrimosSimulador: TcxLabel;
    chkPrimosSimulador: TcxCheckBox;
    edtMinimoPrimosSimulador: TcxCurrencyEdit;
    edtMaximoPrimosSimulador: TcxCurrencyEdit;
    edtMargemPrimosSimulador: TcxCurrencyEdit;
    lblIntervaloSimulador: TcxLabel;
    chkIntervaloSimulador: TcxCheckBox;
    edtMinimoIntervaloSimulador: TcxCurrencyEdit;
    edtMaximoIntervaloSimulador: TcxCurrencyEdit;
    edtMargemIntervaloSimulador: TcxCurrencyEdit;
    lblQuadrantesSimulador: TcxLabel;
    edtMinimoQuadrantesSimulador: TcxCurrencyEdit;
    edtMaximoQuadrantesSimulador: TcxCurrencyEdit;
    edtMargemQuadrantesSimulador: TcxCurrencyEdit;
    lblRepeticaoDezenasSimulador: TcxLabel;
    edtMinimoRepeticaoDezenasSimulador: TcxCurrencyEdit;
    edtMaximoRepeticaoDezenasSimulador: TcxCurrencyEdit;
    edtMargemRepeticaoDezenasSimulador: TcxCurrencyEdit;
    lblLinhaSimulador: TcxLabel;
    chkLinhaSimulador: TcxCheckBox;
    ccbLinhaSimulador: TcxCheckComboBox;
    edtMargemLinhaSimulador: TcxCurrencyEdit;
    lblColunaSimulador: TcxLabel;
    chkColunaSimulador: TcxCheckBox;
    ccbColunaSimulador: TcxCheckComboBox;
    edtMargemColunaSimulador: TcxCurrencyEdit;
    lblSequenciaSimulador: TcxLabel;
    chkSequenciaSimulador: TcxCheckBox;
    ccbSequenciaSimulador: TcxCheckComboBox;
    edtMargemSequenciaSimulador: TcxCurrencyEdit;
    lblFrequenciaSimulador: TcxLabel;
    ccbFrequenciaSimulador: TcxCheckComboBox;
    edtMargemFrequenciaSimulador: TcxCurrencyEdit;
    lblAtrasoSimulador: TcxLabel;
    ccbAtrasoSimulador: TcxCheckComboBox;
    edtMargemAtrasoSimulador: TcxCurrencyEdit;
    lblQuocienteFrequenciaSimulador: TcxLabel;
    cbQuocienteFrequenciaSimulador: TcxComboBox;
    lblQuocienteAtrasoSimulador: TcxLabel;
    cbQuocienteAtrasoSimulador: TcxComboBox;
    lblNumeroAcertos: TcxLabel;
    edtNumeroAcertos: TcxCurrencyEdit;
    lblDezenasPorAcerto: TcxLabel;
    edtDezenasPorAcerto: TcxCurrencyEdit;
    lblSorteios: TcxLabel;
    edtSorteioInicial: TcxCurrencyEdit;
    edtSorteioFinal: TcxCurrencyEdit;
    btnImportarParametrosSimulador: TcxButton;
    btnExportarParametrosSimulador: TcxButton;
    btnLimparParametrosSimulador: TcxButton;
    btnExecutarSimulador: TcxButton;
    grdResultadoSimulador: TcxGrid;
    grdResultadoSimuladorView: TcxGridDBTableView;
    grdResultadoSimuladorLevel: TcxGridLevel;
    tsAposta: TcxTabSheet;
    pnlAposta: TPanel;
    pnlParametrosAposta: TPanel;
    lblQuantidadeJogosAposta: TcxLabel;
    edtQuantidadeJogosAposta: TcxCurrencyEdit;
    lblParesAposta: TcxLabel;
    edtMinimoParesAposta: TcxCurrencyEdit;
    edtMaximoParesAposta: TcxCurrencyEdit;
    edtMargemParesAposta: TcxCurrencyEdit;
    lblPrimosAposta: TcxLabel;
    edtMinimoPrimosAposta: TcxCurrencyEdit;
    edtMaximoPrimosAposta: TcxCurrencyEdit;
    edtMargemPrimosAposta: TcxCurrencyEdit;
    lblIntervaloAposta: TcxLabel;
    edtMinimoIntervaloAposta: TcxCurrencyEdit;
    edtMaximoIntervaloAposta: TcxCurrencyEdit;
    edtMargemIntervaloAposta: TcxCurrencyEdit;
    lblQuadrantesAposta: TcxLabel;
    edtMinimoQuadrantesAposta: TcxCurrencyEdit;
    edtMaximoQuadrantesAposta: TcxCurrencyEdit;
    edtMargemQuadrantesAposta: TcxCurrencyEdit;
    lblRepeticaoDezenasAposta: TcxLabel;
    edtMinimoRepeticaoDezenasAposta: TcxCurrencyEdit;
    edtMaximoRepeticaoDezenasAposta: TcxCurrencyEdit;
    edtMargemRepeticaoDezenasAposta: TcxCurrencyEdit;
    lblLinhaAposta: TcxLabel;
    ccbLinhaAposta: TcxCheckComboBox;
    edtMargemLinhaAposta: TcxCurrencyEdit;
    lblColunaAposta: TcxLabel;
    ccbColunaAposta: TcxCheckComboBox;
    edtMargemColunaAposta: TcxCurrencyEdit;
    lblSequenciaAposta: TcxLabel;
    ccbSequenciaAposta: TcxCheckComboBox;
    edtMargemSequenciaAposta: TcxCurrencyEdit;
    lblFrequenciaAposta: TcxLabel;
    ccbFrequenciaAposta: TcxCheckComboBox;
    edtMargemFrequenciaAposta: TcxCurrencyEdit;
    lblAtrasoAposta: TcxLabel;
    ccbAtrasoAposta: TcxCheckComboBox;
    edtMargemAtrasoAposta: TcxCurrencyEdit;
    lblQuocienteFrequenciaAposta: TcxLabel;
    cbQuocienteFrequenciaAposta: TcxComboBox;
    lblQuocienteAtrasoAposta: TcxLabel;
    cbQuocienteAtrasoAposta: TcxComboBox;
    btnImportarParametrosAposta: TcxButton;
    btnExportarParametrosAposta: TcxButton;
    btnLimparParametrosAposta: TcxButton;
    btnGerarAposta: TcxButton;
    btnCopiarAposta: TcxButton;
    pcAposta: TcxPageControl;
    tsParametros: TcxTabSheet;
    mParametrosAposta: TcxMemo;
    tsJogos: TcxTabSheet;
    grdJogos: TcxGrid;
    grdJogosView: TcxGridDBTableView;
    grdJogosLevel: TcxGridLevel;
    tsComparativo: TcxTabSheet;
    tlComparativo: TcxTreeList;
    tlcComparativoDescricao: TcxTreeListColumn;
    tlcComparativoQuantidade: TcxTreeListColumn;
    tlcComparativoPercentual: TcxTreeListColumn;
    tlcComparativoPercentualHistorico: TcxTreeListColumn;
    tlcComparativoDiferenca: TcxTreeListColumn;
    procedure FormCreate(Sender: TObject);
    procedure cbLoteriaPropertiesChange(Sender: TObject);
    procedure btnImportarHistoricoClick(Sender: TObject);
    procedure btnExportarHistoricoClick(Sender: TObject);
    procedure btnAtualizarEstatisticasHistoricoClick(Sender: TObject);
    procedure btnRecalcularEstatisticasClick(Sender: TObject);
    procedure grdHistoricoViewEditKeyDown(Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word; Shift: TShiftState);
    procedure tvEstatisticasChange(Sender: TObject; Node: TTreeNode);
    procedure tvEstatisticasDeletion(Sender: TObject; Node: TTreeNode);
    procedure chkParesSimuladorPropertiesChange(Sender: TObject);
    procedure chkPrimosSimuladorPropertiesChange(Sender: TObject);
    procedure chkLinhaSimuladorPropertiesChange(Sender: TObject);
    procedure chkColunaSimuladorPropertiesChange(Sender: TObject);
    procedure chkSequenciaSimuladorPropertiesChange(Sender: TObject);
    procedure btnImportarParametrosSimuladorClick(Sender: TObject);
    procedure btnExportarParametrosSimuladorClick(Sender: TObject);
    procedure btnLimparParametrosSimuladorClick(Sender: TObject);
    procedure btnExecutarSimuladorClick(Sender: TObject);
    procedure btnImportarParametrosApostaClick(Sender: TObject);
    procedure btnExportarParametrosApostaClick(Sender: TObject);
    procedure btnLimparParametrosApostaClick(Sender: TObject);
    procedure btnGerarApostaClick(Sender: TObject);
    procedure btnCopiarApostaClick(Sender: TObject);
    procedure chkIntervaloSimuladorPropertiesChange(Sender: TObject);
  private
    FLoteria: TTipoLoteria;
    FHistorico: TDataSet;
    FResultadoSimulador: TDataSet;
    FJogos: TDataSet;
    FComparativo: TDataSet;
    FAcertos: TDataSet;
    procedure AtualizarResumo;
    procedure AtualizarHistorico;
    procedure AtualizarEstatisticas;
    procedure AtualizarResultadoSimulador(const ASimulador: TSimulador);
    procedure AtualizarJogos(const AAposta: TAposta);
    procedure AtualizarComparativo(const AAposta: TAposta);
    procedure PopularLinha(const AItems: TcxCheckComboBoxItems);
    procedure PopularColuna(const AItems: TcxCheckComboBoxItems);
    procedure PopularSequencia(const AItems: TcxCheckComboBoxItems);
    procedure PopularOcorrencia(const AItems: TcxCheckComboBoxItems);
    procedure LerParametrosSimulador(const AParametros: TParametrosSimulador);
    procedure LerParametrosAposta(const AParametros: TParametrosAposta);
    procedure PreencherParametrosSimulador(const AParametros: TParametrosSimulador);
    procedure PreencherParametrosAposta(const AParametros: TParametrosAposta);
    procedure AoGerarJogo(Sender: TObject; Dezenas: TDezenas; Valido: Boolean; var Continuar: Boolean);
  public
    destructor Destroy; override;
  end;

implementation

uses
  Math, Clipbrd, Dialogs, U_Wait, U_DXDialog, U_DXWait;

const
  ImgEstatistica = 4;
  ImgValorEstatistica = 5;

{$R *.dfm}

destructor TMainForm.Destroy;
begin
  if Assigned(FAcertos) then
    FreeAndNil(FAcertos);
  if Assigned(FComparativo) then
    FreeAndNil(FComparativo);
  if Assigned(FJogos) then
    FreeAndNil(FJogos);
  if Assigned(FResultadoSimulador) then
    FreeAndNil(FResultadoSimulador);
  if Assigned(FHistorico) then
    FreeAndNil(FHistorico);
  inherited;
end;

procedure TMainForm.FormCreate(Sender: TObject);
var
  I: Integer;
begin
  Caption := Application.Title;
  AllowSkipControl := True;
  pcOpcoes.ActivePageIndex := 0;
  pcAposta.ActivePageIndex := 0;
  for I := 0 to Pred(TLoteria.Instance.Tipos.Count) do
  begin
    cbLoteria.Properties.Items.AddObject(TLoteria.Instance.Tipos[I].Nome,
      TLoteria.Instance.Tipos[I]);
  end;

  cbLoteria.ItemIndex := 0;
end;

procedure TMainForm.cbLoteriaPropertiesChange(Sender: TObject);
begin
  TRLDXWait.Instance.Show('Carregando loteria...');
  try
    FLoteria := TTipoLoteria(
      cbLoteria.Properties.Items.Objects[cbLoteria.ItemIndex]);
    AtualizarResumo;
    AtualizarHistorico;
    AtualizarEstatisticas;
    chkParesSimulador.Enabled := FLoteria.Estatisticas.Contains(COcorrenciaPorPares);
    chkPrimosSimulador.Enabled := FLoteria.Estatisticas.Contains(COcorrenciaPorPrimos);
    edtMinimoQuadrantesSimulador.Enabled := FLoteria.Estatisticas.Contains(COcorrenciaPorQuadrante);
    edtMaximoQuadrantesSimulador.Enabled := edtMinimoQuadrantesSimulador.Enabled;
    edtMargemQuadrantesSimulador.Enabled := edtMinimoQuadrantesSimulador.Enabled;
    edtMinimoRepeticaoDezenasSimulador.Enabled := FLoteria.Estatisticas.Contains(COcorrenciaPorRepeticaoDezenas);
    edtMaximoRepeticaoDezenasSimulador.Enabled := edtMinimoRepeticaoDezenasSimulador.Enabled;
    edtMargemRepeticaoDezenasSimulador.Enabled := edtMinimoRepeticaoDezenasSimulador.Enabled;
    chkLinhaSimulador.Enabled := FLoteria.Estatisticas.Contains(COcorrenciaPorLinha);
    if chkLinhaSimulador.Enabled then
      PopularLinha(ccbLinhaSimulador.Properties.Items);
    chkColunaSimulador.Enabled := FLoteria.Estatisticas.Contains(COcorrenciaPorColuna);
    if chkColunaSimulador.Enabled then
      PopularColuna(ccbColunaSimulador.Properties.Items);
    chkSequenciaSimulador.Enabled := FLoteria.Estatisticas.Contains(COcorrenciaPorSequencia);
    if chkSequenciaSimulador.Enabled then
      PopularSequencia(ccbSequenciaSimulador.Properties.Items);
    ccbFrequenciaSimulador.Enabled := FLoteria.Estatisticas.Contains(CFrequencia);
    if ccbFrequenciaSimulador.Enabled then
      PopularOcorrencia(ccbFrequenciaSimulador.Properties.Items);
    edtMargemFrequenciaSimulador.Enabled := ccbFrequenciaSimulador.Enabled;
    ccbAtrasoSimulador.Enabled := FLoteria.Estatisticas.Contains(CAtraso);
    if ccbAtrasoSimulador.Enabled then
      PopularOcorrencia(ccbAtrasoSimulador.Properties.Items);
    edtMargemAtrasoSimulador.Enabled := ccbAtrasoSimulador.Enabled;
    cbQuocienteFrequenciaSimulador.Enabled := ccbFrequenciaSimulador.Enabled;
    cbQuocienteAtrasoSimulador.Enabled := ccbAtrasoSimulador.Enabled;
    btnLimparParametrosSimuladorClick(nil);
    edtMinimoParesAposta.Enabled := FLoteria.Estatisticas.Contains(COcorrenciaPorPares);
    edtMaximoParesAposta.Enabled := edtMinimoParesAposta.Enabled;
    edtMargemParesAposta.Enabled := edtMinimoParesAposta.Enabled;
    edtMinimoPrimosAposta.Enabled := FLoteria.Estatisticas.Contains(COcorrenciaPorPrimos);
    edtMaximoPrimosAposta.Enabled := edtMinimoPrimosAposta.Enabled;
    edtMargemPrimosAposta.Enabled := edtMinimoPrimosAposta.Enabled;
    edtMinimoQuadrantesAposta.Enabled := FLoteria.Estatisticas.Contains(COcorrenciaPorQuadrante);
    edtMaximoQuadrantesAposta.Enabled := edtMinimoQuadrantesAposta.Enabled;
    edtMargemQuadrantesAposta.Enabled := edtMinimoQuadrantesAposta.Enabled;
    edtMinimoRepeticaoDezenasAposta.Enabled := FLoteria.Estatisticas.Contains(COcorrenciaPorRepeticaoDezenas);;
    edtMaximoRepeticaoDezenasAposta.Enabled := edtMinimoRepeticaoDezenasAposta.Enabled;
    edtMargemRepeticaoDezenasAposta.Enabled := edtMinimoRepeticaoDezenasAposta.Enabled;
    ccbLinhaAposta.Enabled := FLoteria.Estatisticas.Contains(COcorrenciaPorLinha);
    if ccbLinhaAposta.Enabled then
      PopularLinha(ccbLinhaAposta.Properties.Items);
    edtMargemLinhaAposta.Enabled := ccbLinhaAposta.Enabled;
    ccbColunaAposta.Enabled := FLoteria.Estatisticas.Contains(COcorrenciaPorColuna);
    if ccbColunaAposta.Enabled then
      PopularColuna(ccbColunaAposta.Properties.Items);
    edtMargemColunaAposta.Enabled := ccbColunaAposta.Enabled;
    ccbSequenciaAposta.Enabled := FLoteria.Estatisticas.Contains(COcorrenciaPorSequencia);
    if ccbSequenciaAposta.Enabled then
      PopularSequencia(ccbSequenciaAposta.Properties.Items);
    edtMargemSequenciaAposta.Enabled := ccbSequenciaAposta.Enabled;
    ccbFrequenciaAposta.Enabled := FLoteria.Estatisticas.Contains(CFrequencia);
    if ccbFrequenciaAposta.Enabled then
      PopularOcorrencia(ccbFrequenciaAposta.Properties.Items);
    edtMargemFrequenciaAposta.Enabled := ccbFrequenciaAposta.Enabled;
    ccbAtrasoAposta.Enabled := FLoteria.Estatisticas.Contains(CAtraso);
    if ccbAtrasoAposta.Enabled then
      PopularOcorrencia(ccbAtrasoAposta.Properties.Items);
    edtMargemAtrasoAposta.Enabled := ccbAtrasoAposta.Enabled;
    cbQuocienteFrequenciaAposta.Enabled := ccbFrequenciaAposta.Enabled;
    cbQuocienteAtrasoAposta.Enabled := ccbAtrasoAposta.Enabled;
    btnLimparParametrosApostaClick(nil);
  finally
    TRLDXWait.Instance.Hide;
  end;
end;

procedure TMainForm.btnImportarHistoricoClick(Sender: TObject);
var
  lDialogo: TOpenDialog;
begin
  lDialogo := TOpenDialog.Create(nil);
  try
    lDialogo.Title := 'Importar histórico';
    lDialogo.Filter := 'Arquivos CSV|*.csv';
    lDialogo.DefaultExt := '*.csv';
    lDialogo.Options := lDialogo.Options + [ofFileMustExist];
    if lDialogo.Execute then
    begin
      Screen.Cursor := crHourGlass;
      try
        Application.ProcessMessages;
        FLoteria.ImportarArquivoHistorico(lDialogo.FileName);
        AtualizarHistorico;
      finally
        Screen.Cursor := crDefault;
      end;

      TRLDXDialog.Instance.Show('Importado com sucesso.');
    end;
  finally
    FreeAndNil(lDialogo);
  end;
end;

procedure TMainForm.btnExportarHistoricoClick(Sender: TObject);
var
  lDialogo: TSaveDialog;
begin
  lDialogo := TSaveDialog.Create(nil);
  try
    lDialogo.Title := 'Exportar histórico';
    lDialogo.Filter := 'Arquivos CSV|*.csv';
    lDialogo.DefaultExt := '*.csv';
    lDialogo.Options := lDialogo.Options + [ofPathMustExist, ofOverwritePrompt];
    if lDialogo.Execute then
    begin
      Screen.Cursor := crHourGlass;
      try
        Application.ProcessMessages;
        FLoteria.ExportarArquivoHistorico(lDialogo.FileName);
        AtualizarHistorico;
      finally
        Screen.Cursor := crDefault;
      end;

      TRLDXDialog.Instance.Show('Exportado com sucesso.');
    end;
  finally
    FreeAndNil(lDialogo);
  end;
end;

procedure TMainForm.btnAtualizarEstatisticasHistoricoClick(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  try
    Application.ProcessMessages;
    FLoteria.AtualizarEstatisticasHistorico;
    AtualizarHistorico;
  finally
    Screen.Cursor := crDefault;
  end;

  TRLDXDialog.Instance.Show('Estatísticas atualizadas.');
end;

procedure TMainForm.btnRecalcularEstatisticasClick(Sender: TObject);
begin
  TRLDXWait.Instance.Show('Aguarde, recalculando estatísticas...',
    wkCalculating);
  try
    FLoteria.RecalcularEstatisticas(True);
    AtualizarEstatisticas;
  finally
    TRLDXWait.Instance.Hide;
  end;

  TRLDXDialog.Instance.Show('Estatísticas recalculadas.');
end;

procedure TMainForm.grdHistoricoViewEditKeyDown(
  Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem;
  AEdit: TcxCustomEdit; var Key: Word; Shift: TShiftState);
begin
  if (UpCase(Chr(Key)) = 'C') and (ssCtrl in Shift) then
    Sender.CopyToClipboard(True);
end;

procedure TMainForm.tvEstatisticasChange(Sender: TObject; Node: TTreeNode);
var
  lInfo: TInfoEstatistica;
begin
  if Assigned(Node) and Assigned(Node.Data) then
  begin
    lInfo := TInfoEstatistica(Node.Data);
    if not Assigned(lInfo.Painel) then
    begin
      if Supports(lInfo.Estatistica, ITabela) and
        Supports(lInfo.Estatistica, ITexto) then
        lInfo.Painel := TTextAndTableFrame.Create(nil)
      else
        if Supports(lInfo.Estatistica, ITabela) then
          lInfo.Painel := TTableFrame.Create(nil)
        else
          if Supports(lInfo.Estatistica, ITexto) then
            lInfo.Painel := TTextFrame.Create(nil);
      if Assigned(lInfo.Painel) then
      begin
        lInfo.Painel.Estatistica := lInfo.Estatistica;
        lInfo.Painel.Parent := pnlEstatisticas;
        lInfo.Painel.Align := alClient;
        try
          if not lInfo.Estatistica.Calculada then
          begin
            TRLDXWait.Instance.Show('Aguarde, atualizando estatística...',
              wkCalculating);
          end;

          lInfo.Estatistica.CalcularEmThread(False);
          LockWindowUpdate(pnlEstatisticas.Handle);
          try
            lInfo.Painel.Mostrar;
          finally
            LockWindowUpdate(0);
          end;
        finally
          TRLDXWait.Instance.Hide;
        end;
      end;
    end
    else
      lInfo.Painel.BringToFront;
  end;
end;

procedure TMainForm.tvEstatisticasDeletion(Sender: TObject;
  Node: TTreeNode);
begin
  if Assigned(Node.Data) then
    TObject(Node.Data).Free;
end;

procedure TMainForm.chkParesSimuladorPropertiesChange(Sender: TObject);
begin
  edtMinimoParesSimulador.Enabled := chkParesSimulador.Checked;
  edtMaximoParesSimulador.Enabled := chkParesSimulador.Checked;
  edtMargemParesSimulador.Enabled := chkParesSimulador.Checked;
end;

procedure TMainForm.chkPrimosSimuladorPropertiesChange(Sender: TObject);
begin
  edtMinimoPrimosSimulador.Enabled := chkPrimosSimulador.Checked;
  edtMaximoPrimosSimulador.Enabled := chkPrimosSimulador.Checked;
  edtMargemPrimosSimulador.Enabled := chkPrimosSimulador.Checked;
end;

procedure TMainForm.chkIntervaloSimuladorPropertiesChange(Sender: TObject);
begin
  edtMinimoIntervaloSimulador.Enabled := chkIntervaloSimulador.Checked;
  edtMaximoIntervaloSimulador.Enabled := chkIntervaloSimulador.Checked;
  edtMargemIntervaloSimulador.Enabled := chkIntervaloSimulador.Checked;
end;

procedure TMainForm.chkLinhaSimuladorPropertiesChange(Sender: TObject);
begin
  ccbLinhaSimulador.Enabled := chkLinhaSimulador.Checked;
  edtMargemLinhaSimulador.Enabled := chkLinhaSimulador.Checked;
end;

procedure TMainForm.chkColunaSimuladorPropertiesChange(Sender: TObject);
begin
  ccbColunaSimulador.Enabled := chkColunaSimulador.Checked;
  edtMargemColunaSimulador.Enabled := chkColunaSimulador.Checked;
end;

procedure TMainForm.chkSequenciaSimuladorPropertiesChange(Sender: TObject);
begin
  ccbSequenciaSimulador.Enabled := chkSequenciaSimulador.Checked;
  edtMargemSequenciaSimulador.Enabled := chkSequenciaSimulador.Checked;
end;

procedure TMainForm.btnImportarParametrosSimuladorClick(Sender: TObject);
var
  lDialogo: TOpenDialog;
  lParametros: TParametrosSimulador;
begin
  lDialogo := TOpenDialog.Create(nil);
  try
    lDialogo.Title := 'Importar parâmetros';
    lDialogo.Filter := 'Arquivos XML|*.xml';
    lDialogo.DefaultExt := '*.xml';
    lDialogo.Options := lDialogo.Options + [ofFileMustExist];
    if lDialogo.Execute then
    begin
      Screen.Cursor := crHourGlass;
      try
        Application.ProcessMessages;
        lParametros := TParametrosSimulador.Create;
        try
          lParametros.LerXML(lDialogo.FileName);
          LerParametrosSimulador(lParametros);
        finally
          FreeAndNil(lParametros);
        end;
      finally
        Screen.Cursor := crDefault;
      end;
    end;
  finally
    FreeAndNil(lDialogo);
  end;
end;

procedure TMainForm.btnExportarParametrosSimuladorClick(Sender: TObject);
var
  lDialogo: TSaveDialog;
  lParametros: TParametrosSimulador;
begin
  lDialogo := TSaveDialog.Create(nil);
  try
    lDialogo.Title := 'Exportar parâmetros';
    lDialogo.Filter := 'Arquivos XML|*.xml';
    lDialogo.DefaultExt := '*.xml';
    lDialogo.Options := lDialogo.Options + [ofPathMustExist, ofOverwritePrompt];
    if lDialogo.Execute then
    begin
      Screen.Cursor := crHourGlass;
      try
        Application.ProcessMessages;
        lParametros := TParametrosSimulador.Create;
        try
          PreencherParametrosSimulador(lParametros);
          lParametros.GravarXML(lDialogo.FileName);
        finally
          FreeAndNil(lParametros);
        end;
      finally
        Screen.Cursor := crDefault;
      end;
    end;
  finally
    FreeAndNil(lDialogo);
  end;
end;

procedure TMainForm.btnLimparParametrosSimuladorClick(Sender: TObject);
var
  lParametros: TParametrosSimulador;
begin
  Screen.Cursor := crHourGlass;
  try
    lParametros := TParametrosSimulador.Create;
    try
      lParametros.DezenasPorAcerto := FLoteria.DezenasPremiacaoMinima;
      lParametros.SorteioInicial := FLoteria.Historico.Count;
      lParametros.SorteioFinal := FLoteria.Historico.Count;
      LerParametrosSimulador(lParametros);
    finally
      FreeAndNil(lParametros);
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TMainForm.btnExecutarSimuladorClick(Sender: TObject);
var
  lSimulador: TSimulador;
begin
  lSimulador := TSimulador.Create;
  try
    lSimulador.Loteria := FLoteria;
    lSimulador.AoGerarJogo := AoGerarJogo;
    PreencherParametrosSimulador(lSimulador.Parametros);
    try
      TRLDXWait.Instance.Show('Aguarde, executando simulação...',
        wkProcessing, True);
      try
        lSimulador.ExecutarEmThread;
        AtualizarResultadoSimulador(lSimulador);
      finally
        TRLDXWait.Instance.Hide;
      end;
    except
      on E: Exception do
      begin
        if E is ELoteria then
          TRLDXDialog.Instance.ShowWarning(E.Message)
        else
          raise;
      end;
    end;
  finally
    FreeAndNil(lSimulador);
  end;
end;

procedure TMainForm.btnImportarParametrosApostaClick(Sender: TObject);
var
  lDialogo: TOpenDialog;
  lParametros: TParametrosAposta;
begin
  lDialogo := TOpenDialog.Create(nil);
  try
    lDialogo.Title := 'Importar parâmetros';
    lDialogo.Filter := 'Arquivos XML|*.xml';
    lDialogo.DefaultExt := '*.xml';
    lDialogo.Options := lDialogo.Options + [ofFileMustExist];
    if lDialogo.Execute then
    begin
      Screen.Cursor := crHourGlass;
      try
        Application.ProcessMessages;
        lParametros := TParametrosAposta.Create;
        try
          lParametros.LerXML(lDialogo.FileName);
          LerParametrosAposta(lParametros);
        finally
          FreeAndNil(lParametros);
        end;
      finally
        Screen.Cursor := crDefault;
      end;
    end;
  finally
    FreeAndNil(lDialogo);
  end;
end;

procedure TMainForm.btnExportarParametrosApostaClick(Sender: TObject);
var
  lDialogo: TSaveDialog;
  lParametros: TParametrosAposta;
begin
  lDialogo := TSaveDialog.Create(nil);
  try
    lDialogo.Title := 'Exportar parâmetros';
    lDialogo.Filter := 'Arquivos XML|*.xml';
    lDialogo.DefaultExt := '*.xml';
    lDialogo.Options := lDialogo.Options + [ofPathMustExist, ofOverwritePrompt];
    if lDialogo.Execute then
    begin
      Screen.Cursor := crHourGlass;
      try
        Application.ProcessMessages;
        lParametros := TParametrosAposta.Create;
        try
          PreencherParametrosAposta(lParametros);
          lParametros.GravarXML(lDialogo.FileName);
        finally
          FreeAndNil(lParametros);
        end;
      finally
        Screen.Cursor := crDefault;
      end;
    end;
  finally
    FreeAndNil(lDialogo);
  end;
end;

procedure TMainForm.btnLimparParametrosApostaClick(Sender: TObject);
var
  lParametros: TParametrosAposta;
begin
  Screen.Cursor := crHourGlass;
  try
    lParametros := TParametrosAposta.Create;
    try
      LerParametrosAposta(lParametros);
    finally
      FreeAndNil(lParametros);
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TMainForm.btnGerarApostaClick(Sender: TObject);
var
  lAposta: TAposta;
  lFiltros: TFiltros;
  nTentativas: Integer;
begin
  lAposta := TAposta.Create;
  try
    lAposta.Loteria := FLoteria;
    lAposta.AoGerarJogo := AoGerarJogo;
    PreencherParametrosAposta(lAposta.Parametros);
    lFiltros := lAposta.GerarFiltros;
    try
      lAposta.CopiarEstatisticasLoteria(True);
      Application.ProcessMessages;
      try
        TRLDXWait.Instance.Show('Aguarde, gerando jogos...', wkProcessing, True);
        try
          lAposta.GerarJogosComEstatisticaEmThread(lFiltros, nTentativas);
          lAposta.GerarString(mParametrosAposta.Lines);
          AtualizarJogos(lAposta);
          AtualizarComparativo(lAposta);
        finally
          TRLDXWait.Instance.Hide;
        end;
      except
        on E: Exception do
        begin
          if E is ELoteria then
            TRLDXDialog.Instance.ShowWarning(E.Message)
          else
            raise;
        end;
      end;
    finally
      FreeAndNil(lFiltros);
    end;
  finally
    FreeAndNil(lAposta);
  end;
end;

procedure TMainForm.btnCopiarApostaClick(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  try
    grdJogosView.CopyToClipboard(True);
    Clipboard.AsText := Trim(
      mParametrosAposta.Text + sLineBreak + Clipboard.AsText);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TMainForm.AtualizarResumo;
begin
  lblLoteriaSelecionada.Caption := 'Selecionado: ' + cbLoteria.Text;
end;

procedure TMainForm.AtualizarHistorico;
begin
  grdHistoricoView.BeginUpdate;
  try
    if Assigned(FHistorico) then
      FreeAndNil(FHistorico);
    FHistorico := FLoteria.Historico.CriarDataSet;
    dsHistorico.DataSet := FHistorico;
    grdHistoricoView.ClearItems;
    grdHistoricoView.DataController.CreateAllItems(True);
  finally
    grdHistoricoView.EndUpdate;
  end;

  grdHistoricoView.ApplyBestFit;
end;

procedure TMainForm.AtualizarEstatisticas;
var
  lInfo: TInfoEstatistica;
  lItem: TTreeNode;
  I: Integer;
begin
  tvEstatisticas.Items.BeginUpdate;
  try
    tvEstatisticas.ClearSelection;
    LockWindowUpdate(pnlEstatisticas.Handle);
    try
      tvEstatisticas.Items.Clear;
    finally
      LockWindowUpdate(0);
    end;

    for I := 0 to FLoteria.Estatisticas.Count - 1 do
      if not (FLoteria.Estatisticas[I] is TEstatisticaPorContagem) or
        not TEstatisticaPorContagem(
          FLoteria.Estatisticas[I]).SomenteComparativo then
      begin
        lInfo := TInfoEstatistica.Create;
        lInfo.Estatistica := FLoteria.Estatisticas[I];
        lItem := tvEstatisticas.Items.AddObject(nil,
          FLoteria.Estatisticas[I].Descricao, lInfo);
        lItem.ImageIndex := ImgEstatistica;
        lItem.SelectedIndex := ImgEstatistica;
      end;
  finally
    tvEstatisticas.Items.EndUpdate;
  end;
end;

procedure TMainForm.AtualizarResultadoSimulador(
  const ASimulador: TSimulador);
begin
  grdResultadoSimuladorView.BeginUpdate;
  try
    if Assigned(FResultadoSimulador) then
      FreeAndNil(FResultadoSimulador);
    if Assigned(ASimulador) then
      FResultadoSimulador := ASimulador.CriarDataSetResultados;
    dsSimulador.DataSet := FResultadoSimulador;
    grdResultadoSimuladorView.ClearItems;
    grdResultadoSimuladorView.DataController.CreateAllItems(True);
  finally
    grdResultadoSimuladorView.EndUpdate;
  end;

  grdResultadoSimuladorView.ApplyBestFit;
end;

procedure TMainForm.AtualizarJogos(const AAposta: TAposta);
begin
  grdJogosView.BeginUpdate;
  try
    if Assigned(FJogos) then
      FreeAndNil(FJogos);
    if Assigned(AAposta) then
      FJogos := AAposta.Jogos.CriarDataSet;
    dsJogos.DataSet := FJogos;
    grdJogosView.ClearItems;
    grdJogosView.DataController.CreateAllItems(True);
  finally
    grdJogosView.EndUpdate;
  end;

  grdJogosView.ApplyBestFit;
end;

procedure TMainForm.AtualizarComparativo(const AAposta: TAposta);
var
  lResultado: TcxTreeListNode;
  lValor: TcxTreeListNode;
  I: Integer;
  J: Integer;
begin
  tlComparativo.BeginUpdate;
  try
    tlComparativo.Clear;
    for I := 0 to Pred(AAposta.Comparativo.Resultados.Count) do
    begin
      lResultado := tlComparativo.Root.AddChild;
      lResultado.ImageIndex := ImgEstatistica;
      lResultado.SelectedIndex := ImgEstatistica;
      tlcComparativoDescricao.Values[lResultado] :=
        AAposta.Comparativo.Resultados[I].Descricao;
      for J := 0 to Pred(AAposta.Comparativo.Resultados[I].Valores.Count) do
      begin
        lValor := lResultado.AddChild;
        lValor.ImageIndex := ImgValorEstatistica;
        lValor.SelectedIndex := ImgValorEstatistica;
        tlcComparativoDescricao.Values[lValor] :=
          AAposta.Comparativo.Resultados[I].Valores[J].Valor;
        tlcComparativoQuantidade.Values[lValor] :=
          AAposta.Comparativo.Resultados[I].Valores[J].Quantidade;
        tlcComparativoPercentual.Values[lValor] :=
          AAposta.Comparativo.Resultados[I].Valores[J].Percentual;
        tlcComparativoPercentualHistorico.Values[lValor] :=
          AAposta.Comparativo.Resultados[I].Valores[J].PercentualHistorico;
        tlcComparativoDiferenca.Values[lValor] :=
          AAposta.Comparativo.Resultados[I].Valores[J].Diferenca;
      end;
    end;

    tlComparativo.ApplyBestFit;
  finally
    tlComparativo.EndUpdate;
  end;
end;

procedure TMainForm.PopularLinha(const AItems: TcxCheckComboBoxItems);
var
  I: Integer;
begin
  AItems.BeginUpdate;
  try
    AItems.Clear;
    for I := 0 to Pred(FLoteria.Gabarito.Count) do
      if FLoteria.Gabarito[I].Linha then
        AItems.AddCheckItem(FLoteria.Gabarito[I].Nome);
  finally
    AItems.EndUpdate;
  end;
end;

procedure TMainForm.PopularColuna(const AItems: TcxCheckComboBoxItems);
var
  I: Integer;
begin
  AItems.BeginUpdate;
  try
    AItems.Clear;
    for I := 0 to Pred(FLoteria.Gabarito.Count) do
      if FLoteria.Gabarito[I].Coluna then
        AItems.AddCheckItem(FLoteria.Gabarito[I].Nome);
  finally
    AItems.EndUpdate;
  end;
end;

procedure TMainForm.PopularSequencia(const AItems: TcxCheckComboBoxItems);
var
  I: Integer;
begin
  AItems.BeginUpdate;
  try
    AItems.Clear;
    for I := 0 to Pred(FLoteria.Gabarito.Count) do
      if FLoteria.Gabarito[I].Sequencia then
        AItems.AddCheckItem(FLoteria.Gabarito[I].Nome);
  finally
    AItems.EndUpdate;
  end;
end;

procedure TMainForm.PopularOcorrencia(const AItems: TcxCheckComboBoxItems);
var
  lStrings: TStrings;
  I: Integer;
begin
  AItems.BeginUpdate;
  try
    AItems.Clear;
    lStrings := TStringList.Create;
    try
      FLoteria.GerarStringOcorrencia(lStrings);
      for I := 0 to Pred(lStrings.Count) do
        AItems.AddCheckItem(lStrings[I]);
    finally
      FreeAndNil(lStrings);
    end;
  finally
    AItems.EndUpdate;
  end;
end;

procedure TMainForm.LerParametrosSimulador(
  const AParametros: TParametrosSimulador);
var
  I: Integer;
begin
  edtQuantidadeJogosSimulador.Value := AParametros.QuantidadeJogos;
  edtMinimoParesSimulador.Value := AParametros.MinimoPares;
  edtMaximoParesSimulador.Value := AParametros.MaximoPares;
  edtMargemParesSimulador.Value := AParametros.MargemPares;
  edtMinimoPrimosSimulador.Value := AParametros.MinimoPrimos;
  edtMaximoPrimosSimulador.Value := AParametros.MaximoPrimos;
  edtMargemPrimosSimulador.Value := AParametros.MargemPrimos;
  edtMinimoQuadrantesSimulador.Value := AParametros.MinimoQuadrantes;
  edtMaximoQuadrantesSimulador.Value := AParametros.MaximoQuadrantes;
  edtMargemQuadrantesSimulador.Value := AParametros.MargemQuadrantes;
  edtMinimoRepeticaoDezenasSimulador.Value := AParametros.MinimoRepeticaoDezenas;
  edtMaximoRepeticaoDezenasSimulador.Value := AParametros.MaximoRepeticaoDezenas;
  edtMargemRepeticaoDezenasSimulador.Value := AParametros.MargemRepeticaoDezenas;
  for I := 0 to Pred(ccbLinhaSimulador.Properties.Items.Count) do
    if AParametros.Linha.IndexOf(ccbLinhaSimulador.Properties.Items[I].Description) <> -1 then
      ccbLinhaSimulador.States[I] := cbsChecked
    else
      ccbLinhaSimulador.States[I] := cbsUnchecked;
  edtMargemLinhaSimulador.Value := AParametros.MargemLinha;
  for I := 0 to Pred(ccbColunaSimulador.Properties.Items.Count) do
    if AParametros.Coluna.IndexOf(ccbColunaSimulador.Properties.Items[I].Description) <> -1 then
      ccbColunaSimulador.States[I] := cbsChecked
    else
      ccbColunaSimulador.States[I] := cbsUnchecked;
  edtMargemColunaSimulador.Value := AParametros.MargemColuna;
  for I := 0 to Pred(ccbSequenciaSimulador.Properties.Items.Count) do
    if AParametros.Sequencia.IndexOf(ccbSequenciaSimulador.Properties.Items[I].Description) <> -1 then
      ccbSequenciaSimulador.States[I] := cbsChecked
    else
      ccbSequenciaSimulador.States[I] := cbsUnChecked;
  edtMargemSequenciaSimulador.Value := AParametros.MargemSequencia;
  for I := 0 to Pred(ccbFrequenciaSimulador.Properties.Items.Count) do
    if AParametros.Frequencia.IndexOf(ccbFrequenciaSimulador.Properties.Items[I].Description) <> -1 then
      ccbFrequenciaSimulador.States[I] := cbsChecked
    else
      ccbFrequenciaSimulador.States[I] := cbsUnChecked;
  edtMargemFrequenciaSimulador.Value := AParametros.MargemFrequencia;
  for I := 0 to Pred(ccbAtrasoSimulador.Properties.Items.Count) do
    if AParametros.Atraso.IndexOf(ccbAtrasoSimulador.Properties.Items[I].Description) <> -1 then
      ccbAtrasoSimulador.States[I] := cbsChecked
    else
      ccbAtrasoSimulador.States[I] := cbsUnChecked;
  edtMargemAtrasoSimulador.Value := AParametros.MargemAtraso;
  cbQuocienteFrequenciaSimulador.ItemIndex := IfThen(AParametros.UsarQuocienteFrequencia, 1);
  cbQuocienteAtrasoSimulador.ItemIndex := IfThen(AParametros.UsarQuocienteAtraso, 1);
  edtNumeroAcertos.Value := AParametros.NumeroAcertos;
  edtDezenasPorAcerto.Value := AParametros.DezenasPorAcerto;
  edtSorteioInicial.Value := AParametros.SorteioInicial;
  edtSorteioFinal.Value := AParametros.SorteioFinal;
  chkParesSimulador.Checked := not AParametros.UsarParesSorteio;
  chkPrimosSimulador.Checked := not AParametros.UsarPrimosSorteio;
  chkIntervaloSimulador.Checked := not AParametros.UsarIntervaloSorteio;
  chkLinhaSimulador.Checked := not AParametros.UsarLinhaSorteio;
  chkColunaSimulador.Checked := not AParametros.UsarColunaSorteio;
  chkSequenciaSimulador.Checked := not AParametros.UsarSequenciaSorteio;
end;

procedure TMainForm.LerParametrosAposta(
  const AParametros: TParametrosAposta);
var
  I: Integer;
begin
  edtQuantidadeJogosAposta.Value := AParametros.QuantidadeJogos;
  edtMinimoParesAposta.Value := AParametros.MinimoPares;
  edtMaximoParesAposta.Value := AParametros.MaximoPares;
  edtMargemParesAposta.Value := AParametros.MargemPares;
  edtMinimoPrimosAposta.Value := AParametros.MinimoPrimos;
  edtMaximoPrimosAposta.Value := AParametros.MaximoPrimos;
  edtMargemPrimosAposta.Value := AParametros.MargemPrimos;
  edtMinimoQuadrantesAposta.Value := AParametros.MinimoQuadrantes;
  edtMaximoQuadrantesAposta.Value := AParametros.MaximoQuadrantes;
  edtMargemQuadrantesAposta.Value := AParametros.MargemQuadrantes;
  edtMinimoRepeticaoDezenasAposta.Value := AParametros.MinimoRepeticaoDezenas;
  edtMaximoRepeticaoDezenasAposta.Value := AParametros.MaximoRepeticaoDezenas;
  edtMargemRepeticaoDezenasAposta.Value := AParametros.MargemRepeticaoDezenas;
  for I := 0 to Pred(ccbLinhaAposta.Properties.Items.Count) do
    if AParametros.Linha.IndexOf(ccbLinhaAposta.Properties.Items[I].Description) <> -1 then
      ccbLinhaAposta.States[I] := cbsChecked
    else
      ccbLinhaAposta.States[I] := cbsUnchecked;
  edtMargemLinhaAposta.Value := AParametros.MargemLinha;
  for I := 0 to Pred(ccbColunaAposta.Properties.Items.Count) do
    if AParametros.Coluna.IndexOf(ccbColunaAposta.Properties.Items[I].Description) <> -1 then
      ccbColunaAposta.States[I] := cbsChecked
    else
      ccbColunaAposta.States[I] := cbsUnchecked;
  edtMargemColunaAposta.Value := AParametros.MargemColuna;
  for I := 0 to Pred(ccbSequenciaAposta.Properties.Items.Count) do
    if AParametros.Sequencia.IndexOf(ccbSequenciaAposta.Properties.Items[I].Description) <> -1 then
      ccbSequenciaAposta.States[I] := cbsChecked
    else
      ccbSequenciaAposta.States[I] := cbsUnchecked;
  edtMargemSequenciaAposta.Value := AParametros.MargemSequencia;
  for I := 0 to Pred(ccbFrequenciaAposta.Properties.Items.Count) do
    if AParametros.Frequencia.IndexOf(ccbFrequenciaAposta.Properties.Items[I].Description) <> -1 then
      ccbFrequenciaAposta.States[I] := cbsChecked
    else
      ccbFrequenciaAposta.States[I] := cbsUnchecked;
  edtMargemFrequenciaAposta.Value := AParametros.MargemFrequencia;
  for I := 0 to Pred(ccbAtrasoAposta.Properties.Items.Count) do
    if AParametros.Atraso.IndexOf(ccbAtrasoAposta.Properties.Items[I].Description) <> -1 then
      ccbAtrasoAposta.States[I] := cbsChecked
    else
      ccbAtrasoAposta.States[I] := cbsUnchecked;
  edtMargemAtrasoAposta.Value := AParametros.MargemAtraso;
  cbQuocienteFrequenciaAposta.ItemIndex := IfThen(AParametros.UsarQuocienteFrequencia, 1);
  cbQuocienteAtrasoAposta.ItemIndex := IfThen(AParametros.UsarQuocienteAtraso, 1);
end;

procedure TMainForm.PreencherParametrosSimulador(
  const AParametros: TParametrosSimulador);
var
  I: Integer;
begin
  AParametros.QuantidadeJogos := Trunc(edtQuantidadeJogosSimulador.Value);
  AParametros.MinimoPares := IfThen(chkParesSimulador.Checked, Trunc(edtMinimoParesSimulador.Value));
  AParametros.MaximoPares := IfThen(chkParesSimulador.Checked, Trunc(edtMaximoParesSimulador.Value));
  AParametros.MargemPares := IfThen(chkParesSimulador.Checked, SimpleRoundTo(edtMargemParesSimulador.Value));
  AParametros.MinimoPrimos := IfThen(chkPrimosSimulador.Checked, Trunc(edtMinimoPrimosSimulador.Value));
  AParametros.MaximoPrimos := IfThen(chkPrimosSimulador.Checked, Trunc(edtMaximoPrimosSimulador.Value));
  AParametros.MargemPrimos := IfThen(chkPrimosSimulador.Checked, SimpleRoundTo(edtMargemPrimosSimulador.Value));
  AParametros.MinimoIntervalo := IfThen(chkIntervaloSimulador.Checked, Trunc(edtMinimoIntervaloSimulador.Value));
  AParametros.MaximoIntervalo := IfThen(chkIntervaloSimulador.Checked, Trunc(edtMaximoIntervaloSimulador.Value));
  AParametros.MargemIntervalo := IfThen(chkIntervaloSimulador.Checked, SimpleRoundTo(edtMargemIntervaloSimulador.Value));
  AParametros.MinimoQuadrantes := Trunc(edtMinimoQuadrantesSimulador.Value);
  AParametros.MaximoQuadrantes := Trunc(edtMaximoQuadrantesSimulador.Value);
  AParametros.MargemQuadrantes := SimpleRoundTo(edtMargemQuadrantesSimulador.Value);
  AParametros.MinimoRepeticaoDezenas := Trunc(edtMinimoRepeticaoDezenasSimulador.Value);
  AParametros.MaximoRepeticaoDezenas := Trunc(edtMaximoRepeticaoDezenasSimulador.Value);
  AParametros.MargemRepeticaoDezenas := SimpleRoundTo(edtMargemRepeticaoDezenasSimulador.Value);
  AParametros.Linha.Clear;
  if chkLinhaSimulador.Checked then
    for I := 0 to Pred(ccbLinhaSimulador.Properties.Items.Count) do
      if ccbLinhaSimulador.States[I] = cbsChecked then
        AParametros.Linha.Add(ccbLinhaSimulador.Properties.Items[I].Description);
  AParametros.MargemLinha := IfThen(chkLinhaSimulador.Checked, SimpleRoundTo(edtMargemLinhaSimulador.Value));
  AParametros.Coluna.Clear;
  if chkColunaSimulador.Checked then
    for I := 0 to Pred(ccbColunaSimulador.Properties.Items.Count) do
      if ccbColunaSimulador.States[I] = cbsChecked then
        AParametros.Coluna.Add(ccbColunaSimulador.Properties.Items[I].Description);
  AParametros.MargemColuna := IfThen(chkColunaSimulador.Checked, SimpleRoundTo(edtMargemColunaSimulador.Value));
  AParametros.Sequencia.Clear;
  if chkSequenciaSimulador.Checked then
    for I := 0 to Pred(ccbSequenciaSimulador.Properties.Items.Count) do
      if ccbSequenciaSimulador.States[I] = cbsChecked then
        AParametros.Sequencia.Add(ccbSequenciaSimulador.Properties.Items[I].Description);
  AParametros.MargemSequencia := IfThen(chkSequenciaSimulador.Checked, SimpleRoundTo(edtMargemSequenciaSimulador.Value));
  AParametros.Frequencia.Clear;
  for I := 0 to Pred(ccbFrequenciaSimulador.Properties.Items.Count) do
    if ccbFrequenciaSimulador.States[I] = cbsChecked then
      AParametros.Frequencia.Add(ccbFrequenciaSimulador.Properties.Items[I].Description);
  AParametros.MargemFrequencia := SimpleRoundTo(edtMargemFrequenciaSimulador.Value);
  AParametros.Atraso.Clear;
  for I := 0 to Pred(ccbAtrasoSimulador.Properties.Items.Count) do
    if ccbAtrasoSimulador.States[I] = cbsChecked then
      AParametros.Atraso.Add(ccbAtrasoSimulador.Properties.Items[I].Description);
  AParametros.MargemAtraso := SimpleRoundTo(edtMargemAtrasoSimulador.Value);
  AParametros.UsarQuocienteFrequencia := cbQuocienteFrequenciaSimulador.ItemIndex = 1;
  AParametros.UsarQuocienteAtraso := cbQuocienteAtrasoSimulador.ItemIndex = 1;
  AParametros.NumeroAcertos := Trunc(edtNumeroAcertos.Value);
  AParametros.DezenasPorAcerto := Trunc(edtDezenasPorAcerto.Value);
  AParametros.SorteioInicial := Trunc(edtSorteioInicial.Value);
  AParametros.SorteioFinal := Trunc(edtSorteioFinal.Value);
  AParametros.UsarParesSorteio := not chkParesSimulador.Checked;
  AParametros.UsarPrimosSorteio := not chkPrimosSimulador.Checked;
  AParametros.UsarIntervaloSorteio := not chkIntervaloSimulador.Checked;
  AParametros.UsarLinhaSorteio := not chkLinhaSimulador.Checked;
  AParametros.UsarColunaSorteio := not chkColunaSimulador.Checked;
  AParametros.UsarSequenciaSorteio := not chkSequenciaSimulador.Checked;
end;

procedure TMainForm.PreencherParametrosAposta(
  const AParametros: TParametrosAposta);
var
  I: Integer;
begin
  AParametros.QuantidadeJogos := Trunc(edtQuantidadeJogosAposta.Value);
  AParametros.MinimoPares := Trunc(edtMinimoParesAposta.Value);
  AParametros.MaximoPares := Trunc(edtMaximoParesAposta.Value);
  AParametros.MargemPares := SimpleRoundTo(edtMargemParesAposta.Value);
  AParametros.MinimoPrimos := Trunc(edtMinimoPrimosAposta.Value);
  AParametros.MaximoPrimos := Trunc(edtMaximoPrimosAposta.Value);
  AParametros.MargemPrimos := SimpleRoundTo(edtMargemPrimosAposta.Value);
  AParametros.MinimoIntervalo := Trunc(edtMinimoIntervaloAposta.Value);
  AParametros.MaximoIntervalo := Trunc(edtMaximoIntervaloAposta.Value);
  AParametros.MargemIntervalo := SimpleRoundTo(edtMargemIntervaloAposta.Value);
  AParametros.MinimoQuadrantes := Trunc(edtMinimoQuadrantesAposta.Value);
  AParametros.MaximoQuadrantes := Trunc(edtMaximoQuadrantesAposta.Value);
  AParametros.MargemQuadrantes := SimpleRoundTo(edtMargemQuadrantesAposta.Value);
  AParametros.MinimoRepeticaoDezenas := Trunc(edtMinimoRepeticaoDezenasAposta.Value);
  AParametros.MaximoRepeticaoDezenas := Trunc(edtMaximoRepeticaoDezenasAposta.Value);
  AParametros.MargemRepeticaoDezenas := SimpleRoundTo(edtMargemRepeticaoDezenasAposta.Value);
  AParametros.Linha.Clear;
  for I := 0 to Pred(ccbLinhaAposta.Properties.Items.Count) do
    if ccbLinhaAposta.States[I] = cbsChecked then
      AParametros.Linha.Add(ccbLinhaAposta.Properties.Items[I].Description);
  AParametros.MargemLinha := SimpleRoundTo(edtMargemLinhaAposta.Value);
  AParametros.Coluna.Clear;
  for I := 0 to Pred(ccbColunaAposta.Properties.Items.Count) do
    if ccbColunaAposta.States[I] = cbsChecked then
      AParametros.Coluna.Add(ccbColunaAposta.Properties.Items[I].Description);
  AParametros.MargemColuna := SimpleRoundTo(edtMargemColunaAposta.Value);
  AParametros.Sequencia.Clear;
  for I := 0 to Pred(ccbSequenciaAposta.Properties.Items.Count) do
    if ccbSequenciaAposta.States[I] = cbsChecked then
      AParametros.Sequencia.Add(ccbSequenciaAposta.Properties.Items[I].Description);
  AParametros.MargemSequencia := SimpleRoundTo(edtMargemSequenciaAposta.Value);
  AParametros.Frequencia.Clear;
  for I := 0 to Pred(ccbFrequenciaAposta.Properties.Items.Count) do
    if ccbFrequenciaAposta.States[I] = cbsChecked then
      AParametros.Frequencia.Add(ccbFrequenciaAposta.Properties.Items[I].Description);
  AParametros.MargemFrequencia := SimpleRoundTo(edtMargemFrequenciaAposta.Value);
  AParametros.Atraso.Clear;
  for I := 0 to Pred(ccbAtrasoAposta.Properties.Items.Count) do
    if ccbAtrasoAposta.States[I] = cbsChecked then
      AParametros.Atraso.Add(ccbAtrasoAposta.Properties.Items[I].Description);
  AParametros.MargemAtraso := SimpleRoundTo(edtMargemAtrasoAposta.Value);
  AParametros.UsarQuocienteFrequencia := cbQuocienteFrequenciaAposta.ItemIndex = 1;
  AParametros.UsarQuocienteAtraso := cbQuocienteAtrasoAposta.ItemIndex = 1;
end;

procedure TMainForm.AoGerarJogo(Sender: TObject; Dezenas: TDezenas;
  Valido: Boolean; var Continuar: Boolean);
begin
  Continuar := not TRLDXWait.Instance.Canceled;
end;

end.
