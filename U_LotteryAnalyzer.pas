//=============================================================================
// U_LotteryAnalyzer
// Lottery Analyzer
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_LotteryAnalyzer;

{$R Resources.res}

interface

uses
  SysUtils,
  Classes,
  DB,
  DBClient,
  Forms,
  U_BaseType,
  U_ContainedObject,
  U_StringList,
  U_HashTable,
  U_Collection,
  U_KeyedCollection,
  U_FormType,
  U_Application;

type
  // Forward declarations
  TDezenas = class;
  TTipoLoteria = class;

  // Exceptions
  ELoteria = class(Exception);

  // Events
  TEventoJogo = procedure(Sender: TObject; Dezenas: TDezenas; Valido: Boolean; var Continuar: Boolean) of object;

  // ITipoLoteria
  ITipoLoteria = interface
    ['{B1FF3E23-3E66-4F97-BA18-E8B402D69786}']
    function GetLoteria: TTipoLoteria;
  end;

  // TDezena
  TDezena = class(TRLKeyedCollectionItem)
  private
    function GetDezena: Integer;
    procedure SetDezena(const AValor: Integer);
  public
    procedure Assign(Source: TPersistent); override;
  published
    property Dezena: Integer read GetDezena write SetDezena;
  end;

  // TDezenas
  TDezenas = class(TRLKeyedCollection)
  private
    function GetItem(const AIndex: Integer): TDezena;
    procedure OrdenarDezenas(L, R: Integer);
  protected
    function GetItemClass: TCollectionItemClass; override;
  public
    function Add: TDezena;
    function Insert(const AIndex: Integer): TDezena;
    function Find(const ADezena: Integer): TDezena;
    function Contains(const ADezena: Integer): Boolean; reintroduce;
    function Remove(const ADezena: Integer): Boolean; reintroduce;
    function ConferirDezenas(const ADezenas: TDezenas): Integer;
    function ContemDezenas(const ADezenas: TDezenas): Boolean;
    function GerarString(const ADelimitador: string = ', '): string;
    function ContarPares: Integer;
    function ContarPrimos: Integer;
    function LerIntervalo: Integer;
    procedure Ordenar;
    property Items[const AIndex: Integer]: TDezena read GetItem; default;
  end;

  // TDezenaContada
  TDezenaContada = class(TDezena)
  private
    FQuantidade: Integer;
  public
    procedure Assign(Source: TPersistent); override;
  published
    property Quantidade: Integer read FQuantidade write FQuantidade;
  end;

  // TDezenasContadas
  TDezenasContadas = class(TDezenas)
  private
    function GetItem(const AIndex: Integer): TDezenaContada;
  protected
    function GetItemClass: TCollectionItemClass; override;
  public
    function Add: TDezenaContada;
    function Insert(const AIndex: Integer): TDezenaContada;
    function Find(const ADezena: Integer): TDezenaContada;
    function CriarDataSet: TDataSet;
    property Items[const AIndex: Integer]: TDezenaContada read GetItem; default;
  end;

  // TItemContagem
  TItemContagem = class(TRLKeyedCollectionItem)
  private
    FQuantidade: Integer;
    FPercentual: Double;
  protected
    function GetKey: string; override;
    procedure SetKey(const AValue: string); override;
  public
    procedure Assign(Source: TPersistent); override;
  published
    property Valor: string read GetKey write SetKey;
    property Quantidade: Integer read FQuantidade write FQuantidade;
    property Percentual: Double read FPercentual write FPercentual;
  end;

  // TContagem
  TContagem = class(TRLKeyedCollection)
  private
    function GetItem(const AIndex: Integer): TItemContagem;
  protected
    function GetItemClass: TCollectionItemClass; override;
  public
    function Add: TItemContagem;
    function Insert(const AIndex: Integer): TItemContagem;
    function Find(const AValor: string): TItemContagem;
    function Contains(const AValor: string): Boolean; reintroduce;
    function Remove(const AValor: string): Boolean; reintroduce;
    procedure CalcularPercentual;
    function CriarDataSet(const AMostrarPercentual: Boolean; const ANomeValor: string; const ATipoValor: TFieldType = ftInteger; const ATamanhoValor: Integer = 0): TDataSet;
    property Items[const AIndex: Integer]: TItemContagem read GetItem; default;
  end;

  // TFaixaQuadrante
  TFaixaQuadrante = class(TRLCollectionItem)
  private
    FInicio: Integer;
    FTermino: Integer;
  public
    procedure Assign(Source: TPersistent); override;
  published
    property Inicio: Integer read FInicio write FInicio;
    property Termino: Integer read FTermino write FTermino;
  end;

  // TFaixasQuadrante
  TFaixasQuadrante = class(TRLCollection)
  private
    function GetItem(const AIndex: Integer): TFaixaQuadrante;
  protected
    function GetItemClass: TCollectionItemClass; override;
  public
    function Add: TFaixaQuadrante;
    function Insert(const AIndex: Integer): TFaixaQuadrante;
    function ContemDezena(const ADezena: Integer): Boolean;
    property Items[const AIndex: Integer]: TFaixaQuadrante read GetItem; default;
  end;

  // TQuadrante
  TQuadrante = class(TRLKeyedCollectionItem)
  private
    FFaixas: TFaixasQuadrante;
    function GetNumero: Integer;
    procedure SetNumero(const AValor: Integer);
  public
    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;
  published
    property Numero: Integer read GetNumero write SetNumero;
    property Faixas: TFaixasQuadrante read FFaixas;
  end;

  // TQuadrantes
  TQuadrantes = class(TRLKeyedCollection)
  private
    function GetItem(const AIndex: Integer): TQuadrante;
  protected
    function GetItemClass: TCollectionItemClass; override;
  public
    constructor Create; override;
    function Add: TQuadrante;
    function Insert(const AIndex: Integer): TQuadrante;
    function Find(const ANumero: Integer): TQuadrante;
    function Contains(const ANumero: Integer): Boolean; reintroduce;
    function Remove(const ANumero: Integer): Boolean; reintroduce;
    function LocalizarQuadrante(const ADezena: Integer): TQuadrante;
    function QuadrantesPreenchidos(const ADezenas: TDezenas): Integer;
    property Items[const AIndex: Integer]: TQuadrante read GetItem; default;
  end;

  // TOcorrenciaGabarito
  TOcorrenciaGabarito = class(TRLCollectionItem)
  private
    FDezenas: Integer;
  public
    constructor Create(Collection: TCollection); override;
    procedure Assign(Source: TPersistent); override;
  published
    property Dezenas: Integer read FDezenas write FDezenas;
  end;

  // TOcorrenciasGabarito
  TOcorrenciasGabarito = class(TRLCollection)
  private
    function GetItem(const AIndex: Integer): TOcorrenciaGabarito;
  protected
    function GetItemClass: TCollectionItemClass; override;
  public
    function Add: TOcorrenciaGabarito;
    function Insert(const AIndex: Integer): TOcorrenciaGabarito;
    property Items[const AIndex: Integer]: TOcorrenciaGabarito read GetItem; default;
  end;

  // TItemGabarito
  TItemGabarito = class(TRLKeyedCollectionItem)
  private
    type
      TContagemOcorrencia = record
        Dezenas: Integer;
        Valida: Boolean;
      end;

      TArrayContagemOcorrencia = array of TContagemOcorrencia;
      PArrayContagemOcorrencia = ^TArrayContagemOcorrencia;
  private
    FOcorrencias: TOcorrenciasGabarito;
    FLinha: Boolean;
    FColuna: Boolean;
    FSequencia: Boolean;
    FLoteria: TTipoLoteria;
    FPreferencial: Boolean;
    function GetLoteria: TTipoLoteria;
    function ValidarContagem(const AContagem: PArrayContagemOcorrencia): Boolean;
  protected
    function GetKey: string; override;
    procedure SetKey(const AValue: string); override;
    procedure ChecarLoteria;
  public
    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;
    function ValidarLinha(const ADezenas: TDezenas): Boolean;
    function ValidarColuna(const ADezenas: TDezenas): Boolean;
    function ValidarSequencia(const ADezenas: TDezenas): Boolean;
    property Loteria: TTipoLoteria read GetLoteria write FLoteria;
  published
    property Nome: string read GetKey write SetKey;
    property Ocorrencias: TOcorrenciasGabarito read FOcorrencias;
    property Linha: Boolean read FLinha write FLinha;
    property Coluna: Boolean read FColuna write FColuna;
    property Sequencia: Boolean read FSequencia write FSequencia;
    property Preferencial: Boolean read FPreferencial write FPreferencial;
  end;

  // TGabarito
  TGabarito = class(TRLKeyedCollection)
  private
    FLoteria: TTipoLoteria;
    function GetItem(const AIndex: Integer): TItemGabarito;
  protected
    function GetItemClass: TCollectionItemClass; override;
  public
    constructor Create; override;
    function Add: TItemGabarito;
    function Insert(const AIndex: Integer): TItemGabarito;
    function Find(const ANome: string): TItemGabarito;
    function Contains(const ANome: string): Boolean; reintroduce;
    function Remove(const ANome: string): Boolean; reintroduce;
    function LocalizarPorLinha(const ADezenas: TDezenas): TItemGabarito;
    function LocalizarPorColuna(const ADezenas: TDezenas): TItemGabarito;
    function LocalizarPorSequencia(const ADezenas: TDezenas): TItemGabarito;
    property Items[const AIndex: Integer]: TItemGabarito read GetItem; default;
    property Loteria: TTipoLoteria read FLoteria write FLoteria;
  end;

  // TSorteio
  TSorteio = class(TRLCollectionItem)
  private
    FDezenas: TDezenas;
  public
    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;
    procedure Ordenar;
  published
    property Dezenas: TDezenas read FDezenas;
  end;

  // TSorteios
  TSorteios = class(TRLCollection)
  private
    function GetItem(const AIndex: Integer): TSorteio;
  protected
    function GetItemClass: TCollectionItemClass; override;
  public
    function Add: TSorteio;
    function Insert(const AIndex: Integer): TSorteio;
    function Localizar(const ADezenas: TDezenas): TSorteio;
    function Copiar(const AInicio, ATermino: Integer): TSorteios;
    procedure Ordenar;
    property Items[const AIndex: Integer]: TSorteio read GetItem; default;
  end;

  // TItemHistorico
  TItemHistorico = class(TSorteio)
  private
    FLoteria: TTipoLoteria;
    FConcurso: Integer;
    FData: TDate;
    FPares: Integer;
    FPrimos: Integer;
    FIntervalo: Integer;
    FLinha: string;
    FColuna: string;
    FSequencia: string;
    function GetLoteria: TTipoLoteria;
  protected
    procedure ChecarLoteria;
  public
    procedure Assign(Source: TPersistent); override;
    procedure CalcularEstatisticas;
    property Loteria: TTipoLoteria read GetLoteria write FLoteria;
  published
    property Concurso: Integer read FConcurso write FConcurso;
    property Data: TDate read FData write FData;
    property Pares: Integer read FPares write FPares;
    property Primos: Integer read FPrimos write FPrimos;
    property Intervalo: Integer read FIntervalo write FIntervalo;
    property Linha: string read FLinha write FLinha;
    property Coluna: string read FColuna write FColuna;
    property Sequencia: string read FSequencia write FSequencia;
  end;

  // THistorico
  THistorico = class(TSorteios)
  private
    FLoteria: TTipoLoteria;
    function GetItem(const AIndex: Integer): TItemHistorico;
  protected
    function GetItemClass: TCollectionItemClass; override;
  public
    function Add: TItemHistorico;
    function Insert(const AIndex: Integer): TItemHistorico;
    function Localizar(const ADezenas: TDezenas): TItemHistorico;
    function Copiar(const AInicio, ATermino: Integer): THistorico;
    procedure LerXML(const ANomeArquivo: string);
    procedure GravarXML(const ANomeArquivo: string);
    procedure ImportarCSV(const ANomeArquivo: string);
    procedure ExportarCSV(const ANomeArquivo: string);
    procedure AtualizarEstatisticas;
    function CriarDataSet: TDataSet;
    property Items[const AIndex: Integer]: TItemHistorico read GetItem; default;
    property Loteria: TTipoLoteria read FLoteria write FLoteria;
  end;

  // TLinhaTabelaFrequencia
  TLinhaTabelaFrequencia = class(TRLKeyedCollectionItem)
  private
    FDezenas: TDezenas;
    function GetFrequencia: Integer;
    procedure SetFrequencia(const AValor: Integer);
  public
    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;
  published
    property Frequencia: Integer read GetFrequencia write SetFrequencia;
    property Dezenas: TDezenas read FDezenas;
  end;

  // TTabelaFrequencia
  TTabelaFrequencia = class(TRLKeyedCollection)
  private
    function GetItem(const AIndex: Integer): TLinhaTabelaFrequencia;
  protected
    function GetItemClass: TCollectionItemClass; override;
  public
    constructor Create; override;
    function Add: TLinhaTabelaFrequencia;
    function Insert(const AIndex: Integer): TLinhaTabelaFrequencia;
    function Find(const AFrequencia: Integer): TLinhaTabelaFrequencia;
    function Contains(const AFrequencia: Integer): Boolean; reintroduce;
    function Remove(const AFrequencia: Integer): Boolean; reintroduce;
    property Items[const AIndex: Integer]: TLinhaTabelaFrequencia read GetItem; default;
  end;

  // TLinhaTabelaQuociente
  TLinhaTabelaQuociente = class(TRLKeyedCollectionItem)
  private
    FFrequenciaMedia: Double;
    FFrequenciaMediaDezenas: Double;
    FDesvioPadrao: Double;
    FQuociente: Double;
    function GetConcurso: Integer;
    procedure SetConcurso(const AValor: Integer);
  public
    procedure Assign(Source: TPersistent); override;
  published
    property Concurso: Integer read GetConcurso write SetConcurso;
    property FrequenciaMedia: Double read FFrequenciaMedia write FFrequenciaMedia;
    property FrequenciaMediaDezenas: Double read FFrequenciaMediaDezenas write FFrequenciaMediaDezenas;
    property DesvioPadrao: Double read FDesvioPadrao write FDesvioPadrao;
    property Quociente: Double read FQuociente write FQuociente;
  end;

  // TTabelaQuociente
  TTabelaQuociente = class(TRLKeyedCollection)
  private
    function GetItem(const AIndex: Integer): TLinhaTabelaQuociente;
  protected
    function GetItemClass: TCollectionItemClass; override;
  public
    constructor Create; override;
    function Add: TLinhaTabelaQuociente;
    function Insert(const AIndex: Integer): TLinhaTabelaQuociente;
    function Find(const AConcurso: Integer): TLinhaTabelaQuociente;
    function Contains(const AConcurso: Integer): Boolean; reintroduce;
    function Remove(const AConcurso: Integer): Boolean; reintroduce;
    property Items[const AIndex: Integer]: TLinhaTabelaQuociente read GetItem; default;
  end;

  // TLinhaTabelaAtraso
  TLinhaTabelaAtraso = class(TRLKeyedCollectionItem)
  private
    FDezenas: TDezenas;
    function GetAtraso: Integer;
    procedure SetAtraso(const AValor: Integer);
  public
    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;
    procedure Ordenar;
  published
    property Atraso: Integer read GetAtraso write SetAtraso;
    property Dezenas: TDezenas read FDezenas;
  end;

  // TTabelaAtraso
  TTabelaAtraso = class(TRLKeyedCollection)
  private
    function GetItem(const AIndex: Integer): TLinhaTabelaAtraso;
  protected
    function GetItemClass: TCollectionItemClass; override;
  public
    constructor Create; override;
    function Add: TLinhaTabelaAtraso;
    function Insert(const AIndex: Integer): TLinhaTabelaAtraso;
    function Find(const AAtraso: Integer): TLinhaTabelaAtraso;
    function Contains(const AAtraso: Integer): Boolean; reintroduce;
    function Remove(const AAtraso: Integer): Boolean; reintroduce;
    procedure Ordenar;
    property Items[const AIndex: Integer]: TLinhaTabelaAtraso read GetItem; default;
  end;

  // TLinhaTabelaAtrasoMedio
  TLinhaTabelaAtrasoMedio = class(TRLKeyedCollectionItem)
  private
    FAtrasoEsperado: Double;
    FAtrasoMedio: Double;
    FAtrasoMedioDezenas: Double;
    FQuociente: Double;
    function GetConcurso: Integer;
    procedure SetConcurso(const AValor: Integer);
  public
    procedure Assign(Source: TPersistent); override;
  published
    property Concurso: Integer read GetConcurso write SetConcurso;
    property AtrasoEsperado: Double read FAtrasoEsperado write FAtrasoEsperado;
    property AtrasoMedio: Double read FAtrasoMedio write FAtrasoMedio;
    property AtrasoMedioDezenas: Double read FAtrasoMedioDezenas write FAtrasoMedioDezenas;
    property Quociente: Double read FQuociente write FQuociente;
  end;

  // TTabelaAtrasoMedio
  TTabelaAtrasoMedio = class(TRLKeyedCollection)
  private
    function GetItem(const AIndex: Integer): TLinhaTabelaAtrasoMedio;
  protected
    function GetItemClass: TCollectionItemClass; override;
  public
    constructor Create; override;
    function Add: TLinhaTabelaAtrasoMedio;
    function Insert(const AIndex: Integer): TLinhaTabelaAtrasoMedio;
    function Find(const AConcurso: Integer): TLinhaTabelaAtrasoMedio;
    function Contains(const AConcurso: Integer): Boolean; reintroduce;
    function Remove(const AConcurso: Integer): Boolean; reintroduce;
    property Items[const AIndex: Integer]: TLinhaTabelaAtrasoMedio read GetItem; default;
  end;

  // ITabela
  ITabela = interface
    ['{AE4814E4-6933-4706-8559-7A81D3CC9AED}']
    function CriarDataSet: TDataSet;
  end;

  // ITexto
  ITexto = interface
    ['{78BCD869-3FDC-4BB6-89DB-CF6F25DB1319}']
    procedure GerarString(const AStrings: TStrings);
  end;

  // TEstatistica
  TEstatistica = class(TRLKeyedCollectionItem)
  private
    FLoteria: TTipoLoteria;
    FSorteios: TSorteios;
    FCalculada: Boolean;
    function GetLoteria: TTipoLoteria;
    function GetSorteios: TSorteios;
  protected
    function GetKey: string; override;
    function GetDescricao: string; virtual; abstract;
    procedure CalcularEstatistica; virtual; abstract;
    procedure ChecarLoteria;
    procedure ChecarSorteios;
  public
    constructor Create(Collection: TCollection); override;
    function Calcular(const ARecalcular: Boolean): Boolean;
    function CalcularEmThread(const ARecalcular: Boolean): Boolean;
    function Calculada: Boolean;
    procedure Resetar;
    procedure MarcarComoCalculada;
    property Nome: string read GetKey;
    property Descricao: string read GetDescricao;
    property Loteria: TTipoLoteria read GetLoteria write FLoteria;
    property Sorteios: TSorteios read GetSorteios write FSorteios;
  end;

  // TEstatisticaPorContagem
  TEstatisticaPorContagem = class(TEstatistica)
  private
    FContagem: TContagem;
    FAplicarComparativo: Boolean;
    FSomenteComparativo: Boolean;
    FEstatisticaComparacao: string;
    FMargem: Double;
  public
    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;
    procedure CalcularPercentual;
    procedure Assign(Source: TPersistent); override;
  published
    property Contagem: TContagem read FContagem;
    property AplicarComparativo: Boolean read FAplicarComparativo write FAplicarComparativo;
    property SomenteComparativo: Boolean read FSomenteComparativo write FSomenteComparativo;
    property EstatisticaComparacao: string read FEstatisticaComparacao write FEstatisticaComparacao;
    property Margem: Double read FMargem write FMargem;
  end;

  // TEstatisticaDezenas
  TEstatisticaDezenas = class(TEstatistica, ITabela)
  private
    FDezenas: TDezenasContadas;
  public
    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;
    function CriarDataSet: TDataSet;
  published
    property Dezenas: TDezenasContadas read FDezenas;
  end;

  // TOcorrenciaPorPares
  TOcorrenciaPorPares = class(TEstatisticaPorContagem, ITabela)
  protected
    function GetDescricao: string; override;
    procedure CalcularEstatistica; override;
  public
    constructor Create(Collection: TCollection); override;
    function CriarDataSet: TDataSet;
  end;

  // TOcorrenciaPorPrimos
  TOcorrenciaPorPrimos = class(TEstatisticaPorContagem, ITabela)
  protected
    function GetDescricao: string; override;
    procedure CalcularEstatistica; override;
  public
    constructor Create(Collection: TCollection); override;
    function CriarDataSet: TDataSet;
  end;

  // TOcorrenciaPorIntervalo
  TOcorrenciaPorIntervalo = class(TEstatisticaPorContagem, ITabela)
  protected
    function GetDescricao: string; override;
    procedure CalcularEstatistica; override;
  public
    constructor Create(Collection: TCollection); override;
    function CriarDataSet: TDataSet;
  end;

  // TOcorrenciaPorLinha
  TOcorrenciaPorLinha = class(TEstatisticaPorContagem, ITabela)
  protected
    function GetDescricao: string; override;
    procedure CalcularEstatistica; override;
  public
    constructor Create(Collection: TCollection); override;
    function CriarDataSet: TDataSet;
  end;

  // TOcorrenciaPorColuna
  TOcorrenciaPorColuna = class(TEstatisticaPorContagem, ITabela)
  protected
    function GetDescricao: string; override;
    procedure CalcularEstatistica; override;
  public
    constructor Create(Collection: TCollection); override;
    function CriarDataSet: TDataSet;
  end;

  // TOcorrenciaPorSequencia
  TOcorrenciaPorSequencia = class(TEstatisticaPorContagem, ITabela)
  protected
    function GetDescricao: string; override;
    procedure CalcularEstatistica; override;
  public
    constructor Create(Collection: TCollection); override;
    function CriarDataSet: TDataSet;
  end;

  // TOcorrenciaPorQuadrante
  TOcorrenciaPorQuadrante = class(TEstatisticaPorContagem, ITabela)
  protected
    function GetDescricao: string; override;
    procedure CalcularEstatistica; override;
  public
    constructor Create(Collection: TCollection); override;
    function CriarDataSet: TDataSet;
  end;

  // TOcorrenciaPorDezena
  TOcorrenciaPorDezena = class(TEstatisticaDezenas)
  protected
    function GetDescricao: string; override;
    procedure CalcularEstatistica; override;
  public
    constructor Create(Collection: TCollection); override;
  end;

  // TOcorrenciaPorRepeticaoDezenas
  TOcorrenciaPorRepeticaoDezenas = class(TEstatisticaPorContagem, ITabela)
  protected
    function GetDescricao: string; override;
    procedure CalcularEstatistica; override;
  public
    constructor Create(Collection: TCollection); override;
    function CriarDataSet: TDataSet;
  end;

  // TDezenasMaisSorteadas
  TDezenasMaisSorteadas = class(TEstatisticaDezenas)
  protected
    function GetDescricao: string; override;
    procedure CalcularEstatistica; override;
  public
    constructor Create(Collection: TCollection); override;
  end;

  // TDezenasMenosSorteadas
  TDezenasMenosSorteadas = class(TEstatisticaDezenas)
  protected
    function GetDescricao: string; override;
    procedure CalcularEstatistica; override;
  public
    constructor Create(Collection: TCollection); override;
  end;

  // TFrequenciaDezenas
  TFrequenciaDezenas = class(TEstatistica, ITexto)
  private
    FFrequencia: TDezenasContadas;
    FTabela: TTabelaFrequencia;
    FMedia: Double;
    FDesvioPadrao: Double;
    FFrequenciaMinima: Double;
    FFrequenciaMaxima: Double;
    FDezenasDentroEsperado: TDezenas;
    FDezenasAbaixoEsperado: TDezenas;
    FDezenasAcimaEsperado: TDezenas;
    FPercentualDentroEsperado: Double;
    FPercentualAbaixoEsperado: Double;
    FPercentualAcimaEsperado: Double;
  protected
    function GetDescricao: string; override;
    procedure CalcularEstatistica; override;
  public
    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;
    function ObterFrequencia(const ADezenas: TDezenas; out ADentroEsperado, AAbaixoEsperado, AAcimaEsperado: Integer): string;
    function ObterStringFrequencia(const ADezenas: TDezenas): string;
    function ObterFrequenciaMedia(const ADezenas: TDezenas): Double;
    procedure GerarString(const AStrings: TStrings);
  published
    property Frequencia: TDezenasContadas read FFrequencia;
    property Tabela: TTabelaFrequencia read FTabela;
    property Media: Double read FMedia write FMedia;
    property DesvioPadrao: Double read FDesvioPadrao write FDesvioPadrao;
    property FrequenciaMinima: Double read FFrequenciaMinima write FFrequenciaMinima;
    property FrequenciaMaxima: Double read FFrequenciaMaxima write FFrequenciaMaxima;
    property DezenasDentroEsperado: TDezenas read FDezenasDentroEsperado;
    property DezenasAbaixoEsperado: TDezenas read FDezenasAbaixoEsperado;
    property DezenasAcimaEsperado: TDezenas read FDezenasAcimaEsperado;
    property PercentualDentroEsperado: Double read FPercentualDentroEsperado write FPercentualDentroEsperado;
    property PercentualAbaixoEsperado: Double read FPercentualAbaixoEsperado write FPercentualAbaixoEsperado;
    property PercentualAcimaEsperado: Double read FPercentualAcimaEsperado write FPercentualAcimaEsperado;
  end;

  // TFrequenciaAcumulada
  TFrequenciaAcumulada = class(TEstatisticaPorContagem, ITabela)
  private
    FNumeroSorteios: Integer;
  protected
    function GetDescricao: string; override;
    procedure CalcularEstatistica; override;
  public
    constructor Create(Collection: TCollection); override;
    procedure Assign(Source: TPersistent); override;
    function CriarDataSet: TDataSet;
  published
    property NumeroSorteios: Integer read FNumeroSorteios write FNumeroSorteios;
  end;

  // TFrequencia
  TFrequencia = class(TEstatisticaPorContagem, ITabela)
  protected
    function GetDescricao: string; override;
    procedure CalcularEstatistica; override;
  public
    constructor Create(Collection: TCollection); override;
    function CriarDataSet: TDataSet;
  end;

  // TQuocienteFrequencia
  TQuocienteFrequencia = class(TEstatistica, ITabela, ITexto)
  private
    FNumeroSorteios: Integer;
    FMediaFrequenciaDezenas: Double;
    FDesvioPadraoFrequenciaDezenas: Double;
    FFrequenciaDezenasMinima: Double;
    FFrequenciaDezenasMaxima: Double;
    FMediaQuociente: Double;
    FDesvioPadraoQuociente: Double;
    FQuocienteMinimo: Double;
    FQuocienteMaximo: Double;
    FAcerto: Double;
    FTabela: TTabelaQuociente;
  protected
    function GetDescricao: string; override;
    procedure CalcularEstatistica; override;
  public
    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;
    function QuocienteValido(const AValor: Double): Boolean;
    function CriarDataSet: TDataSet;
    procedure GerarString(const AStrings: TStrings);
  published
    property NumeroSorteios: Integer read FNumeroSorteios write FNumeroSorteios;
    property MediaFrequenciaDezenas: Double read FMediaFrequenciaDezenas write FMediaFrequenciaDezenas;
    property DesvioPadraoFrequenciaDezenas: Double read FDesvioPadraoFrequenciaDezenas write FDesvioPadraoFrequenciaDezenas;
    property FrequenciaDezenasMinima: Double read FFrequenciaDezenasMinima write FFrequenciaDezenasMinima;
    property FrequenciaDezenasMaxima: Double read FFrequenciaDezenasMaxima write FFrequenciaDezenasMaxima;
    property MediaQuociente: Double read FMediaQuociente write FMediaQuociente;
    property DesvioPadraoQuociente: Double read FDesvioPadraoQuociente write FDesvioPadraoQuociente;
    property QuocienteMinimo: Double read FQuocienteMinimo write FQuocienteMinimo;
    property QuocienteMaximo: Double read FQuocienteMaximo write FQuocienteMaximo;
    property Acerto: Double read FAcerto write FAcerto;
    property Tabela: TTabelaQuociente read FTabela;
  end;

  // TAtrasoDezenas
  TAtrasoDezenas = class(TEstatistica, ITexto)
  private
    FAtraso: TDezenasContadas;
    FTabela: TTabelaAtraso;
    FAtrasoEsperado: Double;
    FAtrasoMedio: Double;
  protected
    function GetDescricao: string; override;
    procedure CalcularEstatistica; override;
  public
    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;
    function ObterAtrasoMedio(const ADezenas: TDezenas): Double;
    procedure GerarString(const AStrings: TStrings);
  published
    property Atraso: TDezenasContadas read FAtraso;
    property Tabela: TTabelaAtraso read FTabela;
    property AtrasoEsperado: Double read FAtrasoEsperado write FAtrasoEsperado;
    property AtrasoMedio: Double read FAtrasoMedio write FAtrasoMedio;
  end;

  // TQuocienteAtraso
  TQuocienteAtraso = class(TEstatistica, ITabela, ITexto)
  private
    FNumeroSorteios: Integer;
    FMediaAtraso: Double;
    FDesvioPadraoAtraso: Double;
    FDesvioPadraoAtrasoConsiderado: Integer;
    FAtrasoMinimo: Double;
    FAtrasoMinimoConsiderado: Integer;
    FAtrasoMaximo: Double;
    FAtrasoMaximoConsiderado: Integer;
    FMediaAtrasoDezenas: Double;
    FDesvioPadraoAtrasoDezenas: Double;
    FAtrasoDezenasMinimo: Double;
    FAtrasoDezenasMaximo: Double;
    FMediaQuociente: Double;
    FDesvioPadraoQuociente: Double;
    FQuocienteMinimo: Double;
    FQuocienteMaximo: Double;
    FAcerto: Double;
    FDezenasAtrasoNormal: TDezenas;
    FDezenasMaisAtrasadas: TDezenas;
    FDezenasMaisRecentes: TDezenas;
    FPercentualAtrasoNormal: Double;
    FPercentualMaisAtrasadas: Double;
    FPercentualMaisRecentes: Double;
    FTabela: TTabelaAtrasoMedio;
  protected
    function GetDescricao: string; override;
    procedure CalcularEstatistica; override;
  public
    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;
    function ObterAtraso(const ADezenas: TDezenas; out AAtrasoNormal, AMaisAtrasadas, AMaisRecentes: Integer): string;
    function ObterStringAtraso(const ADezenas: TDezenas): string;
    function QuocienteValido(const AValor: Double): Boolean;
    function CriarDataSet: TDataSet;
    procedure GerarString(const AStrings: TStrings);
  published
    property NumeroSorteios: Integer read FNumeroSorteios write FNumeroSorteios;
    property MediaAtraso: Double read FMediaAtraso write FMediaAtraso;
    property DesvioPadraoAtraso: Double read FDesvioPadraoAtraso write FDesvioPadraoAtraso;
    property DesvioPadraoAtrasoConsiderado: Integer read FDesvioPadraoAtrasoConsiderado write FDesvioPadraoAtrasoConsiderado;
    property AtrasoMinimo: Double read FAtrasoMinimo write FAtrasoMinimo;
    property AtrasoMinimoConsiderado: Integer read FAtrasoMinimoConsiderado write FAtrasoMinimoConsiderado;
    property AtrasoMaximo: Double read FAtrasoMaximo write FAtrasoMaximo;
    property AtrasoMaximoConsiderado: Integer read FAtrasoMaximoConsiderado write FAtrasoMaximoConsiderado;
    property MediaAtrasoDezenas: Double read FMediaAtrasoDezenas write FMediaAtrasoDezenas;
    property DesvioPadraoAtrasoDezenas: Double read FDesvioPadraoAtrasoDezenas write FDesvioPadraoAtrasoDezenas;
    property AtrasoDezenasMinimo: Double read FAtrasoDezenasMinimo write FAtrasoDezenasMinimo;
    property AtrasoDezenasMaximo: Double read FAtrasoDezenasMaximo write FAtrasoDezenasMaximo;
    property MediaQuociente: Double read FMediaQuociente write FMediaQuociente;
    property DesvioPadraoQuociente: Double read FDesvioPadraoQuociente write FDesvioPadraoQuociente;
    property QuocienteMinimo: Double read FQuocienteMinimo write FQuocienteMinimo;
    property QuocienteMaximo: Double read FQuocienteMaximo write FQuocienteMaximo;
    property Acerto: Double read FAcerto write FAcerto;
    property DezenasAtrasoNormal: TDezenas read FDezenasAtrasoNormal;
    property DezenasMaisAtrasadas: TDezenas read FDezenasMaisAtrasadas;
    property DezenasMaisRecentes: TDezenas read FDezenasMaisRecentes;
    property PercentualAtrasoNormal: Double read FPercentualAtrasoNormal write FPercentualAtrasoNormal;
    property PercentualMaisAtrasadas: Double read FPercentualMaisAtrasadas write FPercentualMaisAtrasadas;
    property PercentualMaisRecentes: Double read FPercentualMaisRecentes write FPercentualMaisRecentes;
    property Tabela: TTabelaAtrasoMedio read FTabela;
  end;

  // TAtrasoAcumulado
  TAtrasoAcumulado = class(TEstatisticaPorContagem, ITabela)
  private
    FNumeroSorteios: Integer;
  protected
    function GetDescricao: string; override;
    procedure CalcularEstatistica; override;
  public
    constructor Create(Collection: TCollection); override;
    procedure Assign(Source: TPersistent); override;
    function CriarDataSet: TDataSet;
  published
    property NumeroSorteios: Integer read FNumeroSorteios write FNumeroSorteios;
  end;

  // TAtraso
  TAtraso = class(TEstatisticaPorContagem, ITabela)
  protected
    function GetDescricao: string; override;
    procedure CalcularEstatistica; override;
  public
    constructor Create(Collection: TCollection); override;
    function CriarDataSet: TDataSet;
  end;

  // TThreadOperacao
  TThreadOperacao = class(TThread)
  private
    FErro: string;
    FClasseErro: ExceptClass;
  protected
    procedure Execute; override;
    procedure ExecutarThread; virtual; abstract;
  public
    property Erro: string read FErro write FErro;
    property ClasseErro: ExceptClass read FClasseErro;
  end;

  // TThreadCalculoEstatistica
  TThreadCalculoEstatistica = class(TThreadOperacao)
  private
    FEstatistica: TEstatistica;
    FRecalcular: Boolean;
    FResultado: Boolean;
  protected
    procedure ExecutarThread; override;
  public
    property Estatistica: TEstatistica read FEstatistica write FEstatistica;
    property Recalcular: Boolean read FRecalcular write FRecalcular;
    property Resultado: Boolean read FResultado;
  end;

  // TCacheEstatistica
  TCacheEstatistica = class(TRLKeyedCollectionItem)
  private
    FEstatistica: TEstatistica;
  protected
    function GetKey: string; override;
    procedure SetKey(const AValue: string); override;
  public
    destructor Destroy; override;
    property ID: string read GetKey write SetKey;
    property Estatistica: TEstatistica read FEstatistica write FEstatistica;
  end;

  // TCacheEstatisticas
  TCacheEstatisticas = class(TRLKeyedCollection)
  private
    function GetItem(const AIndex: Integer): TCacheEstatistica;
  protected
    function GetItemClass: TCollectionItemClass; override;
  public
    constructor Create; override;
    function Add: TCacheEstatistica;
    function Insert(const AIndex: Integer): TCacheEstatistica;
    function Find(const AID: string): TCacheEstatistica;
    function Contains(const AID: string): Boolean; reintroduce;
    function Remove(const AID: string): Boolean; reintroduce;
    property Items[const AIndex: Integer]: TCacheEstatistica read GetItem; default;
  end;

  // TEstatisticas
  TEstatisticas = class(TRLKeyedCollection)
  private
    FLoteria: TTipoLoteria;
    FSorteios: TSorteios;
    FCache: TCacheEstatisticas;
    FCaches: Integer;
    function GetItem(const AIndex: Integer): TEstatistica;
    function GetLoteria: TTipoLoteria;
    function GetSorteios: TSorteios;
  protected
    function GetItemClass: TCollectionItemClass; override;
  public
    constructor Create; override;
    destructor Destroy; override;
    function Add: TEstatistica;
    function AdicionarOcorrenciaPorPares: TOcorrenciaPorPares;
    function AdicionarOcorrenciaPorPrimos: TOcorrenciaPorPrimos;
    function AdicionarOcorrenciaPorIntervalo: TOcorrenciaPorIntervalo;
    function AdicionarOcorrenciaPorLinha: TOcorrenciaPorLinha;
    function AdicionarOcorrenciaPorColuna: TOcorrenciaPorColuna;
    function AdicionarOcorrenciaPorSequencia: TOcorrenciaPorSequencia;
    function AdicionarOcorrenciaPorQuadrante: TOcorrenciaPorQuadrante;
    function AdicionarOcorrenciaPorDezena: TOcorrenciaPorDezena;
    function AdicionarOcorrenciaPorRepeticaoDezenas: TOcorrenciaPorRepeticaoDezenas;
    function AdicionarDezenasMaisSorteadas: TDezenasMaisSorteadas;
    function AdicionarDezenasMenosSorteadas: TDezenasMenosSorteadas;
    function AdicionarFrequenciaDezenas: TFrequenciaDezenas;
    function AdicionarFrequenciaAcumulada: TFrequenciaAcumulada;
    function AdicionarFrequencia: TFrequencia;
    function AdicionarQuocienteFrequencia: TQuocienteFrequencia;
    function AdicionarAtrasoDezenas: TAtrasoDezenas;
    function AdicionarQuocienteAtraso: TQuocienteAtraso;
    function AdicionarAtrasoAcumulado: TAtrasoAcumulado;
    function AdicionarAtraso: TAtraso;
    function Insert(const AIndex: Integer): TEstatistica;
    function Find(const ANome: string): TEstatistica;
    function Contains(const ANome: string): Boolean; reintroduce;
    function Remove(const ANome: string): Boolean; reintroduce;
    procedure Calcular(const ARecalcular: Boolean);
    procedure CalcularEmThread(const ARecalcular: Boolean);
    function Resetar(const ANome: string): Boolean;
    procedure ResetarTudo;
    function MarcarComoCalculada(const ANome: string): Boolean;
    procedure MarcarTudoComoCalculado;
    procedure LerXML(const ANomeArquivo: string);
    procedure GravarXML(const ANomeArquivo: string);
    procedure IniciarCache;
    function CacheInicializado: Boolean;
    function AdicionarCache(const AID: string; const AEstatistica: TEstatistica): TCacheEstatistica;
    function LocalizarCache(const AID: string): TCacheEstatistica;
    function LocalizarAtrasoDezenasCache(const AID: string): TAtrasoDezenas;
    procedure FinalizarCache;
    property Items[const AIndex: Integer]: TEstatistica read GetItem; default;
    property Loteria: TTipoLoteria read GetLoteria write FLoteria;
    property Sorteios: TSorteios read GetSorteios write FSorteios;
  end;

  // TThreadCalculoEstatisticas
  TThreadCalculoEstatisticas = class(TThreadOperacao)
  private
    FEstatisticas: TEstatisticas;
    FRecalcular: Boolean;
  protected
    procedure ExecutarThread; override;
  public
    property Estatisticas: TEstatisticas read FEstatisticas write FEstatisticas;
    property Recalcular: Boolean read FRecalcular write FRecalcular;
  end;

  // TPrecoAposta
  TPrecoAposta = class(TRLKeyedCollectionItem)
  private
    FValor: Double;
    function GetDezenas: Integer;
    procedure SetDezenas(const AValor: Integer);
  public
    procedure Assign(Source: TPersistent); override;
  published
    property Dezenas: Integer read GetDezenas write SetDezenas;
    property Valor: Double read FValor write FValor;
  end;

  // TPrecosAposta
  TPrecosAposta = class(TRLKeyedCollection)
  private
    function GetItem(const AIndex: Integer): TPrecoAposta;
  protected
    function GetItemClass: TCollectionItemClass; override;
  public
    constructor Create; override;
    function Add: TPrecoAposta;
    function Insert(const AIndex: Integer): TPrecoAposta;
    function Find(const ADezenas: Integer): TPrecoAposta;
    function Contains(const ADezenas: Integer): Boolean; reintroduce;
    function Remove(const ADezenas: Integer): Boolean; reintroduce;
    property Items[const AIndex: Integer]: TPrecoAposta read GetItem; default;
  end;

  // TJogo
  TJogo = class(TSorteio);

  // TJogos
  TJogos = class(TSorteios)
  private
    function GetItem(const AIndex: Integer): TJogo;
  protected
    function GetItemClass: TCollectionItemClass; override;
  public
    function Add: TJogo;
    function Insert(const AIndex: Integer): TJogo;
    function Localizar(const ADezenas: TDezenas): TJogo;
    function Copiar(const AInicio, ATermino: Integer): TJogos;
    function CriarDataSet: TDataSet;
    property Items[const AIndex: Integer]: TJogo read GetItem; default;
  end;

  // TFiltro
  TFiltro = class(TRLKeyedCollectionItem)
  private
    FLoteria: TTipoLoteria;
    function GetLoteria: TTipoLoteria;
  protected
    function GetKey: string; override;
    procedure ChecarLoteria;
  public
    constructor Create(Collection: TCollection); override;
    function Validar(const ADezenas: TDezenas): Boolean; virtual; abstract;
    property Nome: string read GetKey;
    property Loteria: TTipoLoteria read GetLoteria write FLoteria;
  end;

  // TFiltroPorIntervalo
  TFiltroPorIntervalo = class(TFiltro)
  private
    FMinimo: Integer;
    FMaximo: Integer;
  public
    constructor Create(Collection: TCollection); override;
    procedure Assign(Source: TPersistent); override;
  published
    property Minimo: Integer read FMinimo write FMinimo;
    property Maximo: Integer read FMaximo write FMaximo;
  end;

  // TFiltroPorString
  TFiltroPorString = class(TFiltro)
  private
    type
      TItens = class(TStringList)
      private
        FFiltro: TFiltroPorString;
      protected
        procedure Changed; override;
      public
        property Filtro: TFiltroPorString read FFiltro;
      end;
  private
    FItens: TItens;
    FHash: TRLHashTable;
    FHashDesatualizado: Boolean;
  protected
    function AtualizarHash: Boolean;
    property Hash: TRLHashTable read FHash;
  public
    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;
  published
    property Itens: TItens read FItens;
  end;

  // TFiltroPorPares
  TFiltroPorPares = class(TFiltroPorIntervalo)
  public
    constructor Create(Collection: TCollection); override;
    function Validar(const ADezenas: TDezenas): Boolean; override;
  end;

  // TFiltroPorPrimos
  TFiltroPorPrimos = class(TFiltroPorIntervalo)
  public
    constructor Create(Collection: TCollection); override;
    function Validar(const ADezenas: TDezenas): Boolean; override;
  end;

  // TFiltroPorIntervaloDezenas
  TFiltroPorIntervaloDezenas = class(TFiltroPorIntervalo)
  public
    constructor Create(Collection: TCollection); override;
    function Validar(const ADezenas: TDezenas): Boolean; override;
  end;

  // TFiltroPorQuadrante
  TFiltroPorQuadrante = class(TFiltroPorIntervalo)
  public
    constructor Create(Collection: TCollection); override;
    function Validar(const ADezenas: TDezenas): Boolean; override;
  end;

  // TFiltroPorRepeticaoDezenas
  TFiltroPorRepeticaoDezenas = class(TFiltroPorIntervalo)
  public
    constructor Create(Collection: TCollection); override;
    function Validar(const ADezenas: TDezenas): Boolean; override;
  end;

  // TFiltroPorLinha
  TFiltroPorLinha = class(TFiltroPorString)
  public
    constructor Create(Collection: TCollection); override;
    function Validar(const ADezenas: TDezenas): Boolean; override;
  end;

  // TFiltroPorColuna
  TFiltroPorColuna = class(TFiltroPorString)
  public
    constructor Create(Collection: TCollection); override;
    function Validar(const ADezenas: TDezenas): Boolean; override;
  end;

  // TFiltroPorSequencia
  TFiltroPorSequencia = class(TFiltroPorString)
  public
    constructor Create(Collection: TCollection); override;
    function Validar(const ADezenas: TDezenas): Boolean; override;
  end;

  // TFiltroPorFrequencia
  TFiltroPorFrequencia = class(TFiltroPorString)
  public
    constructor Create(Collection: TCollection); override;
    function Validar(const ADezenas: TDezenas): Boolean; override;
  end;

  // TFiltroPorAtraso
  TFiltroPorAtraso = class(TFiltroPorString)
  public
    constructor Create(Collection: TCollection); override;
    function Validar(const ADezenas: TDezenas): Boolean; override;
  end;

  // TFiltroPorQuocienteFrequencia
  TFiltroPorQuocienteFrequencia = class(TFiltro)
  public
    constructor Create(Collection: TCollection); override;
    function Validar(const ADezenas: TDezenas): Boolean; override;
  end;

  // TFiltroPorQuocienteAtraso
  TFiltroPorQuocienteAtraso = class(TFiltro)
  public
    constructor Create(Collection: TCollection); override;
    function Validar(const ADezenas: TDezenas): Boolean; override;
  end;

  // TFiltros
  TFiltros = class(TRLKeyedCollection)
  private
    FLoteria: TTipoLoteria;
    function GetItem(const AIndex: Integer): TFiltro;
  protected
    function GetItemClass: TCollectionItemClass; override;
  public
    constructor Create; override;
    function Add: TFiltro;
    function AdicionarFiltroPorPares: TFiltroPorPares;
    function AdicionarFiltroPorPrimos: TFiltroPorPrimos;
    function AdicionarFiltroPorIntervaloDezenas: TFiltroPorIntervaloDezenas;
    function AdicionarFiltroPorQuadrante: TFiltroPorQuadrante;
    function AdicionarFiltroPorRepeticaoDezenas: TFiltroPorRepeticaoDezenas;
    function AdicionarFiltroPorLinha: TFiltroPorLinha;
    function AdicionarFiltroPorColuna: TFiltroPorColuna;
    function AdicionarFiltroPorSequencia: TFiltroPorSequencia;
    function AdicionarFiltroPorFrequencia: TFiltroPorFrequencia;
    function AdicionarFiltroPorAtraso: TFiltroPorAtraso;
    function AdicionarFiltroPorQuocienteFrequencia: TFiltroPorQuocienteFrequencia;
    function AdicionarFiltroPorQuocienteAtraso: TFiltroPorQuocienteAtraso;
    function Insert(const AIndex: Integer): TFiltro;
    function Find(const ANome: string): TFiltro;
    function Contains(const ANome: string): Boolean; reintroduce;
    function Remove(const ANome: string): Boolean; reintroduce;
    function Validar(const ADezenas: TDezenas): Boolean;
    property Items[const AIndex: Integer]: TFiltro read GetItem; default;
    property Loteria: TTipoLoteria read FLoteria write FLoteria;
  end;

  // TValorComparativo
  TValorComparativo = class(TRLCollectionItem)
  private
    FValor: string;
    FQuantidade: Integer;
    FPercentual: Double;
    FPercentualHistorico: Double;
    FDiferenca: Double;
  public
    procedure Assign(Source: TPersistent); override;
    property Valor: string read FValor write FValor;
    property Quantidade: Integer read FQuantidade write FQuantidade;
    property Percentual: Double read FPercentual write FPercentual;
    property PercentualHistorico: Double read FPercentualHistorico write FPercentualHistorico;
    property Diferenca: Double read FDiferenca write FDiferenca;
  end;

  // TValoresComparativo
  TValoresComparativo = class(TRLCollection)
  private
    function GetItem(const AIndex: Integer): TValorComparativo;
  protected
    function GetItemClass: TCollectionItemClass; override;
  public
    function Add: TValorComparativo;
    function Insert(const AIndex: Integer): TValorComparativo;
    property Items[const AIndex: Integer]: TValorComparativo read GetItem; default;
  end;

  // TResultadoComparativo
  TResultadoComparativo = class(TRLKeyedCollectionItem)
  private
    FDescricao: string;
    FValores: TValoresComparativo;
  protected
    function GetKey: string; override;
    procedure SetKey(const AValue: string); override;
  public
    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;
    property Estatistica: string read GetKey write SetKey;
    property Descricao: String read FDescricao write FDescricao;
    property Valores: TValoresComparativo read FValores;
  end;

  // TResultadosComparativo
  TResultadosComparativo = class(TRLKeyedCollection)
  private
    function GetItem(const AIndex: Integer): TResultadoComparativo;
  protected
    function GetItemClass: TCollectionItemClass; override;
  public
    function Add: TResultadoComparativo;
    function Insert(const AIndex: Integer): TResultadoComparativo;
    function Find(const AEstatistica: string): TResultadoComparativo;
    function Contains(const AEstatistica: string): Boolean; reintroduce;
    function Remove(const AEstatistica: string): Boolean; reintroduce;
    function CriarDataSet: TDataSet;
    property Items[const AIndex: Integer]: TResultadoComparativo read GetItem; default;
  end;

  // TComparativo
  TComparativo = class(TRLContainedObject)
  private
    FLoteria: TTipoLoteria;
    FEstatisticas: TEstatisticas;
    FResultados: TResultadosComparativo;
    function GetLoteria: TTipoLoteria;
  protected
    procedure ChecarLoteria;
    procedure ChecarEstatisticas;
  public
    constructor CreateContainedObject(const AParent: TObject); override;
    destructor Destroy; override;
    function Executar(const AMargem: Double): Boolean;
    function CriarDataSet: TDataSet;
    property Loteria: TTipoLoteria read GetLoteria write FLoteria;
    property Estatisticas: TEstatisticas read FEstatisticas write FEstatisticas;
    property Resultados: TResultadosComparativo read FResultados;
  end;

  // TParametrosAposta
  TParametrosAposta = class(TRLContainedObject)
  private
    FLoteria: TTipoLoteria;
    FQuantidadeJogos: Integer;
    FMinimoPares: Integer;
    FMaximoPares: Integer;
    FMinimoPrimos: Integer;
    FMaximoPrimos: Integer;
    FMinimoIntervalo: Integer;
    FMaximoIntervalo: Integer;
    FMinimoQuadrantes: Integer;
    FMaximoQuadrantes: Integer;
    FMinimoRepeticaoDezenas: Integer;
    FMaximoRepeticaoDezenas: Integer;
    FLinha: TStrings;
    FColuna: TStrings;
    FSequencia: TStrings;
    FFrequencia: TStrings;
    FAtraso: TStrings;
    FMargemPares: Double;
    FMargemPrimos: Double;
    FMargemIntervalo: Double;
    FMargemQuadrantes: Double;
    FMargemRepeticaoDezenas: Double;
    FMargemLinha: Double;
    FMargemColuna: Double;
    FMargemSequencia: Double;
    FMargemFrequencia: Double;
    FMargemAtraso: Double;
    FUsarQuocienteFrequencia: Boolean;
    FUsarQuocienteAtraso: Boolean;
    function GetLoteria: TTipoLoteria;
  protected
    procedure ChecarLoteria;
    procedure ChecarQuantidadeJogos;
    procedure ChecarPares;
    procedure ChecarPrimos;
    procedure ChecarMargemPares;
    procedure ChecarMargemPrimos;
    procedure ChecarQuadrantes;
    procedure ChecarMargemQuadrantes;
    procedure ChecarRepeticaoDezenas;
    procedure ChecarMargemRepeticaoDezenas;
    procedure ChecarMargemLinha;
    procedure ChecarMargemColuna;
    procedure ChecarMargemSequencia;
    procedure ChecarMargemFrequencia;
    procedure ChecarMargemAtraso;
  public
    constructor CreateContainedObject(const AParent: TObject); override;
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;
    procedure LerXML(const ANomeArquivo: string);
    procedure GravarXML(const ANomeArquivo: string);
    function GerarFiltros: TFiltros;
    procedure GerarString(const AStrings: TStrings); virtual;
    procedure ConfigurarEstatisticas(const AEstatisticas: TEstatisticas);
    property Loteria: TTipoLoteria read GetLoteria write FLoteria;
  published
    property QuantidadeJogos: Integer read FQuantidadeJogos write FQuantidadeJogos;
    property MinimoPares: Integer read FMinimoPares write FMinimoPares;
    property MaximoPares: Integer read FMaximoPares write FMaximoPares;
    property MinimoPrimos: Integer read FMinimoPrimos write FMinimoPrimos;
    property MaximoPrimos: Integer read FMaximoPrimos write FMaximoPrimos;
    property MinimoIntervalo: Integer read FMinimoIntervalo write FMinimoIntervalo;
    property MaximoIntervalo: Integer read FMaximoIntervalo write FMaximoIntervalo;
    property MinimoQuadrantes: Integer read FMinimoQuadrantes write FMinimoQuadrantes;
    property MaximoQuadrantes: Integer read FMaximoQuadrantes write FMaximoQuadrantes;
    property MinimoRepeticaoDezenas: Integer read FMinimoRepeticaoDezenas write FMinimoRepeticaoDezenas;
    property MaximoRepeticaoDezenas: Integer read FMaximoRepeticaoDezenas write FMaximoRepeticaoDezenas;
    property Linha: TStrings read FLinha;
    property Coluna: TStrings read FColuna;
    property Sequencia: TStrings read FSequencia;
    property Frequencia: TStrings read FFrequencia;
    property Atraso: TStrings read FAtraso;
    property MargemPares: Double read FMargemPares write FMargemPares;
    property MargemPrimos: Double read FMargemPrimos write FMargemPrimos;
    property MargemIntervalo: Double read FMargemIntervalo write FMargemIntervalo;
    property MargemQuadrantes: Double read FMargemQuadrantes write FMargemQuadrantes;
    property MargemRepeticaoDezenas: Double read FMargemRepeticaoDezenas write FMargemRepeticaoDezenas;
    property MargemLinha: Double read FMargemLinha write FMargemLinha;
    property MargemColuna: Double read FMargemColuna write FMargemColuna;
    property MargemSequencia: Double read FMargemSequencia write FMargemSequencia;
    property MargemFrequencia: Double read FMargemFrequencia write FMargemFrequencia;
    property MargemAtraso: Double read FMargemAtraso write FMargemAtraso;
    property UsarQuocienteFrequencia: Boolean read FUsarQuocienteFrequencia write FUsarQuocienteFrequencia;
    property UsarQuocienteAtraso: Boolean read FUsarQuocienteAtraso write FUsarQuocienteAtraso;
  end;

  // TAcerto
  TAcerto = class(TRLKeyedCollectionItem)
  private
    FQuantidade: Integer;
    function GetDezenas: Integer;
    procedure SetDezenas(const AValue: Integer);
  public
    procedure Assign(Source: TPersistent); override;
  published
    property Dezenas: Integer read GetDezenas write SetDezenas;
    property Quantidade: Integer read FQuantidade write FQuantidade;
  end;

  // TAcertos
  TAcertos = class(TRLKeyedCollection)
  private
    function GetItem(const AIndex: Integer): TAcerto;
  protected
    function GetItemClass: TCollectionItemClass; override;
  public
    constructor Create; override;
    function Add: TAcerto;
    function Insert(const AIndex: Integer): TAcerto;
    function Find(const ADezenas: Integer): TAcerto;
    function Contains(const ADezenas: Integer): Boolean; reintroduce;
    function Remove(const ADezenas: Integer): Boolean; reintroduce;
    procedure Somar(const AAcertos: TAcertos);
    property Items[const AIndex: Integer]: TAcerto read GetItem; default;
  end;

  // TAposta
  TAposta = class(TRLBaseType, ITipoLoteria)
  private
    FParametros: TParametrosAposta;
    FLoteria: TTipoLoteria;
    FJogos: TJogos;
    FEstatisticas: TEstatisticas;
    FComparativo: TComparativo;
    FAoGerarJogo: TEventoJogo;
    function GetLoteria: TTipoLoteria;
  protected
    procedure ChecarLoteria;
    procedure DispararEventoJogo(const ADezenas: TDezenas; const AValido: Boolean; var AContinuar: Boolean); virtual;
  public
    constructor Create; override;
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;
    procedure LerXML(const ANomeArquivo: string);
    procedure GravarXML(const ANomeArquivo: string);
    procedure CopiarEstatisticasLoteria(const AConfigurar: Boolean);
    procedure ConfigurarEstatisticas;
    procedure GerarString(const AStrings: TStrings);
    function GerarFiltros: TFiltros;
    function GerarJogos(const AFiltros: TFiltros): Boolean;
    function GerarJogosComEstatistica(const AFiltros: TFiltros; out ATentativas: Integer): Boolean;
    function GerarJogosComEstatisticaEmThread(const AFiltros: TFiltros; out ATentativas: Integer): Boolean;
    function VerificarAcerto(const ADezenas: TDezenas; const ADezenasPorAcerto: Integer = 0): TAcertos;
    function CriarDataSetJogos: TDataSet;
    function CriarDataSetComparativo: TDataSet;
    property Loteria: TTipoLoteria read GetLoteria write FLoteria;
    property Jogos: TJogos read FJogos;
    property Estatisticas: TEstatisticas read FEstatisticas;
    property Comparativo: TComparativo read FComparativo;
    property AoGerarJogo: TEventoJogo read FAoGerarJogo write FAoGerarJogo;
  published
    property Parametros: TParametrosAposta read FParametros;
  end;

  // TThreadGeracaoJogosAposta
  TThreadGeracaoJogosAposta = class(TThreadOperacao)
  private
    FAposta: TAposta;
    FFiltros: TFiltros;
    FTentativas: Integer;
    FResultado: Boolean;
  protected
    procedure ExecutarThread; override;
  public
    property Aposta: TAposta read FAposta write FAposta;
    property Filtros: TFiltros read FFiltros write FFiltros;
    property Tentativas: Integer read FTentativas;
    property Resultado: Boolean read FResultado;
  end;

  // TParametrosSimulador
  TParametrosSimulador = class(TParametrosAposta)
  private
    FNumeroAcertos: Integer;
    FDezenasPorAcerto: Integer;
    FSorteioInicial: Integer;
    FSorteioFinal: Integer;
    FUsarParesSorteio: Boolean;
    FUsarPrimosSorteio: Boolean;
    FUsarIntervaloSorteio: Boolean;
    FUsarLinhaSorteio: Boolean;
    FUsarColunaSorteio: Boolean;
    FUsarSequenciaSorteio: Boolean;
  protected
    procedure ChecarNumeroAcertos;
    procedure ChecarDezenasPorAcerto;
    procedure ChecarSorteios;
  public
    constructor CreateContainedObject(const AParent: TObject); override;
    procedure Assign(Source: TPersistent); override;
    function GerarSorteios: TSorteios;
    function UsarParametrosSorteio: Boolean;
  published
    property NumeroAcertos: Integer read FNumeroAcertos write FNumeroAcertos;
    property DezenasPorAcerto: Integer read FDezenasPorAcerto write FDezenasPorAcerto;
    property SorteioInicial: Integer read FSorteioInicial write FSorteioInicial;
    property SorteioFinal: Integer read FSorteioFinal write FSorteioFinal;
    property UsarParesSorteio: Boolean read FUsarParesSorteio write FUsarParesSorteio;
    property UsarPrimosSorteio: Boolean read FUsarPrimosSorteio write FUsarPrimosSorteio;
    property UsarIntervaloSorteio: Boolean read FUsarIntervaloSorteio write FUsarIntervaloSorteio;
    property UsarLinhaSorteio: Boolean read FUsarLinhaSorteio write FUsarLinhaSorteio;
    property UsarColunaSorteio: Boolean read FUsarColunaSorteio write FUsarColunaSorteio;
    property UsarSequenciaSorteio: Boolean read FUsarSequenciaSorteio write FUsarSequenciaSorteio;
  end;

  // TResultadoSimulador
  TResultadoSimulador = class(TSorteio)
  private
    FPares: Integer;
    FPrimos: Integer;
    FIntervaloDezenas: Integer;
    FLinha: string;
    FColuna: string;
    FSequencia: string;
    FTentativas: Integer;
    FMediaTentativas: Integer;
    FMenorIntervalo: Integer;
    FMaiorIntervalo: Integer;
    FAcertos: TAcertos;
  public
    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;
  published
    property Pares: Integer read FPares write FPares;
    property Primos: Integer read FPrimos write FPrimos;
    property IntervaloDezenas: Integer read FIntervaloDezenas write FIntervaloDezenas;
    property Linha: string read FLinha write FLinha;
    property Coluna: string read FColuna write FColuna;
    property Sequencia: string read FSequencia write FSequencia;
    property Tentativas: Integer read FTentativas write FTentativas;
    property MediaTentativas: Integer read FMediaTentativas write FMediaTentativas;
    property MenorIntervalo: Integer read FMenorIntervalo write FMenorIntervalo;
    property MaiorIntervalo: Integer read FMaiorIntervalo write FMaiorIntervalo;
    property Acertos: TAcertos read FAcertos;
  end;

  // TResultadosSimulador
  TResultadosSimulador = class(TSorteios)
  private
    FLoteria: TTipoLoteria;
    function GetItem(const AIndex: Integer): TResultadoSimulador;
    function GetLoteria: TTipoLoteria;
  protected
    function GetItemClass: TCollectionItemClass; override;
    procedure ChecarLoteria;
  public
    function Add: TResultadoSimulador;
    function Insert(const AIndex: Integer): TResultadoSimulador;
    function Localizar(const ADezenas: TDezenas): TResultadoSimulador;
    function Copiar(const AInicio, ATermino: Integer): TResultadoSimulador;
    function CriarDataSet: TDataSet;
    property Items[const AIndex: Integer]: TResultadoSimulador read GetItem; default;
    property Loteria: TTipoLoteria read GetLoteria write FLoteria;
  end;

  // TSimulador
  TSimulador = class(TRLBaseType, ITipoLoteria)
  private
    type
      TApostaSimulador = class(TAposta)
      private
        FSimulador: TSimulador;
      protected
        procedure DispararEventoJogo(const ADezenas: TDezenas; const AValido: Boolean; var AContinuar: Boolean); override;
      public
        property Simulador: TSimulador read FSimulador write FSimulador;
      end;
  private
    FParametros: TParametrosSimulador;
    FLoteria: TTipoLoteria;
    FResultados: TResultadosSimulador;
    FAoGerarJogo: TEventoJogo;
    function GetLoteria: TTipoLoteria;
  protected
    procedure ChecarLoteria;
    procedure DispararEventoJogo(const ADezenas: TDezenas; const AValido: Boolean; var AContinuar: Boolean); virtual;
  public
    constructor Create; override;
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;
    procedure LerXML(const ANomeArquivo: string);
    procedure GravarXML(const ANomeArquivo: string);
    function Executar: Boolean;
    function ExecutarEmThread: Boolean;
    function CriarDataSetResultados: TDataSet;
    property Loteria: TTipoLoteria read GetLoteria write FLoteria;
    property Resultados: TResultadosSimulador read FResultados;
    property AoGerarJogo: TEventoJogo read FAoGerarJogo write FAoGerarJogo;
  published
    property Parametros: TParametrosSimulador read FParametros;
  end;

  // TThreadSimulador
  TThreadSimulador = class(TThread)
  private
    FSimulador: TSimulador;
    FResultado: Boolean;
    FErro: string;
    FClasseErro: ExceptClass;
  protected
    procedure Execute; override;
  public
    property Simulador: TSimulador read FSimulador write FSimulador;
    property Resultado: Boolean read FResultado;
    property Erro: string read FErro;
    property ClasseErro: ExceptClass read FClasseErro;
  end;

  // TTipoLoteria
  TTipoLoteria = class(TRLKeyedCollectionItem)
  private
    FMaiorDezena: Integer;
    FLinhasTabela: Integer;
    FColunasTabela: Integer;
    FDezenasSorteio: Integer;
    FDezenasPremiacaoMinima: Integer;
    FQuadrantes: TQuadrantes;
    FGabarito: TGabarito;
    FPrecos: TPrecosAposta;
    FHistorico: THistorico;
    FEstatisticas: TEstatisticas;
    FNomeArquivoHistorico: string;
    FNomeArquivoEstatisticas: string;
  protected
    function GetKey: string; override;
    procedure SetKey(const AValue: string); override;
    procedure KeyChanged; override;
  public
    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;
    procedure LerArquivoHistorico;
    procedure GravarArquivoHistorico;
    procedure ImportarArquivoHistorico(const ANomeArquivo: string);
    procedure ExportarArquivoHistorico(const ANomeArquivo: string);
    procedure AtualizarEstatisticasHistorico;
    procedure LerArquivoEstatisticas;
    procedure GravarArquivoEstatisticas;
    procedure RecalcularEstatisticas(const AUsarThread: Boolean);
    procedure GerarStringOcorrencia(const AStrings: TStrings);
    function FormatarStringOcorrencia(const ACentro, AEsquerda, ADireita: Integer): string;
    property NomeArquivoHistorico: string read FNomeArquivoHistorico write FNomeArquivoHistorico;
    property NomeArquivoEstatisticas: string read FNomeArquivoEstatisticas write FNomeArquivoEstatisticas;
  published
    property Nome: string read GetKey write SetKey;
    property MaiorDezena: Integer read FMaiorDezena write FMaiorDezena;
    property LinhasTabela: Integer read FLinhasTabela write FLinhasTabela;
    property ColunasTabela: Integer read FColunasTabela write FColunasTabela;
    property DezenasSorteio: Integer read FDezenasSorteio write FDezenasSorteio;
    property DezenasPremiacaoMinima: Integer read FDezenasPremiacaoMinima write FDezenasPremiacaoMinima;
    property Quadrantes: TQuadrantes read FQuadrantes;
    property Gabarito: TGabarito read FGabarito;
    property Precos: TPrecosAposta read FPrecos;
    property Historico: THistorico read FHistorico;
    property Estatisticas: TEstatisticas read FEstatisticas;
  end;

  // TTiposLoteria
  TTiposLoteria = class(TRLKeyedCollection)
  private
    function GetItem(const AIndex: Integer): TTipoLoteria;
  protected
    function GetItemClass: TCollectionItemClass; override;
  public
    constructor Create; override;
    function Add: TTipoLoteria;
    function Insert(const AIndex: Integer): TTipoLoteria;
    function Find(const ANome: string): TTipoLoteria;
    function Contains(const ANome: string): Boolean; reintroduce;
    function Remove(const ANome: string): Boolean; reintroduce;
    property Items[const AIndex: Integer]: TTipoLoteria read GetItem; default;
  end;

  // TFrame
  TFrame = class(Forms.TFrame)
  private
    FEstatistica: TEstatistica;
    procedure SetEstatistica(const AValue: TEstatistica);
  protected
    procedure Preencher; virtual; abstract;
    procedure EstatisticaAlterada; virtual;
  public
    constructor Create(AOwner: TComponent); override;
    procedure Mostrar; virtual;
    property Estatistica: TEstatistica read FEstatistica write SetEstatistica;
  end;

  // TInfoEstatistica
  TInfoEstatistica = class(TRLBaseType)
  private
    FEstatistica: TEstatistica;
    FPainel: TFrame;
  public
    destructor Destroy; override;
    property Estatistica: TEstatistica read FEstatistica write FEstatistica;
    property Painel: TFrame read FPainel write FPainel;
  end;

  // TLoteria
  TLoteria = class(TRLApplication)
  private
    const
      CNumeroSorteiosEstatistica = 1000;
  private
    FTipos: TTiposLoteria;
{$IFDEF MS}
    function ConfigurarMegaSena(const ANome: string): TTipoLoteria;
{$ENDIF}
  public
    constructor Create; override;
    destructor Destroy; override;
    function Initialize: Boolean; override;
    property Tipos: TTiposLoteria read FTipos;
    class function Instance: TLoteria;
  end;

  // TRotinas
  TRotinas = class(TRLBaseType)
  public
    class function Primo(const ANumero: Integer): Boolean;
    class function CalcularFatorial(const ANumero: Integer; const ALimite: Integer = 1): Double;
    class function CalcularProbabilidade(const AConjunto, AElementos: Integer): Double;
    class function Concatenar(const AStrings: TStrings): string;
  end;

const
  CMegaSena = 'Mega-sena';
  COcorrenciaPorPares = 'OcorrenciaPorPares';
  COcorrenciaPorPrimos = 'OcorrenciaPorPrimos';
  COcorrenciaPorIntervalo = 'OcorrenciaPorIntervalo';
  COcorrenciaPorLinha = 'OcorrenciaPorLinha';
  COcorrenciaPorColuna = 'OcorrenciaPorColuna';
  COcorrenciaPorSequencia = 'OcorrenciaPorSequencia';
  COcorrenciaPorQuadrante = 'OcorrenciaPorQuadrante';
  COcorrenciaPorDezena = 'OcorrenciaPorDezena';
  COcorrenciaPorRepeticaoDezenas = 'OcorrenciaPorRepeticaoDezenas';
  CDezenasMaisSorteadas = 'DezenasMaisSorteadas';
  CDezenasMenosSorteadas = 'DezenasMenosSorteadas';
  CFrequenciaDezenas = 'FrequenciaDezenas';
  CFrequenciaAcumulada = 'FrequenciaAcumulada';
  CFrequencia = 'Frequencia';
  CQuocienteFrequencia = 'QuocienteFrequencia';
  CAtrasoDezenas = 'AtrasoDezenas';
  CQuocienteAtraso = 'QuocienteAtraso';
  CAtrasoAcumulado = 'AtrasoAcumulado';
  CAtraso = 'Atraso';
  CFiltroPorPares = 'FiltroPorPares';
  CFiltroPorPrimos = 'FiltroPorPrimos';
  CFiltroPorIntervaloDezenas = 'FiltroPorIntervaloDezenas';
  CFiltroPorQuadrante = 'FiltroPorQuadrante';
  CFiltroPorRepeticaoDezenas = 'FiltroPorRepeticaoDezenas';
  CFiltroPorLinha = 'FiltroPorLinha';
  CFiltroPorColuna = 'FiltroPorColuna';
  CFiltroPorSequencia = 'FiltroPorSequencia';
  CFiltroPorFrequencia = 'FiltroPorFrequencia';
  CFiltroPorAtraso = 'FiltroPorAtraso';
  CFiltroPorQuocienteFrequencia = 'FiltroPorQuocienteFrequencia';
  CFiltroPorQuocienteAtraso = 'FiltroPorQuocienteAtraso';
  CIDAtrasoDezenas = 'AD';

implementation

uses
  Math,
  StrUtils,
  U_RTTIContext,
  U_DXFormManager;

{ TDezena }

procedure TDezena.Assign(Source: TPersistent);
begin
  inherited;
  if Source is TDezena then
    Dezena := TDezena(Source).Dezena;
end;

function TDezena.GetDezena: Integer;
begin
  Result := StrToInt(GetKey);
end;

procedure TDezena.SetDezena(const AValor: Integer);
begin
  SetKey(IntToStr(AValor));
end;

{ TDezenas }

function TDezenas.GetItem(const AIndex: Integer): TDezena;
begin
  Result := TDezena(inherited Items[AIndex]);
end;

procedure TDezenas.OrdenarDezenas(L, R: Integer);
var
  lMaior: TDezena;
  lMenor: TDezena;
  I: Integer;
  J: Integer;
  P: Integer;
begin
  repeat
    I := L;
    J := R;
    P := (L + R) shr 1;
    repeat
      while CompareValue(Items[I].Dezena, Items[P].Dezena) < 0 do
        Inc(I);
      while CompareValue(Items[J].Dezena, Items[P].Dezena) > 0 do
        Dec(J);
      if I <= J then
      begin
        if I <> J then
        begin
          lMaior := TDezena(GetItemClass.Create(nil));
          lMenor := TDezena(GetItemClass.Create(nil));
          try
            lMaior.Assign(Items[I]);
            lMenor.Assign(Items[J]);
            Items[J].Dezena := -1;
            Items[I].Assign(lMenor);
            Items[J].Assign(lMaior);
          finally
            FreeAndNil(lMenor);
            FreeAndNil(lMaior);
          end;
        end;

        if P = I then
          P := J
        else
          if P = J then
            P := I;
        Inc(I);
        Dec(J);
      end;
    until I > J;

    if L < J then
      OrdenarDezenas(L, J);
    L := I;
  until I >= R;
end;

function TDezenas.GetItemClass: TCollectionItemClass;
begin
  Result := TDezena;
end;

function TDezenas.Add: TDezena;
begin
  Result := TDezena(inherited Add);
end;

function TDezenas.Insert(const AIndex: Integer): TDezena;
begin
  Result := TDezena(inherited Insert(AIndex));
end;

function TDezenas.Find(const ADezena: Integer): TDezena;
begin
  Result := TDezena(inherited Find(IntToStr(ADezena)));
end;

function TDezenas.Contains(const ADezena: Integer): Boolean;
begin
  Result := inherited Contains(IntToStr(ADezena));
end;

function TDezenas.Remove(const ADezena: Integer): Boolean;
begin
  Result := inherited Remove(IntToStr(ADezena));
end;

function TDezenas.ConferirDezenas(const ADezenas: TDezenas): Integer;
var
  I: Integer;
begin
  Result := 0;
  if Assigned(ADezenas) then
    for I := 0 to Pred(ADezenas.Count) do
      if Contains(ADezenas[I].Dezena) then
        Inc(Result);
end;

function TDezenas.ContemDezenas(const ADezenas: TDezenas): Boolean;
var
  I: Integer;
begin
  if Assigned(ADezenas) then
  begin
    Result := True;
    I := 0;
    while (I < ADezenas.Count) and Result do
      if not Contains(ADezenas[I].Dezena) then
        Result := False
      else
        Inc(I);
  end
  else
    Result := False;
end;

function TDezenas.GerarString(const ADelimitador: string): string;
var
  I: Integer;
begin
  Result := '';
  for I := 0 to Pred(Count) do
    Result := Result + IfThen(I > 0, ADelimitador) + IntToStr(Items[I].Dezena);
end;

function TDezenas.ContarPares: Integer;
var
  I: Integer;
begin
  Result := 0;
  for I := 0 to Pred(Count) do
    if Items[I].Dezena mod 2 = 0 then
      Inc(Result);
end;

function TDezenas.ContarPrimos: Integer;
var
  I: Integer;
begin
  Result := 0;
  for I := 0 to Pred(Count) do
    if TRotinas.Primo(Items[I].Dezena) then
      Inc(Result);
end;

function TDezenas.LerIntervalo: Integer;
var
  nIntervalo: Integer;
  I: Integer;
begin
  Result := 1;
  for I := 1 to Pred(Count) do
  begin
    nIntervalo := Items[I].Dezena - Items[Pred(I)].Dezena;
    if nIntervalo > Result  then
      Result := nIntervalo;
  end;
end;

procedure TDezenas.Ordenar;
begin
  if Count > 0 then
    OrdenarDezenas(0, Pred(Count));
end;

{ TDezenaContada }

procedure TDezenaContada.Assign(Source: TPersistent);
begin
  inherited;
  if Source is TDezenaContada then
    Quantidade := TDezenaContada(Source).Quantidade;
end;

{ TDezenasContadas }

function TDezenasContadas.GetItem(const AIndex: Integer): TDezenaContada;
begin
  Result := TDezenaContada(inherited Items[AIndex]);
end;

function TDezenasContadas.GetItemClass: TCollectionItemClass;
begin
  Result := TDezenaContada;
end;

function TDezenasContadas.Add: TDezenaContada;
begin
  Result := TDezenaContada(inherited Add);
end;

function TDezenasContadas.Insert(const AIndex: Integer): TDezenaContada;
begin
  Result := TDezenaContada(inherited Insert(AIndex));
end;

function TDezenasContadas.Find(const ADezena: Integer): TDezenaContada;
begin
  Result := TDezenaContada(inherited Find(ADezena));
end;

function TDezenasContadas.CriarDataSet: TDataSet;
var
  lDataSet: TClientDataSet;
  I: Integer;
begin
  lDataSet := TClientDataSet.Create(nil);
  try
    lDataSet.FieldDefs.Add('Dezena', ftInteger);
    lDataSet.FieldDefs.Add('Quantidade', ftInteger);
    lDataSet.CreateDataSet;
    lDataSet.LogChanges := False;
    lDataSet.DisableControls;
    try
      for I := 0 to Pred(Count) do
      begin
        lDataSet.Append;
        lDataSet.FieldByName('Dezena').AsInteger := Items[I].Dezena;
        lDataSet.FieldByName('Quantidade').AsInteger := Items[I].Quantidade;
        lDataSet.Post;
      end;

      lDataSet.First;
    finally
      lDataSet.EnableControls;
    end;

    Result := lDataSet;
  except
    FreeAndNil(lDataSet);
    raise;
  end;
end;

{ TItemContagem }

function TItemContagem.GetKey: string;
begin
  Result := inherited GetKey;
end;

procedure TItemContagem.SetKey(const AValue: string);
begin
  inherited;
end;

procedure TItemContagem.Assign(Source: TPersistent);
var
  lItem: TItemContagem;
begin
  inherited;
  if Source is TItemContagem then
  begin
    lItem := TItemContagem(Source);
    Quantidade := lItem.Quantidade;
    Percentual := lItem.Percentual;
  end;
end;

{ TContagem }

function TContagem.GetItem(const AIndex: Integer): TItemContagem;
begin
  Result := TItemContagem(inherited Items[AIndex]);
end;

function TContagem.GetItemClass: TCollectionItemClass;
begin
  Result := TItemContagem;
end;

function TContagem.Add: TItemContagem;
begin
  Result := TItemContagem(inherited Add);
end;

function TContagem.Insert(const AIndex: Integer): TItemContagem;
begin
  Result := TItemContagem(inherited Insert(AIndex));
end;

function TContagem.Find(const AValor: string): TItemContagem;
begin
  Result := TItemContagem(inherited Find(AValor));
end;

function TContagem.Contains(const AValor: string): Boolean;
begin
  Result := inherited Contains(AValor);
end;

function TContagem.Remove(const AValor: string): Boolean;
begin
  Result := inherited Remove(AValor);
end;

procedure TContagem.CalcularPercentual;
var
  nSoma: Integer;
  I: Integer;
begin
  nSoma := 0;
  for I := 0 to Pred(Count) do
  begin
    nSoma := nSoma + Items[I].Quantidade;
    Items[I].Percentual := 0;
  end;

  if nSoma > 0 then
    for I := 0 to Pred(Count) do
      Items[I].Percentual := SimpleRoundTo((Items[I].Quantidade * 100) / nSoma);
end;

function TContagem.CriarDataSet(const AMostrarPercentual: Boolean;
  const ANomeValor: string; const ATipoValor: TFieldType;
  const ATamanhoValor: Integer): TDataSet;
var
  lDataSet: TClientDataSet;
  I: Integer;
begin
  lDataSet := TClientDataSet.Create(nil);
  try
    lDataSet.FieldDefs.Add(ANomeValor, ATipoValor, ATamanhoValor);
    lDataSet.FieldDefs.Add('Quantidade', ftInteger);
    if AMostrarPercentual then
      lDataSet.FieldDefs.Add('%', ftFloat);
    lDataSet.CreateDataSet;
    lDataSet.LogChanges := False;
    lDataSet.DisableControls;
    try
      for I := 0 to Pred(Count) do
      begin
        lDataSet.Append;
        lDataSet.FieldByName(ANomeValor).AsString := Items[I].Valor;
        lDataSet.FieldByName('Quantidade').AsInteger := Items[I].Quantidade;
        if AMostrarPercentual then
          lDataSet.FieldByName('%').AsFloat := Items[I].Percentual;
        lDataSet.Post;
      end;

      lDataSet.First;
    finally
      lDataSet.EnableControls;
    end;

    Result := lDataSet;
  except
    FreeAndNil(lDataSet);
    raise;
  end;
end;

{ TFaixaQuadrante }

procedure TFaixaQuadrante.Assign(Source: TPersistent);
var
  lFaixa: TFaixaQuadrante;
begin
  if Source is TFaixaQuadrante then
  begin
    lFaixa := TFaixaQuadrante(Source);
    Inicio := lFaixa.Inicio;
    Termino := lFaixa.Termino;
  end;
end;

{ TFaixasQuadrante }

function TFaixasQuadrante.GetItem(const AIndex: Integer): TFaixaQuadrante;
begin
  Result := TFaixaQuadrante(inherited Items[AIndex]);
end;

function TFaixasQuadrante.GetItemClass: TCollectionItemClass;
begin
  Result := TFaixaQuadrante;
end;

function TFaixasQuadrante.Add: TFaixaQuadrante;
begin
  Result := TFaixaQuadrante(inherited Add);
end;

function TFaixasQuadrante.Insert(const AIndex: Integer): TFaixaQuadrante;
begin
  Result := TFaixaQuadrante(inherited Insert(AIndex));
end;

function TFaixasQuadrante.ContemDezena(const ADezena: Integer): Boolean;
var
  I: Integer;
begin
  Result := False;
  I := 0;
  while (I < Count) and not Result do
    if (ADezena >= Items[I].Inicio) and (ADezena <= Items[I].Termino) then
      Result := True
    else
      Inc(I);
end;

{ TQuadrante }

constructor TQuadrante.Create(Collection: TCollection);
begin
  inherited;
  FFaixas := TFaixasQuadrante.Create;
end;

destructor TQuadrante.Destroy;
begin
  FreeAndNil(FFaixas);
  inherited;
end;

function TQuadrante.GetNumero: Integer;
begin
  Result := StrToInt(GetKey);
end;

procedure TQuadrante.SetNumero(const AValor: Integer);
begin
  SetKey(IntToStr(AValor));
end;

procedure TQuadrante.Assign(Source: TPersistent);
var
  lQuadrante: TQuadrante;
begin
  inherited;
  if Source is TQuadrante then
  begin
    lQuadrante := TQuadrante(Source);
    Numero := lQuadrante.Numero;
    Faixas.Assign(lQuadrante.Faixas);
  end;
end;

{ TQuadrantes }

constructor TQuadrantes.Create;
begin
  inherited;
  HashSize := 16;
end;

function TQuadrantes.GetItem(const AIndex: Integer): TQuadrante;
begin
  Result := TQuadrante(inherited Items[AIndex]);
end;

function TQuadrantes.GetItemClass: TCollectionItemClass;
begin
  Result := TQuadrante;
end;

function TQuadrantes.Add: TQuadrante;
begin
  Result := TQuadrante(inherited Add);
end;

function TQuadrantes.Insert(const AIndex: Integer): TQuadrante;
begin
  Result := TQuadrante(inherited Insert(AIndex));
end;

function TQuadrantes.Find(const ANumero: Integer): TQuadrante;
begin
  Result := TQuadrante(inherited Find(IntToStr(ANumero)));
end;

function TQuadrantes.Contains(const ANumero: Integer): Boolean;
begin
  Result := inherited Contains(IntToStr(ANumero));
end;

function TQuadrantes.Remove(const ANumero: Integer): Boolean;
begin
  Result := inherited Remove(IntToStr(ANumero));
end;

function TQuadrantes.LocalizarQuadrante(const ADezena: Integer): TQuadrante;
var
  I: Integer;
begin
  Result := nil;
  I := 0;
  while (I < Count) and not Assigned(Result) do
    if Items[I].Faixas.ContemDezena(ADezena) then
      Result := Items[I]
    else
      Inc(I);
end;

function TQuadrantes.QuadrantesPreenchidos(const ADezenas: TDezenas): Integer;
var
  lQuadrantes: array of Integer;
  lQuadrante: TQuadrante;
  I: Integer;
begin
  Result := 0;
  if Assigned(ADezenas) and (Count > 0) then
  begin
    SetLength(lQuadrantes, Count);
    for I := 0 to Pred(ADezenas.Count) do
    begin
      lQuadrante := LocalizarQuadrante(ADezenas[I].Dezena);
      Inc(lQuadrantes[lQuadrante.Index]);
    end;

    for I := 0 to High(lQuadrantes) do
      if lQuadrantes[I] > 0 then
        Inc(Result);
  end;
end;

{ TOcorrenciaGabarito }

constructor TOcorrenciaGabarito.Create(Collection: TCollection);
begin
  inherited;
  FDezenas := 1;
end;

procedure TOcorrenciaGabarito.Assign(Source: TPersistent);
begin
  if Source is TOcorrenciaGabarito then
    Dezenas := TOcorrenciaGabarito(Source).Dezenas;
end;

{ TOcorrenciasGabarito }

function TOcorrenciasGabarito.GetItem(
  const AIndex: Integer): TOcorrenciaGabarito;
begin
  Result := TOcorrenciaGabarito(inherited Items[AIndex]);
end;

function TOcorrenciasGabarito.GetItemClass: TCollectionItemClass;
begin
  Result := TOcorrenciaGabarito;
end;

function TOcorrenciasGabarito.Add: TOcorrenciaGabarito;
begin
  Result := TOcorrenciaGabarito(inherited Add);
end;

function TOcorrenciasGabarito.Insert(
  const AIndex: Integer): TOcorrenciaGabarito;
begin
  Result := TOcorrenciaGabarito(inherited Insert(AIndex));
end;

{ TItemGabarito }

constructor TItemGabarito.Create(Collection: TCollection);
begin
  inherited;
  FOcorrencias := TOcorrenciasGabarito.Create;
end;

destructor TItemGabarito.Destroy;
begin
  FreeAndNil(FOcorrencias);
  inherited;
end;

function TItemGabarito.GetLoteria: TTipoLoteria;
begin
  if Assigned(FLoteria) then
    Result := FLoteria
  else
    if Collection is TGabarito then
      Result := TGabarito(Collection).Loteria
    else
      Result := nil;
end;

function TItemGabarito.ValidarContagem(
  const AContagem: PArrayContagemOcorrencia): Boolean;
var
  nOcorrencias: Integer;
  I: Integer;
  J: Integer;
  B: Boolean;
begin
  if Assigned(AContagem) then
  begin
    nOcorrencias := 0;
    for I := 0 to Pred(FOcorrencias.Count) do
    begin
      J := 0;
      B := False;
      while (J < Length(AContagem^)) and not B do
      begin
        if (AContagem^[J].Dezenas = FOcorrencias[I].Dezenas) and
          not AContagem^[J].Valida then
        begin
          Inc(nOcorrencias);
          AContagem^[J].Valida := True;
          B := True;
        end
        else
          Inc(J);
      end;
    end;

    Result := nOcorrencias = FOcorrencias.Count;
  end
  else
    Result := False;
end;

function TItemGabarito.GetKey: string;
begin
  Result := inherited GetKey;
end;

procedure TItemGabarito.SetKey(const AValue: string);
begin
  inherited;
end;

procedure TItemGabarito.ChecarLoteria;
begin
  if not Assigned(Loteria) then
    raise ELoteria.Create('Loteria n�o informada para o item de gabarito.');
end;

procedure TItemGabarito.Assign(Source: TPersistent);
var
  lItem: TItemGabarito;
begin
  inherited;
  if Source is TItemGabarito then
  begin
    lItem := TItemGabarito(Source);
    Nome := lItem.Nome;
    Linha := lItem.Linha;
    Coluna := lItem.Coluna;
    Sequencia := lItem.Sequencia;
    Preferencial := lItem.Preferencial;
    Ocorrencias.Assign(lItem.Ocorrencias);
  end;
end;

function TItemGabarito.ValidarLinha(const ADezenas: TDezenas): Boolean;
var
  lContagem: TArrayContagemOcorrencia;
  sDezena: string;
  nLinha: Integer;
  I: Integer;
begin
  ChecarLoteria;
  SetLength(lContagem, Loteria.LinhasTabela);
  for I := 0 to Pred(ADezenas.Count) do
  begin
    sDezena := IntToStr(ADezenas[I].Dezena);
    if Length(sDezena) = 1 then
      sDezena := '0' + sDezena;
    nLinha := StrToInt(sDezena[1]);
    if sDezena[2] <> '0' then
      Inc(nLinha);
    Inc(lContagem[Pred(nLinha)].Dezenas);
  end;

  Result := ValidarContagem(@lContagem);
end;

function TItemGabarito.ValidarColuna(const ADezenas: TDezenas): Boolean;
var
  lContagem: TArrayContagemOcorrencia;
  sDezena: string;
  nColuna: Integer;
  I: Integer;
begin
  ChecarLoteria;
  SetLength(lContagem, Loteria.ColunasTabela);
  for I := 0 to Pred(ADezenas.Count) do
  begin
    sDezena := IntToStr(ADezenas[I].Dezena);
    if Length(sDezena) = 1 then
      sDezena := '0' + sDezena;
    if sDezena[2] <> '0' then
      nColuna := StrToInt(sDezena[2])
    else
      nColuna := Loteria.ColunasTabela;
    Inc(lContagem[Pred(nColuna)].Dezenas);
  end;

  Result := ValidarContagem(@lContagem);
end;

function TItemGabarito.ValidarSequencia(const ADezenas: TDezenas): Boolean;
var
  lContagem: TArrayContagemOcorrencia;
  nDezenas: Integer;
  I: Integer;
begin
  ChecarLoteria;
  nDezenas := 1;
  for I := 0 to Pred(Pred(ADezenas.Count)) do
    if ADezenas[I + 1].Dezena = ADezenas[I].Dezena + 1 then
      Inc(nDezenas)
    else
    begin
      SetLength(lContagem, Length(lContagem) + 1);
      lContagem[High(lContagem)].Dezenas := nDezenas;
      nDezenas := 1;
    end;

  SetLength(lContagem, Length(lContagem) + 1);
  lContagem[High(lContagem)].Dezenas := nDezenas;
  Result := ValidarContagem(@lContagem);
end;

{ TGabarito }

constructor TGabarito.Create;
begin
  inherited;
  HashSize := 32;
end;

function TGabarito.GetItem(const AIndex: Integer): TItemGabarito;
begin
  Result := TItemGabarito(inherited Items[AIndex]);
end;

function TGabarito.GetItemClass: TCollectionItemClass;
begin
  Result := TItemGabarito;
end;

function TGabarito.Add: TItemGabarito;
begin
  Result := TItemGabarito(inherited Add);
end;

function TGabarito.Insert(const AIndex: Integer): TItemGabarito;
begin
  Result := TItemGabarito(inherited Insert(AIndex));
end;

function TGabarito.Find(const ANome: string): TItemGabarito;
begin
  Result := TItemGabarito(inherited Find(ANome));
end;

function TGabarito.Contains(const ANome: string): Boolean;
begin
  Result := inherited Contains(ANome);
end;

function TGabarito.Remove(const ANome: string): Boolean;
begin
  Result := inherited Remove(ANome);
end;

function TGabarito.LocalizarPorLinha(const ADezenas: TDezenas): TItemGabarito;
var
  I: Integer;
begin
  Result := nil;
  I := 0;
  while (I < Count) and not Assigned(Result) do
    if Items[I].Linha and Items[I].ValidarLinha(ADezenas) then
      Result := Items[I]
    else
      Inc(I);
end;

function TGabarito.LocalizarPorColuna(const ADezenas: TDezenas): TItemGabarito;
var
  I: Integer;
begin
  Result := nil;
  I := 0;
  while (I < Count) and not Assigned(Result) do
    if Items[I].Coluna and Items[I].ValidarColuna(ADezenas) then
      Result := Items[I]
    else
      Inc(I);
end;

function TGabarito.LocalizarPorSequencia(
  const ADezenas: TDezenas): TItemGabarito;
var
  I: Integer;
begin
  Result := nil;
  I := 0;
  while (I < Count) and not Assigned(Result) do
    if Items[I].Sequencia and Items[I].ValidarSequencia(ADezenas) then
      Result := Items[I]
    else
      Inc(I);
end;

{ TSorteio }

procedure TSorteio.Assign(Source: TPersistent);
begin
  if Source is TSorteio then
    Dezenas.Assign(TSorteio(Source).Dezenas);
end;

constructor TSorteio.Create(Collection: TCollection);
begin
  inherited;
  FDezenas := TDezenas.Create;
  FDezenas.HashSize := 16;
end;

destructor TSorteio.Destroy;
begin
  FreeAndNil(FDezenas);
  inherited;
end;

procedure TSorteio.Ordenar;
begin
  FDezenas.Ordenar;
end;

{ TSorteios }

function TSorteios.GetItem(const AIndex: Integer): TSorteio;
begin
  Result := TSorteio(inherited Items[AIndex]);
end;

function TSorteios.GetItemClass: TCollectionItemClass;
begin
  Result := TSorteio;
end;

function TSorteios.Add: TSorteio;
begin
  Result := TSorteio(inherited Add);
end;

function TSorteios.Insert(const AIndex: Integer): TSorteio;
begin
  Result := TSorteio(inherited Insert(AIndex));
end;

function TSorteios.Localizar(const ADezenas: TDezenas): TSorteio;
var
  I: Integer;
begin
  Result := nil;
  I := 0;
  while (I < Count) and not Assigned(Result) do
    if Items[I].Dezenas.ContemDezenas(ADezenas) then
      Result := Items[I]
    else
      Inc(I);
end;

function TSorteios.Copiar(const AInicio, ATermino: Integer): TSorteios;
var
  I: Integer;
begin
  if (Count > 0) and (AInicio >= 0) and (AInicio <= ATermino)
    and (ATermino < Count) then
  begin
    Result := TSorteios(TRLCollectionClass(ClassType).Create);
    try
      for I := AInicio to ATermino do
        Result.Add.Assign(Items[I]);
    except
      FreeAndNil(Result);
      raise;
    end;
  end
  else
    Result := nil;
end;

procedure TSorteios.Ordenar;
var
  I: Integer;
begin
  for I := 0 to Pred(Count) do
    Items[I].Ordenar;
end;

{ TItemHistorico }

function TItemHistorico.GetLoteria: TTipoLoteria;
begin
  if Assigned(FLoteria) then
    Result := FLoteria
  else
    if Collection is THistorico then
      Result := THistorico(Collection).Loteria
    else
      Result := nil;
end;

procedure TItemHistorico.ChecarLoteria;
begin
  if not Assigned(Loteria) then
    raise ELoteria.Create('Loteria n�o informada para o item de hist�rico.');
end;

procedure TItemHistorico.Assign(Source: TPersistent);
var
  lItem: TItemHistorico;
begin
  inherited;
  if Source is TItemHistorico then
  begin
    lItem := TItemHistorico(Source);
    Concurso := lItem.Concurso;
    Data := lItem.Data;
    Pares := lItem.Pares;
    Primos := lItem.Primos;
    Intervalo := lItem.Intervalo;
    Linha := lItem.Linha;
    Coluna := lItem.Coluna;
    Sequencia := lItem.Sequencia;
  end;
end;

procedure TItemHistorico.CalcularEstatisticas;
var
  lGabarito: TItemGabarito;
begin
  ChecarLoteria;
  FPares := Dezenas.ContarPares;
  FPrimos := Dezenas.ContarPrimos;
  FIntervalo := Dezenas.LerIntervalo;
  lGabarito := Loteria.Gabarito.LocalizarPorLinha(Dezenas);
  if Assigned(lGabarito) then
    FLinha := lGabarito.Nome
  else
    FLinha := '';
  lGabarito := Loteria.Gabarito.LocalizarPorColuna(Dezenas);
  if Assigned(lGabarito) then
    FColuna := lGabarito.Nome
  else
    FColuna := '';
  lGabarito := Loteria.Gabarito.LocalizarPorSequencia(Dezenas);
  if Assigned(lGabarito) then
    FSequencia := lGabarito.Nome
  else
    FSequencia := '';
end;

{ THistorico }

function THistorico.GetItem(const AIndex: Integer): TItemHistorico;
begin
  Result := TItemHistorico(inherited Items[AIndex]);
end;

function THistorico.GetItemClass: TCollectionItemClass;
begin
  Result := TItemHistorico;
end;

function THistorico.Add: TItemHistorico;
begin
  Result := TItemHistorico(inherited Add);
end;

function THistorico.Insert(const AIndex: Integer): TItemHistorico;
begin
  Result := TItemHistorico(inherited Insert(AIndex));
end;

function THistorico.Localizar(const ADezenas: TDezenas): TItemHistorico;
begin
  Result := TItemHistorico(inherited Localizar(ADezenas));
end;

function THistorico.Copiar(const AInicio, ATermino: Integer): THistorico;
begin
  Result := THistorico(inherited Copiar(AInicio, ATermino));
end;

procedure THistorico.LerXML(const ANomeArquivo: string);
begin
  TRLRTTIContext.Instance.FromFile(Self, ANomeArquivo, sfXML);
end;

procedure THistorico.GravarXML(const ANomeArquivo: string);
begin
  TRLRTTIContext.Instance.ToFile(Self, ANomeArquivo, sfXML);
end;

procedure THistorico.ImportarCSV(const ANomeArquivo: string);
var
  lArquivo: TStrings;
  lLinha: TStrings;
  lItem: TItemHistorico;
  I: Integer;
  J: Integer;
begin
  lArquivo := TRLStringList.Create;
  try
    lArquivo.LoadFromFile(ANomeArquivo);
    lLinha := TRLStringList.Create;
    try
      BeginUpdate;
      try
        Clear;
        for I := 0 to Pred(lArquivo.Count) do
        begin
          lLinha.DelimitedText := lArquivo[I];
          if lLinha.Count > 1 then
          begin
            lItem := Add;
            lItem.Concurso := StrToIntDef(lLinha[0], 0);
            lItem.Data := StrToDateDef(lLinha[1], 0);
            for J := 2 to Pred(lLinha.Count) do
              lItem.Dezenas.Add.Dezena := StrToIntDef(lLinha[J], 0);
          end;
        end;
      finally
        EndUpdate;
      end;
    finally
      FreeAndNil(lLinha);
    end;
  finally
    FreeAndNil(lArquivo);
  end;

  Ordenar;
end;

procedure THistorico.ExportarCSV(const ANomeArquivo: string);
var
  lArquivo: TStrings;
  lLinha: TStrings;
  I: Integer;
  J: Integer;
begin
  lArquivo := TRLStringList.Create;
  try
    lLinha := TRLStringList.Create;
    try
      for I := 0 to Pred(Count) do
      begin
        lLinha.Clear;
        lLinha.Add(IntToStr(Items[I].Concurso));
        lLinha.Add(DateToStr(Items[I].Data));
        for J := 0 to Pred(Items[I].Dezenas.Count) do
          lLinha.Add(IntToStr(Items[I].Dezenas[J].Dezena));
        lArquivo.Add(lLInha.DelimitedText);
      end;
    finally
      FreeAndNil(lLinha);
    end;

    lArquivo.SaveToFile(ANomeArquivo);
  finally
    FreeAndNil(lArquivo);
  end;
end;

procedure THistorico.AtualizarEstatisticas;
var
  I: Integer;
begin
  for I := 0 to Pred(Count) do
    Items[I].CalcularEstatisticas;
end;

function THistorico.CriarDataSet: TDataSet;
var
  lDataSet: TClientDataSet;
  lField: TField;
  I: Integer;
  J: Integer;
begin
  if Count > 0 then
  begin
    lDataSet := TClientDataSet.Create(nil);
    try
      lDataSet.FieldDefs.Add('Concurso', ftInteger);
      lDataSet.FieldDefs.Add('Data', ftDate);
      for I := 0 to Pred(Items[0].Dezenas.Count) do
        lDataSet.FieldDefs.Add('D' + IntToStr(I + 1), ftInteger);
      lDataSet.FieldDefs.Add('Pares', ftInteger);
      lDataSet.FieldDefs.Add('Primos', ftInteger);
      lDataSet.FieldDefs.Add('Intervalo', ftInteger);
      if Loteria.Gabarito.Count > 0 then
      begin
        lDataSet.FieldDefs.Add('Linha', ftString, 10);
        lDataSet.FieldDefs.Add('Coluna', ftString, 10);
        lDataSet.FieldDefs.Add('Sequ�ncia', ftString, 10);
      end;

      lDataSet.CreateDataSet;
      lDataSet.LogChanges := False;
      lDataSet.DisableControls;
      try
        for I := 0 to Pred(Count) do
        begin
          lDataSet.Append;
          lDataSet.FieldByName('Concurso').AsInteger := Items[I].Concurso;
          lDataSet.FieldByName('Data').AsDateTime := Items[I].Data;
          for J := 0 to Pred(Items[I].Dezenas.Count) do
          begin
            lField := lDataSet.FindField('D' + IntToStr(J + 1));
            if Assigned(lField) then
              lField.AsInteger := Items[I].Dezenas[J].Dezena;
          end;

          lDataSet.FieldByName('Pares').AsInteger := Items[I].Pares;
          lDataSet.FieldByName('Primos').AsInteger := Items[I].Primos;
          lDataSet.FieldByName('Intervalo').AsInteger := Items[I].Intervalo;
          if Loteria.Gabarito.Count > 0 then
          begin
            lDataSet.FieldByName('Linha').AsString := Items[I].Linha;
            lDataSet.FieldByName('Coluna').AsString := Items[I].Coluna;
            lDataSet.FieldByName('Sequ�ncia').AsString := Items[I].Sequencia;
          end;

          lDataSet.Post;
        end;

        lDataSet.First;
      finally
        lDataSet.EnableControls;
      end;

      Result := lDataSet;
    except
      FreeAndNil(lDataSet);
      raise;
    end;
  end
  else
    Result := nil;
end;

{ TLinhaTabelaFrequencia }

constructor TLinhaTabelaFrequencia.Create(Collection: TCollection);
begin
  inherited;
  FDezenas := TDezenas.Create;
  FDezenas.HashSize := 32;
end;

destructor TLinhaTabelaFrequencia.Destroy;
begin
  FreeAndNil(FDezenas);
  inherited;
end;

function TLinhaTabelaFrequencia.GetFrequencia: Integer;
begin
  Result := StrToInt(GetKey);
end;

procedure TLinhaTabelaFrequencia.SetFrequencia(const AValor: Integer);
begin
  SetKey(IntToStr(AValor));
end;

procedure TLinhaTabelaFrequencia.Assign(Source: TPersistent);
var
  lLinha: TLinhaTabelaFrequencia;
begin
  inherited;
  if Source is TLinhaTabelaFrequencia then
  begin
    lLinha := TLinhaTabelaFrequencia(Source);
    Frequencia := lLinha.Frequencia;
    Dezenas.Assign(lLinha.Dezenas);
  end;
end;

{ TTabelaFrequencia }

constructor TTabelaFrequencia.Create;
begin
  inherited;
  HashSize := 4096;
end;

function TTabelaFrequencia.GetItem(
  const AIndex: Integer): TLinhaTabelaFrequencia;
begin
  Result := TLinhaTabelaFrequencia(inherited Items[AIndex]);
end;

function TTabelaFrequencia.GetItemClass: TCollectionItemClass;
begin
  Result := TLinhaTabelaFrequencia;
end;

function TTabelaFrequencia.Add: TLinhaTabelaFrequencia;
begin
  Result := TLinhaTabelaFrequencia(inherited Add);
end;

function TTabelaFrequencia.Insert(
  const AIndex: Integer): TLinhaTabelaFrequencia;
begin
  Result := TLinhaTabelaFrequencia(inherited Insert(AIndex));
end;

function TTabelaFrequencia.Find(
  const AFrequencia: Integer): TLinhaTabelaFrequencia;
begin
  Result := TLinhaTabelaFrequencia(inherited Find(IntToStr(AFrequencia)));
end;

function TTabelaFrequencia.Contains(const AFrequencia: Integer): Boolean;
begin
  Result := inherited Contains(IntToStr(AFrequencia));
end;

function TTabelaFrequencia.Remove(const AFrequencia: Integer): Boolean;
begin
  Result := inherited Remove(IntToStr(AFrequencia));
end;

{ TLinhaTabelaQuociente }

function TLinhaTabelaQuociente.GetConcurso: Integer;
begin
  Result := StrToInt(GetKey);
end;

procedure TLinhaTabelaQuociente.SetConcurso(const AValor: Integer);
begin
  SetKey(IntToStr(AValor));
end;

procedure TLinhaTabelaQuociente.Assign(Source: TPersistent);
var
  lLinha: TLinhaTabelaQuociente;
begin
  inherited;
  if Source is TLinhaTabelaQuociente then
  begin
    lLinha := TLinhaTabelaQuociente(Source);
    FrequenciaMedia := lLinha.FrequenciaMedia;
    FrequenciaMediaDezenas := lLinha.FrequenciaMediaDezenas;
    DesvioPadrao := lLinha.DesvioPadrao;
    Quociente := lLinha.Quociente;
  end;
end;

{ TTabelaQuociente }

constructor TTabelaQuociente.Create;
begin
  inherited;
  HashSize := 4096;
end;

function TTabelaQuociente.GetItem(const AIndex: Integer): TLinhaTabelaQuociente;
begin
  Result := TLinhaTabelaQuociente(inherited Items[AIndex]);
end;

function TTabelaQuociente.GetItemClass: TCollectionItemClass;
begin
  Result := TLinhaTabelaQuociente;
end;

function TTabelaQuociente.Add: TLinhaTabelaQuociente;
begin
  Result := TLinhaTabelaQuociente(inherited Add);
end;

function TTabelaQuociente.Insert(const AIndex: Integer): TLinhaTabelaQuociente;
begin
  Result := TLinhaTabelaQuociente(inherited Insert(AIndex));
end;

function TTabelaQuociente.Find(const AConcurso: Integer): TLinhaTabelaQuociente;
begin
  Result := TLinhaTabelaQuociente(inherited Find(IntToStr(AConcurso)));
end;

function TTabelaQuociente.Contains(const AConcurso: Integer): Boolean;
begin
  Result := inherited Contains(IntToStr(AConcurso));
end;

function TTabelaQuociente.Remove(const AConcurso: Integer): Boolean;
begin
  Result := inherited Remove(IntToStr(AConcurso));
end;

{ TLinhaTabelaAtraso }

constructor TLinhaTabelaAtraso.Create(Collection: TCollection);
begin
  inherited;
  FDezenas := TDezenas.Create;
  FDezenas.HashSize := 32;
end;

destructor TLinhaTabelaAtraso.Destroy;
begin
  FreeAndNil(FDezenas);
  inherited;
end;

function TLinhaTabelaAtraso.GetAtraso: Integer;
begin
  Result := StrToInt(GetKey);
end;

procedure TLinhaTabelaAtraso.SetAtraso(const AValor: Integer);
begin
  SetKey(IntToStr(AValor));
end;

procedure TLinhaTabelaAtraso.Assign(Source: TPersistent);
var
  lLinha: TLinhaTabelaAtraso;
begin
  inherited;
  if Source is TLinhaTabelaAtraso then
  begin
    lLinha := TLinhaTabelaAtraso(Source);
    Atraso := lLinha.Atraso;
    Dezenas.Assign(lLinha.Dezenas);
  end;
end;

procedure TLinhaTabelaAtraso.Ordenar;
begin
  FDezenas.Ordenar;
end;

{ TTabelaAtraso }

constructor TTabelaAtraso.Create;
begin
  inherited;
  HashSize := 4096;
end;

function TTabelaAtraso.GetItem(const AIndex: Integer): TLinhaTabelaAtraso;
begin
  Result := TLinhaTabelaAtraso(inherited Items[AIndex]);
end;

function TTabelaAtraso.GetItemClass: TCollectionItemClass;
begin
  Result := TLinhaTabelaAtraso;
end;

function TTabelaAtraso.Add: TLinhaTabelaAtraso;
begin
  Result := TLinhaTabelaAtraso(inherited Add);
end;

function TTabelaAtraso.Insert(const AIndex: Integer): TLinhaTabelaAtraso;
begin
  Result := TLinhaTabelaAtraso(inherited Insert(AIndex));
end;

function TTabelaAtraso.Find(const AAtraso: Integer): TLinhaTabelaAtraso;
begin
  Result := TLinhaTabelaAtraso(inherited Find(IntToStr(AAtraso)));
end;

function TTabelaAtraso.Contains(const AAtraso: Integer): Boolean;
begin
  Result := inherited Contains(IntToStr(AAtraso));
end;

function TTabelaAtraso.Remove(const AAtraso: Integer): Boolean;
begin
  Result := inherited Remove(IntToStr(AAtraso));
end;

procedure TTabelaAtraso.Ordenar;
var
  I: Integer;
begin
  for I := 0 to Pred(Count) do
    Items[I].Ordenar;
end;

{ TLinhaTabelaAtrasoMedio }

function TLinhaTabelaAtrasoMedio.GetConcurso: Integer;
begin
  Result := StrToInt(GetKey);
end;

procedure TLinhaTabelaAtrasoMedio.SetConcurso(const AValor: Integer);
begin
  SetKey(IntToStr(AValor));
end;

procedure TLinhaTabelaAtrasoMedio.Assign(Source: TPersistent);
var
  lLinha: TLinhaTabelaAtrasoMedio;
begin
  inherited;
  if Source is TLinhaTabelaAtrasoMedio then
  begin
    lLinha := TLinhaTabelaAtrasoMedio(Source);
    AtrasoEsperado := lLinha.AtrasoEsperado;
    AtrasoMedio := lLinha.AtrasoMedio;
    AtrasoMedioDezenas := lLinha.AtrasoMedioDezenas;
    Quociente := lLinha.Quociente;
  end;
end;

{ TTabelaAtrasoMedio }

constructor TTabelaAtrasoMedio.Create;
begin
  inherited;
  HashSize := 4096;
end;

function TTabelaAtrasoMedio.GetItem(
  const AIndex: Integer): TLinhaTabelaAtrasoMedio;
begin
  Result := TLinhaTabelaAtrasoMedio(inherited Items[AIndex]);
end;

function TTabelaAtrasoMedio.GetItemClass: TCollectionItemClass;
begin
  Result := TLinhaTabelaAtrasoMedio;
end;

function TTabelaAtrasoMedio.Add: TLinhaTabelaAtrasoMedio;
begin
  Result := TLinhaTabelaAtrasoMedio(inherited Add);
end;

function TTabelaAtrasoMedio.Insert(
  const AIndex: Integer): TLinhaTabelaAtrasoMedio;
begin
  Result := TLinhaTabelaAtrasoMedio(inherited Insert(AIndex));
end;

function TTabelaAtrasoMedio.Find(
  const AConcurso: Integer): TLinhaTabelaAtrasoMedio;
begin
  Result := TLinhaTabelaAtrasoMedio(inherited Find(IntToStr(AConcurso)));
end;

function TTabelaAtrasoMedio.Contains(const AConcurso: Integer): Boolean;
begin
  Result := inherited Contains(IntToStr(AConcurso));
end;

function TTabelaAtrasoMedio.Remove(const AConcurso: Integer): Boolean;
begin
  Result := inherited Remove(IntToStr(AConcurso));
end;

{ TEstatistica }

constructor TEstatistica.Create(Collection: TCollection);
begin
  inherited;
//
end;

function TEstatistica.GetLoteria: TTipoLoteria;
begin
  if Assigned(FLoteria) then
    Result := FLoteria
  else
    if Collection is TEstatisticas then
      Result := TEstatisticas(Collection).Loteria
    else
      Result := nil;
end;

function TEstatistica.GetSorteios: TSorteios;
begin
  if Assigned(FSorteios) then
    Result := FSorteios
  else
    if Collection is TEstatisticas then
      Result := TEstatisticas(Collection).Sorteios
    else
      Result := nil;
end;

function TEstatistica.GetKey: string;
begin
  Result := inherited GetKey;
end;

procedure TEstatistica.ChecarLoteria;
begin
  if not Assigned(Loteria) then
    raise ELoteria.Create('Loteria n�o informada para a estat�stica.');
end;

procedure TEstatistica.ChecarSorteios;
begin
  if not Assigned(Sorteios) then
    raise ELoteria.Create(
      'Lista de sorteios n�o informada para a estat�stica.');
  if Sorteios.Count = 0 then
    raise ELoteria.Create('Lista de sorteios vazia.');
end;

function TEstatistica.Calcular(const ARecalcular: Boolean): Boolean;
begin
  Result := not FCalculada or ARecalcular;
  if Result then
  begin
    CalcularEstatistica;
    FCalculada := True;
  end;
end;

function TEstatistica.CalcularEmThread(const ARecalcular: Boolean): Boolean;
var
  lThread: TThreadCalculoEstatistica;
begin
  lThread := TThreadCalculoEstatistica.Create(True);
  try
    lThread.Estatistica := Self;
    lThread.Recalcular := ARecalcular;
    lThread.Start;
    while not lThread.Finished do
      Application.ProcessMessages;
    lThread.WaitFor;
    if lThread.Erro <> '' then
      raise lThread.ClasseErro.Create(lThread.Erro);
    Result := lThread.Resultado;
  finally
    FreeAndNil(lThread);
  end;
end;

function TEstatistica.Calculada: Boolean;
begin
  Result := FCalculada;
end;

procedure TEstatistica.Resetar;
begin
  FCalculada := False;
end;

procedure TEstatistica.MarcarComoCalculada;
begin
  FCalculada := True;
end;

{ TEstatisticaPorContagem }

constructor TEstatisticaPorContagem.Create(Collection: TCollection);
begin
  inherited;
  FContagem := TContagem.Create;
end;

destructor TEstatisticaPorContagem.Destroy;
begin
  FreeAndNil(FContagem);
  inherited;
end;

procedure TEstatisticaPorContagem.Assign(Source: TPersistent);
var
  lEstatistica: TEstatisticaPorContagem;
begin
  inherited;
  if Source is TEstatisticaPorContagem then
  begin
    lEstatistica := TEstatisticaPorContagem(Source);
    AplicarComparativo := lEstatistica.AplicarComparativo;
    SomenteComparativo := lEstatistica.SomenteComparativo;
    EstatisticaComparacao := lEstatistica.EstatisticaComparacao;
    Margem := lEstatistica.Margem;
    Contagem.Assign(lEstatistica.Contagem);
  end;
end;

procedure TEstatisticaPorContagem.CalcularPercentual;
begin
  FContagem.CalcularPercentual;
end;

{ TEstatisticaDezenas }

constructor TEstatisticaDezenas.Create(Collection: TCollection);
begin
  inherited;
  FDezenas := TDezenasContadas.Create;
end;

destructor TEstatisticaDezenas.Destroy;
begin
  FreeAndNil(FDezenas);
  inherited;
end;

procedure TEstatisticaDezenas.Assign(Source: TPersistent);
begin
  inherited;
  if Source is TEstatisticaDezenas then
    Dezenas.Assign(TEstatisticaDezenas(Source).Dezenas);
end;

function TEstatisticaDezenas.CriarDataSet: TDataSet;
begin
  Result := FDezenas.CriarDataSet;
end;

{ TOcorrenciaPorPares }

constructor TOcorrenciaPorPares.Create(Collection: TCollection);
begin
  inherited;
  SetKey(COcorrenciaPorPares);
end;

function TOcorrenciaPorPares.GetDescricao: string;
begin
  Result := 'Ocorr�ncia por pares';
end;

procedure TOcorrenciaPorPares.CalcularEstatistica;
var
  lItem: TItemContagem;
  I: Integer;
begin
  ChecarLoteria;
  ChecarSorteios;
  Contagem.BeginUpdate;
  try
    Contagem.Clear;
    for I := 0 to Loteria.DezenasSorteio do
      Contagem.Add.Valor := IntToStr(I);
    for I := 0 to Pred(Sorteios.Count) do
    begin
      lItem := Contagem.Find(IntToStr(Sorteios[I].Dezenas.ContarPares));
      lItem.Quantidade := lItem.Quantidade + 1;
    end;

    Contagem.CalcularPercentual;
  finally
    Contagem.EndUpdate;
  end;
end;

function TOcorrenciaPorPares.CriarDataSet: TDataSet;
begin
  Result := Contagem.CriarDataSet(True, 'Pares');
end;

{ TOcorrenciaPorPrimos }

constructor TOcorrenciaPorPrimos.Create(Collection: TCollection);
begin
  inherited;
  SetKey(COcorrenciaPorPrimos);
end;

function TOcorrenciaPorPrimos.GetDescricao: string;
begin
  Result := 'Ocorr�ncia por primos';
end;

procedure TOcorrenciaPorPrimos.CalcularEstatistica;
var
  lItem: TItemContagem;
  I: Integer;
begin
  ChecarLoteria;
  ChecarSorteios;
  Contagem.BeginUpdate;
  try
    Contagem.Clear;
    for I := 0 to Loteria.DezenasSorteio do
      Contagem.Add.Valor := IntToStr(I);
    for I := 0 to Pred(Sorteios.Count) do
    begin
      lItem := Contagem.Find(IntToStr(Sorteios[I].Dezenas.ContarPrimos));
      lItem.Quantidade := lItem.Quantidade + 1;
    end;

    Contagem.CalcularPercentual;
  finally
    Contagem.EndUpdate;
  end;
end;

function TOcorrenciaPorPrimos.CriarDataSet: TDataSet;
begin
  Result := Contagem.CriarDataSet(True, 'Primos');
end;

{ TOcorrenciaPorIntervalo }

constructor TOcorrenciaPorIntervalo.Create(Collection: TCollection);
begin
  inherited;
  SetKey(COcorrenciaPorIntervalo);
end;

function TOcorrenciaPorIntervalo.GetDescricao: string;
begin
  Result := 'Ocorr�ncia por intervalo';
end;

procedure TOcorrenciaPorIntervalo.CalcularEstatistica;
var
  lItem: TItemContagem;
  sIntervalo: string;
  I: Integer;
begin
  ChecarLoteria;
  ChecarSorteios;
  Contagem.BeginUpdate;
  try
    Contagem.Clear;
    for I := 0 to Pred(Sorteios.Count) do
    begin
      sIntervalo := IntToStr(Sorteios[I].Dezenas.LerIntervalo);
      lItem := Contagem.Find(sIntervalo);
      if not Assigned(lItem) then
      begin
        lItem := Contagem.Add;
        lItem.Valor := sIntervalo;
      end;

      lItem.Quantidade := lItem.Quantidade + 1;
    end;

    Contagem.CalcularPercentual;
  finally
    Contagem.EndUpdate;
  end;
end;

function TOcorrenciaPorIntervalo.CriarDataSet: TDataSet;
begin
  Result := Contagem.CriarDataSet(True, 'Intervalo');
end;

{ TOcorrenciaPorLinha }

constructor TOcorrenciaPorLinha.Create(Collection: TCollection);
begin
  inherited;
  SetKey(COcorrenciaPorLinha);
end;

function TOcorrenciaPorLinha.GetDescricao: string;
begin
  Result := 'Ocorr�ncia por linha';
end;

procedure TOcorrenciaPorLinha.CalcularEstatistica;
var
  lGabarito: TItemGabarito;
  lContagem: TItemContagem;
  I: Integer;
begin
  ChecarLoteria;
  ChecarSorteios;
  Contagem.BeginUpdate;
  try
    Contagem.Clear;
    for I := 0 to Pred(Loteria.Gabarito.Count) do
      if Loteria.Gabarito[I].Linha then
        Contagem.Add.Valor := Loteria.Gabarito[I].Nome;
    for I := 0 to Pred(Sorteios.Count) do
    begin
      lGabarito := Loteria.Gabarito.LocalizarPorLinha(Sorteios[I].Dezenas);
      if Assigned(lGabarito) then
      begin
        lContagem := Contagem.Find(lGabarito.Nome);
        if Assigned(lContagem) then
          lContagem.Quantidade := lContagem.Quantidade + 1;
      end;
    end;

    Contagem.CalcularPercentual;
  finally
    Contagem.EndUpdate;
  end;
end;

function TOcorrenciaPorLinha.CriarDataSet: TDataSet;
begin
  Result := Contagem.CriarDataSet(True, 'Gabarito', ftString, 5);
end;

{ TOcorrenciaPorColuna }

constructor TOcorrenciaPorColuna.Create(Collection: TCollection);
begin
  inherited;
  SetKey(COcorrenciaPorColuna);
end;

function TOcorrenciaPorColuna.GetDescricao: string;
begin
  Result := 'Ocorr�ncia por coluna';
end;

procedure TOcorrenciaPorColuna.CalcularEstatistica;
var
  lGabarito: TItemGabarito;
  lContagem: TItemContagem;
  I: Integer;
begin
  ChecarLoteria;
  ChecarSorteios;
  Contagem.BeginUpdate;
  try
    Contagem.Clear;
    for I := 0 to Pred(Loteria.Gabarito.Count) do
      if Loteria.Gabarito[I].Coluna then
        Contagem.Add.Valor := Loteria.Gabarito[I].Nome;
    for I := 0 to Pred(Sorteios.Count) do
    begin
      lGabarito := Loteria.Gabarito.LocalizarPorColuna(Sorteios[I].Dezenas);
      if Assigned(lGabarito) then
      begin
        lContagem := Contagem.Find(lGabarito.Nome);
        if Assigned(lContagem) then
          lContagem.Quantidade := lContagem.Quantidade + 1;
      end;
    end;

    Contagem.CalcularPercentual;
  finally
    Contagem.EndUpdate;
  end;
end;

function TOcorrenciaPorColuna.CriarDataSet: TDataSet;
begin
  Result := Contagem.CriarDataSet(True, 'Gabarito', ftString, 5);
end;

{ TOcorrenciaPorSequencia }

constructor TOcorrenciaPorSequencia.Create(Collection: TCollection);
begin
  inherited;
  SetKey(COcorrenciaPorSequencia);
end;

procedure TOcorrenciaPorSequencia.CalcularEstatistica;
var
  lGabarito: TItemGabarito;
  lContagem: TItemContagem;
  I: Integer;
begin
  ChecarLoteria;
  ChecarSorteios;
  Contagem.BeginUpdate;
  try
    Contagem.Clear;
    for I := 0 to Pred(Loteria.Gabarito.Count) do
      if Loteria.Gabarito[I].Sequencia then
        Contagem.Add.Valor := Loteria.Gabarito[I].Nome;
    for I := 0 to Pred(Sorteios.Count) do
    begin
      lGabarito := Loteria.Gabarito.LocalizarPorSequencia(Sorteios[I].Dezenas);
      if Assigned(lGabarito) then
      begin
        lContagem := Contagem.Find(lGabarito.Nome);
        if Assigned(lContagem) then
          lContagem.Quantidade := lContagem.Quantidade + 1;
      end;
    end;

    Contagem.CalcularPercentual;
  finally
    Contagem.EndUpdate;
  end;
end;

function TOcorrenciaPorSequencia.GetDescricao: string;
begin
  Result := 'Ocorr�ncia por sequ�ncia';
end;

function TOcorrenciaPorSequencia.CriarDataSet: TDataSet;
begin
  Result := Contagem.CriarDataSet(True, 'Gabarito', ftString, 5);
end;

{ TOcorrenciaPorQuadrante }

constructor TOcorrenciaPorQuadrante.Create(Collection: TCollection);
begin
  inherited;
  SetKey(COcorrenciaPorQuadrante);
end;

function TOcorrenciaPorQuadrante.GetDescricao: string;
begin
  Result := 'Ocorr�ncia por quadrante';
end;

procedure TOcorrenciaPorQuadrante.CalcularEstatistica;
var
  lItem: TItemContagem;
  nQuadrantes: Integer;
  I: Integer;
begin
  ChecarLoteria;
  ChecarSorteios;
  Contagem.BeginUpdate;
  try
    Contagem.Clear;
    for I := 0 to Pred(Sorteios.Count) do
    begin
      nQuadrantes := Loteria.Quadrantes.QuadrantesPreenchidos(
        Sorteios[I].Dezenas);
      lItem := Contagem.Find(IntToStr(nQuadrantes));
      if not Assigned(lItem) then
      begin
        lItem := Contagem.Add;
        lItem.Valor := IntToStr(nQuadrantes);
      end;

      lItem.Quantidade := lItem.Quantidade + 1;
    end;

    Contagem.CalcularPercentual;
  finally
    Contagem.EndUpdate;
  end;
end;

function TOcorrenciaPorQuadrante.CriarDataSet: TDataSet;
begin
  Result := Contagem.CriarDataSet(True, 'Quadrantes');
end;

{ TOcorrenciaPorDezena }

constructor TOcorrenciaPorDezena.Create(Collection: TCollection);
begin
  inherited;
  SetKey(COcorrenciaPorDezena);
end;

function TOcorrenciaPorDezena.GetDescricao: string;
begin
  Result := 'Ocorr�ncia por dezena';
end;

procedure TOcorrenciaPorDezena.CalcularEstatistica;
var
  lDezena: TDezenaContada;
  I: Integer;
  J: Integer;
begin
  ChecarLoteria;
  ChecarSorteios;
  Dezenas.BeginUpdate;
  try
    Dezenas.Clear;
    for I := 0 to Pred(Sorteios.Count) do
      for J := 0 to Pred(Sorteios[I].Dezenas.Count) do
      begin
        lDezena := Dezenas.Find(Sorteios[I].Dezenas[J].Dezena);
        if not Assigned(lDezena) then
        begin
          lDezena := Dezenas.Add;
          lDezena.Dezena := Sorteios[I].Dezenas[J].Dezena;
        end;

        lDezena.Quantidade := lDezena.Quantidade + 1;
      end;
  finally
    Dezenas.EndUpdate;
  end;
end;

{ TOcorrenciaPorRepeticaoDezenas }

constructor TOcorrenciaPorRepeticaoDezenas.Create(Collection: TCollection);
begin
  inherited;
  SetKey(COcorrenciaPorRepeticaoDezenas);
end;

function TOcorrenciaPorRepeticaoDezenas.GetDescricao: string;
begin
  Result := 'Ocorr�ncia por repeti��o de dezenas';
end;

procedure TOcorrenciaPorRepeticaoDezenas.CalcularEstatistica;
var
  lItem: TItemContagem;
  lDezenas: TDezenas;
  nInicio: Integer;
  nConferencia: Integer;
  I: Integer;
begin
  ChecarLoteria;
  ChecarSorteios;
  Contagem.BeginUpdate;
  try
    Contagem.Clear;
    if Sorteios = Loteria.Historico then
      nInicio := 1
    else
      nInicio := 0;
    for I := nInicio to Pred(Sorteios.Count) do
    begin
      if Sorteios = Loteria.Historico then
        lDezenas := Sorteios[Pred(I)].Dezenas
      else
        lDezenas := Loteria.Historico[Pred(Loteria.Historico.Count)].Dezenas;
      nConferencia := lDezenas.ConferirDezenas(Sorteios[I].Dezenas);
      lItem := Contagem.Find(IntToStr(nConferencia));
      if not Assigned(lItem) then
      begin
        lItem := Contagem.Add;
        lItem.Valor := IntToStr(nConferencia);
      end;

      lItem.Quantidade := lItem.Quantidade + 1;
    end;

    Contagem.CalcularPercentual;
  finally
    Contagem.EndUpdate;
  end;
end;

function TOcorrenciaPorRepeticaoDezenas.CriarDataSet: TDataSet;
begin
  Result := Contagem.CriarDataSet(True, 'Dezenas');
end;

{ TDezenasMaisSorteadas }

constructor TDezenasMaisSorteadas.Create(Collection: TCollection);
begin
  inherited;
  SetKey(CDezenasMaisSorteadas);
end;

function TDezenasMaisSorteadas.GetDescricao: string;
begin
  Result := 'Dezenas mais sorteadas';
end;

procedure TDezenasMaisSorteadas.CalcularEstatistica;
var
  lOcorrencia: TOcorrenciaPorDezena;
  lDezena: TDezenaContada;
  lMaiorDezena: TDezenaContada;
  I: Integer;
begin
  ChecarLoteria;
  ChecarSorteios;
  lDezena := nil;
  lOcorrencia := TOcorrenciaPorDezena.Create(nil);
  try
    lOcorrencia.Loteria := Loteria;
    lOcorrencia.Sorteios := Sorteios;
    lOcorrencia.Calcular(False);
    if lOcorrencia.Dezenas.Count = Loteria.MaiorDezena then
    begin
      Dezenas.BeginUpdate;
      try
        Dezenas.Clear;
        while Dezenas.Count < Loteria.MaiorDezena div 2 do
        begin
          I := 0;
          lMaiorDezena := lOcorrencia.Dezenas[0];
          while (I < lOcorrencia.Dezenas.Count) do
          begin
            if lOcorrencia.Dezenas[I].Quantidade > lMaiorDezena.Quantidade then
              lMaiorDezena := lOcorrencia.Dezenas[I];
            Inc(I);
          end;

          lDezena := Dezenas.Add;
          lDezena.Dezena := lMaiorDezena.Dezena;
          lDezena.Quantidade := lMaiorDezena.Quantidade;
          lOcorrencia.Dezenas.Remove(lMaiorDezena.Dezena);
        end;

        lMaiorDezena := lDezena;
        for I := 0 to Pred(lOcorrencia.Dezenas.Count) do
        begin
          if lOcorrencia.Dezenas[I].Quantidade = lMaiorDezena.Quantidade then
          begin
            lDezena := Dezenas.Add;
            lDezena.Dezena := lOcorrencia.Dezenas[I].Dezena;
            lDezena.Quantidade := lOcorrencia.Dezenas[I].Quantidade;
          end;
        end;
      finally
        Dezenas.EndUpdate;
      end;
    end;
  finally
    FreeAndNil(lOcorrencia);
  end;
end;

{ TDezenasMenosSorteadas }

constructor TDezenasMenosSorteadas.Create(Collection: TCollection);
begin
  inherited;
  SetKey(CDezenasMenosSorteadas);
end;

function TDezenasMenosSorteadas.GetDescricao: string;
begin
  Result := 'Dezenas menos sorteadas';
end;

procedure TDezenasMenosSorteadas.CalcularEstatistica;
var
  lOcorrencia: TOcorrenciaPorDezena;
  lDezenasMaisSorteadas: TDezenasMaisSorteadas;
  lDezena: TDezenaContada;
  I: Integer;
begin
  ChecarLoteria;
  ChecarSorteios;
  lOcorrencia := TOcorrenciaPorDezena.Create(nil);
  try
    lOcorrencia.Loteria := Loteria;
    lOcorrencia.Sorteios := Sorteios;
    lOcorrencia.Calcular(False);
    if lOcorrencia.Dezenas.Count = Loteria.MaiorDezena then
    begin
      lDezenasMaisSorteadas := TDezenasMaisSorteadas.Create(nil);
      try
        lDezenasMaisSorteadas.Loteria := Loteria;
        lDezenasMaisSorteadas.Sorteios := Sorteios;
        lDezenasMaisSorteadas.Calcular(False);
        Dezenas.BeginUpdate;
        try
          Dezenas.Clear;
          for I := 0 to Pred(lOcorrencia.Dezenas.Count) do
            if not lDezenasMaisSorteadas.Dezenas.Contains(
              lOcorrencia.Dezenas[I].Dezena) then
            begin
              lDezena := Dezenas.Add;
              lDezena.Dezena := lOcorrencia.Dezenas[I].Dezena;
              lDezena.Quantidade := lOcorrencia.Dezenas[I].Quantidade;
            end;
        finally
          Dezenas.EndUpdate;
        end;
      finally
        FreeAndNil(lDezenasMaisSorteadas);
      end;
    end;
  finally
    FreeAndNil(lOcorrencia);
  end;
end;


{ TFrequenciaDezenas }

constructor TFrequenciaDezenas.Create(Collection: TCollection);
begin
  inherited;
  SetKey(CFrequenciaDezenas);
  FFrequencia := TDezenasContadas.Create;
  FFrequencia.HashSize := 64;
  FTabela := TTabelaFrequencia.Create;
  FDezenasDentroEsperado := TDezenas.Create;
  FDezenasDentroEsperado.HashSize := 64;
  FDezenasAbaixoEsperado := TDezenas.Create;
  FDezenasAbaixoEsperado.HashSize := 64;
  FDezenasAcimaEsperado := TDezenas.Create;
  FDezenasAcimaEsperado.HashSize := 64;
end;

destructor TFrequenciaDezenas.Destroy;
begin
  FreeAndNil(FDezenasAcimaEsperado);
  FreeAndNil(FDezenasAbaixoEsperado);
  FreeAndNil(FDezenasDentroEsperado);
  FreeAndNil(FFrequencia);
  FreeAndNil(FTabela);
  inherited;
end;

function TFrequenciaDezenas.GetDescricao: string;
begin
  Result := 'Frequ�ncia de dezenas';
end;

procedure TFrequenciaDezenas.Assign(Source: TPersistent);
var
  lEstatistica: TFrequenciaDezenas;
begin
  inherited;
  if Source is TFrequenciaDezenas then
  begin
    lEstatistica := TFrequenciaDezenas(Source);
    Frequencia.Assign(lEstatistica.Frequencia);
    Tabela.Assign(lEstatistica.Tabela);
    Media := lEstatistica.Media;
    DesvioPadrao := lEstatistica.DesvioPadrao;
    FrequenciaMinima := lEstatistica.FrequenciaMinima;
    FrequenciaMaxima := lEstatistica.FrequenciaMaxima;
    DezenasDentroEsperado.Assign(lEstatistica.DezenasDentroEsperado);
    DezenasAbaixoEsperado.Assign(lEstatistica.DezenasAbaixoEsperado);
    DezenasAcimaEsperado.Assign(DezenasAcimaEsperado);
    PercentualDentroEsperado := lEstatistica.PercentualDentroEsperado;
    PercentualAbaixoEsperado := lEstatistica.PercentualAbaixoEsperado;
    PercentualAcimaEsperado := lEstatistica.PercentualAcimaEsperado;
  end;
end;

procedure TFrequenciaDezenas.CalcularEstatistica;
var
  lDezena: TDezenaContada;
  nDiferenca: Double;
  nTotal: Double;
  I: Integer;
  J: Integer;
begin
//Calcula a frequ�ncia m�dia e o desvio padr�o de um conjunto de sorteios
  ChecarLoteria;
  ChecarSorteios;
  FFrequencia.BeginUpdate;
  try
    FFrequencia.Clear;
    FTabela.BeginUpdate;
    try
      FTabela.Clear;
      for I := 1 to Sorteios.Count do
        FTabela.Add.Frequencia := I;
      for I := 0 to Pred(Sorteios.Count) do
        for J := 0 to Pred(Sorteios[I].Dezenas.Count) do
        begin
          lDezena := FFrequencia.Find(Sorteios[I].Dezenas[J].Dezena);
          if not Assigned(lDezena) then
          begin
            lDezena := FFrequencia.Add;
            lDezena.Dezena := Sorteios[I].Dezenas[J].Dezena;
          end;

          if lDezena.Quantidade > 0 then
            FTabela.Find(lDezena.Quantidade).Dezenas.Remove(
              Sorteios[I].Dezenas[J].Dezena);
          lDezena.Quantidade := lDezena.Quantidade + 1;
          FTabela.Find(lDezena.Quantidade).Dezenas.Add.Dezena :=
            Sorteios[I].Dezenas[J].Dezena;
        end;

      while FTabela[0].Dezenas.Count = 0 do
        FTabela.Delete(0);
      while FTabela[Pred(FTabela.Count)].Dezenas.Count = 0 do
        FTabela.Delete(Pred(FTabela.Count));
    finally
      FTabela.EndUpdate;
    end;
  finally
    FFrequencia.EndUpdate;
  end;

  FMedia := SimpleRoundTo(
    (Sorteios.Count * Loteria.DezenasSorteio) / Loteria.MaiorDezena);
  nTotal := 0;
  for I := 0 to Pred(FTabela.Count) do
    for J := 0 to Pred(FTabela[I].Dezenas.Count) do
    begin
      nDiferenca := FTabela[I].Frequencia - FMedia;
      nTotal := nTotal + SimpleRoundTo(nDiferenca * nDiferenca);
    end;

  FDesvioPadrao := SimpleRoundTo(
    Sqrt(SimpleRoundTo(nTotal / Loteria.MaiorDezena)));
  FFrequenciaMinima := SimpleRoundTo(FMedia - FDesvioPadrao, 0);
  FFrequenciaMaxima := SimpleRoundTo(FMedia + FDesvioPadrao, 0);
  FDezenasDentroEsperado.BeginUpdate;
  FDezenasAbaixoEsperado.BeginUpdate;
  FDezenasAcimaEsperado.BeginUpdate;
  try
    FDezenasDentroEsperado.Clear;
    FDezenasAbaixoEsperado.Clear;
    FDezenasAcimaEsperado.Clear;
    for I := 0 to Pred(FTabela.Count) do
      for J := 0 to Pred(FTabela[I].Dezenas.Count) do
      begin
        if (CompareValue(FTabela[I].Frequencia, FFrequenciaMinima) <> -1) and
          (CompareValue(FTabela[I].Frequencia, FFrequenciaMaxima) <> 1) then
          FDezenasDentroEsperado.Add.Dezena := FTabela[I].Dezenas[J].Dezena;
        if CompareValue(FTabela[I].Frequencia, FFrequenciaMinima) = -1 then
          FDezenasAbaixoEsperado.Add.Dezena := FTabela[I].Dezenas[J].Dezena;
        if CompareValue(FTabela[I].Frequencia, FFrequenciaMaxima) = 1 then
          FDezenasAcimaEsperado.Add.Dezena := FTabela[I].Dezenas[J].Dezena;
      end;
  finally
    FDezenasAcimaEsperado.EndUpdate;
    FDezenasAbaixoEsperado.EndUpdate;
    FDezenasDentroEsperado.EndUpdate;
  end;

  FDezenasDentroEsperado.Ordenar;
  FDezenasAbaixoEsperado.Ordenar;
  FDezenasAcimaEsperado.Ordenar;
  FPercentualDentroEsperado := SimpleRoundTo(
    (FDezenasDentroEsperado.Count * 100) / Loteria.FMaiorDezena);
  FPercentualAbaixoEsperado := SimpleRoundTo(
    (FDezenasAbaixoEsperado.Count * 100) / Loteria.FMaiorDezena);
  FPercentualAcimaEsperado := SimpleRoundTo(
    (FDezenasAcimaEsperado.Count * 100) / Loteria.FMaiorDezena);
end;

function TFrequenciaDezenas.ObterFrequencia(const ADezenas: TDezenas;
  out ADentroEsperado, AAbaixoEsperado, AAcimaEsperado: Integer): string;
begin
  if Assigned(ADezenas) then
  begin
    ADentroEsperado := FDezenasDentroEsperado.ConferirDezenas(ADezenas);
    AAbaixoEsperado := FDezenasAbaixoEsperado.ConferirDezenas(ADezenas);
    AAcimaEsperado := FDezenasAcimaEsperado.ConferirDezenas(ADezenas);
  end
  else
  begin
    ADentroEsperado := 0;
    AAbaixoEsperado := 0;
    AAcimaEsperado := 0;
  end;
end;

function TFrequenciaDezenas.ObterStringFrequencia(
  const ADezenas: TDezenas): string;
var
  nDentroEsperado: Integer;
  nAbaixoEsperado: Integer;
  nAcimaEsperado: Integer;
begin
  ChecarLoteria;
  ObterFrequencia(ADezenas, nDentroEsperado, nAbaixoEsperado, nAcimaEsperado);
  Result := Loteria.FormatarStringOcorrencia(nDentroEsperado, nAbaixoEsperado,
    nAcimaEsperado);
end;

function TFrequenciaDezenas.ObterFrequenciaMedia(
  const ADezenas: TDezenas): Double;
var
  lDezena: TDezenaContada;
  nFrequenciaTotal: Integer;
  I: Integer;
begin
  if Assigned(ADezenas) then
  begin
    nFrequenciaTotal := 0;
    for I := 0 to Pred(ADezenas.Count) do
    begin
      lDezena := FFrequencia.Find(ADezenas[I].Dezena);
      if Assigned(lDezena) then
        nFrequenciaTotal := nFrequenciaTotal + lDezena.Quantidade;
    end;

    Result := SimpleRoundTo(nFrequenciaTotal / ADezenas.Count);
  end
  else
    Result := 0;
end;

procedure TFrequenciaDezenas.GerarString(const AStrings: TStrings);
begin
  if Assigned(AStrings) then
  begin
    AStrings.BeginUpdate;
    try
      AStrings.Clear;
      AStrings.Add('M�dia: ' + FloatToStr(FMedia));
      AStrings.Add('Desvio padr�o: ' + FloatToStr(FDesvioPadrao));
      AStrings.Add('Frequ�ncia m�nima: ' + FloatToStr(FFrequenciaMinima));
      AStrings.Add('Frequ�ncia m�xima: ' + FloatToStr(FFrequenciaMaxima));
      AStrings.Add(
        'Dezenas dentro do esperado: ' + FDezenasDentroEsperado.GerarString);
      AStrings.Add(
        'Dezenas abaixo do esperado: ' + FDezenasAbaixoEsperado.GerarString);
      AStrings.Add(
        'Dezenas acima do esperado: ' + FDezenasAcimaEsperado.GerarString);
      AStrings.Add(
        Format('%% dentro do esperado: %s%% (%d)',
          [FloatToStr(FPercentualDentroEsperado),
            FDezenasDentroEsperado.Count]));
      AStrings.Add(
        Format('%% abaixo do esperado: %s%% (%d)',
          [FloatToStr(FPercentualAbaixoEsperado),
            FDezenasAbaixoEsperado.Count]));
      AStrings.Add(
        Format('%% acima do esperado: %s%% (%d)',
          [FloatToStr(FPercentualAcimaEsperado),
            FDezenasAcimaEsperado.Count]));
    finally
      AStrings.EndUpdate;
    end;
  end;
end;

{ TFrequenciaAcumulada }

constructor TFrequenciaAcumulada.Create(Collection: TCollection);
begin
  inherited;
  SetKey(CFrequenciaAcumulada);
end;

function TFrequenciaAcumulada.GetDescricao: string;
begin
  Result := 'Frequ�ncia acumulada';
end;

procedure TFrequenciaAcumulada.CalcularEstatistica;
var
  lHistorico: THistorico;
  lFrequencia: TFrequenciaDezenas;
  lDezenas: TDezenas;
  lItem: TItemContagem;
  sFrequencia: string;
  nNumeroSorteios: Integer;
begin
//Sumariza a string de frequ�ncia com base na frequ�ncia do sorteio anterior
  ChecarLoteria;
  ChecarSorteios;
  lHistorico := THistorico.Create;
  try
    lHistorico.Assign(Sorteios);
    lDezenas := TDezenas.Create;
    try
      lFrequencia := TFrequenciaDezenas.Create(nil);
      try
        lFrequencia.Loteria := Loteria;
        lFrequencia.Sorteios := lHistorico;
        Contagem.BeginUpdate;
        try
          Contagem.Clear;
          if (FNumeroSorteios > 0) and (FNumeroSorteios <= Sorteios.Count) then
            nNumeroSorteios := FNumeroSorteios
          else
            nNumeroSorteios := Sorteios.Count;
          while nNumeroSorteios > 0 do
          begin
            lDezenas.Assign(lHistorico[Pred(lHistorico.Count)].Dezenas);
            lHistorico.Delete(Pred(lHistorico.Count));
            lFrequencia.Calcular(True);
            sFrequencia := lFrequencia.ObterStringFrequencia(lDezenas);
            lItem := Contagem.Find(sFrequencia);
            if not Assigned(lItem) then
            begin
              lItem := Contagem.Add;
              lItem.Valor := sFrequencia;
            end;

            lItem.Quantidade := lItem.Quantidade + 1;
            Dec(nNumeroSorteios);
          end;

          Contagem.CalcularPercentual;
        finally
          Contagem.EndUpdate;
        end;
      finally
        FreeAndNil(lFrequencia);
      end;
    finally
      FreeAndNil(lDezenas);
    end;
  finally
    FreeAndNil(lHistorico);
  end;
end;

procedure TFrequenciaAcumulada.Assign(Source: TPersistent);
begin
  inherited;
  if Source is TFrequenciaAcumulada then
    NumeroSorteios := TFrequenciaAcumulada(Source).NumeroSorteios;
end;

function TFrequenciaAcumulada.CriarDataSet: TDataSet;
begin
  Result := Contagem.CriarDataSet(True, 'Frequ�ncia', ftString, 10);
end;

{ TFrequencia }

constructor TFrequencia.Create(Collection: TCollection);
begin
  inherited;
  SetKey(CFrequencia);
end;

function TFrequencia.GetDescricao: string;
begin
  Result := 'Frequ�ncia';
end;

procedure TFrequencia.CalcularEstatistica;
var
  lFrequencia: TFrequenciaDezenas;
  lItem: TItemContagem;
  sFrequencia: string;
  I: Integer;
begin
//Sumariza a string de frequ�ncia com base na frequ�ncia do �ltimo sorteio
  ChecarLoteria;
  ChecarSorteios;
  lFrequencia := TFrequenciaDezenas(
    Loteria.Estatisticas.Find(CFrequenciaDezenas));
  if Assigned(lFrequencia) then
  begin
    lFrequencia.Calcular(False);
    Contagem.BeginUpdate;
    try
      Contagem.Clear;
      for I := 0 to Pred(Sorteios.Count) do
      begin
        sFrequencia := lFrequencia.ObterStringFrequencia(Sorteios[I].Dezenas);
        lItem := Contagem.Find(sFrequencia);
        if not Assigned(lItem) then
        begin
          lItem := Contagem.Add;
          lItem.Valor := sFrequencia;
        end;

        lItem.Quantidade := lItem.Quantidade + 1;
      end;

      Contagem.CalcularPercentual;
    finally
      Contagem.EndUpdate;
    end;
  end;
end;

function TFrequencia.CriarDataSet: TDataSet;
begin
  Result := Contagem.CriarDataSet(True, 'Frequ�ncia', ftString, 10);
end;

{ TQuocienteFrequencia }

constructor TQuocienteFrequencia.Create(Collection: TCollection);
begin
  inherited;
  SetKey(CQuocienteFrequencia);
  FTabela := TTabelaQuociente.Create;
end;

destructor TQuocienteFrequencia.Destroy;
begin
  FreeAndNil(FTabela);
  inherited;
end;

function TQuocienteFrequencia.GetDescricao: string;
begin
  Result := 'Quociente de frequ�ncia';
end;

function TQuocienteFrequencia.QuocienteValido(const AValor: Double): Boolean;
begin
  Result := (CompareValue(AValor, FQuocienteMinimo) <> -1) and
    (CompareValue(AValor, FQuocienteMaximo) <> 1);
end;

procedure TQuocienteFrequencia.CalcularEstatistica;
var
  lHistorico: THistorico;
  lFrequencia: TFrequenciaDezenas;
  lDezenas: TDezenas;
  lLinha: TLinhaTabelaQuociente;
  nNumeroSorteios: Integer;
  nFrequenciaDezenasTotal: Double;
  nQuocienteTotal: Double;
  nDiferenca: Double;
  nAcertos: Integer;
  I: Integer;
begin
//Calcula o quociente de frequ�ncia sorteio � sorteio
  ChecarLoteria;
  ChecarSorteios;
  lHistorico := THistorico.Create;
  try
    lHistorico.Assign(Sorteios);
    lDezenas := TDezenas.Create;
    try
      lFrequencia := TFrequenciaDezenas.Create(nil);
      try
        lFrequencia.Loteria := Loteria;
        lFrequencia.Sorteios := lHistorico;
        FTabela.BeginUpdate;
        try
          FTabela.Clear;
          if (FNumeroSorteios > 0) and (FNumeroSorteios <= Sorteios.Count) then
            nNumeroSorteios := FNumeroSorteios
          else
            nNumeroSorteios := Sorteios.Count;
          nFrequenciaDezenasTotal := 0;
          nQuocienteTotal := 0;
          while nNumeroSorteios > 0 do
          begin
            lDezenas.Assign(lHistorico[Pred(lHistorico.Count)].Dezenas);
            lHistorico.Delete(Pred(lHistorico.Count));
            lFrequencia.Calcular(True);
            lLinha := FTabela.Insert(0);
            lLinha.Concurso := lHistorico[Pred(lHistorico.Count)].Concurso;
            lLinha.FrequenciaMedia := lFrequencia.Media;
            lLinha.FrequenciaMediaDezenas := lFrequencia.ObterFrequenciaMedia(
              lDezenas);
            lLinha.DesvioPadrao := lFrequencia.DesvioPadrao;
            lLinha.Quociente := SimpleRoundTo(
              lLinha.FrequenciaMediaDezenas / lLinha.FrequenciaMedia, -4);
            nFrequenciaDezenasTotal := nFrequenciaDezenasTotal +
              lLinha.FrequenciaMediaDezenas;
            nQuocienteTotal := nQuocienteTotal + lLinha.Quociente;
            Dec(nNumeroSorteios);
          end;
        finally
          FTabela.EndUpdate;
        end;
      finally
        FreeAndNil(lFrequencia);
      end;
    finally
      FreeAndNil(lDezenas);
    end;
  finally
    FreeAndNil(lHistorico);
  end;

  FMediaFrequenciaDezenas := SimpleRoundTo(
    nFrequenciaDezenasTotal / FTabela.Count);
  FMediaQuociente := SimpleRoundTo(nQuocienteTotal / FTabela.Count, -4);
  nFrequenciaDezenasTotal := 0;
  nQuocienteTotal := 0;
  for I := 0 to Pred(FTabela.Count) do
  begin
    //arredonda tudo para eliminar o lixo visto durante a depura��o
    nDiferenca := SimpleRoundTo(
      FTabela[I].FrequenciaMediaDezenas - FMediaFrequenciaDezenas);
    nFrequenciaDezenasTotal := SimpleRoundTo(
      nFrequenciaDezenasTotal + SimpleRoundTo(nDiferenca * nDiferenca));
    nDiferenca := SimpleRoundTo(FTabela[I].Quociente - FMediaQuociente, -4);
    nQuocienteTotal := SimpleRoundTo(
      nQuocienteTotal + SimpleRoundTo(nDiferenca * nDiferenca, -4), -4);
  end;

  FDesvioPadraoFrequenciaDezenas := SimpleRoundTo(
    Sqrt(SimpleRoundTo(nFrequenciaDezenasTotal / FTabela.Count)));
  FFrequenciaDezenasMinima := SimpleRoundTo(
    FMediaFrequenciaDezenas - FDesvioPadraoFrequenciaDezenas, -1);
  FFrequenciaDezenasMaxima := SimpleRoundTo(
    FMediaFrequenciaDezenas + FDesvioPadraoFrequenciaDezenas, -1);
  FDesvioPadraoQuociente := SimpleRoundTo(
    Sqrt(SimpleRoundTo(nQuocienteTotal / FTabela.Count, -4)), -4);
  FQuocienteMinimo := SimpleRoundTo(FMediaQuociente - FDesvioPadraoQuociente);
  FQuocienteMaximo := SimpleRoundTo(FMediaQuociente + FDesvioPadraoQuociente);
  nAcertos := 0;
  for I := 0 to Pred(FTabela.Count) do
    if QuocienteValido(FTabela[I].Quociente) then
      Inc(nAcertos);
  FAcerto := SimpleRoundTo((nAcertos * 100) / FTabela.Count);
end;

procedure TQuocienteFrequencia.Assign(Source: TPersistent);
var
  lEstatistica: TQuocienteFrequencia;
begin
  inherited;
  if Source is TQuocienteFrequencia then
  begin
    lEstatistica := TQuocienteFrequencia(Source);
    NumeroSorteios := lEstatistica.NumeroSorteios;
    MediaFrequenciaDezenas := lEstatistica.MediaFrequenciaDezenas;
    DesvioPadraoFrequenciaDezenas := lEstatistica.DesvioPadraoFrequenciaDezenas;
    FrequenciaDezenasMinima := lEstatistica.FrequenciaDezenasMinima;
    FrequenciaDezenasMaxima := lEstatistica.FrequenciaDezenasMaxima;
    MediaQuociente := lEstatistica.MediaQuociente;
    DesvioPadraoQuociente := lEstatistica.DesvioPadraoQuociente;
    QuocienteMinimo := lEstatistica.QuocienteMinimo;
    QuocienteMaximo := lEstatistica.QuocienteMaximo;
    Acerto := lEstatistica.Acerto;
    Tabela.Assign(lEstatistica.Tabela);
  end;
end;

function TQuocienteFrequencia.CriarDataSet: TDataSet;
var
  lDataSet: TClientDataSet;
  I: Integer;
begin
  lDataSet := TClientDataSet.Create(nil);
  try
    lDataSet.FieldDefs.Add('Concurso', ftInteger);
    lDataSet.FieldDefs.Add('Frequ�ncia m�dia', ftFloat);
    lDataSet.FieldDefs.Add('Frequ�ncia m�dia dezenas', ftFloat);
    lDataSet.FieldDefs.Add('Desvio padr�o', ftFloat);
    lDataSet.FieldDefs.Add('Quociente', ftFloat);
    lDataSet.CreateDataSet;
    lDataSet.LogChanges := False;
    lDataSet.DisableControls;
    try
      for I := 0 to Pred(FTabela.Count) do
      begin
        lDataSet.Append;
        lDataSet.FieldByName('Concurso').AsInteger := FTabela[I].Concurso;
        lDataSet.FieldByName('Frequ�ncia m�dia').AsFloat := FTabela[I].FrequenciaMedia;
        lDataSet.FieldByName('Frequ�ncia m�dia dezenas').AsFloat := FTabela[I].FrequenciaMediaDezenas;
        lDataSet.FieldByName('Desvio padr�o').AsFloat := FTabela[I].DesvioPadrao;
        lDataSet.FieldByName('Quociente').AsFloat := FTabela[I].Quociente;
        lDataSet.Post;
      end;

      lDataSet.First;
    finally
      lDataSet.EnableControls;
    end;

    Result := lDataSet;
  except
    FreeAndNil(lDataSet);
    raise;
  end;
end;

procedure TQuocienteFrequencia.GerarString(const AStrings: TStrings);
begin
  AStrings.Add('M�dia da frequ�ncia das dezenas: ' +
    FloatToStr(FMediaFrequenciaDezenas));
  AStrings.Add('Desvio padr�o da frequ�ncia das dezenas: ' +
    FloatToStr(FDesvioPadraoFrequenciaDezenas));
  AStrings.Add('Frequ�ncia m�nima das dezenas: ' +
    FloatToStr(FFrequenciaDezenasMinima));
  AStrings.Add('Frequ�ncia m�xima das dezenas: ' +
    FloatToStr(FFrequenciaDezenasMaxima));
  AStrings.Add('---');
  AStrings.Add('M�dia do quociente: ' + FloatToStr(FMediaQuociente));
  AStrings.Add('Desvio padr�o do quociente: ' +
    FloatToStr(FDesvioPadraoQuociente));
  AStrings.Add('Quociente m�nimo: ' + FloatToStr(FQuocienteMinimo));
  AStrings.Add('Quociente m�ximo: ' + FloatToStr(FQuocienteMaximo));
  AStrings.Add(Format('Acerto: %s%%', [FloatToStr(FAcerto)]));
  AStrings.Add('---');
end;

{ TAtrasoDezenas }

constructor TAtrasoDezenas.Create(Collection: TCollection);
begin
  inherited;
  SetKey(CAtrasoDezenas);
  FAtraso := TDezenasContadas.Create;
  FAtraso.HashSize := 64;
  FTabela := TTabelaAtraso.Create;
end;

destructor TAtrasoDezenas.Destroy;
begin
  FreeAndNil(FTabela);
  FreeAndNil(FAtraso);
  inherited;
end;

function TAtrasoDezenas.GetDescricao: string;
begin
  Result := 'Atraso de dezenas';
end;

procedure TAtrasoDezenas.CalcularEstatistica;
var
  lDezena: TDezenaContada;
  nTotal: Double;
  I: Integer;
  J: Integer;
begin
  ChecarLoteria;
  ChecarSorteios;
  FAtraso.BeginUpdate;
  try
    FAtraso.Clear;
    for I := 0 to Pred(Sorteios.Count) do
    begin
      for J := 0 to Pred(Sorteios[I].Dezenas.Count) do
      begin
        lDezena := FAtraso.Find(Sorteios[I].Dezenas[J].Dezena);
        if not Assigned(lDezena) then
        begin
          lDezena := FAtraso.Add;
          lDezena.Dezena := Sorteios[I].Dezenas[J].Dezena;
        end
        else
          lDezena.Quantidade := 0;
      end;

      for J := 0 to Pred(FAtraso.Count) do
        if not Sorteios[I].Dezenas.Contains(FAtraso[J].Dezena) then
          FAtraso[J].Quantidade := FAtraso[J].Quantidade + 1;
    end;
  finally
    FAtraso.EndUpdate;
  end;

  FTabela.BeginUpdate;
  try
    FTabela.Clear;
    for I := 0 to Pred(Sorteios.Count) do
      FTabela.Add.Atraso := I;
    for I := 0 to Pred(FAtraso.Count) do
      FTabela.Find(FAtraso[I].Quantidade).Dezenas.Add.Dezena :=
        FAtraso[I].Dezena;
    FTabela.Ordenar;
  finally
    FTabela.EndUpdate;
  end;

  FAtrasoEsperado := SimpleRoundTo(
    Loteria.MaiorDezena / Loteria.DezenasSorteio);
  nTotal := 0;
  for I := 0 to Pred(FAtraso.Count) do
    nTotal := nTotal + FAtraso[I].Quantidade;
  FAtrasoMedio := SimpleRoundTo(nTotal / Loteria.MaiorDezena);
end;

procedure TAtrasoDezenas.Assign(Source: TPersistent);
var
  lEstatistica: TAtrasoDezenas;
begin
  inherited;
  if Source is TAtrasoDezenas then
  begin
    lEstatistica := TAtrasoDezenas(Source);
    Atraso.Assign(lEstatistica.Atraso);
    Tabela.Assign(lEstatistica.Tabela);
    AtrasoEsperado := lEstatistica.AtrasoEsperado;
    AtrasoMedio := lEstatistica.AtrasoMedio;
  end;
end;

function TAtrasoDezenas.ObterAtrasoMedio(const ADezenas: TDezenas): Double;
var
  lDezena: TDezenaContada;
  nAtrasoTotal: Integer;
  I: Integer;
begin
  if Assigned(ADezenas) then
  begin
    nAtrasoTotal := 0;
    for I := 0 to Pred(ADezenas.Count) do
    begin
      lDezena := FAtraso.Find(ADezenas[I].Dezena);
      if Assigned(lDezena) then
        nAtrasoTotal := nAtrasoTotal + lDezena.Quantidade;
    end;

    Result := SimpleRoundTo(nAtrasoTotal / ADezenas.Count);
  end
  else
    Result := 0;
end;

procedure TAtrasoDezenas.GerarString(const AStrings: TStrings);
var
  I: Integer;
begin
  AStrings.Add('Atraso esperado: ' + FloatToStr(FAtrasoEsperado));
  AStrings.Add('Atraso m�dio: ' + FloatToStr(FAtrasoMedio));
  for I := 0 to Pred(FTabela.Count) do
    if FTabela[I].Dezenas.Count > 0 then
      AStrings.Add(
        Format('Atraso %d: %s',
          [FTabela[I].Atraso, FTabela[I].Dezenas.GerarString]));
end;

{ TQuocienteAtraso }

constructor TQuocienteAtraso.Create(Collection: TCollection);
begin
  inherited;
  SetKey(CQuocienteAtraso);
  FDezenasAtrasoNormal := TDezenas.Create;
  FDezenasAtrasoNormal.HashSize := 64;
  FDezenasMaisAtrasadas := TDezenas.Create;
  FDezenasMaisAtrasadas.HashSize := 64;
  FDezenasMaisRecentes := TDezenas.Create;
  FDezenasMaisRecentes.HashSize := 64;
  FTabela := TTabelaAtrasoMedio.Create;
end;

destructor TQuocienteAtraso.Destroy;
begin
  FreeAndNil(FTabela);
  FreeAndNil(FDezenasMaisRecentes);
  FreeAndNil(FDezenasMaisAtrasadas);
  FreeAndNil(FDezenasAtrasoNormal);
  inherited;
end;

function TQuocienteAtraso.GetDescricao: string;
begin
  Result := 'Quociente de atraso';
end;

procedure TQuocienteAtraso.CalcularEstatistica;
var
  lHistorico: THistorico;
  lAtraso: TAtrasoDezenas;
  lAtrasoAtual: TAtrasoDezenas;
  lDezenas: TDezenas;
  lLinha: TLinhaTabelaAtrasoMedio;
  sID: string;
  nNumeroSorteios: Integer;
  nAtrasoMedioTotal: Double;
  nAtrasoDezenasTotal: Double;
  nQuocienteTotal: Double;
  nDiferenca: Double;
  nAcertos: Integer;
  I: Integer;
  J: Integer;
begin
  ChecarLoteria;
  ChecarSorteios;
  Loteria.Estatisticas.IniciarCache;
  try
    lHistorico := THistorico.Create;
    try
      lHistorico.Assign(Sorteios);
      lDezenas := TDezenas.Create;
      try
          FTabela.BeginUpdate;
          try
            FTabela.Clear;
            if (FNumeroSorteios > 0) and (FNumeroSorteios <= Sorteios.Count) then
              nNumeroSorteios := FNumeroSorteios
            else
              nNumeroSorteios := Sorteios.Count;
            lAtrasoAtual := nil;
            nAtrasoMedioTotal := 0;
            nAtrasoDezenasTotal := 0;
            nQuocienteTotal := 0;
            while nNumeroSorteios > 0 do
            begin
              lDezenas.Assign(lHistorico[Pred(lHistorico.Count)].Dezenas);
              lHistorico.Delete(Pred(lHistorico.Count));
              sID := CIDAtrasoDezenas + IntToStr(
                lHistorico[Pred(lHistorico.Count)].Concurso);
              lAtraso := Loteria.Estatisticas.LocalizarAtrasoDezenasCache(sID);
              if not Assigned(lAtraso) then
              begin
                lAtraso := TAtrasoDezenas.Create(nil);
                lAtraso.Loteria := Loteria;
                lAtraso.Sorteios := lHistorico;
                lAtraso.Calcular(True);
                Loteria.Estatisticas.AdicionarCache(sID, lAtraso);
              end;

              lLinha := FTabela.Insert(0);
              lLinha.Concurso := lHistorico[Pred(lHistorico.Count)].Concurso;
              lLinha.AtrasoEsperado := lAtraso.AtrasoEsperado;
              lLinha.AtrasoMedio := lAtraso.AtrasoMedio;
              lLinha.AtrasoMedioDezenas := lAtraso.ObterAtrasoMedio(lDezenas);
              lLinha.Quociente := SimpleRoundTo(
                lLinha.AtrasoMedioDezenas / lLinha.AtrasoMedio);
              nAtrasoMedioTotal := nAtrasoMedioTotal + lLinha.AtrasoMedio;
              nAtrasoDezenasTotal := nAtrasoDezenasTotal +
                lLinha.AtrasoMedioDezenas;
              nQuocienteTotal := nQuocienteTotal + lLinha.Quociente;
              if not Assigned(lAtrasoAtual) then
                lAtrasoAtual := lAtraso;
              Dec(nNumeroSorteios);
            end;
          finally
            FTabela.EndUpdate;
          end;
      finally
        FreeAndNil(lDezenas);
      end;
    finally
      FreeAndNil(lHistorico);
    end;

    FMediaAtraso := SimpleRoundTo(nAtrasoMedioTotal / FTabela.Count);
    FMediaAtrasoDezenas := SimpleRoundTo(nAtrasoDezenasTotal / FTabela.Count);
    FMediaQuociente := SimpleRoundTo(nQuocienteTotal / FTabela.Count);
    nAtrasoMedioTotal := 0;
    nAtrasoDezenasTotal := 0;
    nQuocienteTotal := 0;
    for I := 0 to Pred(FTabela.Count) do
    begin
      nDiferenca := SimpleRoundTo(FTabela[I].AtrasoMedio - FMediaAtraso);
      nAtrasoMedioTotal := SimpleRoundTo(
        nAtrasoMedioTotal + SimpleRoundTo(nDiferenca * nDiferenca));
      nDiferenca := SimpleRoundTo(
        FTabela[I].AtrasoMedioDezenas - FMediaAtrasoDezenas);
      nAtrasoDezenasTotal := SimpleRoundTo(
        nAtrasoDezenasTotal + SimpleRoundTo(nDiferenca * nDiferenca));
      nDiferenca := SimpleRoundTo(FTabela[I].Quociente - FMediaQuociente);
      nQuocienteTotal := SimpleRoundTo(
        nQuocienteTotal + SimpleRoundTo(nDiferenca * nDiferenca));
    end;

    FDesvioPadraoAtraso := SimpleRoundTo(
      Sqrt(SimpleRoundTo(nAtrasoMedioTotal / FTabela.Count)));
    FDesvioPadraoAtrasoConsiderado := Trunc(
      SimpleRoundTo(FDesvioPadraoAtraso, 0));
    FAtrasoMinimo := FMediaAtraso - FDesvioPadraoAtrasoConsiderado;
    FAtrasoMaximo := FMediaAtraso + FDesvioPadraoAtrasoConsiderado;
    FAtrasoMinimoConsiderado := Trunc(SimpleRoundTo(FAtrasoMinimo, 0));
    FAtrasoMaximoConsiderado := Trunc(SimpleRoundTo(FAtrasoMaximo, 0));
    FDesvioPadraoAtrasoDezenas := SimpleRoundTo(
      Sqrt(SimpleRoundTo(nAtrasoDezenasTotal / FTabela.Count)));
    FAtrasoDezenasMinimo := SimpleRoundTo(
      FMediaAtrasoDezenas - FDesvioPadraoAtrasoDezenas, -1);
    FAtrasoDezenasMaximo := SimpleRoundTo(
      FMediaAtrasoDezenas + FDesvioPadraoAtrasoDezenas, -1);
    FDesvioPadraoQuociente := SimpleRoundTo(
      Sqrt(SimpleRoundTo(nQuocienteTotal / FTabela.Count)));
    FQuocienteMinimo := SimpleRoundTo(
      FMediaQuociente - FDesvioPadraoQuociente, -1);
    FQuocienteMaximo := SimpleRoundTo(
      FMediaQuociente + FDesvioPadraoQuociente, -1);
    nAcertos := 0;
    for I := 0 to Pred(FTabela.Count) do
      if QuocienteValido(FTabela[I].Quociente) then
        Inc(nAcertos);
    FAcerto := SimpleRoundTo((nAcertos * 100) / FTabela.Count);
    if Assigned(lAtrasoAtual) then
    begin
      FDezenasAtrasoNormal.BeginUpdate;
      FDezenasMaisAtrasadas.BeginUpdate;
      FDezenasMaisRecentes.BeginUpdate;
      try
        FDezenasAtrasoNormal.Clear;
        FDezenasMaisAtrasadas.Clear;
        FDezenasMaisRecentes.Clear;
        for I := 0 to Pred(lAtrasoAtual.Tabela.Count) do
          for J := 0 to Pred(lAtrasoAtual.Tabela[I].Dezenas.Count) do
          begin
            if (CompareValue(lAtrasoAtual.Tabela[I].Atraso, FAtrasoMinimoConsiderado) <> -1) and
              (CompareValue(lAtrasoAtual.Tabela[I].Atraso, FAtrasoMaximoConsiderado) <> 1) then
              FDezenasAtrasoNormal.Add.Dezena :=
                lAtrasoAtual.Tabela[I].Dezenas[J].Dezena;
            if CompareValue(lAtrasoAtual.Tabela[I].Atraso, FAtrasoMinimoConsiderado) = -1 then
              FDezenasMaisRecentes.Add.Dezena :=
                lAtrasoAtual.Tabela[I].Dezenas[J].Dezena;
            if CompareValue(lAtrasoAtual.Tabela[I].Atraso, FAtrasoMaximoConsiderado) = 1 then
              FDezenasMaisAtrasadas.Add.Dezena :=
                lAtrasoAtual.Tabela[I].Dezenas[J].Dezena;
          end;
      finally
        FDezenasMaisRecentes.BeginUpdate;
        FDezenasMaisAtrasadas.BeginUpdate;
        FDezenasAtrasoNormal.BeginUpdate;
      end;

      FDezenasAtrasoNormal.Ordenar;
      FDezenasMaisAtrasadas.Ordenar;
      FDezenasMaisRecentes.Ordenar;
      FPercentualAtrasoNormal := SimpleRoundTo(
        (FDezenasAtrasoNormal.Count * 100) / Loteria.FMaiorDezena);
      FPercentualMaisAtrasadas := SimpleRoundTo(
        (FDezenasMaisAtrasadas.Count * 100) / Loteria.FMaiorDezena);
      FPercentualMaisRecentes := SimpleRoundTo(
        (FDezenasMaisRecentes.Count * 100) / Loteria.FMaiorDezena);
    end;
  finally
    Loteria.Estatisticas.FinalizarCache;
  end;
end;

procedure TQuocienteAtraso.Assign(Source: TPersistent);
var
  lEstatistica: TQuocienteAtraso;
begin
  inherited;
  if Source is TQuocienteAtraso then
  begin
    lEstatistica := TQuocienteAtraso(Source);
    NumeroSorteios := lEstatistica.NumeroSorteios;
    MediaAtraso := lEstatistica.MediaAtraso;
    DesvioPadraoAtraso := lEstatistica.DesvioPadraoAtraso;
    DesvioPadraoAtrasoConsiderado := lEstatistica.DesvioPadraoAtrasoConsiderado;
    AtrasoMinimo := lEstatistica.AtrasoMinimo;
    AtrasoMinimoConsiderado := lEstatistica.AtrasoMinimoConsiderado;
    AtrasoMaximo := lEstatistica.AtrasoMaximo;
    AtrasoMaximoConsiderado := lEstatistica.AtrasoMaximoConsiderado;
    MediaAtrasoDezenas := lEstatistica.MediaAtrasoDezenas;
    DesvioPadraoAtrasoDezenas := lEstatistica.DesvioPadraoAtrasoDezenas;
    AtrasoDezenasMinimo := lEstatistica.AtrasoDezenasMinimo;
    AtrasoDezenasMaximo := lEstatistica.AtrasoDezenasMaximo;
    MediaQuociente := lEstatistica.MediaQuociente;
    DesvioPadraoQuociente := lEstatistica.DesvioPadraoQuociente;
    QuocienteMinimo := lEstatistica.QuocienteMinimo;
    QuocienteMaximo := lEstatistica.QuocienteMaximo;
    Acerto := lEstatistica.Acerto;
    DezenasAtrasoNormal.Assign(lEstatistica.DezenasAtrasoNormal);
    DezenasMaisAtrasadas.Assign(lEstatistica.DezenasMaisAtrasadas);
    DezenasMaisRecentes.Assign(lEstatistica.DezenasMaisRecentes);
    PercentualAtrasoNormal := lEstatistica.PercentualAtrasoNormal;
    PercentualMaisAtrasadas := lEstatistica.PercentualMaisAtrasadas;
    PercentualMaisRecentes := lEstatistica.PercentualMaisRecentes;
    Tabela.Assign(lEstatistica.Tabela);
  end;
end;

function TQuocienteAtraso.ObterAtraso(const ADezenas: TDezenas;
  out AAtrasoNormal, AMaisAtrasadas, AMaisRecentes: Integer): string;
begin
  if Assigned(ADezenas) then
  begin
    AAtrasoNormal := FDezenasAtrasoNormal.ConferirDezenas(ADezenas);
    AMaisAtrasadas := FDezenasMaisAtrasadas.ConferirDezenas(ADezenas);
    AMaisRecentes := FDezenasMaisRecentes.ConferirDezenas(ADezenas);
  end
  else
  begin
    AAtrasoNormal := 0;
    AMaisAtrasadas := 0;
    AMaisRecentes := 0;
  end;
end;

function TQuocienteAtraso.ObterStringAtraso(const ADezenas: TDezenas): string;
var
  nAtrasoNormal: Integer;
  nMaisAtrasadas: Integer;
  nMaisRecentes: Integer;
begin
  ChecarLoteria;
  ObterAtraso(ADezenas, nAtrasoNormal, nMaisAtrasadas, nMaisRecentes);
  Result := Loteria.FormatarStringOcorrencia(nAtrasoNormal,
    nMaisAtrasadas, nMaisRecentes);
end;

function TQuocienteAtraso.QuocienteValido(const AValor: Double): Boolean;
begin
  Result := (CompareValue(AValor, FQuocienteMinimo) <> -1) and
    (CompareValue(AValor, FQuocienteMaximo) <> 1);
end;

function TQuocienteAtraso.CriarDataSet: TDataSet;
var
  lDataSet: TClientDataSet;
  I: Integer;
begin
  lDataSet := TClientDataSet.Create(nil);
  try
    lDataSet.FieldDefs.Add('Concurso', ftInteger);
    lDataSet.FieldDefs.Add('Atraso esperado', ftFloat);
    lDataSet.FieldDefs.Add('Atraso m�dio', ftFloat);
    lDataSet.FieldDefs.Add('Atraso m�dio dezenas', ftFloat);
    lDataSet.FieldDefs.Add('Quociente', ftFloat);
    lDataSet.CreateDataSet;
    lDataSet.LogChanges := False;
    lDataSet.DisableControls;
    try
      for I := 0 to Pred(FTabela.Count) do
      begin
        lDataSet.Append;
        lDataSet.FieldByName('Concurso').AsInteger := FTabela[I].Concurso;
        lDataSet.FieldByName('Atraso esperado').AsFloat := FTabela[I].AtrasoEsperado;
        lDataSet.FieldByName('Atraso m�dio').AsFloat := FTabela[I].AtrasoMedio;
        lDataSet.FieldByName('Atraso m�dio dezenas').AsFloat := FTabela[I].AtrasoMedioDezenas;
        lDataSet.FieldByName('Quociente').AsFloat := FTabela[I].Quociente;
        lDataSet.Post;
      end;

      lDataSet.First;
    finally
      lDataSet.EnableControls;
    end;

    Result := lDataSet;
  except
    FreeAndNil(lDataSet);
    raise;
  end;
end;

procedure TQuocienteAtraso.GerarString(const AStrings: TStrings);
begin
  AStrings.Add('M�dia do atraso: ' + FloatToStr(FMediaAtraso));
  AStrings.Add(
    Format('Desvio padr�o do atraso: %s (%d)',
      [FloatToStr(FDesvioPadraoAtraso), FDesvioPadraoAtrasoConsiderado]));
  AStrings.Add(
    Format('Atraso m�nimo: %s (%d)',
      [FloatToStr(FAtrasoMinimo), FAtrasoMinimoConsiderado]));
  AStrings.Add(
    Format('Atraso m�ximo: %s (%d)',
      [FloatToStr(FAtrasoMaximo), FAtrasoMaximoConsiderado]));
  AStrings.Add('---');
  AStrings.Add('M�dia do atraso das dezenas: ' + FloatToStr(FMediaAtrasoDezenas));
  AStrings.Add('Desvio padr�o do atraso das dezenas: ' + FloatToStr(FDesvioPadraoAtrasoDezenas));
  AStrings.Add('Atraso m�nimo das dezenas: ' + FloatToStr(FAtrasoDezenasMinimo));
  AStrings.Add('Atraso m�ximo das dezenas: ' + FloatToStr(FAtrasoDezenasMaximo));
  AStrings.Add('---');
  AStrings.Add('M�dia do quociente: ' + FloatToStr(FMediaQuociente));
  AStrings.Add('Desvio padr�o do quociente: ' + FloatToStr(FDesvioPadraoQuociente));
  AStrings.Add('Quociente m�nimo: ' + FloatToStr(FQuocienteMinimo));
  AStrings.Add('Quociente m�ximo: ' + FloatToStr(FQuocienteMaximo));
  AStrings.Add(Format('Acerto: %s%%', [FloatToStr(FAcerto)]));
  AStrings.Add('---');
  AStrings.Add('Dezenas com atraso normal: ' + FDezenasAtrasoNormal.GerarString);
  AStrings.Add('Dezenas mais atrasadas: ' + FDezenasMaisAtrasadas.GerarString);
  AStrings.Add('Dezenas mais recentes: ' + FDezenasMaisRecentes.GerarString);
  AStrings.Add(
    Format('%% com atraso normal: %s%% (%d)',
      [FloatToStr(FPercentualAtrasoNormal),
        FDezenasAtrasoNormal.Count]));
  AStrings.Add(
    Format('%% mais atrasadas: %s%% (%d)',
      [FloatToStr(FPercentualMaisAtrasadas),
        FDezenasMaisAtrasadas.Count]));
  AStrings.Add(
    Format('%% mais recentes: %s%% (%d)',
      [FloatToStr(FPercentualMaisRecentes),
        FDezenasMaisRecentes.Count]));
  AStrings.Add('---');
end;

{ TAtrasoAcumulado }

constructor TAtrasoAcumulado.Create(Collection: TCollection);
begin
  inherited;
  SetKey(CAtrasoAcumulado);
end;

function TAtrasoAcumulado.GetDescricao: string;
begin
  Result := 'Atraso acumulado';
end;

procedure TAtrasoAcumulado.CalcularEstatistica;
var
  lHistorico: THistorico;
  lQuociente: TQuocienteAtraso;
  lItem: TItemContagem;
  sFrequencia: string;
  nNumeroSorteios: Integer;
begin
  ChecarLoteria;
  ChecarSorteios;
  Loteria.Estatisticas.IniciarCache;
  try
    lHistorico := THistorico.Create;
    try
      lHistorico.Assign(Sorteios);
      lQuociente := TQuocienteAtraso.Create(nil);
      try
        lQuociente.Loteria := Loteria;
        lQuociente.Sorteios := lHistorico;
        Contagem.BeginUpdate;
        try
          Contagem.Clear;
          if (FNumeroSorteios > 0) and (FNumeroSorteios <= Sorteios.Count) then
            nNumeroSorteios := FNumeroSorteios
          else
            nNumeroSorteios := Sorteios.Count;
          while nNumeroSorteios > 0 do
          begin
            lQuociente.NumeroSorteios := nNumeroSorteios;
            lQuociente.Calcular(True);
            sFrequencia := lQuociente.ObterStringAtraso(
              lHistorico[Pred(lHistorico.Count)].Dezenas);
            lItem := Contagem.Find(sFrequencia);
            if not Assigned(lItem) then
            begin
              lItem := Contagem.Add;
              lItem.Valor := sFrequencia;
            end;

            lItem.Quantidade := lItem.Quantidade + 1;
            lHistorico.Delete(Pred(lHistorico.Count));
            Dec(nNumeroSorteios);
          end;

          Contagem.CalcularPercentual;
        finally
          Contagem.EndUpdate;
        end;
      finally
        FreeAndNil(lQuociente);
      end;
    finally
      FreeAndNil(lHistorico);
    end;
  finally
    Loteria.Estatisticas.FinalizarCache;
  end;
end;

procedure TAtrasoAcumulado.Assign(Source: TPersistent);
begin
  inherited;
  if Source is TAtrasoAcumulado then
    NumeroSorteios := TAtrasoAcumulado(Source).NumeroSorteios;
end;

function TAtrasoAcumulado.CriarDataSet: TDataSet;
begin
  Result := Contagem.CriarDataSet(True, 'Atraso', ftString, 10);
end;

{ TAtraso }

constructor TAtraso.Create(Collection: TCollection);
begin
  inherited;
  SetKey(CAtraso);
end;

function TAtraso.GetDescricao: string;
begin
  Result := 'Atraso';
end;

procedure TAtraso.CalcularEstatistica;
var
  lQuociente: TQuocienteAtraso;
  lItem: TItemContagem;
  sAtraso: string;
  I: Integer;
begin
  ChecarLoteria;
  ChecarSorteios;
  lQuociente := TQuocienteAtraso(Loteria.Estatisticas.Find(CQuocienteAtraso));
  if Assigned(lQuociente) then
  begin
    lQuociente.Calcular(False);
    Contagem.BeginUpdate;
    try
      Contagem.Clear;
      for I := 0 to Pred(Sorteios.Count) do
      begin
        sAtraso := lQuociente.ObterStringAtraso(Sorteios[I].Dezenas);
        lItem := Contagem.Find(sAtraso);
        if not Assigned(lItem) then
        begin
          lItem := Contagem.Add;
          lItem.Valor := sAtraso;
        end;

        lItem.Quantidade := lItem.Quantidade + 1;
      end;

      Contagem.CalcularPercentual;
    finally
      Contagem.EndUpdate;
    end;
  end;
end;

function TAtraso.CriarDataSet: TDataSet;
begin
  Result := Contagem.CriarDataSet(True, 'Atraso', ftString, 10);
end;

{ TThreadOperacao }

procedure TThreadOperacao.Execute;
begin
  try
    ExecutarThread;
  except
    on E: Exception do
    begin
      FErro := E.Message;
      FClasseErro := ExceptClass(E.ClassType);
    end;
  end;

  Terminate;
end;

{ TThreadCalculoEstatistica }

procedure TThreadCalculoEstatistica.ExecutarThread;
begin
  if Assigned(FEstatistica) then
    FResultado := FEstatistica.Calcular(FRecalcular);
end;

{ TCacheEstatistica }

destructor TCacheEstatistica.Destroy;
begin
  if Assigned(FEstatistica) then
    FreeAndNil(FEstatistica);
  inherited;
end;

function TCacheEstatistica.GetKey: string;
begin
  Result := inherited GetKey;
end;

procedure TCacheEstatistica.SetKey(const AValue: string);
begin
  inherited;
end;

{ TCacheEstatisticas }

constructor TCacheEstatisticas.Create;
begin
  inherited;
  HashSize := 16384;
end;

function TCacheEstatisticas.GetItem(const AIndex: Integer): TCacheEstatistica;
begin
  Result := TCacheEstatistica(inherited Items[AIndex]);
end;

function TCacheEstatisticas.GetItemClass: TCollectionItemClass;
begin
  Result := TCacheEstatistica;
end;

function TCacheEstatisticas.Add: TCacheEstatistica;
begin
  Result := TCacheEstatistica(inherited Add);
end;

function TCacheEstatisticas.Insert(const AIndex: Integer): TCacheEstatistica;
begin
  Result := TCacheEstatistica(inherited Insert(AIndex));
end;

function TCacheEstatisticas.Find(const AID: string): TCacheEstatistica;
begin
  Result := TCacheEstatistica(inherited Find(AID));
end;

function TCacheEstatisticas.Contains(const AID: string): Boolean;
begin
  Result := inherited Contains(AID);
end;

function TCacheEstatisticas.Remove(const AID: string): Boolean;
begin
  Result := inherited Remove(AID);
end;

{ TEstatisticas }

constructor TEstatisticas.Create;
begin
  inherited;
  HashSize := 32;
end;

destructor TEstatisticas.Destroy;
begin
  if Assigned(FCache) then
    FreeAndNil(FCache);
  inherited;
end;

function TEstatisticas.GetItem(const AIndex: Integer): TEstatistica;
begin
  Result := TEstatistica(inherited Items[AIndex]);
end;

function TEstatisticas.GetLoteria: TTipoLoteria;
var
  lTipoLoteria: ITipoLoteria;
begin
  if Assigned(FLoteria) then
    Result := FLoteria
  else
    if Assigned(Parent) and Parent.GetInterface(ITipoLoteria, lTipoLoteria) then
      Result := lTipoLoteria.GetLoteria
    else
      Result := nil;
end;

function TEstatisticas.GetSorteios: TSorteios;
begin
  if Assigned(FSorteios) then
    Result := FSorteios
  else
    if Assigned(FLoteria) then
      Result := FLoteria.Historico
    else
      Result := nil;
end;

function TEstatisticas.GetItemClass: TCollectionItemClass;
begin
  Result := TEstatistica;
end;

function TEstatisticas.Add: TEstatistica;
begin
  Result := TEstatistica(inherited Add);
end;

function TEstatisticas.AdicionarOcorrenciaPorPares: TOcorrenciaPorPares;
begin
  Result := TOcorrenciaPorPares(AddType(TOcorrenciaPorPares));
end;

function TEstatisticas.AdicionarOcorrenciaPorPrimos: TOcorrenciaPorPrimos;
begin
  Result := TOcorrenciaPorPrimos(AddType(TOcorrenciaPorPrimos));
end;

function TEstatisticas.AdicionarOcorrenciaPorIntervalo: TOcorrenciaPorIntervalo;
begin
  Result := TOcorrenciaPorIntervalo(AddType(TOcorrenciaPorIntervalo));
end;

function TEstatisticas.AdicionarOcorrenciaPorLinha: TOcorrenciaPorLinha;
begin
  Result := TOcorrenciaPorLinha(AddType(TOcorrenciaPorLinha));
end;

function TEstatisticas.AdicionarOcorrenciaPorColuna: TOcorrenciaPorColuna;
begin
  Result := TOcorrenciaPorColuna(AddType(TOcorrenciaPorColuna));
end;

function TEstatisticas.AdicionarOcorrenciaPorSequencia: TOcorrenciaPorSequencia;
begin
  Result := TOcorrenciaPorSequencia(AddType(TOcorrenciaPorSequencia));
end;

function TEstatisticas.AdicionarOcorrenciaPorQuadrante: TOcorrenciaPorQuadrante;
begin
  Result := TOcorrenciaPorQuadrante(AddType(TOcorrenciaPorQuadrante));
end;

function TEstatisticas.AdicionarOcorrenciaPorDezena: TOcorrenciaPorDezena;
begin
  Result := TOcorrenciaPorDezena(AddType(TOcorrenciaPorDezena));
end;

function TEstatisticas.AdicionarOcorrenciaPorRepeticaoDezenas: TOcorrenciaPorRepeticaoDezenas;
begin
  Result := TOcorrenciaPorRepeticaoDezenas(
    AddType(TOcorrenciaPorRepeticaoDezenas));
end;

function TEstatisticas.AdicionarDezenasMaisSorteadas: TDezenasMaisSorteadas;
begin
  Result := TDezenasMaisSorteadas(AddType(TDezenasMaisSorteadas));
end;

function TEstatisticas.AdicionarDezenasMenosSorteadas: TDezenasMenosSorteadas;
begin
  Result := TDezenasMenosSorteadas(AddType(TDezenasMenosSorteadas));
end;

function TEstatisticas.AdicionarFrequenciaDezenas: TFrequenciaDezenas;
begin
  Result := TFrequenciaDezenas(AddType(TFrequenciaDezenas));
end;

function TEstatisticas.AdicionarFrequenciaAcumulada: TFrequenciaAcumulada;
begin
  Result := TFrequenciaAcumulada(AddType(TFrequenciaAcumulada));
end;

function TEstatisticas.AdicionarFrequencia: TFrequencia;
begin
  Result := TFrequencia(AddType(TFrequencia));
end;

function TEstatisticas.AdicionarQuocienteFrequencia: TQuocienteFrequencia;
begin
  Result := TQuocienteFrequencia(AddType(TQuocienteFrequencia));
end;

function TEstatisticas.AdicionarAtrasoDezenas: TAtrasoDezenas;
begin
  Result := TAtrasoDezenas(AddType(TAtrasoDezenas));
end;

function TEstatisticas.AdicionarQuocienteAtraso: TQuocienteAtraso;
begin
  Result := TQuocienteAtraso(AddType(TQuocienteAtraso));
end;

function TEstatisticas.AdicionarAtrasoAcumulado: TAtrasoAcumulado;
begin
  Result := TAtrasoAcumulado(AddType(TAtrasoAcumulado));
end;

function TEstatisticas.AdicionarAtraso: TAtraso;
begin
  Result := TAtraso(AddType(TAtraso));
end;

function TEstatisticas.Insert(const AIndex: Integer): TEstatistica;
begin
  Result := TEstatistica(inherited Insert(AIndex));
end;

function TEstatisticas.Find(const ANome: string): TEstatistica;
begin
  Result := TEstatistica(inherited Find(ANome));
end;

function TEstatisticas.Contains(const ANome: string): Boolean;
begin
  Result := inherited Contains(ANome);
end;

function TEstatisticas.Remove(const ANome: string): Boolean;
begin
  Result := inherited Remove(ANome);
end;

procedure TEstatisticas.Calcular(const ARecalcular: Boolean);
var
  I: Integer;
begin
  IniciarCache;
  try
    for I := 0 to Pred(Count) do
      Items[I].Calcular(ARecalcular);
  finally
    FinalizarCache;
  end;
end;

procedure TEstatisticas.CalcularEmThread(const ARecalcular: Boolean);
var
  lThread: TThreadCalculoEstatisticas;
begin
  lThread := TThreadCalculoEstatisticas.Create(True);
  try
    lThread.Estatisticas := Self;
    lThread.Recalcular := ARecalcular;
    lThread.Start;
    while not lThread.Finished do
      Application.ProcessMessages;
    lThread.WaitFor;
    if lThread.Erro <> '' then
      raise lThread.ClasseErro.Create(lThread.Erro);
  finally
    FreeAndNil(lThread);
  end;
end;

function TEstatisticas.Resetar(const ANome: string): Boolean;
var
  lEstatistica: TEstatistica;
begin
  lEstatistica := Find(ANome);
  Result := Assigned(lEstatistica);
  if Result then
    lEstatistica.Resetar;
end;

procedure TEstatisticas.ResetarTudo;
var
  I: Integer;
begin
  for I := 0 to Pred(Count) do
    Items[I].Resetar;
end;

function TEstatisticas.MarcarComoCalculada(const ANome: string): Boolean;
var
  lEstatistica: TEstatistica;
begin
  lEstatistica := Find(ANome);
  Result := Assigned(lEstatistica);
  if Result then
    lEstatistica.MarcarComoCalculada;
end;

procedure TEstatisticas.MarcarTudoComoCalculado;
var
  I: Integer;
begin
  for I := 0 to Pred(Count) do
    Items[I].MarcarComoCalculada;
end;

procedure TEstatisticas.LerXML(const ANomeArquivo: string);
begin
  TRLRTTIContext.Instance.FromFile(Self, ANomeArquivo, sfXML);
end;

procedure TEstatisticas.GravarXML(const ANomeArquivo: string);
begin
  TRLRTTIContext.Instance.ToFile(Self, ANomeArquivo, sfXML);
end;

procedure TEstatisticas.IniciarCache;
begin
  Inc(FCaches);
  if (FCaches = 1) and not Assigned(FCache) then
    FCache := TCacheEstatisticas.Create;
end;

function TEstatisticas.CacheInicializado: Boolean;
begin
  Result := Assigned(FCache);
end;

function TEstatisticas.AdicionarCache(const AID: string;
  const AEstatistica: TEstatistica): TCacheEstatistica;
var
  lCache: TCacheEstatistica;
begin
  if Assigned(FCache) then
  begin
    lCache := FCache.Add;
    lCache.ID := AID;
    lCache.Estatistica := AEstatistica;
    Result := lCache;
  end
  else
    Result := nil;
end;

function TEstatisticas.LocalizarCache(const AID: string): TCacheEstatistica;
begin
  if Assigned(FCache) then
    Result := FCache.Find(AID)
  else
    Result := nil;
end;

function TEstatisticas.LocalizarAtrasoDezenasCache(
  const AID: string): TAtrasoDezenas;
var
  lCache: TCacheEstatistica;
begin
  lCache := LocalizarCache(AID);
  if Assigned(lCache) and (lCache.Estatistica is TAtrasoDezenas) then
    Result := TAtrasoDezenas(lCache.Estatistica)
  else
    Result := nil;
end;

procedure TEstatisticas.FinalizarCache;
begin
  if FCaches > 0 then
  begin
    Dec(FCaches);
    if (FCaches = 0) and Assigned(FCache) then
      FreeAndNil(FCache);
  end;
end;

{ TThreadCalculoEstatisticas }

procedure TThreadCalculoEstatisticas.ExecutarThread;
begin
  if Assigned(FEstatisticas) then
    FEstatisticas.Calcular(FRecalcular);
end;

{ TPrecoAposta }

function TPrecoAposta.GetDezenas: Integer;
begin
  Result := StrToInt(GetKey);
end;

procedure TPrecoAposta.SetDezenas(const AValor: Integer);
begin
  SetKey(IntToStr(AValor));
end;

procedure TPrecoAposta.Assign(Source: TPersistent);
var
  lPreco: TPrecoAposta;
begin
  inherited;
  if Source is TPrecoAposta then
  begin
    lPreco := TPrecoAposta(Source);
    Dezenas := lPreco.Dezenas;
    Valor := lPreco.Valor;
  end;
end;

{ TPrecosAposta }

constructor TPrecosAposta.Create;
begin
  inherited;
  HashSize := 16;
end;

function TPrecosAposta.GetItem(const AIndex: Integer): TPrecoAposta;
begin
  Result := TPrecoAposta(inherited Items[AIndex]);
end;

function TPrecosAposta.GetItemClass: TCollectionItemClass;
begin
  Result := TPrecoAposta;
end;

function TPrecosAposta.Add: TPrecoAposta;
begin
  Result := TPrecoAposta(inherited Add);
end;

function TPrecosAposta.Insert(const AIndex: Integer): TPrecoAposta;
begin
  Result := TPrecoAposta(inherited Insert(AIndex));
end;

function TPrecosAposta.Find(const ADezenas: Integer): TPrecoAposta;
begin
  Result := TPrecoAposta(inherited Find(IntToStr(ADezenas)));
end;

function TPrecosAposta.Contains(const ADezenas: Integer): Boolean;
begin
  Result := inherited Contains(IntToStr(ADezenas));
end;

function TPrecosAposta.Remove(const ADezenas: Integer): Boolean;
begin
  Result := inherited Remove(IntToStr(ADezenas));
end;

{ TJogos }

function TJogos.GetItem(const AIndex: Integer): TJogo;
begin
  Result := TJogo(inherited Items[AIndex]);
end;

function TJogos.GetItemClass: TCollectionItemClass;
begin
  Result := TJogo;
end;

function TJogos.Add: TJogo;
begin
  Result := TJogo(inherited Add);
end;

function TJogos.Insert(const AIndex: Integer): TJogo;
begin
  Result := TJogo(inherited Insert(AIndex));
end;

function TJogos.Localizar(const ADezenas: TDezenas): TJogo;
begin
  Result := TJogo(inherited Localizar(ADezenas));
end;

function TJogos.Copiar(const AInicio, ATermino: Integer): TJogos;
begin
  Result := TJogos(inherited Copiar(AInicio, ATermino));
end;

function TJogos.CriarDataSet: TDataSet;
var
  lDataSet: TClientDataSet;
  lField: TField;
  I: Integer;
  J: Integer;
begin
  if Count > 0 then
  begin
    lDataSet := TClientDataSet.Create(nil);
    try
      lDataSet.FieldDefs.Add('Seq.', ftInteger);
      for I := 0 to Pred(Items[0].Dezenas.Count) do
        lDataSet.FieldDefs.Add('D' + IntToStr(I + 1), ftInteger);
      lDataSet.CreateDataSet;
      lDataSet.LogChanges := False;
      lDataSet.DisableControls;
      try
        for I := 0 to Pred(Count) do
        begin
          lDataSet.Append;
          lDataSet.FieldByName('Seq.').AsInteger := I + 1;
          for J := 0 to Pred(Items[I].Dezenas.Count) do
          begin
            lField := lDataSet.FindField('D' + IntToStr(J + 1));
            if Assigned(lField) then
              lField.AsInteger := Items[I].Dezenas[J].Dezena;
          end;

          lDataSet.Post;
        end;

        lDataSet.First;
      finally
        lDataSet.EnableControls;
      end;

      Result := lDataSet;
    except
      FreeAndNil(lDataSet);
      raise;
    end;
  end
  else
    Result := nil;
end;

{ TFiltro }

constructor TFiltro.Create(Collection: TCollection);
begin
  inherited;
//
end;

function TFiltro.GetLoteria: TTipoLoteria;
begin
  if Assigned(FLoteria) then
    Result := FLoteria
  else
    if Collection is TFiltros then
      Result := TFiltros(Collection).Loteria
    else
      Result := nil;
end;

function TFiltro.GetKey: string;
begin
  Result := inherited GetKey;
end;

procedure TFiltro.ChecarLoteria;
begin
  if not Assigned(Loteria) then
    raise ELoteria.Create('Loteria n�o informada para o filtro.');
end;

{ TFiltroPorIntervalo }

constructor TFiltroPorIntervalo.Create(Collection: TCollection);
begin
  inherited;
  FMaximo := MaxInt;
end;

procedure TFiltroPorIntervalo.Assign(Source: TPersistent);
var
  lFiltro: TFiltroPorIntervalo;
begin
  inherited;
  if Source is TFiltroPorIntervalo then
  begin
    lFiltro := TFiltroPorIntervalo(Source);
    Minimo := lFiltro.Minimo;
    Maximo := lFiltro.Maximo;
  end;
end;

{ TFiltroPorString.TItens }

procedure TFiltroPorString.TItens.Changed;
begin
  inherited;
  FFiltro.FHashDesatualizado := True;
end;

{ TFiltroPorString }

constructor TFiltroPorString.Create(Collection: TCollection);
begin
  inherited;
  FItens := TItens.Create;
  TItens(FItens).FFiltro := Self;
  FHash := TRLHashTable.Create;
  FHash.CaseSensitive := False;
  FHash.Size := 32;
end;

destructor TFiltroPorString.Destroy;
begin
  FreeAndNil(FHash);
  FreeAndNil(FItens);
  inherited;
end;

function TFiltroPorString.AtualizarHash: Boolean;
var
  I: Integer;
begin
  Result := FHashDesatualizado;
  if Result then
  begin
    FHash.Clear;
    for I := 0 to Pred(FItens.Count) do
      FHash.Add(FItens[I], nil);
    FHashDesatualizado := False;
  end;
end;

procedure TFiltroPorString.Assign(Source: TPersistent);
begin
  inherited;
  if Source is TFiltroPorString then
    Itens.Assign(TFiltroPorString(Source).Itens);
end;

{ TFiltroPorPares }

constructor TFiltroPorPares.Create(Collection: TCollection);
begin
  inherited;
  SetKey(CFiltroPorPares);
end;

function TFiltroPorPares.Validar(const ADezenas: TDezenas): Boolean;
var
  nPares: Integer;
begin
  if Assigned(ADezenas) then
  begin
    nPares := ADezenas.ContarPares;
    Result := (nPares >= Minimo) and (nPares <= Maximo);
  end
  else
    Result := False;
end;

{ TFiltroPorPrimos }

constructor TFiltroPorPrimos.Create(Collection: TCollection);
begin
  inherited;
  SetKey(CFiltroPorPrimos);
end;

function TFiltroPorPrimos.Validar(const ADezenas: TDezenas): Boolean;
var
  nPrimos: Integer;
begin
  if Assigned(ADezenas) then
  begin
    nPrimos := ADezenas.ContarPrimos;
    Result := (nPrimos >= Minimo) and (nPrimos <= Maximo);
  end
  else
    Result := False;
end;

{ TFiltroPorIntervaloDezenas }

constructor TFiltroPorIntervaloDezenas.Create(Collection: TCollection);
begin
  inherited;
  SetKey(CFiltroPorIntervaloDezenas);
end;

function TFiltroPorIntervaloDezenas.Validar(const ADezenas: TDezenas): Boolean;
var
  nIntervalo: Integer;
begin
  if Assigned(ADezenas) then
  begin
    nIntervalo := ADezenas.LerIntervalo;
    Result := (nIntervalo >= Minimo) and (nIntervalo <= Maximo);
  end
  else
    Result := False;
end;

{ TFiltroPorQuadrante }

constructor TFiltroPorQuadrante.Create(Collection: TCollection);
begin
  inherited;
  SetKey(CFiltroPorQuadrante);
end;

function TFiltroPorQuadrante.Validar(const ADezenas: TDezenas): Boolean;
var
  nQuadrantes: Integer;
begin
  ChecarLoteria;
  if Assigned(ADezenas) then
  begin
    nQuadrantes := Loteria.Quadrantes.QuadrantesPreenchidos(ADezenas);
    Result := (nQuadrantes >= Minimo) and (nQuadrantes <= Maximo);
  end
  else
    Result := False;
end;

{ TFiltroPorRepeticaoDezenas }

constructor TFiltroPorRepeticaoDezenas.Create(Collection: TCollection);
begin
  inherited;
  SetKey(CFiltroPorRepeticaoDezenas);
end;

function TFiltroPorRepeticaoDezenas.Validar(
  const ADezenas: TDezenas): Boolean;
var
  nDezenas: Integer;
begin
  ChecarLoteria;
  if Assigned(ADezenas) and (Loteria.Historico.Count > 0) then
  begin
    nDezenas :=
      Loteria.Historico[
        Pred(Loteria.Historico.Count)].Dezenas.ConferirDezenas(ADezenas);
    Result := (nDezenas >= Minimo) and (nDezenas <= Maximo);
  end
  else
    Result := False;
end;

{ TFiltroPorLinha }

constructor TFiltroPorLinha.Create(Collection: TCollection);
begin
  inherited;
  SetKey(CFiltroPorLinha);
end;

function TFiltroPorLinha.Validar(const ADezenas: TDezenas): Boolean;
var
  lGabarito: TItemGabarito;
begin
  ChecarLoteria;
  if Assigned(ADezenas) then
  begin
    AtualizarHash;
    lGabarito := Loteria.Gabarito.LocalizarPorLinha(ADezenas);
    Result := Assigned(lGabarito) and Hash.Contains(lGabarito.Nome);
  end
  else
    Result := False;
end;

{ TFiltroPorColuna }

constructor TFiltroPorColuna.Create(Collection: TCollection);
begin
  inherited;
  SetKey(CFiltroPorColuna);
end;

function TFiltroPorColuna.Validar(const ADezenas: TDezenas): Boolean;
var
  lGabarito: TItemGabarito;
begin
  ChecarLoteria;
  if Assigned(ADezenas) then
  begin
    AtualizarHash;
    lGabarito := Loteria.Gabarito.LocalizarPorColuna(ADezenas);
    Result := Assigned(lGabarito) and Hash.Contains(lGabarito.Nome);
  end
  else
    Result := False;
end;

{ TFiltroPorSequencia }

constructor TFiltroPorSequencia.Create(Collection: TCollection);
begin
  inherited;
  SetKey(CFiltroPorSequencia);
end;

function TFiltroPorSequencia.Validar(const ADezenas: TDezenas): Boolean;
var
  lGabarito: TItemGabarito;
begin
  ChecarLoteria;
  if Assigned(ADezenas) then
  begin
    AtualizarHash;
    lGabarito := Loteria.Gabarito.LocalizarPorSequencia(ADezenas);
    Result := Assigned(lGabarito) and Hash.Contains(lGabarito.Nome);
  end
  else
    Result := False;
end;

{ TFiltroPorFrequencia }

constructor TFiltroPorFrequencia.Create(Collection: TCollection);
begin
  inherited;
  SetKey(CFiltroPorFrequencia);
end;

function TFiltroPorFrequencia.Validar(const ADezenas: TDezenas): Boolean;
var
  lFrequencia: TFrequenciaDezenas;
begin
  Result := False;
  ChecarLoteria;
  if Assigned(ADezenas) then
  begin
    AtualizarHash;
    lFrequencia := TFrequenciaDezenas(
      Loteria.Estatisticas.Find(CFrequenciaDezenas));
    if Assigned(lFrequencia) then
    begin
      lFrequencia.Calcular(False);
      Result := Hash.Contains(lFrequencia.ObterStringFrequencia(ADezenas));
    end;
  end;
end;

{ TFiltroPorAtraso }

constructor TFiltroPorAtraso.Create(Collection: TCollection);
begin
  inherited;
  SetKey(CFiltroPorAtraso);
end;

function TFiltroPorAtraso.Validar(const ADezenas: TDezenas): Boolean;
var
  lQuociente: TQuocienteAtraso;
begin
  Result := False;
  ChecarLoteria;
  if Assigned(ADezenas) then
  begin
    AtualizarHash;
    lQuociente := TQuocienteAtraso(Loteria.Estatisticas.Find(CQuocienteAtraso));
    if Assigned(lQuociente) then
    begin
      lQuociente.Calcular(False);
      Result := Hash.Contains(lQuociente.ObterStringAtraso(ADezenas));
    end;
  end;
end;

{ TFiltroPorQuocienteFrequencia }

constructor TFiltroPorQuocienteFrequencia.Create(Collection: TCollection);
begin
  inherited;
  SetKey(CFiltroPorQuocienteFrequencia);
end;

function TFiltroPorQuocienteFrequencia.Validar(
  const ADezenas: TDezenas): Boolean;
var
  lFrequencia: TFrequenciaDezenas;
  lQuociente: TQuocienteFrequencia;
  nQuociente: Double;
begin
  Result := False;
  ChecarLoteria;
  if Assigned(ADezenas) then
  begin
    lFrequencia := TFrequenciaDezenas(
      Loteria.Estatisticas.Find(CFrequenciaDezenas));
    if Assigned(lFrequencia) then
    begin
      lFrequencia.Calcular(False);
      nQuociente := SimpleRoundTo(
        lFrequencia.ObterFrequenciaMedia(ADezenas) / lFrequencia.Media, -4);
      lQuociente := TQuocienteFrequencia(
        Loteria.Estatisticas.Find(CQuocienteFrequencia));
      if Assigned(lQuociente) then
      begin
        lQuociente.Calcular(False);
        Result := lQuociente.QuocienteValido(nQuociente);
      end;
    end;
  end;
end;

{ TFiltroPorQuocienteAtraso }

constructor TFiltroPorQuocienteAtraso.Create(Collection: TCollection);
begin
  inherited;
  SetKey(CFiltroPorQuocienteAtraso);
end;

function TFiltroPorQuocienteAtraso.Validar(const ADezenas: TDezenas): Boolean;
var
  lAtraso: TAtrasoDezenas;
  lQuociente: TQuocienteAtraso;
  nQuociente: Double;
begin
  Result := False;
  ChecarLoteria;
  if Assigned(ADezenas) then
  begin
    lAtraso := TAtrasoDezenas(Loteria.Estatisticas.Find(CAtrasoDezenas));
    if Assigned(lAtraso) then
    begin
      lAtraso.Calcular(False);
      nQuociente := SimpleRoundTo(
        lAtraso.ObterAtrasoMedio(ADezenas) / lAtraso.AtrasoMedio);
      lQuociente := TQuocienteAtraso(
        Loteria.Estatisticas.Find(CQuocienteAtraso));
      if Assigned(lQuociente) then
      begin
        lQuociente.Calcular(False);
        Result := lQuociente.QuocienteValido(nQuociente);
      end;
    end;
  end;
end;

{ TFiltros }

constructor TFiltros.Create;
begin
  inherited;
  HashSize := 32;
end;

function TFiltros.GetItem(const AIndex: Integer): TFiltro;
begin
  Result := TFiltro(inherited Items[AIndex]);
end;

function TFiltros.GetItemClass: TCollectionItemClass;
begin
  Result := TFiltro;
end;

function TFiltros.Add: TFiltro;
begin
  Result := TFiltro(inherited Add);
end;

function TFiltros.AdicionarFiltroPorPares: TFiltroPorPares;
begin
  Result := TFiltroPorPares(AddType(TFiltroPorPares));
end;

function TFiltros.AdicionarFiltroPorPrimos: TFiltroPorPrimos;
begin
  Result := TFiltroPorPrimos(AddType(TFiltroPorPrimos));
end;

function TFiltros.AdicionarFiltroPorQuadrante: TFiltroPorQuadrante;
begin
  Result := TFiltroPorQuadrante(AddType(TFiltroPorQuadrante));
end;

function TFiltros.AdicionarFiltroPorRepeticaoDezenas: TFiltroPorRepeticaoDezenas;
begin
  Result := TFiltroPorRepeticaoDezenas(
    AddType(TFiltroPorRepeticaoDezenas));
end;

function TFiltros.AdicionarFiltroPorLinha: TFiltroPorLinha;
begin
  Result := TFiltroPorLinha(AddType(TFiltroPorLinha));
end;

function TFiltros.AdicionarFiltroPorColuna: TFiltroPorColuna;
begin
  Result := TFiltroPorColuna(AddType(TFiltroPorColuna));
end;

function TFiltros.AdicionarFiltroPorSequencia: TFiltroPorSequencia;
begin
  Result := TFiltroPorSequencia(AddType(TFiltroPorSequencia));
end;

function TFiltros.AdicionarFiltroPorFrequencia: TFiltroPorFrequencia;
begin
  Result := TFiltroPorFrequencia(AddType(TFiltroPorFrequencia));
end;

function TFiltros.AdicionarFiltroPorIntervaloDezenas: TFiltroPorIntervaloDezenas;
begin
  Result := TFiltroPorIntervaloDezenas(AddType(TFiltroPorIntervaloDezenas));
end;

function TFiltros.AdicionarFiltroPorAtraso: TFiltroPorAtraso;
begin
  Result := TFiltroPorAtraso(AddType(TFiltroPorAtraso));
end;

function TFiltros.AdicionarFiltroPorQuocienteFrequencia: TFiltroPorQuocienteFrequencia;
begin
  Result := TFiltroPorQuocienteFrequencia(
    AddType(TFiltroPorQuocienteFrequencia));
end;

function TFiltros.AdicionarFiltroPorQuocienteAtraso: TFiltroPorQuocienteAtraso;
begin
  Result := TFiltroPorQuocienteAtraso(AddType(TFiltroPorQuocienteAtraso));
end;

function TFiltros.Insert(const AIndex: Integer): TFiltro;
begin
  Result := TFiltro(inherited Insert(AIndex));
end;

function TFiltros.Find(const ANome: string): TFiltro;
begin
  Result := TFiltro(inherited Find(ANome));
end;

function TFiltros.Contains(const ANome: string): Boolean;
begin
  Result := inherited Contains(ANome);
end;

function TFiltros.Remove(const ANome: string): Boolean;
begin
  Result := inherited Remove(ANome);
end;

function TFiltros.Validar(const ADezenas: TDezenas): Boolean;
var
  I: Integer;
begin
  Result := True;
  I := 0;
  while (I < Count) and Result do
    if not Items[I].Validar(ADezenas) then
      Result := False
    else
      Inc(I);
end;

{ TValorComparativo }

procedure TValorComparativo.Assign(Source: TPersistent);
var
  lValor: TValorComparativo;
begin
  inherited;
  if Source is TValorComparativo then
  begin
    lValor := TValorComparativo(Source);
    Valor := lValor.Valor;
    Quantidade := lValor.Quantidade;
    Percentual := lValor.Percentual;
    PercentualHistorico := lValor.PercentualHistorico;
    Diferenca := lValor.Diferenca;
  end;
end;

{ TValoresComparativo }

function TValoresComparativo.GetItem(const AIndex: Integer): TValorComparativo;
begin
  Result := TValorComparativo(inherited Items[AIndex]);
end;

function TValoresComparativo.GetItemClass: TCollectionItemClass;
begin
  Result := TValorComparativo;
end;

function TValoresComparativo.Add: TValorComparativo;
begin
  Result := TValorComparativo(inherited Add);
end;

function TValoresComparativo.Insert(const AIndex: Integer): TValorComparativo;
begin
  Result := TValorComparativo(inherited Insert(AIndex));
end;

{ TResultadoComparativo }

constructor TResultadoComparativo.Create(Collection: TCollection);
begin
  inherited;
  FValores := TValoresComparativo.Create;
end;

destructor TResultadoComparativo.Destroy;
begin
  FreeAndNil(FValores);
  inherited;
end;

function TResultadoComparativo.GetKey: string;
begin
  Result := inherited GetKey;
end;

procedure TResultadoComparativo.SetKey(const AValue: string);
begin
  inherited;
end;

{ TResultadosComparativo }

function TResultadosComparativo.GetItem(
  const AIndex: Integer): TResultadoComparativo;
begin
  Result := TResultadoComparativo(inherited Items[AIndex]);
end;

function TResultadosComparativo.GetItemClass: TCollectionItemClass;
begin
  Result := TResultadoComparativo;
end;

function TResultadosComparativo.Add: TResultadoComparativo;
begin
  Result := TResultadoComparativo(inherited Add);
end;

function TResultadosComparativo.Insert(
  const AIndex: Integer): TResultadoComparativo;
begin
  Result := TResultadoComparativo(inherited Insert(AIndex));
end;

function TResultadosComparativo.Find(
  const AEstatistica: string): TResultadoComparativo;
begin
  Result := TResultadoComparativo(inherited Find(AEstatistica));
end;

function TResultadosComparativo.Contains(const AEstatistica: string): Boolean;
begin
  Result := inherited Contains(AEstatistica);
end;

function TResultadosComparativo.Remove(const AEstatistica: string): Boolean;
begin
  Result := inherited Remove(AEstatistica);
end;

function TResultadosComparativo.CriarDataSet: TDataSet;
var
  lDataSet: TClientDataSet;
  I: Integer;
  J: Integer;
begin
  lDataSet := TClientDataSet.Create(nil);
  try
    lDataSet.FieldDefs.Add('Descri��o', ftString, 40);
    lDataSet.FieldDefs.Add('Quantidade', ftInteger);
    lDataSet.FieldDefs.Add('Percentual', ftFloat);
    lDataSet.FieldDefs.Add('Percentual hist�rico', ftFloat);
    lDataSet.FieldDefs.Add('Diferen�a', ftFloat);
    lDataSet.CreateDataSet;
    lDataSet.LogChanges := False;
    lDataSet.DisableControls;
    try
      for I := 0 to Pred(Count) do
      begin
        lDataSet.Append;
        lDataSet.FieldByName('Descri��o').AsString := 'Estat�stica: ' + Items[I].Descricao;
        lDataSet.Post;
        for J := 0 to Pred(Items[I].Valores.Count) do
        begin
          lDataSet.Append;
          lDataSet.FieldByName('Descri��o').AsString := 'Valor: ' + Items[I].Valores[J].Valor;
          lDataSet.FieldByName('Quantidade').AsInteger := Items[I].Valores[J].Quantidade;
          lDataSet.FieldByName('Percentual').AsFloat := Items[I].Valores[J].Percentual;
          lDataSet.FieldByName('Percentual hist�rico').AsFloat := Items[I].Valores[J].PercentualHistorico;
          lDataSet.FieldByName('Diferen�a').AsFloat := Items[I].Valores[J].Diferenca;
          lDataSet.Post;
        end;
      end;

      lDataSet.First;
    finally
      lDataSet.EnableControls;
    end;

    Result := lDataSet;
  except
    FreeAndNil(lDataSet);
    raise;
  end;
end;

{ TComparativo }

constructor TComparativo.CreateContainedObject(const AParent: TObject);
begin
  inherited;
  FResultados := TResultadosComparativo.Create;
end;

destructor TComparativo.Destroy;
begin
  FreeAndNil(FResultados);
  inherited;
end;

function TComparativo.GetLoteria: TTipoLoteria;
var
  lTipoLoteria: ITipoLoteria;
begin
  if Assigned(FLoteria) then
    Result := FLoteria
  else
    if Assigned(Parent) and Parent.GetInterface(ITipoLoteria, lTipoLoteria) then
      Result := lTipoLoteria.GetLoteria
    else
      Result := nil;
end;

procedure TComparativo.ChecarLoteria;
begin
  if not Assigned(Loteria) then
    raise ELoteria.Create('Loteria n�o informada para o comparativo.');
end;

procedure TComparativo.ChecarEstatisticas;
begin
  if not Assigned(FEstatisticas) then
    raise ELoteria.Create(
      'Lista de estat�sticas n�o informada para o comparativo.');
end;

function TComparativo.Executar(const AMargem: Double): Boolean;
var
  lEstatisticas: TEstatisticas;
  lEstatistica: TEstatisticaPorContagem;
  lEstatisticaComparativo: TEstatisticaPorContagem;
  lEstatisticaLoteria: TEstatisticaPorContagem;
  lResultado: TResultadoComparativo;
  lValor: TValorComparativo;
  lContagem: TItemContagem;
  nContagem: Integer;
  nMargem: Double;
  nDiferenca: Integer;
  I: Integer;
  J: Integer;
begin
  ChecarLoteria;
  ChecarEstatisticas;
  lEstatisticas := TEstatisticas.Create;
  try
    for I := 0 to Pred(FEstatisticas.Count) do
      if FEstatisticas[I] is TEstatisticaPorContagem then
      begin
        lEstatisticaComparativo := TEstatisticaPorContagem(FEstatisticas[I]);
        if lEstatisticaComparativo.EstatisticaComparacao <> '' then
          lEstatisticaLoteria := TEstatisticaPorContagem(
            Loteria.Estatisticas.Find(
              lEstatisticaComparativo.EstatisticaComparacao))
        else
          lEstatisticaLoteria := TEstatisticaPorContagem(
            Loteria.Estatisticas.Find(lEstatisticaComparativo.Nome));
        lEstatisticaLoteria.Calcular(False);
        lEstatistica := TEstatisticaPorContagem(
          lEstatisticas.AddType(
            TCollectionItemClass(lEstatisticaComparativo.ClassType)));
        lEstatistica.Contagem.Assign(lEstatisticaLoteria.Contagem);
        J := 0;
        while J < lEstatistica.Contagem.Count do
        begin
          lContagem := lEstatisticaComparativo.Contagem.Find(
            lEstatistica.Contagem[J].Valor);
          if not Assigned(lContagem) or SameValue(lContagem.Percentual, 0) then
            lEstatistica.Contagem.Delete(J)
          else
            Inc(J);
        end;

        lEstatistica.CalcularPercentual;
      end;

    Resultados.BeginUpdate;
    try
      Resultados.Clear;
      nContagem := 0;
      nDiferenca := 0;
      for I := 0 to Pred(FEstatisticas.Count) do
        if FEstatisticas[I] is TEstatisticaPorContagem then
        begin
          lEstatisticaComparativo := TEstatisticaPorContagem(FEstatisticas[I]);
          if CompareValue(lEstatisticaComparativo.Margem, 0) = 1 then
            nMargem := lEstatisticaComparativo.Margem
          else
            nMargem := AMargem;
          lEstatistica := TEstatisticaPorContagem(
            lEstatisticas.Find(lEstatisticaComparativo.Nome));
          lResultado := Resultados.Add;
          lResultado.Estatistica := lEstatisticaComparativo.Nome;
          lResultado.Descricao := lEstatisticaComparativo.Descricao;
          for J := 0 to Pred(lEstatisticaComparativo.Contagem.Count) do
          begin
            lContagem := lEstatistica.Contagem.Find(
              lEstatisticaComparativo.Contagem[J].Valor);
            if Assigned(lContagem) and
              not SameValue(
                lEstatisticaComparativo.Contagem[J].Percentual, 0) then
            begin
              lValor := lResultado.Valores.Add;
              lValor.Valor := lEstatisticaComparativo.Contagem[J].Valor;
              lValor.Quantidade := lEstatisticaComparativo.Contagem[J].Quantidade;
              lValor.Percentual := lEstatisticaComparativo.Contagem[J].Percentual;
              lValor.PercentualHistorico := lContagem.Percentual;
              lValor.Diferenca := SimpleRoundTo(
                lValor.Percentual - lValor.PercentualHistorico);
              if SameValue(nMargem, 0) or
                ((CompareValue(lValor.Diferenca, nMargem * -1) <> -1) and
                  (CompareValue(lValor.Diferenca, nMargem) <> 1)) then
                Inc(nDiferenca);
              Inc(nContagem);
            end;
          end;
        end;

      Result := (nContagem > 0) and (nDiferenca = nContagem);
    finally
      Resultados.EndUpdate;
    end;
  finally
    FreeAndNil(lEstatisticas);
  end;
end;

function TComparativo.CriarDataSet: TDataSet;
begin
  Result := FResultados.CriarDataSet;
end;

{ TParametrosAposta }

constructor TParametrosAposta.CreateContainedObject(const AParent: TObject);
begin
  inherited;
  FLinha := TRLStringList.Create;
  FColuna := TRLStringList.Create;
  FSequencia := TRLStringList.Create;
  FFrequencia := TRLStringList.Create;
  FAtraso := TRLStringList.Create;
end;

destructor TParametrosAposta.Destroy;
begin
  FreeAndNil(FAtraso);
  FreeAndNil(FFrequencia);
  FreeAndNil(FSequencia);
  FreeAndNil(FColuna);
  FreeAndNil(FLinha);
  inherited;
end;

function TParametrosAposta.GetLoteria: TTipoLoteria;
var
  lTipoLoteria: ITipoLoteria;
begin
  if Assigned(FLoteria) then
    Result := FLoteria
  else
    if Assigned(Parent) and Parent.GetInterface(ITipoLoteria, lTipoLoteria) then
      Result := lTipoLoteria.GetLoteria
    else
      Result := nil;
end;

procedure TParametrosAposta.ChecarLoteria;
begin
  if not Assigned(Loteria) then
    raise ELoteria.Create(
      'Loteria n�o informada para os par�metros de aposta.');
end;

procedure TParametrosAposta.ChecarQuantidadeJogos;
begin
  if FQuantidadeJogos <= 0 then
    raise ELoteria.Create('Quantidade de jogos inv�lida.');
end;

procedure TParametrosAposta.ChecarPares;
begin
  if (FMinimoPares < 0) then
    raise ELoteria.Create('N�mero m�nimo de pares inv�lido.');
  if (FMaximoPares < 0) or (FMaximoPares - FMinimoPares < 0) or
    (FMaximoPares > Loteria.DezenasSorteio) then
    raise ELoteria.Create('N�mero m�ximo de pares inv�lido.');
end;

procedure TParametrosAposta.ChecarPrimos;
begin
  if (FMinimoPrimos < 0) then
    raise ELoteria.Create('N�mero m�nimo de primos inv�lido.');
  if (FMaximoPrimos < 0) or (FMaximoPrimos - FMinimoPrimos < 0) or
    (FMaximoPrimos > Loteria.DezenasSorteio) then
    raise ELoteria.Create('N�mero m�ximo de primos inv�lido.');
end;

procedure TParametrosAposta.ChecarMargemPares;
begin
  if FMargemPares < 0 then
    raise ELoteria.Create('Margem de pares inv�lida.');
end;

procedure TParametrosAposta.ChecarMargemPrimos;
begin
  if FMargemPrimos < 0 then
    raise ELoteria.Create('Margem de primos inv�lida.');
end;

procedure TParametrosAposta.ChecarQuadrantes;
begin
  if (FMinimoQuadrantes < 0) then
    raise ELoteria.Create('N�mero m�nimo de quadrantes inv�lido.');
  if (FMaximoQuadrantes < 0) or (FMaximoQuadrantes - FMinimoQuadrantes < 0) or
    (FMaximoQuadrantes > Loteria.Quadrantes.Count) then
    raise ELoteria.Create('N�mero m�ximo de quadrantes inv�lido.');
end;

procedure TParametrosAposta.ChecarMargemQuadrantes;
begin
  if FMargemQuadrantes < 0 then
    raise ELoteria.Create('Margem de quadrantes inv�lida.');
end;

procedure TParametrosAposta.ChecarRepeticaoDezenas;
begin
  if (FMinimoRepeticaoDezenas < 0) then
    raise ELoteria.Create('N�mero m�nimo de repeti��o de dezenas inv�lido.');
  if (FMaximoRepeticaoDezenas < 0) or
    (FMaximoRepeticaoDezenas - FMinimoRepeticaoDezenas < 0) or
    (FMaximoRepeticaoDezenas > Loteria.DezenasSorteio) then
    raise ELoteria.Create('N�mero m�ximo de repeti��o de dezenas inv�lido.');
end;

procedure TParametrosAposta.ChecarMargemRepeticaoDezenas;
begin
  if FMargemRepeticaoDezenas < 0 then
    raise ELoteria.Create('Margem de repeti��o de dezenas inv�lida.');
end;

procedure TParametrosAposta.ChecarMargemLinha;
begin
  if FMargemLinha < 0 then
    raise ELoteria.Create('Margem de linha inv�lida.');
end;

procedure TParametrosAposta.ChecarMargemColuna;
begin
  if FMargemColuna < 0 then
    raise ELoteria.Create('Margem de coluna inv�lida.');
end;

procedure TParametrosAposta.ChecarMargemSequencia;
begin
  if FMargemSequencia < 0 then
    raise ELoteria.Create('Margem de sequ�ncia inv�lida.');
end;

procedure TParametrosAposta.ChecarMargemFrequencia;
begin
  if FMargemFrequencia < 0 then
    raise ELoteria.Create('Margem de frequ�ncia inv�lida.');
end;

procedure TParametrosAposta.ChecarMargemAtraso;
begin
  if FMargemAtraso < 0 then
    raise ELoteria.Create('Margem de atraso inv�lida.');
end;

procedure TParametrosAposta.Assign(Source: TPersistent);
var
  lParametros: TParametrosAposta;
begin
  if Source is TParametrosAposta then
  begin
    lParametros := TParametrosAposta(Source);
    QuantidadeJogos := lParametros.QuantidadeJogos;
    MinimoPares := lParametros.MinimoPares;
    MaximoPares := lParametros.MaximoPares;
    MinimoPrimos := lParametros.MinimoPrimos;
    MaximoPrimos := lParametros.MaximoPrimos;
    MinimoIntervalo := lParametros.MinimoIntervalo;
    MinimoIntervalo := lParametros.MaximoIntervalo;
    MinimoQuadrantes := lParametros.MinimoQuadrantes;
    MaximoQuadrantes := lParametros.MaximoQuadrantes;
    MinimoRepeticaoDezenas := lParametros.MinimoRepeticaoDezenas;
    MaximoRepeticaoDezenas := lParametros.MaximoRepeticaoDezenas;
    Linha.Assign(lParametros.Linha);
    Coluna.Assign(lParametros.Coluna);
    Sequencia.Assign(lParametros.Sequencia);
    Frequencia.Assign(lParametros.Frequencia);
    Atraso.Assign(lParametros.Atraso);
    MargemPares := lParametros.MargemPares;
    MargemPrimos := lParametros.MargemPrimos;
    MargemIntervalo := lParametros.MargemIntervalo;
    MargemQuadrantes := lParametros.MargemQuadrantes;
    MargemRepeticaoDezenas := lParametros.MargemRepeticaoDezenas;
    MargemLinha := lParametros.MargemLinha;
    MargemColuna := lParametros.MargemColuna;
    MargemSequencia := lParametros.MargemSequencia;
    MargemFrequencia := lParametros.MargemFrequencia;
    MargemAtraso := lParametros.MargemAtraso;
    UsarQuocienteFrequencia := lParametros.UsarQuocienteFrequencia;
    UsarQuocienteAtraso := lParametros.UsarQuocienteAtraso;
  end;
end;

procedure TParametrosAposta.LerXML(const ANomeArquivo: string);
begin
  TRLRTTIContext.Instance.FromFile(Self, ANomeArquivo, sfXML);
end;

procedure TParametrosAposta.GravarXML(const ANomeArquivo: string);
begin
  TRLRTTIContext.Instance.ToFile(Self, ANomeArquivo, sfXML);
end;

function TParametrosAposta.GerarFiltros: TFiltros;
var
  lPares: TFiltroPorPares;
  lPrimos: TFiltroPorPrimos;
  lIntervaloDezenas: TFiltroPorIntervaloDezenas;
  lQuadrante: TFiltroPorQuadrante;
  lRepeticaoDezenas: TFiltroPorRepeticaoDezenas;
  lLinha: TFiltroPorLinha;
  lColuna: TFiltroPorColuna;
  lSequencia: TFiltroPorSequencia;
  lFrequencia: TFiltroPorFrequencia;
  lAtraso: TFiltroPorAtraso;
begin
  ChecarLoteria;
  ChecarPares;
  ChecarPrimos;
  ChecarQuadrantes;
  ChecarRepeticaoDezenas;
  Result := TFiltros.Create;
  Result.Loteria := Loteria;
  Result.BeginUpdate;
  try
    if FMaximoPares > 0 then
    begin
      lPares := Result.AdicionarFiltroPorPares;
      lPares.Minimo := FMinimoPares;
      lPares.Maximo := FMaximoPares;
    end;

    if FMaximoPrimos > 0 then
    begin
      lPrimos := Result.AdicionarFiltroPorPrimos;
      lPrimos.Minimo := FMinimoPrimos;
      lPrimos.Maximo := FMaximoPrimos;
    end;

    if FMaximoIntervalo > 0 then
    begin
      lIntervaloDezenas := Result.AdicionarFiltroPorIntervaloDezenas;
      lIntervaloDezenas.Minimo := FMinimoIntervalo;
      lIntervaloDezenas.Maximo := FMaximoIntervalo;
    end;

    if FMaximoQuadrantes > 0 then
    begin
      lQuadrante := Result.AdicionarFiltroPorQuadrante;
      lQuadrante.Minimo := FMinimoQuadrantes;
      lQuadrante.Maximo := FMaximoQuadrantes;
    end;

    if FMaximoRepeticaoDezenas > 0 then
    begin
      lRepeticaoDezenas := Result.AdicionarFiltroPorRepeticaoDezenas;
      lRepeticaoDezenas.Minimo := FMinimoRepeticaoDezenas;
      lRepeticaoDezenas.Maximo := FMaximoRepeticaoDezenas;
    end;

    if FLinha.Count > 0 then
    begin
      lLinha := Result.AdicionarFiltroPorLinha;
      lLinha.Itens.Assign(FLinha);
    end;

    if FColuna.Count > 0 then
    begin
      lColuna := Result.AdicionarFiltroPorColuna;
      lColuna.Itens.Assign(FColuna);
    end;

    if FSequencia.Count > 0 then
    begin
      lSequencia := Result.AdicionarFiltroPorSequencia;
      lSequencia.Itens.Assign(FSequencia);
    end;

    if FFrequencia.Count > 0 then
    begin
      lFrequencia := Result.AdicionarFiltroPorFrequencia;
      lFrequencia.Itens.Assign(FFrequencia);
    end;

    if FAtraso.Count > 0 then
    begin
      lAtraso := Result.AdicionarFiltroPorAtraso;
      lAtraso.Itens.Assign(FAtraso);
    end;

    if FUsarQuocienteFrequencia then
      Result.AdicionarFiltroPorQuocienteFrequencia;
    if FUsarQuocienteAtraso then
      Result.AdicionarFiltroPorQuocienteAtraso;
  finally
    Result.EndUpdate;
  end;
end;

procedure TParametrosAposta.GerarString(const AStrings: TStrings);
begin
  if Assigned(AStrings) then
  begin
    AStrings.BeginUpdate;
    try
      AStrings.Clear;
      AStrings.Add(Format('Jogos: %d', [FQuantidadeJogos]));
      if FMaximoPares > 0 then
        AStrings.Add(
          Format('Pares: %d-%d%s',
            [FMinimoPares, FMaximoPares,
              IfThen(FMargemPares > 0,
                Format(' (%s%%)', [FloatToStr(FMargemPares)]))]));
      if FMaximoPrimos > 0 then
        AStrings.Add(
          Format('Primos: %d-%d%s',
            [FMinimoPrimos, FMaximoPrimos,
              IfThen(FMargemPrimos > 0,
                Format(' (%s%%)', [FloatToStr(FMargemPrimos)]))]));
      if FMaximoQuadrantes > 0 then
        AStrings.Add(
          Format('Quadrantes: %d-%d%s',
            [FMinimoQuadrantes, FMaximoQuadrantes,
              IfThen(FMargemQuadrantes > 0,
                Format(' (%s%%)', [FloatToStr(FMargemQuadrantes)]))]));
      if FMaximoRepeticaoDezenas > 0 then
        AStrings.Add(
          Format('Repeti��o: %d-%d%s',
            [FMinimoRepeticaoDezenas, FMaximoRepeticaoDezenas,
              IfThen(FMargemRepeticaoDezenas > 0,
                Format(' (%s%%)', [FloatToStr(FMargemRepeticaoDezenas)]))]));
      if FLinha.Count > 0 then
        AStrings.Add(Format('Linha: %s%s',
          [TRotinas.Concatenar(FLinha),
            IfThen(FMargemLinha > 0,
              Format(' (%s%%)', [FloatToStr(FMargemLinha)]))]));
      if FColuna.Count > 0 then
        AStrings.Add(Format('Coluna: %s%s',
          [TRotinas.Concatenar(FColuna),
            IfThen(FMargemColuna > 0,
              Format(' (%s%%)', [FloatToStr(FMargemColuna)]))]));
      if FSequencia.Count > 0 then
        AStrings.Add(Format('Sequ�ncia: %s%s',
          [TRotinas.Concatenar(FSequencia),
            IfThen(FMargemSequencia > 0,
              Format(' (%s%%)', [FloatToStr(FMargemSequencia)]))]));
      if FFrequencia.Count > 0 then
        AStrings.Add(Format('Frequ�ncia: %s%s',
          [TRotinas.Concatenar(FFrequencia),
            IfThen(FMargemFrequencia > 0,
              Format(' (%s%%)', [FloatToStr(FMargemFrequencia)]))]));
      if FAtraso.Count > 0 then
        AStrings.Add(Format('Atraso: %s%s',
          [TRotinas.Concatenar(FAtraso),
            IfThen(FMargemAtraso > 0,
              Format(' (%s%%)', [FloatToStr(FMargemAtraso)]))]));
      if FUsarQuocienteFrequencia then
        AStrings.Add(
          'Quociente de frequ�ncia: ' +
            IfThen(FUsarQuocienteFrequencia, 'Sim', 'N�o'));
      if FUsarQuocienteAtraso then
        AStrings.Add(
          'Quociente de atraso: ' +
            IfThen(FUsarQuocienteAtraso, 'Sim', 'N�o'));
    finally
      AStrings.EndUpdate;
    end;
  end;
end;

procedure TParametrosAposta.ConfigurarEstatisticas(
  const AEstatisticas: TEstatisticas);
var
  lEstatistica: TEstatistica;
begin
  ChecarMargemPares;
  ChecarMargemPrimos;
  ChecarMargemQuadrantes;
  ChecarMargemRepeticaoDezenas;
  ChecarMargemLinha;
  ChecarMargemColuna;
  ChecarMargemSequencia;
  ChecarMargemFrequencia;
  ChecarMargemAtraso;
  if Assigned(AEstatisticas) then
  begin
    lEstatistica := AEstatisticas.Find(COcorrenciaPorPares);
    if lEstatistica is TOcorrenciaPorPares then
      TOcorrenciaPorPares(lEstatistica).Margem := SimpleRoundTo(FMargemPares);
    lEstatistica := AEstatisticas.Find(COcorrenciaPorPrimos);
    if lEstatistica is TOcorrenciaPorPrimos then
      TOcorrenciaPorPrimos(lEstatistica).Margem := SimpleRoundTo(FMargemPrimos);
    lEstatistica := AEstatisticas.Find(COcorrenciaPorQuadrante);
    if lEstatistica is TOcorrenciaPorQuadrante then
      TOcorrenciaPorQuadrante(lEstatistica).Margem := SimpleRoundTo(
        FMargemQuadrantes);
    lEstatistica := AEstatisticas.Find(COcorrenciaPorRepeticaoDezenas);
    if lEstatistica is TOcorrenciaPorRepeticaoDezenas then
      TOcorrenciaPorRepeticaoDezenas(lEstatistica).Margem := SimpleRoundTo(
        FMargemRepeticaoDezenas);
    lEstatistica := AEstatisticas.Find(COcorrenciaPorLinha);
    if lEstatistica is TOcorrenciaPorLinha then
      TOcorrenciaPorLinha(lEstatistica).Margem := SimpleRoundTo(
        FMargemLinha);
    if lEstatistica is TOcorrenciaPorColuna then
      TOcorrenciaPorColuna(lEstatistica).Margem := SimpleRoundTo(
        FMargemColuna);
    lEstatistica := AEstatisticas.Find(COcorrenciaPorSequencia);
    if lEstatistica is TOcorrenciaPorSequencia then
      TOcorrenciaPorSequencia(lEstatistica).Margem := SimpleRoundTo(
        FMargemSequencia);
    lEstatistica := AEstatisticas.Find(CFrequencia);
    if lEstatistica is TFrequencia then
      TFrequencia(lEstatistica).Margem := SimpleRoundTo(FMargemFrequencia);
    if lEstatistica is TAtraso then
      TAtraso(lEstatistica).Margem := SimpleRoundTo(FMargemAtraso);
  end;
end;

{ TAcerto }

function TAcerto.GetDezenas: Integer;
begin
  Result := StrToInt(GetKey);
end;

procedure TAcerto.SetDezenas(const AValue: Integer);
begin
  SetKey(IntToStr(AValue));
end;

procedure TAcerto.Assign(Source: TPersistent);
var
  lAcerto: TAcerto;
begin
  inherited;
  if Source is TAcerto then
  begin
    lAcerto := TAcerto(Source);
    Dezenas := lAcerto.Dezenas;
    Quantidade := lAcerto.Quantidade;
  end;
end;

{ TAcertos }

constructor TAcertos.Create;
begin
  inherited;
  HashSize := 16;
end;

function TAcertos.GetItem(const AIndex: Integer): TAcerto;
begin
  Result := TAcerto(inherited Items[AIndex]);
end;

function TAcertos.GetItemClass: TCollectionItemClass;
begin
  Result := TAcerto;
end;

function TAcertos.Add: TAcerto;
begin
  Result := TAcerto(inherited Add);
end;

function TAcertos.Insert(const AIndex: Integer): TAcerto;
begin
  Result := TAcerto(inherited Insert(AIndex));
end;

function TAcertos.Find(const ADezenas: Integer): TAcerto;
begin
  Result := TAcerto(inherited Find(IntToStr(ADezenas)));
end;

function TAcertos.Contains(const ADezenas: Integer): Boolean;
begin
  Result := inherited Contains(IntToStr(ADezenas));
end;

function TAcertos.Remove(const ADezenas: Integer): Boolean;
begin
  Result := inherited Remove(IntToStr(ADezenas));
end;

procedure TAcertos.Somar(const AAcertos: TAcertos);
var
  lAcerto: TAcerto;
  I: Integer;
begin
  if Assigned(AAcertos) then
  begin
    BeginUpdate;
    try
      for I := 0 to Pred(AAcertos.Count) do
      begin
        lAcerto := Find(AAcertos[I].Dezenas);
        if not Assigned(lAcerto) then
        begin
          lAcerto := Add;
          lAcerto.Dezenas := AAcertos[I].Dezenas;
        end;

        lAcerto.Quantidade := lAcerto.Quantidade + AAcertos[I].Quantidade;
      end;
    finally
      EndUpdate;
    end;
  end;
end;

{ TAposta }

constructor TAposta.Create;
begin
  inherited;
  FParametros := TParametrosAposta.CreateContainedObject(Self);
  FJogos := TJogos.Create;
  FEstatisticas := TEstatisticas.CreateParented(Self);
  FEstatisticas.Sorteios := FJogos;
  FComparativo := TComparativo.CreateContainedObject(Self);
  FComparativo.Estatisticas := FEstatisticas;
end;

destructor TAposta.Destroy;
begin
  FreeAndNil(FComparativo);
  FreeAndNil(FEstatisticas);
  FreeAndNil(FJogos);
  FreeAndNil(FParametros);
  inherited;
end;

function TAposta.GetLoteria: TTipoLoteria;
begin
  Result := FLoteria;
end;

procedure TAposta.DispararEventoJogo(const ADezenas: TDezenas;
  const AValido: Boolean; var AContinuar: Boolean);
begin
  if Assigned(FAoGerarJogo) then
    FAoGerarJogo(Self, ADezenas, AValido, AContinuar);
end;

procedure TAposta.ChecarLoteria;
begin
  if not Assigned(FLoteria) then
    raise ELoteria.Create('Loteria n�o informada para a aposta.');
end;

procedure TAposta.Assign(Source: TPersistent);
begin
  inherited;
  if Source is TAposta then
    Parametros.Assign(TAposta(Source).Parametros);
end;

procedure TAposta.LerXML(const ANomeArquivo: string);
begin
  TRLRTTIContext.Instance.FromFile(Self, ANomeArquivo, sfXML);
end;

procedure TAposta.GravarXML(const ANomeArquivo: string);
begin
  TRLRTTIContext.Instance.ToFile(Self, ANomeArquivo, sfXML);
end;

procedure TAposta.CopiarEstatisticasLoteria(const AConfigurar: Boolean);
var
  lEstatistica: TEstatisticaPorContagem;
  lEstatisticaAposta: TEstatisticaPorContagem;
  I: Integer;
begin
  ChecarLoteria;
  FEstatisticas.BeginUpdate;
  try
    for I := 0 to Pred(Loteria.Estatisticas.Count) do
      if Loteria.Estatisticas[I] is TEstatisticaPorContagem then
      begin
        lEstatistica := TEstatisticaPorContagem(Loteria.Estatisticas[I]);
        if lEstatistica.AplicarComparativo and
          not FEstatisticas.Contains(lEstatistica.Nome) then
        begin
          lEstatisticaAposta := TEstatisticaPorContagem(
            FEstatisticas.AddType(
              TCollectionItemClass(lEstatistica.ClassType)));
          lEstatisticaAposta.EstatisticaComparacao :=
            lEstatistica.EstatisticaComparacao;
          lEstatisticaAposta.Margem := lEstatistica.Margem;
        end;
      end;
  finally
    FEstatisticas.EndUpdate;
  end;

  if AConfigurar then
    ConfigurarEstatisticas;
end;

procedure TAposta.ConfigurarEstatisticas;
begin
  FParametros.ConfigurarEstatisticas(FEstatisticas);
end;

procedure TAposta.GerarString(const AStrings: TStrings);
begin
  FParametros.GerarString(AStrings);
end;

function TAposta.GerarFiltros: TFiltros;
begin
  Result := FParametros.GerarFiltros;
end;

function TAposta.GerarJogos(const AFiltros: TFiltros): Boolean;
var
  lFiltros: TFiltros;
  lDezenas: TDezenas;
  bValido: Boolean;
  nDezenas: Integer;
  nDezena: Integer;
  I: Integer;
begin
  ChecarLoteria;
  FParametros.ChecarQuantidadeJogos;
  if Assigned(AFiltros) then
    lFiltros := AFiltros
  else
    lFiltros := GerarFiltros;
  try
    Randomize;
    FJogos.BeginUpdate;
    try
      FJogos.Clear;
      lDezenas := TDezenas.Create;
      try
        I := 0;
        Result := True;
        while (I < FParametros.QuantidadeJogos) and Result do
        begin
          lDezenas.Clear;
          nDezenas := 0;
          while nDezenas < Loteria.DezenasSorteio do
          begin
            nDezena := RandomRange(1, Loteria.MaiorDezena + 1);
            if not lDezenas.Contains(nDezena) then
            begin
              lDezenas.Add.Dezena := nDezena;
              Inc(nDezenas);
            end;
          end;

          bValido := False;
          if not Assigned(FJogos.Localizar(lDezenas)) then
          begin
            lDezenas.Ordenar;
            if not Assigned(lFiltros) or lFiltros.Validar(lDezenas) then
            begin
              FJogos.Add.Dezenas.Assign(lDezenas);
              Inc(I);
              bValido := True;
            end;
          end;

          DispararEventoJogo(lDezenas, bValido, Result);
        end;
      finally
        FreeAndNil(lDezenas);
      end;
    finally
      FJogos.EndUpdate;
    end;
  finally
    if lFiltros <> AFiltros then
      FreeAndNil(lFiltros);
  end;
end;

function TAposta.GerarJogosComEstatistica(const AFiltros: TFiltros;
  out ATentativas: Integer): Boolean;
var
  lFiltros: TFiltros;
  bJogosGerados: Boolean;
begin
  ATentativas := 0;
  if Assigned(AFiltros) then
    lFiltros := AFiltros
  else
    lFiltros := GerarFiltros;
  try
    repeat
      Inc(ATentativas);
      bJogosGerados := GerarJogos(lFiltros);
      FEstatisticas.Calcular(True);
    until not bJogosGerados or FComparativo.Executar(0);
    Result := bJogosGerados;
  finally
    if lFiltros <> AFiltros then
      FreeAndNil(lFiltros);
  end;
end;

function TAposta.GerarJogosComEstatisticaEmThread(const AFiltros: TFiltros;
  out ATentativas: Integer): Boolean;
var
  lThread: TThreadGeracaoJogosAposta;
begin
  lThread := TThreadGeracaoJogosAposta.Create(True);
  try
    lThread.Aposta := Self;
    lThread.Filtros := AFiltros;
    lThread.Start;
    while not lThread.Finished do
      Application.ProcessMessages;
    lThread.WaitFor;
    if lThread.Erro <> '' then
      raise lThread.ClasseErro.Create(lThread.Erro);
    ATentativas := lThread.Tentativas;
    Result := lThread.Resultado;
  finally
    FreeAndNil(lThread);
  end;
end;

function TAposta.VerificarAcerto(const ADezenas: TDezenas;
  const ADezenasPorAcerto: Integer): TAcertos;
var
  lAcerto: TAcerto;
  nDezenasPorAcerto: Integer;
  nConferencia: Integer;
  I: Integer;
begin
  ChecarLoteria;
  if Assigned(ADezenas) then
  begin
    if ADezenasPorAcerto > 0 then
      nDezenasPorAcerto := ADezenasPorAcerto
    else
      nDezenasPorAcerto := Loteria.DezenasPremiacaoMinima;
    Result := TAcertos.Create;
    try
      Result.BeginUpdate;
      try
        for I := 0 to Pred(FJogos.Count) do
        begin
          nConferencia := ADezenas.ConferirDezenas(FJogos[I].Dezenas);
          if nConferencia >= nDezenasPorAcerto then
          begin
            lAcerto := Result.Find(nConferencia);
            if not Assigned(lAcerto) then
            begin
              lAcerto := Result.Add;
              lAcerto.Dezenas := nConferencia;
            end;

            lAcerto.Quantidade := lAcerto.Quantidade + 1;
          end;
        end;
      finally
        Result.EndUpdate;
      end;
    except
      FreeAndNil(Result);
      raise;
    end;
  end
  else
    Result := nil;
end;

function TAposta.CriarDataSetJogos: TDataSet;
begin
  Result := FJogos.CriarDataSet;
end;

function TAposta.CriarDataSetComparativo: TDataSet;
begin
  Result := FComparativo.CriarDataSet;
end;

{ TThreadGeracaoJogosAposta }

procedure TThreadGeracaoJogosAposta.ExecutarThread;
begin
  if Assigned(FAposta) then
    FResultado := FAposta.GerarJogosComEstatistica(FFiltros, FTentativas);
end;

{ TParametrosSimulador }

constructor TParametrosSimulador.CreateContainedObject(const AParent: TObject);
begin
  inherited;
  FUsarParesSorteio := True;
  FUsarPrimosSorteio := True;
  FUsarIntervaloSorteio := True;
  FUsarLinhaSorteio := True;
  FUsarColunaSorteio := True;
  FUsarSequenciaSorteio := True;
end;

procedure TParametrosSimulador.ChecarNumeroAcertos;
begin
  if FNumeroAcertos <= 0 then
    raise ELoteria.Create('N�mero de acertos inv�lido.');
end;

procedure TParametrosSimulador.ChecarDezenasPorAcerto;
begin
  if (FDezenasPorAcerto < Loteria.DezenasPremiacaoMinima) or
    (FDezenasPorAcerto > Loteria.DezenasSorteio) then
    raise ELoteria.Create('N�mero de dezenas por acerto inv�lido.');
end;

procedure TParametrosSimulador.ChecarSorteios;
begin
  if (FSorteioInicial < 1) then
    raise ELoteria.Create('Sorteio inicial inv�lido.');
  if (FSorteioFinal < 1) or (FSorteioFinal - FSorteioInicial < 0) or
    (FSorteioFinal > Loteria.Historico.Count) then
    raise ELoteria.Create('Sorteio final inv�lido.');
end;

procedure TParametrosSimulador.Assign(Source: TPersistent);
var
  lParametros: TParametrosSimulador;
begin
  if Source is TParametrosSimulador then
  begin
    lParametros := TParametrosSimulador(Source);
    NumeroAcertos := lParametros.NumeroAcertos;
    DezenasPorAcerto := lParametros.DezenasPorAcerto;
    SorteioInicial := lParametros.SorteioInicial;
    SorteioFinal := lParametros.SorteioFinal;
    UsarParesSorteio := lParametros.UsarParesSorteio;
    UsarPrimosSorteio := lParametros.UsarPrimosSorteio;
    UsarIntervaloSorteio := lParametros.UsarIntervaloSorteio;
    UsarLinhaSorteio := lParametros.UsarLinhaSorteio;
    UsarColunaSorteio := lParametros.UsarColunaSorteio;
    UsarSequenciaSorteio := lParametros.UsarSequenciaSorteio;
  end;
end;

function TParametrosSimulador.GerarSorteios: TSorteios;
begin
  ChecarLoteria;
  ChecarSorteios;
  Result := Loteria.Historico.Copiar(FSorteioInicial - 1,
    FSorteioFinal - 1);
end;

function TParametrosSimulador.UsarParametrosSorteio: Boolean;
begin
  Result := FUsarParesSorteio or FUsarPrimosSorteio or FUsarIntervaloSorteio or
    FUsarLinhaSorteio or FUsarColunaSorteio or FUsarSequenciaSorteio;
end;

{ TResultadoSimulador }

constructor TResultadoSimulador.Create(Collection: TCollection);
begin
  inherited;
  FAcertos := TAcertos.Create;
end;

destructor TResultadoSimulador.Destroy;
begin
  FreeAndNil(FAcertos);
  inherited;
end;

procedure TResultadoSimulador.Assign(Source: TPersistent);
var
  lResultado: TResultadoSimulador;
begin
  inherited;
  if Source is TResultadoSimulador then
  begin
    lResultado := TResultadoSimulador(Source);
    Pares := lResultado.Pares;
    Primos := lResultado.Primos;
    IntervaloDezenas := lResultado.IntervaloDezenas;
    Linha := lResultado.Linha;
    Coluna := lResultado.Coluna;
    Sequencia := lResultado.Sequencia;
    Tentativas := lResultado.Tentativas;
    MediaTentativas := lResultado.MediaTentativas;
    MenorIntervalo := lResultado.MenorIntervalo;
    MaiorIntervalo := lResultado.MaiorIntervalo;
    Acertos.Assign(lResultado.Acertos);
  end;
end;

{ TResultadosSimulador }

function TResultadosSimulador.GetItem(
  const AIndex: Integer): TResultadoSimulador;
begin
  Result := TResultadoSimulador(inherited Items[AIndex]);
end;

function TResultadosSimulador.GetLoteria: TTipoLoteria;
var
  lTipoLoteria: ITipoLoteria;
begin
  if Assigned(FLoteria) then
    Result := FLoteria
  else
    if Assigned(Parent) and Parent.GetInterface(ITipoLoteria, lTipoLoteria) then
      Result := lTipoLoteria.GetLoteria
    else
      Result := nil;
end;

function TResultadosSimulador.GetItemClass: TCollectionItemClass;
begin
  Result := TResultadoSimulador;
end;

procedure TResultadosSimulador.ChecarLoteria;
begin
  if not Assigned(Loteria) then
    raise ELoteria.Create(
      'Loteria n�o informada para os resuldados do simulador.');
end;

function TResultadosSimulador.Add: TResultadoSimulador;
begin
  Result := TResultadoSimulador(inherited Add);
end;

function TResultadosSimulador.Insert(
  const AIndex: Integer): TResultadoSimulador;
begin
  Result := TResultadoSimulador(inherited Insert(AIndex));
end;

function TResultadosSimulador.Localizar(
  const ADezenas: TDezenas): TResultadoSimulador;
begin
  Result := TResultadoSimulador(inherited Localizar(ADezenas));
end;

function TResultadosSimulador.Copiar(const AInicio,
  ATermino: Integer): TResultadoSimulador;
begin
  Result := TResultadoSimulador(inherited Copiar(AInicio, ATermino));
end;

function TResultadosSimulador.CriarDataSet: TDataSet;
var
  lDataSet: TClientDataSet;
  lField: TField;
  lAcerto: TAcerto;
  I: Integer;
  J: Integer;
begin
  ChecarLoteria;
  if Count > 0 then
  begin
    lDataSet := TClientDataSet.Create(nil);
    try
      lDataSet.FieldDefs.Add('Seq.', ftInteger);
      for I := 0 to Pred(Items[0].Dezenas.Count) do
        lDataSet.FieldDefs.Add('D' + IntToStr(I + 1), ftInteger);
      lDataSet.FieldDefs.Add('Pares', ftInteger);
      lDataSet.FieldDefs.Add('Primos', ftInteger);
      if Loteria.Gabarito.Count > 0 then
      begin
        lDataSet.FieldDefs.Add('Linha', ftString, 10);
        lDataSet.FieldDefs.Add('Coluna', ftString, 10);
        lDataSet.FieldDefs.Add('Sequ�ncia', ftString, 10);
      end;

      lDataSet.FieldDefs.Add('Tentativas', ftInteger);
      lDataSet.FieldDefs.Add('M�dia tentativas', ftInteger);
      lDataSet.FieldDefs.Add('Menor intervalo', ftInteger);
      lDataSet.FieldDefs.Add('Maior intervalo', ftInteger);
      for I := Loteria.DezenasPremiacaoMinima to Loteria.DezenasSorteio do
        lDataSet.FieldDefs.Add(Format('%d acertos', [I]), ftInteger);
      lDataSet.CreateDataSet;
      lDataSet.LogChanges := False;
      lDataSet.DisableControls;
      try
        for I := 0 to Pred(Count) do
        begin
          lDataSet.Append;
          lDataSet.FieldByName('Seq.').AsInteger := I + 1;
          for J := 0 to Pred(Items[I].Dezenas.Count) do
          begin
            lField := lDataSet.FindField('D' + IntToStr(J + 1));
            if Assigned(lField) then
              lField.AsInteger := Items[I].Dezenas[J].Dezena;
          end;

          lDataSet.FieldByName('Pares').AsInteger := Items[I].Pares;
          lDataSet.FieldByName('Primos').AsInteger := Items[I].Primos;
          if Loteria.Gabarito.Count > 0 then
          begin
            lDataSet.FieldByName('Linha').AsString := Items[I].Linha;
            lDataSet.FieldByName('Coluna').AsString := Items[I].Coluna;
            lDataSet.FieldByName('Sequ�ncia').AsString := Items[I].Sequencia;
          end;

          lDataSet.FieldByName('Tentativas').AsInteger := Items[I].Tentativas;
          lDataSet.FieldByName('M�dia tentativas').AsInteger := Items[I].MediaTentativas;
          lDataSet.FieldByName('Menor intervalo').AsInteger := Items[I].MenorIntervalo;
          lDataSet.FieldByName('Maior intervalo').AsInteger := Items[I].MaiorIntervalo;
          for J := Loteria.DezenasPremiacaoMinima to Loteria.DezenasSorteio do
          begin
            lAcerto := Items[I].Acertos.Find(J);
            if Assigned(lAcerto) then
              lDataSet.FieldByName(
                Format('%d acertos', [J])).AsInteger := lAcerto.Quantidade;
          end;

          lDataSet.Post;
        end;

        lDataSet.First;
      finally
        lDataSet.EnableControls;
      end;

      Result := lDataSet;
    except
      FreeAndNil(lDataSet);
      raise;
    end;
  end
  else
    Result := nil;
end;

{ TSimulador.TApostaSimulador }

procedure TSimulador.TApostaSimulador.DispararEventoJogo(
  const ADezenas: TDezenas; const AValido: Boolean; var AContinuar: Boolean);
begin
  if Assigned(FSimulador) then
    FSimulador.DispararEventoJogo(ADezenas, AValido, AContinuar);
end;

{ TSimulador }

constructor TSimulador.Create;
begin
  inherited;
  FParametros := TParametrosSimulador.CreateContainedObject(Self);
  FResultados := TResultadosSimulador.CreateParented(Self);
end;

destructor TSimulador.Destroy;
begin
  FreeAndNil(FResultados);
  FreeAndNil(FParametros);
  inherited;
end;

function TSimulador.GetLoteria: TTipoLoteria;
begin
  Result := FLoteria;
end;

procedure TSimulador.ChecarLoteria;
begin
  if not Assigned(FLoteria) then
    raise ELoteria.Create('Loteria n�o informada para o simulador.');
end;

procedure TSimulador.DispararEventoJogo(const ADezenas: TDezenas;
  const AValido: Boolean; var AContinuar: Boolean);
begin
  if Assigned(FAoGerarJogo) then
    FAoGerarJogo(Self, ADezenas, AValido, AContinuar);
end;

procedure TSimulador.Assign(Source: TPersistent);
begin
  if Source is TSimulador then
    Parametros.Assign(TSimulador(Source).Parametros);
end;

procedure TSimulador.LerXML(const ANomeArquivo: string);
begin
  TRLRTTIContext.Instance.FromFile(Self, ANomeArquivo, sfXML);
end;

procedure TSimulador.GravarXML(const ANomeArquivo: string);
begin
  TRLRTTIContext.Instance.ToFile(Self, ANomeArquivo, sfXML);
end;

function TSimulador.Executar: Boolean;
var
  lSorteios: TSorteios;
  lResultado: TResultadoSimulador;
  lAposta: TApostaSimulador;
  lFiltros: TFiltros;
  lAcertos: TAcertos;
  nTentativasAcerto: Integer;
  nTentativasJogos: Integer;
  I: Integer;
  J: Integer;
begin
  ChecarLoteria;
  FParametros.ChecarQuantidadeJogos;
  FParametros.ChecarNumeroAcertos;
  FParametros.ChecarDezenasPorAcerto;
  FParametros.ChecarSorteios;
  lSorteios := FParametros.GerarSorteios;
  try
    FResultados.BeginUpdate;
    try
      FResultados.Clear;
      lAposta := TApostaSimulador.Create;
      try
        lAposta.Simulador := Self;
        lAposta.Loteria := Loteria;
        lAposta.Parametros.Assign(FParametros);
        lAposta.CopiarEstatisticasLoteria(True);
        if not FParametros.UsarParametrosSorteio then
          lFiltros := lAposta.GerarFiltros;
        try
          I := 0;
          Result := True;
          while (I < lSorteios.Count) and Result do
          begin
            lResultado := FResultados.Add;
            lResultado.Dezenas.Assign(lSorteios[I].Dezenas);
            lResultado.Pares := lResultado.Dezenas.ContarPares;
            lResultado.Primos := lResultado.Dezenas.ContarPrimos;
            if Loteria.Gabarito.Count > 0 then
            begin
              lResultado.Linha := Loteria.Gabarito.LocalizarPorLinha(
                lSorteios[I].Dezenas).Nome;
              lResultado.Coluna := Loteria.Gabarito.LocalizarPorColuna(
                lSorteios[I].Dezenas).Nome;
              lResultado.Sequencia := Loteria.Gabarito.LocalizarPorSequencia(
                lSorteios[I].Dezenas).Nome;
            end;

            if FParametros.UsarParesSorteio then
            begin
              lAposta.Parametros.MinimoPares := lResultado.Pares;
              lAposta.Parametros.MaximoPares := lResultado.Pares;
            end;

            if FParametros.UsarPrimosSorteio then
            begin
              lAposta.Parametros.MinimoPrimos := lResultado.Primos;
              lAposta.Parametros.MaximoPrimos := lResultado.Primos;
            end;

            if FParametros.UsarIntervaloSorteio then
            begin
              lAposta.Parametros.MinimoIntervalo := lResultado.IntervaloDezenas;
              lAposta.Parametros.MaximoIntervalo := lResultado.IntervaloDezenas;
            end;

            if FParametros.UsarLinhaSorteio then
              lAposta.Parametros.Linha.Text := lResultado.Linha;
            if FParametros.UsarColunaSorteio then
              lAposta.Parametros.Coluna.Text := lResultado.Coluna;
            if FParametros.UsarSequenciaSorteio then
              lAposta.Parametros.Sequencia.Text := lResultado.Sequencia;
            if FParametros.UsarParametrosSorteio then
              lFiltros := lAposta.GerarFiltros;
            try
              J := 0;
              nTentativasAcerto := 0;
              while (J < FParametros.NumeroAcertos) and Result do
              begin
                Inc(nTentativasAcerto);
                if lAposta.GerarJogosComEstatistica(lFiltros,
                  nTentativasJogos) then
                begin
                  lAcertos := lAposta.VerificarAcerto(lSorteios[I].Dezenas,
                    FParametros.DezenasPorAcerto);
                  if Assigned(lAcertos) then
                    try
                      if lAcertos.Count > 0 then
                      begin
                        lResultado.Tentativas := lResultado.Tentativas +
                          nTentativasAcerto;
                        if (nTentativasAcerto < lResultado.MenorIntervalo) or
                          (lResultado.MenorIntervalo = 0) then
                          lResultado.MenorIntervalo := nTentativasAcerto;
                        if nTentativasAcerto > lResultado.MaiorIntervalo then
                          lResultado.MaiorIntervalo := nTentativasAcerto;
                        lResultado.Acertos.Somar(lAcertos);
                        Inc(J);
                        nTentativasAcerto := 0;
                      end;
                    finally
                      FreeAndNil(lAcertos);
                    end;
                end
                else
                  Result := False;
              end;

              if Result then
              begin
                lResultado.MediaTentativas := lResultado.Tentativas div
                  FParametros.NumeroAcertos;
                Inc(I);
              end;
            finally
              if FParametros.UsarParametrosSorteio then
                FreeAndNil(lFiltros);
            end;
          end;
        finally
          if not FParametros.UsarParametrosSorteio then
            FreeAndNil(lFiltros);
        end;
      finally
        FreeAndNil(lAposta);
      end;
    finally
      FResultados.EndUpdate;
    end;
  finally
    FreeAndNil(lSorteios);
  end;
end;

function TSimulador.ExecutarEmThread: Boolean;
var
  lThread: TThreadSimulador;
begin
  lThread := TThreadSimulador.Create(True);
  try
    lThread.Simulador := Self;
    lThread.Start;
    while not lThread.Finished do
      Application.ProcessMessages;
    lThread.WaitFor;
    if lThread.Erro <> '' then
      raise lThread.ClasseErro.Create(lThread.Erro);
    Result := lThread.Resultado;
  finally
    FreeAndNil(lThread);
  end;
end;

function TSimulador.CriarDataSetResultados: TDataSet;
begin
  Result := FResultados.CriarDataSet;
end;

{ TThreadSimulador }

procedure TThreadSimulador.Execute;
begin
  try
    if Assigned(FSimulador) then
      FResultado := FSimulador.Executar
  except
    on E: Exception do
    begin
      FErro := E.Message;
      FClasseErro := ExceptClass(E.ClassType);
    end;
  end;

  Terminate;
end;

{ TTipoLoteria }

constructor TTipoLoteria.Create(Collection: TCollection);
begin
  inherited;
  FQuadrantes := TQuadrantes.Create;
  FGabarito := TGabarito.Create;
  FGabarito.Loteria := Self;
  FPrecos := TPrecosAposta.Create;
  FHistorico := THistorico.Create;
  FHistorico.Loteria := Self;
  FEstatisticas := TEstatisticas.Create;
  FEstatisticas.Loteria := Self;
end;

destructor TTipoLoteria.Destroy;
begin
  FreeAndNil(FEstatisticas);
  FreeAndNil(FHistorico);
  FreeAndNil(FPrecos);
  FreeAndNil(FGabarito);
  FreeAndNil(FQuadrantes);
  inherited;
end;

function TTipoLoteria.GetKey: string;
begin
  Result := inherited GetKey;
end;

procedure TTipoLoteria.SetKey(const AValue: string);
begin
  inherited;
end;

procedure TTipoLoteria.KeyChanged;
begin
  inherited;
  if FNomeArquivoHistorico = '' then
    FNomeArquivoHistorico := Format('%s\Hist�rico %s.xml',
      [IncludeTrailingPathDelimiter(ExtractFilePath(ParamStr(0))),
       AnsiLowerCase(Nome)]);
  if FNomeArquivoEstatisticas = '' then
    FNomeArquivoEstatisticas := Format('%s\Estat�sticas %s.xml',
      [IncludeTrailingPathDelimiter(ExtractFilePath(ParamStr(0))),
       AnsiLowerCase(Nome)]);
end;

procedure TTipoLoteria.Assign(Source: TPersistent);
var
  lLoteria: TTipoLoteria;
begin
  inherited;
  if Source is TTipoLoteria then
  begin
    lLoteria := TTipoLoteria(Source);
    MaiorDezena := lLoteria.MaiorDezena;
    LinhasTabela := lLoteria.LinhasTabela;
    ColunasTabela := lLoteria.ColunasTabela;
    DezenasSorteio := lLoteria.DezenasSorteio;
    DezenasPremiacaoMinima := lLoteria.DezenasPremiacaoMinima;
    Quadrantes.Assign(lLoteria.Quadrantes);
    Gabarito.Assign(lLoteria.Gabarito);
    Precos.Assign(lLoteria.Precos);
    Historico.Assign(lLoteria.Historico);
    Estatisticas.Assign(Estatisticas);
  end;
end;

procedure TTipoLoteria.LerArquivoHistorico;
begin
  FHistorico.LerXML(FNomeArquivoHistorico);
end;

procedure TTipoLoteria.GravarArquivoHistorico;
begin
  FHistorico.GravarXML(FNomeArquivoHistorico);
end;

procedure TTipoLoteria.ImportarArquivoHistorico(const ANomeArquivo: string);
begin
  FHistorico.ImportarCSV(ANomeArquivo);
  AtualizarEstatisticasHistorico;
end;

procedure TTipoLoteria.ExportarArquivoHistorico(const ANomeArquivo: string);
begin
  FHistorico.ExportarCSV(ANomeArquivo);
end;

procedure TTipoLoteria.AtualizarEstatisticasHistorico;
begin
  FHistorico.AtualizarEstatisticas;
  GravarArquivoHistorico;
end;

procedure TTipoLoteria.LerArquivoEstatisticas;
begin
  FEstatisticas.LerXML(FNomeArquivoEstatisticas);
  FEstatisticas.MarcarTudoComoCalculado;
end;

procedure TTipoLoteria.GravarArquivoEstatisticas;
begin
  FEstatisticas.GravarXML(FNomeArquivoEstatisticas);
end;

procedure TTipoLoteria.RecalcularEstatisticas(const AUsarThread: Boolean);
begin
  if AUsarThread then
    FEstatisticas.CalcularEmThread(True)
  else
    FEstatisticas.Calcular(True);
  GravarArquivoEstatisticas;
end;

procedure TTipoLoteria.GerarStringOcorrencia(const AStrings: TStrings);
var
  I: Integer;
  J: Integer;
  L: Integer;
begin
  if Assigned(AStrings) and (FDezenasSorteio > 2) then
  begin
    AStrings.BeginUpdate;
    try
      AStrings.Clear;
      for I := 0 to FDezenasSorteio do
        for J := 0 to FDezenasSorteio do
          for L := 0 to FDezenasSorteio do
            if I + J + L = FDezenasSorteio then
              AStrings.Add(FormatarStringOcorrencia(J, I, L));
    finally
      AStrings.EndUpdate;
    end;
  end;
end;

function TTipoLoteria.FormatarStringOcorrencia(const ACentro, AEsquerda,
  ADireita: Integer): string;
begin
  Result := Format('%d - %d - %d', [AEsquerda, ACentro, ADireita]);
end;

{ TTiposLoteria }

constructor TTiposLoteria.Create;
begin
  inherited;
  HashSize := 16;
end;

function TTiposLoteria.GetItem(const AIndex: Integer): TTipoLoteria;
begin
  Result := TTipoLoteria(inherited Items[AIndex]);
end;

function TTiposLoteria.GetItemClass: TCollectionItemClass;
begin
  Result := TTipoLoteria;
end;

function TTiposLoteria.Add: TTipoLoteria;
begin
  Result := TTipoLoteria(inherited Add);
end;

function TTiposLoteria.Insert(const AIndex: Integer): TTipoLoteria;
begin
  Result := TTipoLoteria(inherited Insert(AIndex));
end;

function TTiposLoteria.Find(const ANome: string): TTipoLoteria;
begin
  Result := TTipoLoteria(inherited Find(ANome));
end;

function TTiposLoteria.Contains(const ANome: string): Boolean;
begin
  Result := inherited Contains(ANome);
end;

function TTiposLoteria.Remove(const ANome: string): Boolean;
begin
  Result := inherited Remove(ANome);
end;

{ TFrame }

constructor TFrame.Create(AOwner: TComponent);
begin
  inherited;
  Visible := False;
end;

procedure TFrame.SetEstatistica(const AValue: TEstatistica);
begin
  if FEstatistica <> AValue then
  begin
    FEstatistica := AValue;
    EstatisticaAlterada;
  end;
end;

procedure TFrame.EstatisticaAlterada;
begin
end;

procedure TFrame.Mostrar;
begin
  Preencher;
  Visible := True;
end;

{ TInfoEstatistica }

destructor TInfoEstatistica.Destroy;
begin
  if Assigned(FPainel) then
    FreeAndNil(FPainel);
  inherited;
end;

{ TLoteria }

constructor TLoteria.Create;
begin
  inherited;
  FTipos := TTiposLoteria.Create;
  TRLDXFormManager.Instance.UseSystemFont := True;
  FormatSettings.ShortDateFormat := 'DD/MM/YYYY';
  ReportMemoryLeaksOnShutdown := True;
end;

destructor TLoteria.Destroy;
begin
  FreeAndNil(FTipos);
  inherited;
end;

{$IFDEF MS}
function TLoteria.ConfigurarMegaSena(const ANome: string): TTipoLoteria;
var
  lQuadrante: TQuadrante;
  lFaixa: TFaixaQuadrante;
  lGabarito: TItemGabarito;
  lOcorrenciaPorPares: TOcorrenciaPorPares;
  lOcorrenciaPorPrimos: TOcorrenciaPorPrimos;
  lOcorrenciaPorIntervalo: TOcorrenciaPorIntervalo;
  lOcorrenciaPorLinha: TOcorrenciaPorLinha;
  lOcorrenciaPorColuna: TOcorrenciaPorColuna;
  lOcorrenciaPorSequencia: TOcorrenciaPorSequencia;
  lOcorrenciaPorQuadrante: TOcorrenciaPorQuadrante;
  lOcorrenciaPorRepeticaoDezenas: TOcorrenciaPorRepeticaoDezenas;
  lFrequenciaAcumulada: TFrequenciaAcumulada;
  lFrequencia: TFrequencia;
  lQuocienteFrequencia: TQuocienteFrequencia;
  lQuocienteAtraso: TQuocienteAtraso;
  lAtrasoAcumulado: TAtrasoAcumulado;
  lAtraso: TAtraso;
begin
  Result := FTipos.Add;
  Result.Nome := ANome;
  Result.MaiorDezena := 60;
  Result.LinhasTabela := 6;
  Result.ColunasTabela := 10;
  Result.DezenasSorteio := 6;
  Result.DezenasPremiacaoMinima := 4;
//Q1
  lQuadrante := Result.Quadrantes.Add;
  lQuadrante.Numero := 1;
  lFaixa := lQuadrante.Faixas.Add;
  lFaixa.Inicio := 1;
  lFaixa.Termino := 5;
  lFaixa := lQuadrante.Faixas.Add;
  lFaixa.Inicio := 11;
  lFaixa.Termino := 15;
  lFaixa := lQuadrante.Faixas.Add;
  lFaixa.Inicio := 21;
  lFaixa.Termino := 25;
//Q2
  lQuadrante := Result.Quadrantes.Add;
  lQuadrante.Numero := 2;
  lFaixa := lQuadrante.Faixas.Add;
  lFaixa.Inicio := 6;
  lFaixa.Termino := 10;
  lFaixa := lQuadrante.Faixas.Add;
  lFaixa.Inicio := 16;
  lFaixa.Termino := 20;
  lFaixa := lQuadrante.Faixas.Add;
  lFaixa.Inicio := 26;
  lFaixa.Termino := 30;
//Q3
  lQuadrante := Result.Quadrantes.Add;
  lQuadrante.Numero := 3;
  lFaixa := lQuadrante.Faixas.Add;
  lFaixa.Inicio := 31;
  lFaixa.Termino := 35;
  lFaixa := lQuadrante.Faixas.Add;
  lFaixa.Inicio := 41;
  lFaixa.Termino := 45;
  lFaixa := lQuadrante.Faixas.Add;
  lFaixa.Inicio := 51;
  lFaixa.Termino := 55;
//Q4
  lQuadrante := Result.Quadrantes.Add;
  lQuadrante.Numero := 4;
  lFaixa := lQuadrante.Faixas.Add;
  lFaixa.Inicio := 36;
  lFaixa.Termino := 40;
  lFaixa := lQuadrante.Faixas.Add;
  lFaixa.Inicio := 46;
  lFaixa.Termino := 50;
  lFaixa := lQuadrante.Faixas.Add;
  lFaixa.Inicio := 56;
  lFaixa.Termino := 60;
//Gabarito
  lGabarito := Result.Gabarito.Add;
  lGabarito.Nome := 'S';
  lGabarito.Linha := True;
  lGabarito.Coluna := True;
  lGabarito.Sequencia := True;
  lGabarito.Ocorrencias.Add.Dezenas := 1;
  lGabarito.Ocorrencias.Add.Dezenas := 1;
  lGabarito.Ocorrencias.Add.Dezenas := 1;
  lGabarito.Ocorrencias.Add.Dezenas := 1;
  lGabarito.Ocorrencias.Add.Dezenas := 1;
  lGabarito.Ocorrencias.Add.Dezenas := 1;
  lGabarito := Result.Gabarito.Add;
  lGabarito.Nome := 'P';
  lGabarito.Linha := True;
  lGabarito.Coluna := True;
  lGabarito.Sequencia := True;
  lGabarito.Ocorrencias.Add.Dezenas := 2;
  lGabarito.Ocorrencias.Add.Dezenas := 1;
  lGabarito.Ocorrencias.Add.Dezenas := 1;
  lGabarito.Ocorrencias.Add.Dezenas := 1;
  lGabarito.Ocorrencias.Add.Dezenas := 1;
  lGabarito.Preferencial := True;
  lGabarito := Result.Gabarito.Add;
  lGabarito.Nome := '2P';
  lGabarito.Linha := True;
  lGabarito.Coluna := True;
  lGabarito.Sequencia := True;
  lGabarito.Ocorrencias.Add.Dezenas := 2;
  lGabarito.Ocorrencias.Add.Dezenas := 2;
  lGabarito.Ocorrencias.Add.Dezenas := 1;
  lGabarito.Ocorrencias.Add.Dezenas := 1;
  lGabarito.Preferencial := True;
  lGabarito := Result.Gabarito.Add;
  lGabarito.Nome := '3P';
  lGabarito.Linha := True;
  lGabarito.Coluna := True;
  lGabarito.Sequencia := True;
  lGabarito.Ocorrencias.Add.Dezenas := 2;
  lGabarito.Ocorrencias.Add.Dezenas := 2;
  lGabarito.Ocorrencias.Add.Dezenas := 2;
  lGabarito := Result.Gabarito.Add;
  lGabarito.Nome := 'T';
  lGabarito.Linha := True;
  lGabarito.Coluna := True;
  lGabarito.Sequencia := True;
  lGabarito.Ocorrencias.Add.Dezenas := 3;
  lGabarito.Ocorrencias.Add.Dezenas := 1;
  lGabarito.Ocorrencias.Add.Dezenas := 1;
  lGabarito.Ocorrencias.Add.Dezenas := 1;
  lGabarito.Preferencial := True;
  lGabarito := Result.Gabarito.Add;
  lGabarito.Nome := '2T';
  lGabarito.Linha := True;
  lGabarito.Coluna := True;
  lGabarito.Sequencia := True;
  lGabarito.Ocorrencias.Add.Dezenas := 3;
  lGabarito.Ocorrencias.Add.Dezenas := 3;
  lGabarito := Result.Gabarito.Add;
  lGabarito.Nome := 'TP';
  lGabarito.Linha := True;
  lGabarito.Coluna := True;
  lGabarito.Sequencia := True;
  lGabarito.Ocorrencias.Add.Dezenas := 3;
  lGabarito.Ocorrencias.Add.Dezenas := 2;
  lGabarito.Ocorrencias.Add.Dezenas := 1;
  lGabarito := Result.Gabarito.Add;
  lGabarito.Nome := 'Q';
  lGabarito.Linha := True;
  lGabarito.Coluna := True;
  lGabarito.Sequencia := True;
  lGabarito.Ocorrencias.Add.Dezenas := 4;
  lGabarito.Ocorrencias.Add.Dezenas := 1;
  lGabarito.Ocorrencias.Add.Dezenas := 1;
  lGabarito := Result.Gabarito.Add;
  lGabarito.Nome := 'QP';
  lGabarito.Linha := True;
  lGabarito.Coluna := True;
  lGabarito.Sequencia := True;
  lGabarito.Ocorrencias.Add.Dezenas := 4;
  lGabarito.Ocorrencias.Add.Dezenas := 2;
  lGabarito := Result.Gabarito.Add;
  lGabarito.Nome := 'Qn';
  lGabarito.Linha := True;
  lGabarito.Coluna := True;
  lGabarito.Sequencia := True;
  lGabarito.Ocorrencias.Add.Dezenas := 5;
  lGabarito.Ocorrencias.Add.Dezenas := 1;
  lGabarito := Result.Gabarito.Add;
  lGabarito.Nome := 'N';
  lGabarito.Linha := True;
  lGabarito.Coluna := False;
  lGabarito.Sequencia := True;
  lGabarito.Ocorrencias.Add.Dezenas := 6;
//Hist�rico
  Result.LerArquivoHistorico;
//Estat�sticas
  lOcorrenciaPorPares := Result.Estatisticas.AdicionarOcorrenciaPorPares;
  lOcorrenciaPorPares.AplicarComparativo := True;
  lOcorrenciaPorPrimos := Result.Estatisticas.AdicionarOcorrenciaPorPrimos;
  lOcorrenciaPorPrimos.AplicarComparativo := True;
  lOcorrenciaPorIntervalo := Result.Estatisticas.AdicionarOcorrenciaPorIntervalo;
  lOcorrenciaPorIntervalo.AplicarComparativo := True;
  lOcorrenciaPorLinha := Result.Estatisticas.AdicionarOcorrenciaPorLinha;
  lOcorrenciaPorLinha.AplicarComparativo := True;
  lOcorrenciaPorColuna := Result.Estatisticas.AdicionarOcorrenciaPorColuna;
  lOcorrenciaPorColuna.AplicarComparativo := True;
  lOcorrenciaPorSequencia :=
    Result.Estatisticas.AdicionarOcorrenciaPorSequencia;
  lOcorrenciaPorSequencia.AplicarComparativo := True;
  lOcorrenciaPorQuadrante :=
    Result.Estatisticas.AdicionarOcorrenciaPorQuadrante;
  lOcorrenciaPorQuadrante.AplicarComparativo := True;
  Result.Estatisticas.AdicionarOcorrenciaPorDezena;
  lOcorrenciaPorRepeticaoDezenas :=
    Result.Estatisticas.AdicionarOcorrenciaPorRepeticaoDezenas;
  lOcorrenciaPorRepeticaoDezenas.AplicarComparativo := True;
  Result.Estatisticas.AdicionarDezenasMaisSorteadas;
  Result.Estatisticas.AdicionarDezenasMenosSorteadas;
  Result.Estatisticas.AdicionarFrequenciaDezenas;
  lFrequenciaAcumulada := Result.Estatisticas.AdicionarFrequenciaAcumulada;
  lFrequenciaAcumulada.NumeroSorteios := CNumeroSorteiosEstatistica;
  lFrequencia := Result.Estatisticas.AdicionarFrequencia;
  lFrequencia.AplicarComparativo := True;
  lFrequencia.SomenteComparativo := True;
  lFrequencia.EstatisticaComparacao := CFrequenciaAcumulada;
  lQuocienteFrequencia := Result.Estatisticas.AdicionarQuocienteFrequencia;
  lQuocienteFrequencia.NumeroSorteios := CNumeroSorteiosEstatistica;
  Result.Estatisticas.AdicionarAtrasoDezenas;
  lQuocienteAtraso := Result.Estatisticas.AdicionarQuocienteAtraso;
  lQuocienteAtraso.NumeroSorteios := CNumeroSorteiosEstatistica;
  lAtrasoAcumulado := Result.Estatisticas.AdicionarAtrasoAcumulado;
  lAtrasoAcumulado.NumeroSorteios := CNumeroSorteiosEstatistica;
  lAtraso := Result.Estatisticas.AdicionarAtraso;
  lAtraso.AplicarComparativo := True;
  lAtraso.SomenteComparativo := True;
  lAtraso.EstatisticaComparacao := CAtrasoAcumulado;
  Result.LerArquivoEstatisticas;
end;
{$ENDIF}

function TLoteria.Initialize: Boolean;
begin
  Result := inherited Initialize;
  if Result then
  begin
{$IFDEF MS}
    ConfigurarMegaSena(CMegaSena);
{$ENDIF}
  end;
end;

class function TLoteria.Instance: TLoteria;
begin
  Result := TLoteria(inherited Instance);
end;

{ TRotinas }

class function TRotinas.Primo(const ANumero: Integer): Boolean;
var
  nDivisores: Integer;
  I: Integer;
begin
  nDivisores := 0;
  for I := 1 to ANumero do
    if ANumero mod I = 0 then
      Inc(nDivisores);
  Result := nDivisores = 2;
end;

class function TRotinas.CalcularFatorial(const ANumero,
  ALimite: Integer): Double;
var
  nNumero: Integer;
begin
  Result := ANumero;
  nNumero := ANumero - 1;
  while nNumero >= ALimite do
  begin
    Result := Result * nNumero;
    Dec(nNumero);
  end;
end;

class function TRotinas.CalcularProbabilidade(const AConjunto,
  AElementos: Integer): Double;
begin
  if AConjunto = AElementos then
    Result := 1
  else
    Result := CalcularFatorial(AConjunto, AElementos + 1) /
      CalcularFatorial(AConjunto - AElementos);
end;

class function TRotinas.Concatenar(const AStrings: TStrings): string;
var
  I: Integer;
begin
  Result := '';
  if Assigned(AStrings) then
    for I := 0 to Pred(AStrings.Count) do
      Result := Result + IfThen(Result <> '', ', ') + AStrings[I];
end;

initialization
  RegisterClasses([TOcorrenciaPorPares, TOcorrenciaPorPrimos,
    TOcorrenciaPorIntervalo, TOcorrenciaPorLinha, TOcorrenciaPorColuna,
    TOcorrenciaPorSequencia, TOcorrenciaPorQuadrante, TOcorrenciaPorDezena,
    TOcorrenciaPorRepeticaoDezenas, TDezenasMaisSorteadas,
    TDezenasMenosSorteadas, TFrequenciaDezenas, TFrequenciaAcumulada,
    TFrequencia, TQuocienteFrequencia, TAtrasoDezenas, TQuocienteAtraso,
    TAtrasoAcumulado, TAtraso]);

end.
