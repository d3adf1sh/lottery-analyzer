object MainForm: TMainForm
  Left = 0
  Top = 0
  Caption = 'MainForm'
  ClientHeight = 700
  ClientWidth = 900
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object pcOpcoes: TcxPageControl
    AlignWithMargins = True
    Left = 5
    Top = 35
    Width = 890
    Height = 660
    Margins.Left = 5
    Margins.Top = 5
    Margins.Right = 5
    Margins.Bottom = 5
    Align = alClient
    TabOrder = 0
    Properties.ActivePage = tsHome
    Properties.CustomButtons.Buttons = <>
    Properties.Images = ilImagens
    LookAndFeel.Kind = lfUltraFlat
    ClientRectBottom = 654
    ClientRectLeft = 2
    ClientRectRight = 884
    ClientRectTop = 28
    object tsHome: TcxTabSheet
      Caption = 'Home'
      ImageIndex = 0
      DesignSize = (
        882
        626)
      object lblDesenvolvidoPor: TcxLabel
        Left = 718
        Top = 580
        Anchors = [akRight, akBottom]
        Caption = 'Desenvolvido por Rafael C. Luiz'
        Style.TransparentBorder = False
        Properties.Alignment.Horz = taRightJustify
        Transparent = True
        AnchorX = 870
      end
      object lblEmail: TcxLabel
        Left = 764
        Top = 600
        Anchors = [akRight, akBottom]
        Caption = 'rafael.lz@hotmail.com'
        Style.TransparentBorder = False
        Properties.Alignment.Horz = taRightJustify
        Transparent = True
        AnchorX = 870
      end
      object lblLoteriaSelecionada: TcxLabel
        Left = 10
        Top = 15
        Caption = '...'
        Style.TransparentBorder = False
        Transparent = True
      end
      object btnRecalcularEstatisticas: TcxButton
        Left = 20
        Top = 200
        Width = 150
        Height = 20
        Caption = 'Recalcular estat'#237'sticas'
        OptionsImage.Glyph.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00010000000A0D0A0865251A17EC5A4238EC1A13105C00000007000000000000
          0000000000000000000000000000000000000000000000000000000000010000
          0009130E0C6968554CFEC9AA97FFC8AA97FF584037EA0000000E000000000000
          0000000000000000000000000001000000060000000B0000000D000000101812
          1069776358FED8BBAAFFD8BBA8FFDCC9BCFF5C433AEC0000000D000000020000
          00060000000A0000000D01010119271C1788422D25D84B332BFF3D2921E6836E
          63FEDCC1B2FFDCC2B0FFEAD7CCFF917B71FE1F16145F00000005000000068060
          54BDC3A396FFCCB5ADFF7A6057FF937A6FFFBFA99CFFDEC8BAFFC7B0A2FFE0C8
          B8FFDFC7B7FFE9DACFFF9B857BFF95827AEC000000100000000100000008B78A
          79FFF6F0ECFFAB9991FFA08A80FFEEE1D9FFF5EDE7FFF0E4DBFFE9D6C8FFE2CC
          BDFFE2CEBFFFA08A80FFBDAFA7FFCDB7AEFF0000000D0000000000000008B98F
          7EFFF5EFEBFF8D7368FFC8B6ADFFB4A098FF8F766AFFB09C93FFD8C8BFFFE5CE
          BFFFD1B9ABFF82675DFFEDE6E0FFB89182FF0000000C0000000000000007BE93
          81FFD3B6A9FF8B6F63FFB7A299FFAE9286FFCDAFA2FFAA8E82FFAF9B92FFE6D1
          C3FFE6D2C7FF7E6459FFF1E9E4FFAE806FFF0000000B0000000000000007C197
          86FFF7F3F0FF9D857AFFC7B8B0FFD5B8ABFFF8F1EDFFEAE2DDFF71574DFFE7D4
          C6FFD2C0B8FF8C746AFFF4ECE8FFB18473FF0000000B0000000000000006C59C
          8BFFFBF7F5FFF8F2EEFFF9F2EEFFD9BCAFFFF6F0ECFFA99891FF937C72FFEEE0
          D8FFB09C92FFB9A9A2FFF7F0ECFFB58877FF0000000A0000000000000006C9A0
          8FFFDCC1B5FFDCC0B3FFDCC0B3FFDABEB2FF9D8175FF8F7A72FFCABAB2FFB6A2
          97FF9C8073FFD6B8AEFFDABCB1FFB88D7BFF0000000A0000000000000005CCA4
          93FFFDF9F8FFFBF6F4FFFBF7F4FFDBC0B5FFAC958AFF987D6FFFA58E85FFB79C
          90FFF6F0EDFFFAF5F2FFFBF6F3FFBC907FFF000000090000000000000005CFA8
          97FFFDFAF9FFFBF7F5FFFCF7F5FFDFC5B8FFFAF6F3FFFAF5F3FFF9F5F3FFDDC3
          B6FFFCF7F5FFFBF7F4FFFBF7F4FFC09584FF000000080000000000000004C38A
          5BFFE6C79EFFD6A771FFD6A56FFFB67948FFD5A26AFFD49F69FFD49E65FFB474
          43FFD39961FFD2985FFFD1965EFFB37240FF000000080000000000000004C58E
          5EFFF2DBB5FFF2DAB3FFF0D7B0FFC6966BFFEFD4AAFFEFD2A7FFEECFA4FFBE89
          5BFFEBCB9CFFEAC99AFFEAC696FFB67744FF000000070000000000000002936A
          48BFC68E60FFC58D5FFFC48C5CFFC38A5BFFC28958FFC08756FFC08554FFBE83
          52FFBD8150FFBB7F4DFFBA7E4CFF895C36C10000000400000000}
        TabOrder = 6
        OnClick = btnRecalcularEstatisticasClick
      end
      object btnImportarHistorico: TcxButton
        Left = 20
        Top = 75
        Width = 150
        Height = 20
        Caption = 'Importar hist'#243'rico'
        OptionsImage.Glyph.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000090000
          000E000000100000001000000010000000100000001000000011000000110000
          001100000011000000100000000B00000003000000000000000019427ACA245A
          A5FF255CA7FF255BA7FF245AA6FF2459A6FF2358A5FF2358A4FF2356A4FF2256
          A4FF2255A3FF2154A3FF2153A1FF1C468AE303080F2900000002255DA5FF316B
          AEFF6DA6D5FF86CAF0FF46A6E4FF44A3E4FF41A1E3FF3FA0E2FF3C9EE2FF3B9C
          E1FF389BE0FF369AE0FF3498DFFF2C77C1FF10284D8B000000082B68AEFF4984
          BEFF4B8BC5FFB2E3F8FF68BBECFF55B0E8FF52AEE8FF4EACE7FF4CA9E6FF49A8
          E5FF47A6E4FF44A4E4FF41A2E3FF3A92D6FF1C4885D50000000D2F6FB4FF6CA7
          D2FF3F87C4FFAED9F0FF9AD8F5FF66BDEEFF63BBEDFF60B9EBFF5DB6EBFF5BB5
          EAFF57B2EAFF55B0E9FF51AEE7FF4FABE7FF2D69B1FF040B142F3276B9FF8FC7
          E6FF509FD4FF86BCE0FFC5EFFCFF78CAF2FF74C8F1FF72C5F0FF6FC4F0FF6DC2
          EFFF69C0EEFF66BDEEFF63BBEDFF60B9EBFF448BC9FF122D4D81357CBCFFAFE3
          F5FF75C8EDFF59A2D4FFDDF7FDFFDFF8FEFFDDF7FEFFDBF7FEFFD8F5FEFFD4F4
          FDFFD0F2FDFFCCEFFCFFC7EDFBFFC1EBFBFF9ACBE9FF215187CB3882C1FFC7F5
          FEFF97E5FCFF64BAE5FF4D9FD3FF4D9DD2FF4B9BD1FF4A99CFFF4998CFFF4896
          CEFF4694CCFF4592CBFF3073B7FF3072B6FF2F71B5FF2A65A4EA3A88C5FFCDF7
          FEFFA6ECFEFF9CE8FDFF93E4FBFF8EE1FBFF89DFFBFF86DEFAFF81DAFAFF7ED8
          F9FF7BD7F9FF79D6F9FF2A6BB0FF000000140000000A000000073D8EC8FFD0F8
          FEFFAEF0FEFFAAEEFEFFA6EDFEFFA5EBFDFFBBF2FDFFD4F9FEFFD5F9FEFFD3F8
          FEFFD1F8FEFFCEF7FDFF3680BFFF0000000800000000000000003F92CBFFD3F9
          FEFFB6F3FEFFB3F1FDFFB0F1FEFFB8EDFAFF4895CBFF3B8CC6FF3B8AC6FF3A89
          C5FF3A88C5FF3A87C3FF2A6391C20000000500000000000000004197CEFFE2FC
          FEFFE2FCFEFFE1FCFEFFD4F3FAFF458FBFEC040A0E1B00000006000000060000
          000600000006000000060000000400000001000000000000000031739ABF429A
          D0FF4299D0FF4299D0FF4297CFFF153244590000000200000000000000000000
          0000000000000000000000000000000000000000000000000000000000020000
          0003000000030000000400000003000000020000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        TabOrder = 1
        OnClick = btnImportarHistoricoClick
      end
      object btnAtualizarEstatisticasHistorico: TcxButton
        Left = 20
        Top = 135
        Width = 150
        Height = 20
        Caption = 'Atualizar estat'#237'sticas'
        OptionsImage.Glyph.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000100000008000000130000
          001A0000001A0000001500000009000000020000000000000000000000000000
          000000000000000000000000000000000003000000110605225E100F61C81512
          7DEE14117EF00F0D63CB05042364000000130000000300000000000000000000
          0000000000000000000000000001000000100F0E4797333099FFA79CC0FFDCD1
          D4FFE3D6D7FFB2A7C5FF353198FF0D0B49A10000001200000002000000000000
          00000000000000000000000000070B0A2D6637349DFFDCCECCFFF0E1D4FFF2E4
          D9FF71554CFFF2E3D9FFE6D8D1FF46419EFF09082E6F00000008000000000000
          000000000000000000000000000D1F1F76CDA497B7FFECDACCFFF0E0D5FFF2E3
          D8FF71554CFFF0E2D6FFEDDDD0FFBAACBFFF1A1774D4000000100000000E0000
          00150000001700000018000000252D2D98F4D8C5C0FFEEE0D3FFF3E6DEFFF3E7
          DFFF71554CFFF3E8DFFFEFE2D7FFE7D4C6FF2A2797FB000000126E4A3FC59867
          57FF996656FF986656FFC0A9A1FF3A3AA6FFD7C3BDFF454BBDFF4248BAFF4045
          B7FF574C80FFF6EFE8FFF6EEE7FFE5D1C3FF2F2D9CFA0000000F9B6A5AFFF4ED
          E5FFF1E8DDFFF0E8DDFFEFE9E4FF6161B6FFA194B5FFF7F1ECFFFBF7F4FFFBF7
          F5FFFBF7F5FFFBF7F5FFF7F1EBFFB6A6B7FF26267DCC0000000A9F6E5FFFF5EF
          E9FFF2E9E0FFF2E9E0FFF1EAE4FFB6B4D3FF4F51B1FFDCCCC8FFFEFDFCFFFFFF
          FFFFFFFFFFFFFEFDFCFFE5D4CCFF5A56ADFF13143C6700000004A17362FFF7F2
          ECFFF3ECE3FFF3EBE3FFF3EBE3FFF4EFEAFF9192CBFF5555B5FFAFA1B9FFDED2
          D0FFE2D6D1FFB6A7BAFF5855B1FF232463A00000000600000001A47766FFF8F4
          EFFFF4EEE6FFF4EDE6FFF5EDE6FFF5EEE6FFF5F1ECFFBDBCDBFF7779C6FF5559
          BBFF5256BBFF7273C3FF9C8FAEFF000000140000000100000000A87A6BFFF9F6
          F1FFF6F0EAFFA97E6EFFFAF7F3FFF6EFE8FFAA7F6FFFFAF8F4FFF7F2EDFFD1BB
          B3FFF9F7F5FFF6F1ECFFB79386FF0000000E0000000000000000AB7F6FFFFBF8
          F4FFF8F3EDFFAC8272FFFBF8F5FFF7F1EBFFAC8171FFFAF8F4FFF7F0E9FFAB80
          70FFFAF7F4FFF6EFE7FFA87C6CFF0000000C0000000000000000AE8373FFFCF9
          F6FFF9F5F0FFB08676FFFCF9F7FFF9F4EFFFAF8575FFFBF9F6FFF8F3EDFFAF84
          75FFFBF8F5FFF7F1EBFFAB8070FF0000000A0000000000000000B08777FFFFFF
          FFFFFFFFFFFFB28A7AFFFFFFFFFFFFFFFFFFB28979FFFFFFFFFFFFFFFFFFB189
          78FFFFFFFFFFFFFFFFFFAE8474FF0000000800000000000000008B6E63BFBB95
          85FFBB9485FFBB9484FFBA9384FFBA9383FFB99283FFB99282FFB89181FFB891
          81FFB89080FFB78F80FF876A5FC1000000040000000000000000}
        TabOrder = 3
        OnClick = btnAtualizarEstatisticasHistoricoClick
      end
      object lblTarefasHistorico: TcxLabel
        Left = 10
        Top = 50
        Anchors = [akLeft, akTop, akRight]
        AutoSize = False
        Caption = 'Hist'#243'rico'
        Properties.LineOptions.Visible = True
        Transparent = True
        Height = 17
        Width = 860
      end
      object lblTarefasEstatisticas: TcxLabel
        Left = 10
        Top = 175
        Anchors = [akLeft, akTop, akRight]
        AutoSize = False
        Caption = 'Estat'#237'sticas'
        Properties.LineOptions.Visible = True
        Transparent = True
        Height = 17
        Width = 860
      end
      object btnExportarHistorico: TcxButton
        Left = 20
        Top = 105
        Width = 150
        Height = 20
        Caption = 'Exportar hist'#243'rico'
        OptionsImage.Glyph.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000020000
          000A0000000F0000001000000010000000100000001100000011000000110000
          001100000011000000100000000B000000030000000000000000000000094633
          2CC160453BFF644A41FFB87D4EFFB97A4AFFB47444FFB06C3DFFA76436FFA764
          36FF583D36FF5B3E37FF3B2821C20000000A00000000000000000000000D6F53
          47FF947869FF6A4F46FFD8B07BFFD7AE77FFD7AB74FFD6A970FFD5A66DFFD4A5
          6AFF5D413AFF684B41FF543931FF0000000E00000000000000000000000C7357
          4AFF987D6EFF70564BFFFFFFFFFFF6EFEAFFF6EFE9FFF6EEE9FFF5EEE9FFF6EE
          E8FF62473FFF715348FF573B33FF0000000F00000000000000000000000B785C
          4EFF9D8273FF765C50FFFFFFFFFFF7F0EBFFF7F0EBFFF7EFEBFFF6EFEAFFF6EF
          EAFF684E44FF72554BFF593E35FF0000000E00000000000000000000000A7C60
          50FFA28777FF7B6154FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFBFBFFF1F1
          F1FF89756EFF8A7269FF5F443BFF0000000C0000000000000000000000097F63
          54FFA78E7DFF977A6AFF967969FF957967FF84695CFF705548FF8F7B73FF0B67
          37FF295D3CFF81746BFF806C63FF0000000C0000000000000000000000088367
          57FFAB9382FF634A41FF614740FF5E463DFF5C443CFF5B433BFF776761FF0A67
          37FF2AAF7FFF106137FF5B6352FF00000016000000030000000000000007866A
          59FFAF9788FF674E44FFF3EAE4FFE8D9CEFFE9DFD7FFE5DBD5FFD8CFC9FF0B6A
          3BFF4EDCB2FF27C48DFF0C7746FF022E179C000201190000000500000006886D
          5CFFB39C8CFF6B5248FFF4ECE6FFEBE3DCFF47916BFF046B38FF046B38FF056B
          38FF3AD7A5FF37D6A2FF32D3A0FF199966FF044A26D5000D063A000000058B70
          5EFFB7A091FF70564DFFF6EFEAFFEBE4DFFF167E4EFF84EDD1FF52E1B6FF4DDF
          B3FF48DDAFFF44DBACFF3FD9A9FF3AD7A6FF32BE8EFF0F6A3FF6000000048E72
          60FFBBA595FF755A50FFF7F1ECFFF1EEEBFF188252FFB8F7E7FFB4F5E6FFAFF4
          E4FF85EDD2FF52E1B7FF4DDFB3FF5DE2BBFF66D0AEFF16794CF6000000026A56
          49BF8F7361FF795E54FF765D52FFAFA19CFF3B8963FF0D814DFF0D804DFF0D80
          4CFF95F1DAFF65E7C2FF83ECCFFF57B28FFF065932D2010E0832000000010000
          000200000003000000030000000300000005000000090000000C000000140D7B
          4BF2AEF6E5FF94E5CEFF339167FD033A1F910001010F00000003000000000000
          0000000000000000000000000000000000000000000000000000000000070F7F
          4EF287CBB3FF106D42E6011C0F4C000000060000000100000000000000000000
          0000000000000000000000000000000000000000000000000000000000051081
          52F1034B2AAE0007041700000003000000010000000000000000}
        TabOrder = 2
        OnClick = btnExportarHistoricoClick
      end
    end
    object tsHistorico: TcxTabSheet
      Caption = 'Hist'#243'rico'
      ImageIndex = 1
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object grdHistorico: TcxGrid
        AlignWithMargins = True
        Left = 5
        Top = 5
        Width = 872
        Height = 616
        Margins.Left = 5
        Margins.Top = 5
        Margins.Right = 5
        Margins.Bottom = 5
        Align = alClient
        TabOrder = 0
        object grdHistoricoView: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          OnEditKeyDown = grdHistoricoViewEditKeyDown
          DataController.DataSource = dsHistorico
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsData.Deleting = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsSelection.MultiSelect = True
          OptionsSelection.CellMultiSelect = True
          OptionsSelection.InvertSelect = False
        end
        object grdHistoricoLevel: TcxGridLevel
          GridView = grdHistoricoView
        end
      end
    end
    object tsEstatisticas: TcxTabSheet
      Caption = 'Estat'#237'sticas'
      ImageIndex = 2
      object pnlEstatisticas: TPanel
        AlignWithMargins = True
        Left = 5
        Top = 5
        Width = 872
        Height = 616
        Margins.Left = 5
        Margins.Top = 5
        Margins.Right = 5
        Margins.Bottom = 5
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object splEstatisticas: TcxSplitter
          Left = 275
          Top = 0
          Width = 6
          Height = 616
          Control = tvEstatisticas
        end
        object tvEstatisticas: TcxTreeView
          Left = 0
          Top = 0
          Width = 275
          Height = 616
          Align = alLeft
          Style.TransparentBorder = True
          TabOrder = 1
          HideSelection = False
          Images = ilImagens
          ReadOnly = True
          RowSelect = True
          OnChange = tvEstatisticasChange
          OnDeletion = tvEstatisticasDeletion
        end
        object lblEstatisticas: TcxLabel
          Left = 281
          Top = 0
          Margins.Left = 0
          Margins.Top = 1
          Margins.Right = 0
          Margins.Bottom = 1
          Align = alClient
          Caption = 'Selecione um item ao lado'
          Style.BorderStyle = ebsFlat
          Style.TransparentBorder = False
          Properties.Alignment.Horz = taCenter
          Properties.Alignment.Vert = taVCenter
          AnchorX = 577
          AnchorY = 308
        end
      end
    end
    object tsSimulador: TcxTabSheet
      Caption = 'Simulador'
      ImageIndex = 6
      object pnlSimulador: TPanel
        AlignWithMargins = True
        Left = 5
        Top = 5
        Width = 872
        Height = 616
        Margins.Left = 5
        Margins.Top = 5
        Margins.Right = 5
        Margins.Bottom = 5
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object pnlParametrosSimulador: TPanel
          Left = 0
          Top = 0
          Width = 872
          Height = 160
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          DesignSize = (
            872
            160)
          object lblQuantidadeJogosSimulador: TcxLabel
            Left = 34
            Top = 8
            Caption = 'Jogos:'
            FocusControl = edtQuantidadeJogosSimulador
            Style.TransparentBorder = False
            Properties.Alignment.Horz = taRightJustify
            Transparent = True
            AnchorX = 66
          end
          object lblQuadrantesSimulador: TcxLabel
            Left = 5
            Top = 108
            Caption = 'Quadrantes:'
            FocusControl = edtMinimoQuadrantesSimulador
            Style.TransparentBorder = False
            Properties.Alignment.Horz = taRightJustify
            Transparent = True
            AnchorX = 66
          end
          object edtMinimoQuadrantesSimulador: TcxCurrencyEdit
            Left = 70
            Top = 105
            Properties.DecimalPlaces = 0
            Properties.DisplayFormat = '0'
            Properties.MaxLength = 5
            Properties.Nullable = False
            Style.TransparentBorder = False
            TabOrder = 15
            Width = 40
          end
          object edtMinimoParesSimulador: TcxCurrencyEdit
            Left = 70
            Top = 30
            Enabled = False
            Properties.DecimalPlaces = 0
            Properties.DisplayFormat = '0'
            Properties.MaxLength = 5
            Properties.Nullable = False
            Style.TransparentBorder = False
            TabOrder = 3
            Width = 40
          end
          object edtQuantidadeJogosSimulador: TcxCurrencyEdit
            Left = 70
            Top = 5
            Properties.DecimalPlaces = 0
            Properties.DisplayFormat = '0'
            Properties.MaxLength = 5
            Properties.Nullable = False
            Style.TransparentBorder = False
            TabOrder = 1
            Width = 40
          end
          object edtMaximoParesSimulador: TcxCurrencyEdit
            Left = 115
            Top = 30
            Enabled = False
            Properties.DecimalPlaces = 0
            Properties.DisplayFormat = '0'
            Properties.MaxLength = 5
            Properties.Nullable = False
            Style.TransparentBorder = False
            TabOrder = 4
            Width = 40
          end
          object edtMargemParesSimulador: TcxCurrencyEdit
            Left = 160
            Top = 30
            Enabled = False
            Properties.DisplayFormat = '0.##'
            Properties.MaxLength = 5
            Properties.Nullable = False
            Style.TransparentBorder = False
            TabOrder = 5
            Width = 40
          end
          object edtMargemQuadrantesSimulador: TcxCurrencyEdit
            Left = 160
            Top = 105
            Properties.DisplayFormat = '0.##'
            Properties.MaxLength = 5
            Properties.Nullable = False
            Style.TransparentBorder = False
            TabOrder = 17
            Width = 40
          end
          object edtMaximoQuadrantesSimulador: TcxCurrencyEdit
            Left = 115
            Top = 105
            Properties.DecimalPlaces = 0
            Properties.DisplayFormat = '0'
            Properties.MaxLength = 5
            Properties.Nullable = False
            Style.TransparentBorder = False
            TabOrder = 16
            Width = 40
          end
          object ccbLinhaSimulador: TcxCheckComboBox
            Left = 300
            Top = 5
            Enabled = False
            Properties.Delimiter = ', '
            Properties.EmptySelectionText = 'Nenhuma'
            Properties.DropDownSizeable = True
            Properties.ImmediatePost = True
            Properties.Items = <>
            Style.TransparentBorder = False
            TabOrder = 22
            Width = 100
          end
          object edtMargemLinhaSimulador: TcxCurrencyEdit
            Left = 405
            Top = 5
            Enabled = False
            Properties.DisplayFormat = '0.##'
            Properties.MaxLength = 5
            Properties.Nullable = False
            Style.TransparentBorder = False
            TabOrder = 23
            Width = 40
          end
          object edtMargemSequenciaSimulador: TcxCurrencyEdit
            Left = 405
            Top = 55
            Enabled = False
            Properties.DisplayFormat = '0.##'
            Properties.MaxLength = 5
            Properties.Nullable = False
            Style.TransparentBorder = False
            TabOrder = 29
            Width = 40
          end
          object ccbSequenciaSimulador: TcxCheckComboBox
            Left = 300
            Top = 55
            Enabled = False
            Properties.Delimiter = ', '
            Properties.EmptySelectionText = 'Nenhuma'
            Properties.DropDownSizeable = True
            Properties.ImmediatePost = True
            Properties.Items = <>
            Style.TransparentBorder = False
            TabOrder = 28
            Width = 100
          end
          object ccbFrequenciaSimulador: TcxCheckComboBox
            Left = 300
            Top = 80
            Properties.Delimiter = ', '
            Properties.EmptySelectionText = 'Nenhuma'
            Properties.DropDownSizeable = True
            Properties.ImmediatePost = True
            Properties.Items = <>
            Style.TransparentBorder = False
            TabOrder = 30
            Width = 100
          end
          object edtMargemFrequenciaSimulador: TcxCurrencyEdit
            Left = 405
            Top = 80
            Properties.DisplayFormat = '0.##'
            Properties.MaxLength = 5
            Properties.Nullable = False
            Style.TransparentBorder = False
            TabOrder = 31
            Width = 40
          end
          object cbQuocienteFrequenciaSimulador: TcxComboBox
            Left = 600
            Top = 5
            Properties.DropDownListStyle = lsFixedList
            Properties.Items.Strings = (
              'N'#227'o'
              'Sim')
            Style.TransparentBorder = False
            TabOrder = 36
            Text = 'N'#227'o'
            Width = 50
          end
          object lblQuocienteFrequenciaSimulador: TcxLabel
            Left = 475
            Top = 8
            Caption = 'Quociente de frequ'#234'ncia:'
            FocusControl = cbQuocienteFrequenciaSimulador
            Style.TransparentBorder = False
            Properties.Alignment.Horz = taRightJustify
            Transparent = True
            AnchorX = 597
          end
          object lblNumeroAcertos: TcxLabel
            Left = 556
            Top = 58
            Caption = 'Acertos:'
            Style.TransparentBorder = False
            Properties.Alignment.Horz = taRightJustify
            Transparent = True
            AnchorX = 597
          end
          object lblDezenasPorAcerto: TcxLabel
            Left = 499
            Top = 83
            Caption = 'Dezenas por acerto:'
            Style.TransparentBorder = False
            Properties.Alignment.Horz = taRightJustify
            Transparent = True
            AnchorX = 597
          end
          object lblSorteios: TcxLabel
            Left = 554
            Top = 108
            Caption = 'Sorteios:'
            Style.TransparentBorder = False
            Properties.Alignment.Horz = taRightJustify
            Transparent = True
            AnchorX = 597
          end
          object edtSorteioInicial: TcxCurrencyEdit
            Left = 600
            Top = 105
            EditValue = 0.000000000000000000
            Properties.DecimalPlaces = 0
            Properties.DisplayFormat = '0'
            Properties.MaxLength = 5
            Style.TransparentBorder = False
            TabOrder = 42
            Width = 40
          end
          object edtSorteioFinal: TcxCurrencyEdit
            Left = 645
            Top = 105
            EditValue = 0.000000000000000000
            Properties.DecimalPlaces = 0
            Properties.DisplayFormat = '0'
            Properties.MaxLength = 5
            Style.TransparentBorder = False
            TabOrder = 43
            Width = 40
          end
          object edtDezenasPorAcerto: TcxCurrencyEdit
            Left = 600
            Top = 80
            EditValue = 0.000000000000000000
            Properties.DecimalPlaces = 0
            Properties.DisplayFormat = '0'
            Properties.MaxLength = 5
            Style.TransparentBorder = False
            TabOrder = 41
            Width = 40
          end
          object edtNumeroAcertos: TcxCurrencyEdit
            Left = 600
            Top = 55
            EditValue = 0.000000000000000000
            Properties.DecimalPlaces = 0
            Properties.DisplayFormat = '0'
            Properties.MaxLength = 5
            Style.TransparentBorder = False
            TabOrder = 39
            Width = 40
          end
          object chkLinhaSimulador: TcxCheckBox
            Left = 277
            Top = 5
            Properties.Alignment = taRightJustify
            Properties.ImmediatePost = True
            Properties.OnChange = chkLinhaSimuladorPropertiesChange
            TabOrder = 21
            Transparent = True
          end
          object chkSequenciaSimulador: TcxCheckBox
            Left = 277
            Top = 55
            Properties.Alignment = taRightJustify
            Properties.ImmediatePost = True
            Properties.OnChange = chkSequenciaSimuladorPropertiesChange
            TabOrder = 27
            Transparent = True
          end
          object btnExecutarSimulador: TcxButton
            Left = 785
            Top = 80
            Width = 80
            Height = 20
            Anchors = [akTop, akRight]
            Caption = 'Executar'
            OptionsImage.Glyph.Data = {
              36040000424D3604000000000000360000002800000010000000100000000100
              2000000000000004000000000000000000000000000000000000000000000000
              0000000000020000000A0000000F000000110000001100000012000000120000
              00140000001804030339261F1BDD2D2520FF0000001A00000003000000000000
              000000000009896253C1BF8873FFBE8872FFBE8772FFBE856FFFBF8A76FFD5B5
              A8FFC0A89FFF4E433EFF786E65FF473C36FF97847DC80000000C000000000000
              00000000000DBF8C7AFFF0F8F8FFE4EFF0FFE2EDF0FFE9F1F1FFEDEEECFFD6D2
              CEFF564F48FF887B72FF534B46FFD6CEC9FFD5B4A9FF00000012000000000000
              0000050C0E1AB99E91FFD3F0F7FF80B1D4FF71A2CFFFC7E1E6FFC1CCCDFF5852
              4CFF988B80FF574F49FFD5CBC5FFF5EDE8FFC18B7AFF0000001300000000050C
              0E0E0E1F2530B9C3C5FF91CFEAFF1763BDFF266BBAFF98C5D2FF585752FFAB9C
              8FFF5A544FFFD1CDC9FFF2E8E1FFF9F1EDFFC18A77FF000000120413171B0D2C
              353A1746526090C5D4FF216EC3FF38BBEEFF0F59B8FF4A6A76FFBAAC9FFF5958
              54FFBBCDD0FFE2E7E6FFF4EDE8FFFAF4EFFFC38E7AFF000000110F5AA4CE0F63
              C5FF0E5FC1FF0E5DBEFF36B9EFFF3FD2FBFF33A5E1FFB5C0C2FF4A6A76FF8EC7
              D6FFBAE1EAFFD9EAECFFEEEEE9FFFAF5F1FFC6927DFF000000100C3749541572
              CAF938C9F7FF3BCFFAFF3BCFFAFF3BCEFAFF3BCFFAFF30A2E0FF0D56B7FF246C
              BDFF69A5D3FFCEE9EDFFEAEEECFFFBF6F3FFC79681FF0000000F0B202628155F
              8FA6559BDDFFAAE8FCFF8AE0FBFF4DD0FAFF37CBF8FF36CBF8FF2DB1EAFF1461
              BCFF71B2DAFFCDEAEFFFEAEFEDFFFCF7F4FFC99886FF0000000F09171B1B1648
              575C146ECCFBD6F0FCFFD8F4FDFFD2F2FDFF89DEFAFF2AAFEAFF1D6CC2FF79C9
              E9FFB5E5F3FFD4EDF1FFECF1F0FFFDF9F7FFCC9B8AFF0000000E061317181351
              727F4890DBFFF4FCFEFFE3F2FBFFEAF9FEFFE4F7FEFF105DBEFF7FD7F2FFACE5
              F6FFC7ECF6FFDEF0F3FFF0F3F1FFFDFAF8FFCEA08FFF0000000D04101314115F
              97AF88B9E8FF448EDAFF1671CFFF639CDCFFE9F4FCFF0E5FC1FF9DE1F5FFC2EB
              F8FFD6EFF6FFE9F2F4FFF7F5F1FFFDFBF9FFD0A493FF0000000C010A0C0D105A
              94AC126095AD89C0DBFFAFEAFBFF68B8E7FF2F7ED1FF166AC8FFB5E7F6FFD6F0
              F7FFE5F3F6FFF2F5F5FFFCF7F3FFFDFCFAFFD2A897FF0000000B01030404010A
              0C0D07161A21CEC6C1FFDFF8FFFFCEEFF9FFA7DDF3FF4895D9FFD2EFF7FFE9F4
              F8FFF2F6F6FFFBF7F6FFFCF7F5FFFEFCFCFFD5AA9AFF0000000B000000000000
              000000000005D6B4A5FFF5FDFFFFF0FBFFFFEAFAFFFFE7FAFDFFF2FBFEFFFAFD
              FEFFFFFEFEFFFEFEFEFFFEFEFEFFFEFEFDFFD7AE9EFF00000009000000000000
              000000000003A3867AC0DBB5A5FFDAB5A4FFDAB5A4FFDAB4A4FFD9B3A3FFD9B3
              A3FFD9B3A2FFD9B2A2FFD8B2A2FFD8B1A1FFA08277C200000006}
            TabOrder = 56
            OnClick = btnExecutarSimuladorClick
          end
          object lblRepeticaoDezenasSimulador: TcxLabel
            Left = 14
            Top = 133
            Caption = 'Repeti'#231#227'o:'
            FocusControl = edtMinimoRepeticaoDezenasSimulador
            Style.TransparentBorder = False
            Properties.Alignment.Horz = taRightJustify
            Transparent = True
            AnchorX = 66
          end
          object edtMinimoRepeticaoDezenasSimulador: TcxCurrencyEdit
            Left = 70
            Top = 130
            Properties.DecimalPlaces = 0
            Properties.DisplayFormat = '0'
            Properties.MaxLength = 5
            Properties.Nullable = False
            Style.TransparentBorder = False
            TabOrder = 18
            Width = 40
          end
          object edtMaximoRepeticaoDezenasSimulador: TcxCurrencyEdit
            Left = 115
            Top = 130
            Properties.DecimalPlaces = 0
            Properties.DisplayFormat = '0'
            Properties.MaxLength = 5
            Properties.Nullable = False
            Style.TransparentBorder = False
            TabOrder = 19
            Width = 40
          end
          object edtMargemRepeticaoDezenasSimulador: TcxCurrencyEdit
            Left = 160
            Top = 130
            Properties.DisplayFormat = '0.##'
            Properties.MaxLength = 5
            Properties.Nullable = False
            Style.TransparentBorder = False
            TabOrder = 20
            Width = 40
          end
          object chkColunaSimulador: TcxCheckBox
            Left = 277
            Top = 30
            Properties.Alignment = taRightJustify
            Properties.ImmediatePost = True
            Properties.OnChange = chkColunaSimuladorPropertiesChange
            TabOrder = 24
            Transparent = True
          end
          object ccbColunaSimulador: TcxCheckComboBox
            Left = 300
            Top = 30
            Enabled = False
            Properties.Delimiter = ', '
            Properties.EmptySelectionText = 'Nenhuma'
            Properties.DropDownSizeable = True
            Properties.ImmediatePost = True
            Properties.Items = <>
            Style.TransparentBorder = False
            TabOrder = 25
            Width = 100
          end
          object edtMargemColunaSimulador: TcxCurrencyEdit
            Left = 405
            Top = 30
            Enabled = False
            Properties.DisplayFormat = '0.##'
            Properties.MaxLength = 5
            Properties.Nullable = False
            Style.TransparentBorder = False
            TabOrder = 26
            Width = 40
          end
          object lblFrequenciaSimulador: TcxLabel
            Left = 240
            Top = 83
            Caption = 'Frequ'#234'ncia:'
            FocusControl = cbQuocienteFrequenciaSimulador
            Style.TransparentBorder = False
            Properties.Alignment.Horz = taRightJustify
            Transparent = True
            AnchorX = 297
          end
          object edtMinimoPrimosSimulador: TcxCurrencyEdit
            Left = 70
            Top = 55
            Enabled = False
            Properties.DecimalPlaces = 0
            Properties.DisplayFormat = '0'
            Properties.MaxLength = 5
            Properties.Nullable = False
            Style.TransparentBorder = False
            TabOrder = 7
            Width = 40
          end
          object edtMaximoPrimosSimulador: TcxCurrencyEdit
            Left = 115
            Top = 55
            Enabled = False
            Properties.DecimalPlaces = 0
            Properties.DisplayFormat = '0'
            Properties.MaxLength = 5
            Properties.Nullable = False
            Style.TransparentBorder = False
            TabOrder = 8
            Width = 40
          end
          object edtMargemPrimosSimulador: TcxCurrencyEdit
            Left = 160
            Top = 55
            Enabled = False
            Properties.DisplayFormat = '0.##'
            Properties.MaxLength = 5
            Properties.Nullable = False
            Style.TransparentBorder = False
            TabOrder = 9
            Width = 40
          end
          object lblAtrasoSimulador: TcxLabel
            Left = 261
            Top = 108
            Caption = 'Atraso:'
            FocusControl = ccbAtrasoSimulador
            Style.TransparentBorder = False
            Properties.Alignment.Horz = taRightJustify
            Transparent = True
            AnchorX = 297
          end
          object ccbAtrasoSimulador: TcxCheckComboBox
            Left = 300
            Top = 105
            Properties.Delimiter = ', '
            Properties.EmptySelectionText = 'Nenhuma'
            Properties.DropDownSizeable = True
            Properties.ImmediatePost = True
            Properties.Items = <>
            Style.TransparentBorder = False
            TabOrder = 32
            Width = 100
          end
          object edtMargemAtrasoSimulador: TcxCurrencyEdit
            Left = 405
            Top = 105
            Properties.DisplayFormat = '0.##'
            Properties.MaxLength = 5
            Properties.Nullable = False
            Style.TransparentBorder = False
            TabOrder = 33
            Width = 40
          end
          object lblQuocienteAtrasoSimulador: TcxLabel
            Left = 495
            Top = 33
            Caption = 'Quociente de atraso:'
            FocusControl = cbQuocienteAtrasoSimulador
            Style.TransparentBorder = False
            Properties.Alignment.Horz = taRightJustify
            Transparent = True
            AnchorX = 597
          end
          object cbQuocienteAtrasoSimulador: TcxComboBox
            Left = 600
            Top = 30
            Properties.DropDownListStyle = lsFixedList
            Properties.Items.Strings = (
              'N'#227'o'
              'Sim')
            Style.TransparentBorder = False
            TabOrder = 37
            Text = 'N'#227'o'
            Width = 50
          end
          object lblParesSimulador: TcxLabel
            Left = 16
            Top = 33
            Caption = 'Pares:'
            FocusControl = edtMinimoParesAposta
            Style.TransparentBorder = False
            Properties.Alignment.Horz = taRightJustify
            Transparent = True
            AnchorX = 47
          end
          object lblPrimosSimulador: TcxLabel
            Left = 12
            Top = 58
            Caption = 'Primos:'
            FocusControl = edtMinimoPrimosAposta
            Style.TransparentBorder = False
            Properties.Alignment.Horz = taRightJustify
            Transparent = True
            AnchorX = 47
          end
          object lblLinhaSimulador: TcxLabel
            Left = 249
            Top = 8
            Caption = 'Linha:'
            FocusControl = ccbLinhaAposta
            Style.TransparentBorder = False
            Properties.Alignment.Horz = taRightJustify
            Transparent = True
            AnchorX = 278
          end
          object lblColunaSimulador: TcxLabel
            Left = 241
            Top = 33
            Caption = 'Coluna:'
            FocusControl = ccbColunaAposta
            Style.TransparentBorder = False
            Properties.Alignment.Horz = taRightJustify
            Transparent = True
            AnchorX = 278
          end
          object lblSequenciaSimulador: TcxLabel
            Left = 225
            Top = 58
            Caption = 'Sequ'#234'ncia:'
            FocusControl = ccbSequenciaAposta
            Style.TransparentBorder = False
            Properties.Alignment.Horz = taRightJustify
            Transparent = True
            AnchorX = 278
          end
          object chkPrimosSimulador: TcxCheckBox
            Left = 46
            Top = 55
            Properties.Alignment = taRightJustify
            Properties.ImmediatePost = True
            Properties.OnChange = chkPrimosSimuladorPropertiesChange
            TabOrder = 6
            Transparent = True
          end
          object chkParesSimulador: TcxCheckBox
            Left = 46
            Top = 30
            Properties.Alignment = taRightJustify
            Properties.ImmediatePost = True
            Properties.OnChange = chkParesSimuladorPropertiesChange
            TabOrder = 2
            Transparent = True
          end
          object btnImportarParametrosSimulador: TcxButton
            Left = 785
            Top = 5
            Width = 80
            Height = 20
            Anchors = [akTop, akRight]
            Caption = 'Importar'
            OptionsImage.Glyph.Data = {
              36040000424D3604000000000000360000002800000010000000100000000100
              2000000000000004000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000090000
              000E000000100000001000000010000000100000001000000011000000110000
              001100000011000000100000000B00000003000000000000000019427ACA245A
              A5FF255CA7FF255BA7FF245AA6FF2459A6FF2358A5FF2358A4FF2356A4FF2256
              A4FF2255A3FF2154A3FF2153A1FF1C468AE303080F2900000002255DA5FF316B
              AEFF6DA6D5FF86CAF0FF46A6E4FF44A3E4FF41A1E3FF3FA0E2FF3C9EE2FF3B9C
              E1FF389BE0FF369AE0FF3498DFFF2C77C1FF10284D8B000000082B68AEFF4984
              BEFF4B8BC5FFB2E3F8FF68BBECFF55B0E8FF52AEE8FF4EACE7FF4CA9E6FF49A8
              E5FF47A6E4FF44A4E4FF41A2E3FF3A92D6FF1C4885D50000000D2F6FB4FF6CA7
              D2FF3F87C4FFAED9F0FF9AD8F5FF66BDEEFF63BBEDFF60B9EBFF5DB6EBFF5BB5
              EAFF57B2EAFF55B0E9FF51AEE7FF4FABE7FF2D69B1FF040B142F3276B9FF8FC7
              E6FF509FD4FF86BCE0FFC5EFFCFF78CAF2FF74C8F1FF72C5F0FF6FC4F0FF6DC2
              EFFF69C0EEFF66BDEEFF63BBEDFF60B9EBFF448BC9FF122D4D81357CBCFFAFE3
              F5FF75C8EDFF59A2D4FFDDF7FDFFDFF8FEFFDDF7FEFFDBF7FEFFD8F5FEFFD4F4
              FDFFD0F2FDFFCCEFFCFFC7EDFBFFC1EBFBFF9ACBE9FF215187CB3882C1FFC7F5
              FEFF97E5FCFF64BAE5FF4D9FD3FF4D9DD2FF4B9BD1FF4A99CFFF4998CFFF4896
              CEFF4694CCFF4592CBFF3073B7FF3072B6FF2F71B5FF2A65A4EA3A88C5FFCDF7
              FEFFA6ECFEFF9CE8FDFF93E4FBFF8EE1FBFF89DFFBFF86DEFAFF81DAFAFF7ED8
              F9FF7BD7F9FF79D6F9FF2A6BB0FF000000140000000A000000073D8EC8FFD0F8
              FEFFAEF0FEFFAAEEFEFFA6EDFEFFA5EBFDFFBBF2FDFFD4F9FEFFD5F9FEFFD3F8
              FEFFD1F8FEFFCEF7FDFF3680BFFF0000000800000000000000003F92CBFFD3F9
              FEFFB6F3FEFFB3F1FDFFB0F1FEFFB8EDFAFF4895CBFF3B8CC6FF3B8AC6FF3A89
              C5FF3A88C5FF3A87C3FF2A6391C20000000500000000000000004197CEFFE2FC
              FEFFE2FCFEFFE1FCFEFFD4F3FAFF458FBFEC040A0E1B00000006000000060000
              000600000006000000060000000400000001000000000000000031739ABF429A
              D0FF4299D0FF4299D0FF4297CFFF153244590000000200000000000000000000
              0000000000000000000000000000000000000000000000000000000000020000
              0003000000030000000400000003000000020000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000}
            TabOrder = 44
            OnClick = btnImportarParametrosSimuladorClick
          end
          object btnExportarParametrosSimulador: TcxButton
            Left = 785
            Top = 30
            Width = 80
            Height = 20
            Anchors = [akTop, akRight]
            Caption = 'Exportar'
            OptionsImage.Glyph.Data = {
              36040000424D3604000000000000360000002800000010000000100000000100
              2000000000000004000000000000000000000000000000000000000000020000
              000A0000000F0000001000000010000000100000001100000011000000110000
              001100000011000000100000000B000000030000000000000000000000094633
              2CC160453BFF644A41FFB87D4EFFB97A4AFFB47444FFB06C3DFFA76436FFA764
              36FF583D36FF5B3E37FF3B2821C20000000A00000000000000000000000D6F53
              47FF947869FF6A4F46FFD8B07BFFD7AE77FFD7AB74FFD6A970FFD5A66DFFD4A5
              6AFF5D413AFF684B41FF543931FF0000000E00000000000000000000000C7357
              4AFF987D6EFF70564BFFFFFFFFFFF6EFEAFFF6EFE9FFF6EEE9FFF5EEE9FFF6EE
              E8FF62473FFF715348FF573B33FF0000000F00000000000000000000000B785C
              4EFF9D8273FF765C50FFFFFFFFFFF7F0EBFFF7F0EBFFF7EFEBFFF6EFEAFFF6EF
              EAFF684E44FF72554BFF593E35FF0000000E00000000000000000000000A7C60
              50FFA28777FF7B6154FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFBFBFFF1F1
              F1FF89756EFF8A7269FF5F443BFF0000000C0000000000000000000000097F63
              54FFA78E7DFF977A6AFF967969FF957967FF84695CFF705548FF8F7B73FF0B67
              37FF295D3CFF81746BFF806C63FF0000000C0000000000000000000000088367
              57FFAB9382FF634A41FF614740FF5E463DFF5C443CFF5B433BFF776761FF0A67
              37FF2AAF7FFF106137FF5B6352FF00000016000000030000000000000007866A
              59FFAF9788FF674E44FFF3EAE4FFE8D9CEFFE9DFD7FFE5DBD5FFD8CFC9FF0B6A
              3BFF4EDCB2FF27C48DFF0C7746FF022E179C000201190000000500000006886D
              5CFFB39C8CFF6B5248FFF4ECE6FFEBE3DCFF47916BFF046B38FF046B38FF056B
              38FF3AD7A5FF37D6A2FF32D3A0FF199966FF044A26D5000D063A000000058B70
              5EFFB7A091FF70564DFFF6EFEAFFEBE4DFFF167E4EFF84EDD1FF52E1B6FF4DDF
              B3FF48DDAFFF44DBACFF3FD9A9FF3AD7A6FF32BE8EFF0F6A3FF6000000048E72
              60FFBBA595FF755A50FFF7F1ECFFF1EEEBFF188252FFB8F7E7FFB4F5E6FFAFF4
              E4FF85EDD2FF52E1B7FF4DDFB3FF5DE2BBFF66D0AEFF16794CF6000000026A56
              49BF8F7361FF795E54FF765D52FFAFA19CFF3B8963FF0D814DFF0D804DFF0D80
              4CFF95F1DAFF65E7C2FF83ECCFFF57B28FFF065932D2010E0832000000010000
              000200000003000000030000000300000005000000090000000C000000140D7B
              4BF2AEF6E5FF94E5CEFF339167FD033A1F910001010F00000003000000000000
              0000000000000000000000000000000000000000000000000000000000070F7F
              4EF287CBB3FF106D42E6011C0F4C000000060000000100000000000000000000
              0000000000000000000000000000000000000000000000000000000000051081
              52F1034B2AAE0007041700000003000000010000000000000000}
            TabOrder = 45
            OnClick = btnExportarParametrosSimuladorClick
          end
          object btnLimparParametrosSimulador: TcxButton
            Left = 785
            Top = 55
            Width = 80
            Height = 20
            Anchors = [akTop, akRight]
            Caption = 'Limpar'
            OptionsImage.Glyph.Data = {
              36040000424D3604000000000000360000002800000010000000100000000100
              2000000000000004000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              00020000000900000012000000180000001A0000001A00000018000000100000
              0005000000010000000000000000000000000000000000000000000000020000
              000D3524146A936338E5A56B3AFFA36938FFA16736FF9D6233FB633E20B70805
              022800000006000000010000000000000000000000000000000000000008442F
              1D78C18B59FEE1AC76FFE4C296FFB5793BFFB5793CFFB5793CFFAD7239FF7E50
              2AD80302042A00000006000000010000000000000000000000000000000DB07D
              4EF3E6B17AFFE9B47DFFE9B47DFFE7C79DFFB67A3DFFB57A3DFFB57A3DFF6953
              7BFF090E5ED50001052800000006000000010000000000000000000000086A4E
              329DEFD7B3FFE9B47DFFE9B47DFFE9B47DFFEACDA4FFB57B3EFF735C86FF313F
              CCFF2935B8FF0B1161D501010627000000050000000100000000000000010000
              000C745538A5F2DDBBFFE9B47DFFE9B47DFFE9B47DFFD1CEE1FF3443CEFF3443
              CDFF3443CEFF2C39BAFF0D1463D4010106260000000500000001000000000000
              00020000000B76583BA4F5E2C1FFE9B47DFFB5A9B8FF829FF1FFB1C9F5FF3949
              D1FF3A4AD1FF3A49D1FF303FBDFF111767D30101062500000005000000000000
              0000000000010000000B785B3DA3E9E1D2FF87A3F2FF87A4F1FF87A3F2FFB9D0
              F7FF3E50D5FF3E50D5FF3F50D5FF3545C2FF141B6AD201010622000000000000
              000000000000000000010000000A2C386FA2C9E2F9FF8CA8F3FF8DA8F3FF8CA8
              F3FFC0D8F9FF4457D9FF4356D9FF4456D9FF3949C2FF141A61C2000000000000
              000000000000000000000000000100000009303D74A1CFE7FBFF92ADF3FF91AD
              F4FF92ADF4FFC6DEFAFF495EDBFF495DDCFF475AD7FF232F8BF0000000000000
              00000000000000000000000000000000000100000008334177A0D4ECFCFF97B2
              F5FF97B2F4FF97B3F5FFCCE4FBFF4A5FDAFF3141A4F6090C214A000000000000
              000000000000000000000000000000000000000000010000000736457A9FD8F0
              FDFF9DB7F5FF9CB7F5FFD9F1FEFF6B81CAF50B0E234700000006000000000000
              0000000000000000000000000000000000000000000000000001000000063947
              7D9EDBF3FEFFDBF3FFFF677FCFF513192C440000000500000001000000000000
              0000000000000000000000000000000000000000000000000000000000010000
              00053543728E4F63AACD151A2D40000000040000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0001000000030000000400000002000000000000000000000000}
            TabOrder = 46
            OnClick = btnLimparParametrosSimuladorClick
          end
          object lblIntervaloSimulador: TcxLabel
            Left = 9
            Top = 83
            Caption = 'Interv.:'
            FocusControl = edtMinimoPrimosAposta
            Style.TransparentBorder = False
            Properties.Alignment.Horz = taRightJustify
            Transparent = True
            AnchorX = 47
          end
          object chkIntervaloSimulador: TcxCheckBox
            Left = 46
            Top = 80
            Properties.Alignment = taRightJustify
            Properties.ImmediatePost = True
            Properties.OnChange = chkIntervaloSimuladorPropertiesChange
            TabOrder = 10
            Transparent = True
          end
          object edtMinimoIntervaloSimulador: TcxCurrencyEdit
            Left = 70
            Top = 80
            Enabled = False
            Properties.DecimalPlaces = 0
            Properties.DisplayFormat = '0'
            Properties.MaxLength = 5
            Properties.Nullable = False
            Style.TransparentBorder = False
            TabOrder = 11
            Width = 40
          end
          object edtMaximoIntervaloSimulador: TcxCurrencyEdit
            Left = 115
            Top = 80
            Enabled = False
            Properties.DecimalPlaces = 0
            Properties.DisplayFormat = '0'
            Properties.MaxLength = 5
            Properties.Nullable = False
            Style.TransparentBorder = False
            TabOrder = 12
            Width = 40
          end
          object edtMargemIntervaloSimulador: TcxCurrencyEdit
            Left = 160
            Top = 80
            Enabled = False
            Properties.DisplayFormat = '0.##'
            Properties.MaxLength = 5
            Properties.Nullable = False
            Style.TransparentBorder = False
            TabOrder = 13
            Width = 40
          end
        end
        object grdResultadoSimulador: TcxGrid
          Left = 0
          Top = 160
          Width = 872
          Height = 456
          Margins.Left = 5
          Margins.Top = 5
          Margins.Right = 5
          Margins.Bottom = 5
          Align = alClient
          TabOrder = 1
          object grdResultadoSimuladorView: TcxGridDBTableView
            Navigator.Buttons.CustomButtons = <>
            OnEditKeyDown = grdHistoricoViewEditKeyDown
            DataController.DataSource = dsSimulador
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            OptionsData.Deleting = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            OptionsSelection.MultiSelect = True
            OptionsSelection.CellMultiSelect = True
            OptionsSelection.InvertSelect = False
          end
          object grdResultadoSimuladorLevel: TcxGridLevel
            GridView = grdResultadoSimuladorView
          end
        end
      end
    end
    object tsAposta: TcxTabSheet
      Caption = 'Aposta'
      ImageIndex = 3
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object pnlAposta: TPanel
        AlignWithMargins = True
        Left = 5
        Top = 5
        Width = 872
        Height = 616
        Margins.Left = 5
        Margins.Top = 5
        Margins.Right = 5
        Margins.Bottom = 5
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object pnlParametrosAposta: TPanel
          Left = 0
          Top = 0
          Width = 872
          Height = 160
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          DesignSize = (
            872
            160)
          object lblParesAposta: TcxLabel
            Left = 35
            Top = 33
            Caption = 'Pares:'
            FocusControl = edtMinimoParesAposta
            Style.TransparentBorder = False
            Properties.Alignment.Horz = taRightJustify
            Transparent = True
            AnchorX = 66
          end
          object lblQuadrantesAposta: TcxLabel
            Left = 5
            Top = 108
            Caption = 'Quadrantes:'
            FocusControl = edtMinimoQuadrantesAposta
            Style.TransparentBorder = False
            Properties.Alignment.Horz = taRightJustify
            Transparent = True
            AnchorX = 66
          end
          object lblLinhaAposta: TcxLabel
            Left = 268
            Top = 8
            Caption = 'Linha:'
            FocusControl = ccbLinhaAposta
            Style.TransparentBorder = False
            Properties.Alignment.Horz = taRightJustify
            Transparent = True
            AnchorX = 297
          end
          object lblFrequenciaAposta: TcxLabel
            Left = 240
            Top = 83
            Caption = 'Frequ'#234'ncia:'
            FocusControl = ccbFrequenciaAposta
            Style.TransparentBorder = False
            Properties.Alignment.Horz = taRightJustify
            Transparent = True
            AnchorX = 297
          end
          object lblQuantidadeJogosAposta: TcxLabel
            Left = 34
            Top = 8
            Caption = 'Jogos:'
            FocusControl = edtQuantidadeJogosAposta
            Style.TransparentBorder = False
            Properties.Alignment.Horz = taRightJustify
            Transparent = True
            AnchorX = 66
          end
          object lblSequenciaAposta: TcxLabel
            Left = 244
            Top = 58
            Caption = 'Sequ'#234'ncia:'
            FocusControl = ccbSequenciaAposta
            Style.TransparentBorder = False
            Properties.Alignment.Horz = taRightJustify
            Transparent = True
            AnchorX = 297
          end
          object ccbLinhaAposta: TcxCheckComboBox
            Left = 300
            Top = 5
            Properties.Delimiter = ', '
            Properties.EmptySelectionText = 'Nenhuma'
            Properties.DropDownSizeable = True
            Properties.ImmediatePost = True
            Properties.Items = <>
            Style.TransparentBorder = False
            TabOrder = 22
            Width = 100
          end
          object btnGerarAposta: TcxButton
            Left = 785
            Top = 80
            Width = 80
            Height = 20
            Anchors = [akTop, akRight]
            Caption = 'Gerar'
            OptionsImage.Glyph.Data = {
              36040000424D3604000000000000360000002800000010000000100000000100
              2000000000000004000000000000000000000000000000000000000000000000
              0000000000020000000A0000000F000000110000001100000012000000120000
              00140000001804030339261F1BDD2D2520FF0000001A00000003000000000000
              000000000009896253C1BF8873FFBE8872FFBE8772FFBE856FFFBF8A76FFD5B5
              A8FFC0A89FFF4E433EFF786E65FF473C36FF97847DC80000000C000000000000
              00000000000DBF8C7AFFF0F8F8FFE4EFF0FFE2EDF0FFE9F1F1FFEDEEECFFD6D2
              CEFF564F48FF887B72FF534B46FFD6CEC9FFD5B4A9FF00000012000000000000
              0000050C0E1AB99E91FFD3F0F7FF80B1D4FF71A2CFFFC7E1E6FFC1CCCDFF5852
              4CFF988B80FF574F49FFD5CBC5FFF5EDE8FFC18B7AFF0000001300000000050C
              0E0E0E1F2530B9C3C5FF91CFEAFF1763BDFF266BBAFF98C5D2FF585752FFAB9C
              8FFF5A544FFFD1CDC9FFF2E8E1FFF9F1EDFFC18A77FF000000120413171B0D2C
              353A1746526090C5D4FF216EC3FF38BBEEFF0F59B8FF4A6A76FFBAAC9FFF5958
              54FFBBCDD0FFE2E7E6FFF4EDE8FFFAF4EFFFC38E7AFF000000110F5AA4CE0F63
              C5FF0E5FC1FF0E5DBEFF36B9EFFF3FD2FBFF33A5E1FFB5C0C2FF4A6A76FF8EC7
              D6FFBAE1EAFFD9EAECFFEEEEE9FFFAF5F1FFC6927DFF000000100C3749541572
              CAF938C9F7FF3BCFFAFF3BCFFAFF3BCEFAFF3BCFFAFF30A2E0FF0D56B7FF246C
              BDFF69A5D3FFCEE9EDFFEAEEECFFFBF6F3FFC79681FF0000000F0B202628155F
              8FA6559BDDFFAAE8FCFF8AE0FBFF4DD0FAFF37CBF8FF36CBF8FF2DB1EAFF1461
              BCFF71B2DAFFCDEAEFFFEAEFEDFFFCF7F4FFC99886FF0000000F09171B1B1648
              575C146ECCFBD6F0FCFFD8F4FDFFD2F2FDFF89DEFAFF2AAFEAFF1D6CC2FF79C9
              E9FFB5E5F3FFD4EDF1FFECF1F0FFFDF9F7FFCC9B8AFF0000000E061317181351
              727F4890DBFFF4FCFEFFE3F2FBFFEAF9FEFFE4F7FEFF105DBEFF7FD7F2FFACE5
              F6FFC7ECF6FFDEF0F3FFF0F3F1FFFDFAF8FFCEA08FFF0000000D04101314115F
              97AF88B9E8FF448EDAFF1671CFFF639CDCFFE9F4FCFF0E5FC1FF9DE1F5FFC2EB
              F8FFD6EFF6FFE9F2F4FFF7F5F1FFFDFBF9FFD0A493FF0000000C010A0C0D105A
              94AC126095AD89C0DBFFAFEAFBFF68B8E7FF2F7ED1FF166AC8FFB5E7F6FFD6F0
              F7FFE5F3F6FFF2F5F5FFFCF7F3FFFDFCFAFFD2A897FF0000000B01030404010A
              0C0D07161A21CEC6C1FFDFF8FFFFCEEFF9FFA7DDF3FF4895D9FFD2EFF7FFE9F4
              F8FFF2F6F6FFFBF7F6FFFCF7F5FFFEFCFCFFD5AA9AFF0000000B000000000000
              000000000005D6B4A5FFF5FDFFFFF0FBFFFFEAFAFFFFE7FAFDFFF2FBFEFFFAFD
              FEFFFFFEFEFFFEFEFEFFFEFEFEFFFEFEFDFFD7AE9EFF00000009000000000000
              000000000003A3867AC0DBB5A5FFDAB5A4FFDAB5A4FFDAB4A4FFD9B3A3FFD9B3
              A3FFD9B3A2FFD9B2A2FFD8B2A2FFD8B1A1FFA08277C200000006}
            TabOrder = 43
            OnClick = btnGerarApostaClick
          end
          object ccbFrequenciaAposta: TcxCheckComboBox
            Left = 300
            Top = 80
            Properties.Delimiter = ', '
            Properties.EmptySelectionText = 'Nenhuma'
            Properties.DropDownSizeable = True
            Properties.ImmediatePost = True
            Properties.Items = <>
            Style.TransparentBorder = False
            TabOrder = 28
            Width = 100
          end
          object ccbSequenciaAposta: TcxCheckComboBox
            Left = 300
            Top = 55
            Properties.Delimiter = ', '
            Properties.EmptySelectionText = 'Nenhuma'
            Properties.DropDownSizeable = True
            Properties.ImmediatePost = True
            Properties.Items = <>
            Style.TransparentBorder = False
            TabOrder = 26
            Width = 100
          end
          object edtMargemParesAposta: TcxCurrencyEdit
            Left = 160
            Top = 30
            Properties.DisplayFormat = '0.##'
            Properties.MaxLength = 5
            Properties.Nullable = False
            Style.TransparentBorder = False
            TabOrder = 5
            Width = 40
          end
          object edtMargemQuadrantesAposta: TcxCurrencyEdit
            Left = 160
            Top = 105
            Properties.DisplayFormat = '0.##'
            Properties.MaxLength = 5
            Properties.Nullable = False
            Style.TransparentBorder = False
            TabOrder = 16
            Width = 40
          end
          object edtMargemLinhaAposta: TcxCurrencyEdit
            Left = 405
            Top = 5
            Properties.DisplayFormat = '0.##'
            Properties.MaxLength = 5
            Properties.Nullable = False
            Style.TransparentBorder = False
            TabOrder = 23
            Width = 40
          end
          object edtMargemSequenciaAposta: TcxCurrencyEdit
            Left = 405
            Top = 55
            Properties.DisplayFormat = '0.##'
            Properties.MaxLength = 5
            Properties.Nullable = False
            Style.TransparentBorder = False
            TabOrder = 27
            Width = 40
          end
          object edtMargemFrequenciaAposta: TcxCurrencyEdit
            Left = 405
            Top = 80
            Properties.DisplayFormat = '0.##'
            Properties.MaxLength = 5
            Properties.Nullable = False
            Style.TransparentBorder = False
            TabOrder = 29
            Width = 40
          end
          object edtMinimoParesAposta: TcxCurrencyEdit
            Left = 70
            Top = 30
            Properties.DecimalPlaces = 0
            Properties.DisplayFormat = '0'
            Properties.MaxLength = 5
            Properties.Nullable = False
            Style.TransparentBorder = False
            TabOrder = 3
            Width = 40
          end
          object edtMaximoParesAposta: TcxCurrencyEdit
            Left = 115
            Top = 30
            Properties.DecimalPlaces = 0
            Properties.DisplayFormat = '0'
            Properties.MaxLength = 5
            Properties.Nullable = False
            Style.TransparentBorder = False
            TabOrder = 4
            Width = 40
          end
          object edtMinimoQuadrantesAposta: TcxCurrencyEdit
            Left = 70
            Top = 105
            Properties.DecimalPlaces = 0
            Properties.DisplayFormat = '0'
            Properties.MaxLength = 5
            Properties.Nullable = False
            Style.TransparentBorder = False
            TabOrder = 14
            Width = 40
          end
          object edtMaximoQuadrantesAposta: TcxCurrencyEdit
            Left = 115
            Top = 105
            Properties.DecimalPlaces = 0
            Properties.DisplayFormat = '0'
            Properties.MaxLength = 5
            Properties.Nullable = False
            Style.TransparentBorder = False
            TabOrder = 15
            Width = 40
          end
          object edtQuantidadeJogosAposta: TcxCurrencyEdit
            Left = 70
            Top = 5
            Properties.DecimalPlaces = 0
            Properties.DisplayFormat = '0'
            Properties.MaxLength = 5
            Properties.Nullable = False
            Style.TransparentBorder = False
            TabOrder = 1
            Width = 40
          end
          object cbQuocienteFrequenciaAposta: TcxComboBox
            Left = 600
            Top = 5
            Properties.DropDownListStyle = lsFixedList
            Properties.Items.Strings = (
              'N'#227'o'
              'Sim')
            Style.TransparentBorder = False
            TabOrder = 32
            Text = 'N'#227'o'
            Width = 50
          end
          object lblQuocienteFrequenciaAposta: TcxLabel
            Left = 475
            Top = 8
            Caption = 'Quociente de frequ'#234'ncia:'
            FocusControl = cbQuocienteFrequenciaAposta
            Style.TransparentBorder = False
            Properties.Alignment.Horz = taRightJustify
            Transparent = True
            AnchorX = 597
          end
          object lblRepeticaoDezenasAposta: TcxLabel
            Left = 14
            Top = 133
            Caption = 'Repeti'#231#227'o:'
            FocusControl = edtMinimoRepeticaoDezenasAposta
            Style.TransparentBorder = False
            Properties.Alignment.Horz = taRightJustify
            Transparent = True
            AnchorX = 66
          end
          object edtMinimoRepeticaoDezenasAposta: TcxCurrencyEdit
            Left = 70
            Top = 130
            Properties.DecimalPlaces = 0
            Properties.DisplayFormat = '0'
            Properties.MaxLength = 5
            Properties.Nullable = False
            Style.TransparentBorder = False
            TabOrder = 18
            Width = 40
          end
          object edtMaximoRepeticaoDezenasAposta: TcxCurrencyEdit
            Left = 115
            Top = 130
            Properties.DecimalPlaces = 0
            Properties.DisplayFormat = '0'
            Properties.MaxLength = 5
            Properties.Nullable = False
            Style.TransparentBorder = False
            TabOrder = 19
            Width = 40
          end
          object edtMargemRepeticaoDezenasAposta: TcxCurrencyEdit
            Left = 160
            Top = 130
            Properties.DisplayFormat = '0.##'
            Properties.MaxLength = 5
            Properties.Nullable = False
            Style.TransparentBorder = False
            TabOrder = 20
            Width = 40
          end
          object lblColunaAposta: TcxLabel
            Left = 260
            Top = 33
            Caption = 'Coluna:'
            FocusControl = ccbColunaAposta
            Style.TransparentBorder = False
            Properties.Alignment.Horz = taRightJustify
            Transparent = True
            AnchorX = 297
          end
          object ccbColunaAposta: TcxCheckComboBox
            Left = 300
            Top = 30
            Properties.Delimiter = ', '
            Properties.EmptySelectionText = 'Nenhuma'
            Properties.DropDownSizeable = True
            Properties.ImmediatePost = True
            Properties.Items = <>
            Style.TransparentBorder = False
            TabOrder = 24
            Width = 100
          end
          object edtMargemColunaAposta: TcxCurrencyEdit
            Left = 405
            Top = 30
            Properties.DisplayFormat = '0.##'
            Properties.MaxLength = 5
            Properties.Nullable = False
            Style.TransparentBorder = False
            TabOrder = 25
            Width = 40
          end
          object lblPrimosAposta: TcxLabel
            Left = 31
            Top = 58
            Caption = 'Primos:'
            FocusControl = edtMinimoPrimosAposta
            Style.TransparentBorder = False
            Properties.Alignment.Horz = taRightJustify
            Transparent = True
            AnchorX = 66
          end
          object edtMinimoPrimosAposta: TcxCurrencyEdit
            Left = 70
            Top = 55
            Properties.DecimalPlaces = 0
            Properties.DisplayFormat = '0'
            Properties.MaxLength = 5
            Properties.Nullable = False
            Style.TransparentBorder = False
            TabOrder = 7
            Width = 40
          end
          object edtMaximoPrimosAposta: TcxCurrencyEdit
            Left = 115
            Top = 55
            Properties.DecimalPlaces = 0
            Properties.DisplayFormat = '0'
            Properties.MaxLength = 5
            Properties.Nullable = False
            Style.TransparentBorder = False
            TabOrder = 8
            Width = 40
          end
          object edtMargemPrimosAposta: TcxCurrencyEdit
            Left = 160
            Top = 55
            Properties.DisplayFormat = '0.##'
            Properties.MaxLength = 5
            Properties.Nullable = False
            Style.TransparentBorder = False
            TabOrder = 9
            Width = 40
          end
          object lblAtrasoAposta: TcxLabel
            Left = 261
            Top = 108
            Caption = 'Atraso:'
            FocusControl = ccbAtrasoAposta
            Style.TransparentBorder = False
            Properties.Alignment.Horz = taRightJustify
            Transparent = True
            AnchorX = 297
          end
          object ccbAtrasoAposta: TcxCheckComboBox
            Left = 300
            Top = 105
            Properties.Delimiter = ', '
            Properties.EmptySelectionText = 'Nenhuma'
            Properties.DropDownSizeable = True
            Properties.ImmediatePost = True
            Properties.Items = <>
            Style.TransparentBorder = False
            TabOrder = 30
            Width = 100
          end
          object edtMargemAtrasoAposta: TcxCurrencyEdit
            Left = 405
            Top = 105
            Properties.DisplayFormat = '0.##'
            Properties.MaxLength = 5
            Properties.Nullable = False
            Style.TransparentBorder = False
            TabOrder = 31
            Width = 40
          end
          object lblQuocienteAtrasoAposta: TcxLabel
            Left = 495
            Top = 33
            Caption = 'Quociente de atraso:'
            FocusControl = cbQuocienteAtrasoAposta
            Style.TransparentBorder = False
            Properties.Alignment.Horz = taRightJustify
            Transparent = True
            AnchorX = 597
          end
          object cbQuocienteAtrasoAposta: TcxComboBox
            Left = 600
            Top = 30
            Properties.DropDownListStyle = lsFixedList
            Properties.Items.Strings = (
              'N'#227'o'
              'Sim')
            Style.TransparentBorder = False
            TabOrder = 33
            Text = 'N'#227'o'
            Width = 50
          end
          object btnImportarParametrosAposta: TcxButton
            Left = 785
            Top = 5
            Width = 80
            Height = 20
            Anchors = [akTop, akRight]
            Caption = 'Importar'
            OptionsImage.Glyph.Data = {
              36040000424D3604000000000000360000002800000010000000100000000100
              2000000000000004000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000090000
              000E000000100000001000000010000000100000001000000011000000110000
              001100000011000000100000000B00000003000000000000000019427ACA245A
              A5FF255CA7FF255BA7FF245AA6FF2459A6FF2358A5FF2358A4FF2356A4FF2256
              A4FF2255A3FF2154A3FF2153A1FF1C468AE303080F2900000002255DA5FF316B
              AEFF6DA6D5FF86CAF0FF46A6E4FF44A3E4FF41A1E3FF3FA0E2FF3C9EE2FF3B9C
              E1FF389BE0FF369AE0FF3498DFFF2C77C1FF10284D8B000000082B68AEFF4984
              BEFF4B8BC5FFB2E3F8FF68BBECFF55B0E8FF52AEE8FF4EACE7FF4CA9E6FF49A8
              E5FF47A6E4FF44A4E4FF41A2E3FF3A92D6FF1C4885D50000000D2F6FB4FF6CA7
              D2FF3F87C4FFAED9F0FF9AD8F5FF66BDEEFF63BBEDFF60B9EBFF5DB6EBFF5BB5
              EAFF57B2EAFF55B0E9FF51AEE7FF4FABE7FF2D69B1FF040B142F3276B9FF8FC7
              E6FF509FD4FF86BCE0FFC5EFFCFF78CAF2FF74C8F1FF72C5F0FF6FC4F0FF6DC2
              EFFF69C0EEFF66BDEEFF63BBEDFF60B9EBFF448BC9FF122D4D81357CBCFFAFE3
              F5FF75C8EDFF59A2D4FFDDF7FDFFDFF8FEFFDDF7FEFFDBF7FEFFD8F5FEFFD4F4
              FDFFD0F2FDFFCCEFFCFFC7EDFBFFC1EBFBFF9ACBE9FF215187CB3882C1FFC7F5
              FEFF97E5FCFF64BAE5FF4D9FD3FF4D9DD2FF4B9BD1FF4A99CFFF4998CFFF4896
              CEFF4694CCFF4592CBFF3073B7FF3072B6FF2F71B5FF2A65A4EA3A88C5FFCDF7
              FEFFA6ECFEFF9CE8FDFF93E4FBFF8EE1FBFF89DFFBFF86DEFAFF81DAFAFF7ED8
              F9FF7BD7F9FF79D6F9FF2A6BB0FF000000140000000A000000073D8EC8FFD0F8
              FEFFAEF0FEFFAAEEFEFFA6EDFEFFA5EBFDFFBBF2FDFFD4F9FEFFD5F9FEFFD3F8
              FEFFD1F8FEFFCEF7FDFF3680BFFF0000000800000000000000003F92CBFFD3F9
              FEFFB6F3FEFFB3F1FDFFB0F1FEFFB8EDFAFF4895CBFF3B8CC6FF3B8AC6FF3A89
              C5FF3A88C5FF3A87C3FF2A6391C20000000500000000000000004197CEFFE2FC
              FEFFE2FCFEFFE1FCFEFFD4F3FAFF458FBFEC040A0E1B00000006000000060000
              000600000006000000060000000400000001000000000000000031739ABF429A
              D0FF4299D0FF4299D0FF4297CFFF153244590000000200000000000000000000
              0000000000000000000000000000000000000000000000000000000000020000
              0003000000030000000400000003000000020000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000}
            TabOrder = 34
            OnClick = btnImportarParametrosApostaClick
          end
          object btnExportarParametrosAposta: TcxButton
            Left = 785
            Top = 30
            Width = 80
            Height = 20
            Anchors = [akTop, akRight]
            Caption = 'Exportar'
            OptionsImage.Glyph.Data = {
              36040000424D3604000000000000360000002800000010000000100000000100
              2000000000000004000000000000000000000000000000000000000000020000
              000A0000000F0000001000000010000000100000001100000011000000110000
              001100000011000000100000000B000000030000000000000000000000094633
              2CC160453BFF644A41FFB87D4EFFB97A4AFFB47444FFB06C3DFFA76436FFA764
              36FF583D36FF5B3E37FF3B2821C20000000A00000000000000000000000D6F53
              47FF947869FF6A4F46FFD8B07BFFD7AE77FFD7AB74FFD6A970FFD5A66DFFD4A5
              6AFF5D413AFF684B41FF543931FF0000000E00000000000000000000000C7357
              4AFF987D6EFF70564BFFFFFFFFFFF6EFEAFFF6EFE9FFF6EEE9FFF5EEE9FFF6EE
              E8FF62473FFF715348FF573B33FF0000000F00000000000000000000000B785C
              4EFF9D8273FF765C50FFFFFFFFFFF7F0EBFFF7F0EBFFF7EFEBFFF6EFEAFFF6EF
              EAFF684E44FF72554BFF593E35FF0000000E00000000000000000000000A7C60
              50FFA28777FF7B6154FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFBFBFFF1F1
              F1FF89756EFF8A7269FF5F443BFF0000000C0000000000000000000000097F63
              54FFA78E7DFF977A6AFF967969FF957967FF84695CFF705548FF8F7B73FF0B67
              37FF295D3CFF81746BFF806C63FF0000000C0000000000000000000000088367
              57FFAB9382FF634A41FF614740FF5E463DFF5C443CFF5B433BFF776761FF0A67
              37FF2AAF7FFF106137FF5B6352FF00000016000000030000000000000007866A
              59FFAF9788FF674E44FFF3EAE4FFE8D9CEFFE9DFD7FFE5DBD5FFD8CFC9FF0B6A
              3BFF4EDCB2FF27C48DFF0C7746FF022E179C000201190000000500000006886D
              5CFFB39C8CFF6B5248FFF4ECE6FFEBE3DCFF47916BFF046B38FF046B38FF056B
              38FF3AD7A5FF37D6A2FF32D3A0FF199966FF044A26D5000D063A000000058B70
              5EFFB7A091FF70564DFFF6EFEAFFEBE4DFFF167E4EFF84EDD1FF52E1B6FF4DDF
              B3FF48DDAFFF44DBACFF3FD9A9FF3AD7A6FF32BE8EFF0F6A3FF6000000048E72
              60FFBBA595FF755A50FFF7F1ECFFF1EEEBFF188252FFB8F7E7FFB4F5E6FFAFF4
              E4FF85EDD2FF52E1B7FF4DDFB3FF5DE2BBFF66D0AEFF16794CF6000000026A56
              49BF8F7361FF795E54FF765D52FFAFA19CFF3B8963FF0D814DFF0D804DFF0D80
              4CFF95F1DAFF65E7C2FF83ECCFFF57B28FFF065932D2010E0832000000010000
              000200000003000000030000000300000005000000090000000C000000140D7B
              4BF2AEF6E5FF94E5CEFF339167FD033A1F910001010F00000003000000000000
              0000000000000000000000000000000000000000000000000000000000070F7F
              4EF287CBB3FF106D42E6011C0F4C000000060000000100000000000000000000
              0000000000000000000000000000000000000000000000000000000000051081
              52F1034B2AAE0007041700000003000000010000000000000000}
            TabOrder = 35
            OnClick = btnExportarParametrosApostaClick
          end
          object btnLimparParametrosAposta: TcxButton
            Left = 785
            Top = 55
            Width = 80
            Height = 20
            Anchors = [akTop, akRight]
            Caption = 'Limpar'
            OptionsImage.Glyph.Data = {
              36040000424D3604000000000000360000002800000010000000100000000100
              2000000000000004000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              00020000000900000012000000180000001A0000001A00000018000000100000
              0005000000010000000000000000000000000000000000000000000000020000
              000D3524146A936338E5A56B3AFFA36938FFA16736FF9D6233FB633E20B70805
              022800000006000000010000000000000000000000000000000000000008442F
              1D78C18B59FEE1AC76FFE4C296FFB5793BFFB5793CFFB5793CFFAD7239FF7E50
              2AD80302042A00000006000000010000000000000000000000000000000DB07D
              4EF3E6B17AFFE9B47DFFE9B47DFFE7C79DFFB67A3DFFB57A3DFFB57A3DFF6953
              7BFF090E5ED50001052800000006000000010000000000000000000000086A4E
              329DEFD7B3FFE9B47DFFE9B47DFFE9B47DFFEACDA4FFB57B3EFF735C86FF313F
              CCFF2935B8FF0B1161D501010627000000050000000100000000000000010000
              000C745538A5F2DDBBFFE9B47DFFE9B47DFFE9B47DFFD1CEE1FF3443CEFF3443
              CDFF3443CEFF2C39BAFF0D1463D4010106260000000500000001000000000000
              00020000000B76583BA4F5E2C1FFE9B47DFFB5A9B8FF829FF1FFB1C9F5FF3949
              D1FF3A4AD1FF3A49D1FF303FBDFF111767D30101062500000005000000000000
              0000000000010000000B785B3DA3E9E1D2FF87A3F2FF87A4F1FF87A3F2FFB9D0
              F7FF3E50D5FF3E50D5FF3F50D5FF3545C2FF141B6AD201010622000000000000
              000000000000000000010000000A2C386FA2C9E2F9FF8CA8F3FF8DA8F3FF8CA8
              F3FFC0D8F9FF4457D9FF4356D9FF4456D9FF3949C2FF141A61C2000000000000
              000000000000000000000000000100000009303D74A1CFE7FBFF92ADF3FF91AD
              F4FF92ADF4FFC6DEFAFF495EDBFF495DDCFF475AD7FF232F8BF0000000000000
              00000000000000000000000000000000000100000008334177A0D4ECFCFF97B2
              F5FF97B2F4FF97B3F5FFCCE4FBFF4A5FDAFF3141A4F6090C214A000000000000
              000000000000000000000000000000000000000000010000000736457A9FD8F0
              FDFF9DB7F5FF9CB7F5FFD9F1FEFF6B81CAF50B0E234700000006000000000000
              0000000000000000000000000000000000000000000000000001000000063947
              7D9EDBF3FEFFDBF3FFFF677FCFF513192C440000000500000001000000000000
              0000000000000000000000000000000000000000000000000000000000010000
              00053543728E4F63AACD151A2D40000000040000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0001000000030000000400000002000000000000000000000000}
            TabOrder = 36
            OnClick = btnLimparParametrosApostaClick
          end
          object btnCopiarAposta: TcxButton
            Left = 785
            Top = 105
            Width = 80
            Height = 20
            Anchors = [akTop, akRight]
            Caption = 'Copiar'
            OptionsImage.Glyph.Data = {
              36040000424D3604000000000000360000002800000010000000100000000100
              2000000000000004000000000000000000000000000000000000000000000000
              0002000000090000000E0000000F0000000F0000001000000010000000110000
              0011000000100000000B00000003000000000000000000000000000000000000
              00087C5345C0AD725EFFAC725DFFAC715DFFAC6F5BFFAB705CFFAB705CFFAB6E
              5CFFAB6E5AFF7A4E41C30000000B000000000000000000000000000000000000
              000BAF7462FFFDFBF9FFFBF6F2FFFBF5F2FFFAF5F1FFFBF4EFFFF9F3EEFFF9F2
              EEFFFAF2ECFFAC715DFF0000000F000000000000000000000000000000000000
              000BB17964FFFDFBFAFFF7EEE7FFF8EDE7FFF7EDE7FFF7EDE6FFF6ECE5FFF6EC
              E5FFFAF2EEFFAE7260FF01010120010101100101010B00000003000000000000
              000BB37C69FFFDFCFBFFF8EFE8FFF7EEE8FFF7EEE8FFF8EEE7FFF7EEE7FFF7EC
              E6FFFAF3EFFFB07863FFC19D92FFB57D6AFF815A4EC30101010B000000000000
              000AB57F6CFFFEFCFBFFF9F0EAFFF8F0EAFFF8EFE9FFF8EFE8FFF8EEE9FFF8EE
              E7FFFBF5F1FFB27A66FFEBE6E2FFFAF3EDFFB6806DFF0101010F000000000000
              0009B98270FFFEFDFCFFF9F2EDFFF9F2EDFFF9F0EBFFF9F0EAFFF8F0EAFFF8F0
              E9FFFBF6F3FFB37D6AFFE9E1DAFFFAF3EFFFB88170FF01010110000000000000
              0008BB8775FFFEFDFDFFFAF3EFFFFAF4EEFFFAF3EEFFFAF1ECFFF9F1EBFFF9F0
              EBFFFCF8F5FFB6806DFFEAE1DBFFFAF4F0FFB98673FF0101010F000000000000
              0007BF8B78FFFEFEFDFFFBF5F1FFFBF5F0FFFBF4F0FFFAF3EFFFFAF3EFFFF9F3
              EDFFFCF9F7FFBA8471FFECE4DDFFFBF5F2FFBB8876FF0101010E000000000000
              0007C18E7EFFFEFEFDFFFAF5F3FFFBF6F2FFFBF5F1FFFBF5F0FFFBF5F0FFFAF4
              EFFFFDFAF8FFBC8978FFEDE7E0FFFBF6F4FFBC8B7AFF0101010D000000000000
              0006C49382FFFEFEFEFFFBF6F4FFFBF6F4FFFCF6F3FFFCF6F3FFFCF4F2FFFBF5
              F1FFFDFBF9FFBF8C7CFFEFE8E3FFFCF8F5FFBF8E7CFF0101010D000000000000
              0005C49785FFFFFEFEFFFEFEFEFFFEFEFEFFFEFEFEFFFEFEFDFFFEFDFDFFFEFD
              FDFFFEFDFCFFC2907FFFF0EBE6FFFCF9F7FFC29180FF0101010C000000000000
              0003967265C0C89988FFC99887FFC79887FFC59786FFC79785FFC79784FFC596
              84FFC59683FFCDA79AFFF4EFEAFFFDFAF8FFC49686FF0101010B000000000000
              000100000003000000040000000BD8BBB0FFF8F8F8FFF5F0EFFFF5F0EFFFF5EF
              EDFFF5EFEDFFF7F0EEFFFAF4F1FFFDFBF9FFC7998AFF0000000A000000000000
              0000000000000000000000000005CCA392FFFFFEFEFFFEFEFEFFFEFEFEFFFEFE
              FEFFFEFEFDFFFEFDFDFFFEFDFDFFFEFDFCFFCA9D8DFF00000009000000000000
              00000000000000000000000000039A7B6FC0CEA495FFCFA494FFCDA494FFCCA3
              93FFCDA392FFCDA391FFCCA291FFCCA290FF97776BC200000006}
            TabOrder = 44
            OnClick = btnCopiarApostaClick
          end
          object lblIntervaloAposta: TcxLabel
            Left = 28
            Top = 83
            Caption = 'Interv.:'
            FocusControl = edtMinimoIntervaloAposta
            Style.TransparentBorder = False
            Properties.Alignment.Horz = taRightJustify
            Transparent = True
            AnchorX = 66
          end
          object edtMinimoIntervaloAposta: TcxCurrencyEdit
            Left = 70
            Top = 80
            Properties.DecimalPlaces = 0
            Properties.DisplayFormat = '0'
            Properties.MaxLength = 5
            Properties.Nullable = False
            Style.TransparentBorder = False
            TabOrder = 10
            Width = 40
          end
          object edtMaximoIntervaloAposta: TcxCurrencyEdit
            Left = 115
            Top = 80
            Properties.DecimalPlaces = 0
            Properties.DisplayFormat = '0'
            Properties.MaxLength = 5
            Properties.Nullable = False
            Style.TransparentBorder = False
            TabOrder = 11
            Width = 40
          end
          object edtMargemIntervaloAposta: TcxCurrencyEdit
            Left = 160
            Top = 80
            Properties.DisplayFormat = '0.##'
            Properties.MaxLength = 5
            Properties.Nullable = False
            Style.TransparentBorder = False
            TabOrder = 12
            Width = 40
          end
        end
        object pcAposta: TcxPageControl
          Left = 0
          Top = 160
          Width = 872
          Height = 456
          Align = alClient
          TabOrder = 1
          Properties.ActivePage = tsParametros
          Properties.CustomButtons.Buttons = <>
          LookAndFeel.Kind = lfUltraFlat
          ClientRectBottom = 450
          ClientRectLeft = 2
          ClientRectRight = 866
          ClientRectTop = 27
          object tsParametros: TcxTabSheet
            Caption = 'Par'#226'metros'
            ImageIndex = 2
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object mParametrosAposta: TcxMemo
              AlignWithMargins = True
              Left = 5
              Top = 5
              Margins.Left = 5
              Margins.Top = 5
              Margins.Right = 5
              Margins.Bottom = 5
              Align = alClient
              Properties.ReadOnly = True
              Properties.ScrollBars = ssBoth
              Properties.WordWrap = False
              Style.TransparentBorder = False
              TabOrder = 0
              Height = 413
              Width = 854
            end
          end
          object tsJogos: TcxTabSheet
            Caption = 'Jogos'
            ImageIndex = 1
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object grdJogos: TcxGrid
              AlignWithMargins = True
              Left = 5
              Top = 5
              Width = 854
              Height = 413
              Margins.Left = 5
              Margins.Top = 5
              Margins.Right = 5
              Margins.Bottom = 5
              Align = alClient
              TabOrder = 0
              object grdJogosView: TcxGridDBTableView
                Navigator.Buttons.CustomButtons = <>
                OnEditKeyDown = grdHistoricoViewEditKeyDown
                DataController.DataSource = dsJogos
                DataController.Summary.DefaultGroupSummaryItems = <>
                DataController.Summary.FooterSummaryItems = <>
                DataController.Summary.SummaryGroups = <>
                OptionsData.Deleting = False
                OptionsData.Editing = False
                OptionsData.Inserting = False
                OptionsSelection.MultiSelect = True
                OptionsSelection.CellMultiSelect = True
                OptionsSelection.InvertSelect = False
              end
              object grdJogosLevel: TcxGridLevel
                GridView = grdJogosView
              end
            end
          end
          object tsComparativo: TcxTabSheet
            Caption = 'Comparativo'
            ImageIndex = 0
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object tlComparativo: TcxTreeList
              AlignWithMargins = True
              Left = 5
              Top = 5
              Width = 854
              Height = 413
              Margins.Left = 5
              Margins.Top = 5
              Margins.Right = 5
              Margins.Bottom = 5
              Align = alClient
              Bands = <
                item
                end>
              Images = ilImagens
              Navigator.Buttons.CustomButtons = <>
              OptionsData.Editing = False
              OptionsData.Deleting = False
              OptionsSelection.InvertSelect = False
              OptionsView.CellEndEllipsis = True
              TabOrder = 0
              object tlcComparativoDescricao: TcxTreeListColumn
                Caption.Text = 'Descri'#231#227'o'
                DataBinding.ValueType = 'String'
                Width = 250
                Position.ColIndex = 0
                Position.RowIndex = 0
                Position.BandIndex = 0
                Summary.FooterSummaryItems = <>
                Summary.GroupFooterSummaryItems = <>
              end
              object tlcComparativoQuantidade: TcxTreeListColumn
                PropertiesClassName = 'TcxCalcEditProperties'
                Properties.DisplayFormat = ',0'
                Properties.ImmediateDropDownWhenActivated = True
                Caption.Text = 'Quantidade'
                DataBinding.ValueType = 'Integer'
                Width = 75
                Position.ColIndex = 1
                Position.RowIndex = 0
                Position.BandIndex = 0
                Summary.FooterSummaryItems = <>
                Summary.GroupFooterSummaryItems = <>
              end
              object tlcComparativoPercentual: TcxTreeListColumn
                PropertiesClassName = 'TcxCalcEditProperties'
                Properties.DisplayFormat = ',0.##%'
                Caption.Text = 'Percentual'
                DataBinding.ValueType = 'Float'
                Width = 75
                Position.ColIndex = 2
                Position.RowIndex = 0
                Position.BandIndex = 0
                Summary.FooterSummaryItems = <>
                Summary.GroupFooterSummaryItems = <>
              end
              object tlcComparativoPercentualHistorico: TcxTreeListColumn
                PropertiesClassName = 'TcxCalcEditProperties'
                Properties.DisplayFormat = ',0.##%'
                Caption.Text = 'Hist'#243'rico'
                DataBinding.ValueType = 'Float'
                Width = 75
                Position.ColIndex = 3
                Position.RowIndex = 0
                Position.BandIndex = 0
                Summary.FooterSummaryItems = <>
                Summary.GroupFooterSummaryItems = <>
              end
              object tlcComparativoDiferenca: TcxTreeListColumn
                PropertiesClassName = 'TcxCalcEditProperties'
                Properties.DisplayFormat = ',0.##%'
                Caption.Text = 'Diferen'#231'a'
                DataBinding.ValueType = 'Float'
                Width = 75
                Position.ColIndex = 4
                Position.RowIndex = 0
                Position.BandIndex = 0
                Summary.FooterSummaryItems = <>
                Summary.GroupFooterSummaryItems = <>
              end
            end
          end
        end
      end
    end
  end
  object pnlLoteria: TPanel
    Left = 0
    Top = 0
    Width = 900
    Height = 30
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object lblLoteria: TcxLabel
      Left = 15
      Top = 8
      Caption = '&Loteria:'
      FocusControl = cbLoteria
      Style.TransparentBorder = False
      Properties.Alignment.Horz = taRightJustify
      Transparent = True
      AnchorX = 52
    end
    object cbLoteria: TcxComboBox
      Left = 55
      Top = 5
      Properties.DropDownListStyle = lsFixedList
      Properties.OnChange = cbLoteriaPropertiesChange
      Style.TransparentBorder = False
      TabOrder = 0
      Width = 100
    end
  end
  object scSkin: TdxSkinController
    Kind = lfFlat
    NativeStyle = False
    SkinName = 'Office2010Silver'
    Left = 32
    Top = 368
  end
  object ilImagens: TcxImageList
    FormatVersion = 1
    DesignInfo = 24117312
    ImageInfo = <
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          00030000000C3030303F00000014000000140000001500000015000000150000
          00150000001500000016000000140000000E0000000300000000000000000000
          000B855B4FC4B8806DFFB87F6CFFB77D6BFF275392FF265190FF264F8EFF244D
          8BFFB47967FFB37866FFB47864FF805648C50000000D00000000000000000000
          0010BA8471FFFCF6F2FFF5E0D2FFF4DFD1FF2F6BBBFF5EB0ECFF4FA7E8FF306B
          BBFFF6E6DDFFF3DBCCFFF6E4DAFFB57966FF0000001200000000000000000000
          000FBD8674FFFDF9F7FFF5E2D7FFF5E1D6FF2B66B8FF6CB8EEFF56ACEAFF2B68
          B8FFF7E8DFFFF4DED0FFF7E8E0FFB67C69FF0000001200000000000000000000
          000EBF8A77FFFEFCFAFFF6E6DCFFF6E5DBFF2761B5FF7BC1F0FF5EB1EBFF2661
          B5FFF8EAE2FFF5E0D4FFF9EDE6FFB77E6BFF0000001100000000000000000000
          000CC18D7BFFFEFDFDFFF7E9E1FFF7E8DFFF235CB2FF88C9F2FF64B4EDFF235B
          B2FFF9EDE6FFF6E3D9FFFAF0EAFFB9806EFF0000000F00000000000000000000
          000BC4907EFFFEFDFDFFF9ECE5FFF8ECE4FF1D57AFFF95D0F4FF93CFF4FF1D57
          AFFFF9EDE6FFF7E7DEFFFAF3EEFFBB8471FF0000000E00000000000000090000
          0016AF7E6CFFF6F6F5FFF9EEE8FFFAEFE9FF1B53AEFF1B53AEFF1B53AEFF1B53
          AEFFF8EBE4FFF7E9E1FFF3EEEBFFA77462FF000000190000000B20865EFF2796
          6CFF1F855DFFB5C1B8FFF0E6E1FFFAF0EDFFFAF1ECFFFAF1ECFFFAF1EBFFF9EF
          E9FFF8EDE7FFEEE0DBFFB0BAAEFF1E7C56FF1D8B60FF1D7A54FF0718113C2D93
          6CFF53E3B6FF1C7951FFABB0A3FFEFE6E3FFFAF4F0FFFBF3EFFFFBF3EEFFF9F1
          EDFFECE1DEFFA8A79BFF15724BFF2AD89DFF188056FF04140D3F00000003081A
          123C339B74FF65E8C0FF1E7750FFA9ACA0FFEEE7E4FFF9F5F2FFF9F4F1FFECE2
          DEFFA7A89AFF17724BFF39DBA6FF1D885DFF04160E4300000004000000000000
          0003081C143A3AA57DFF75EBC9FF1E744EFFA5A89CFFE9E0DDFFE6DDD8FFA4A6
          9AFF17714AFF4BE0B1FF218F63FF937A63FF0000000B00000000000000000000
          000000000003091E163841AC87FF86EFD1FF1E734CFFA1A296FF829280FF1870
          48FF61E7BEFF26976CFFAFBDB2FFB98270FF0000000600000000000000000000
          000000000000000000020A1F183648B58FFF96F3DBFF348B65FF2C8961FF76EC
          C9FF2AA074FFB0C1B5FFF6F4F4FFBE8876FF0000000400000000000000000000
          00000000000000000000000000020B2119364FBD97FFA3F5E1FF89F3D7FF2FA8
          7CFF7D7B69CDCC9E8DFFCB9B8AFF946E61BF0000000200000000000000000000
          0000000000000000000000000000000000010C221B3440B78FFF40B78EFF051E
          1535000000030000000200000002000000010000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          20000000000000040000000000000000000000000000000000000000000A0000
          0010000000110000001100000011000000120000001200000012000000120000
          0012000000120000001300000013000000120000000C0000000381594CC2B47C
          69FFB37B69FFB37B68FFB37A68FFB37A68FFB27A68FFB27A68FFB37968FFB279
          68FFB27967FFB27867FFB17867FFB17866FF7F5649C30000000BB77F6EFFFBF8
          F5FFF8EEE9FFF8EEE9FFF7EFE8FFF7EEE8FFF7EEE8FFF7EEE8FFF7EDE7FFF7ED
          E6FFF6EDE6FFF6ECE6FFF6ECE6FFF6ECE5FFB47B69FF00000011B98472FFFBF8
          F6FFBF998AFFEBDAD3FFBE9788FFEBDAD3FFBD9586FFEBDAD3FFBC9484FFEBDA
          D3FF5D6DDDFFE4DDE1FF5A69DCFFF7EDE7FFB77F6EFF00000011BC8978FFFCFA
          F8FFEBDDD5FFECDCD5FFEBDDD5FFECDCD5FFEBDDD5FFECDCD5FFEBDDD5FFECDC
          D5FFE5DFE3FFE5DFE2FFE5DEE2FFF8EEE9FFB98472FF00000010C08E7DFFFCFA
          F9FFC6A294FFEDDED6FFC4A092FFEDDED6FFC29E8EFFEDDED6FFC19B8CFFEDDE
          D6FF6577E1FFE5E0E4FF6272E0FFF8F1EBFFBC8977FF00000010C39482FFFCFA
          FAFFEDDFD9FFEDDFD8FFEDDFD9FFEDDFD8FFEDDFD9FFEDDFD8FFEDDFD9FFEDDF
          D8FFE6E2E6FFE6E2E6FFE6E2E5FFF9F2EEFFC08E7CFF0000000FC79887FFFDFB
          FAFFCCAB9DFFEEE0DBFFCAA99BFFEEE0DBFFC9A799FFEEE0DBFFC8A496FFEEE0
          DBFF6D81E5FFE8E3E8FF6A7DE4FFFAF4F0FFC49381FF0000000EC99D8CFFFDFC
          FCFFEEE2DCFFEEE2DCFFEEE2DCFFEEE2DCFFEEE2DCFFEEE2DCFFEEE2DCFFEEE2
          DCFFE8E6EAFFE8E5EAFFE8E4E9FFFAF6F2FFC69886FF0000000DCDA190FFFEFC
          FCFFD0B1A3FFEFE3DFFFCFB0A2FFEFE3DFFFCFAFA0FFEFE3DFFFCDAD9FFFEFE3
          DFFF7388E8FFE9E6EBFF7186E7FFFBF7F5FFC99D8BFF0000000DCFA594FFFEFC
          FCFFFDF9F9FFFDF9F9FFFDF9F9FFFDFAF8FFFDF9F8FFFDFAF8FFFCF9F7FFFCF9
          F7FFFCF9F7FFFDF8F7FFFCF9F7FFFCF9F7FFCCA290FF0000000C4B53C3FF8D9E
          ECFF687CE3FF6678E2FF6476E1FF6172E0FF5F70DFFF5F70DFFF5D6CDEFF5B69
          DCFF5966DBFF5664DAFF5462D9FF616DDCFF3337AAFF0000000B4C55C4FF93A4
          EEFF6C80E6FF6A7EE4FF687BE4FF6678E2FF6375E1FF6375E1FF6172E0FF5E6F
          DEFF5C6CDDFF5A69DCFF5766DAFF6472DDFF3538ABFF0000000A4D56C6FF96A7
          EFFF95A6EFFF93A4EDFF90A2EDFF8F9FEDFF8B9BEBFF8B9BEBFF8898EAFF8595
          EAFF8291E7FF7F8DE7FF7D89E5FF7987E5FF3539ACFF000000093A4093C14D55
          C5FF4B53C3FF4A51C1FF484FBFFF464DBEFF444BBBFF444BBBFF4249B9FF4046
          B7FF3E44B4FF3C41B3FF3A3EB0FF393CAEFF282B80C200000006000000040000
          0006000000060000000600000007000000070000000700000007000000070000
          0007000000070000000800000008000000070000000500000001}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          20000000000000040000000000000000000000000000000000007E5446B7B074
          62FFAF7461FFAF7460FFAE735FFFAE725FFFAD715EFFAD715EFFAC705CFFAC70
          5CFFAC705CFFAC6F5BFFAC6F5BFFAB6F5BFFAB6E5AFF7B4F41B7B67E6CFFFBF6
          F3FFFAF5F2FFFAF5F2FFFAF5F2FFFAF5F1FFFAF4F1FFFAF4F1FFF9F4F0FFF9F4
          F0FFF9F4F0FFF9F3F0FFF8F3EFFFF9F3EFFFF8F2EEFFB37968FFB77F6FFFFBF7
          F4FFF7EFE9FFF6EEE9FFF6EEE9FFF6EEE9FFF6EEE9FFF7EEE8FFF6EEE8FFF6EE
          E8FFF6EEE8FFF6EEE8FFF6EDE7FFF5EDE7FFF9F4F0FFB47B6AFFB88171FFFBF8
          F5FFC3976BFFAE7338FFC2956AFFF7F0EAFF6289C0FF245EACFF5F85BDFFF6EE
          E9FF616BC9FF202FB7FF6068C6FFF6EEE9FFFAF4F2FFB47C6BFFB98472FFFCF9
          F7FFBD8951FFF0B87CFFBB844BFFF8F0EBFF3472BBFF6FC2FEFF306EB9FFF7F0
          EAFF3245C6FF5B72ECFF3142C6FFF6EFEAFFFAF6F3FFB67F6DFFBB8674FFFDFA
          F8FFC3905AFFF4C591FFC08B54FFF8F1ECFF3D7AC1FF77C6FDFF3977BEFFF7F0
          EBFF384BCAFF647AEFFF364AC9FFF7F0EAFFFAF7F5FFB7806FFFBC8877FFFDFB
          F9FFC79763FFFBDEB9FFC4935DFFF8F2EEFF4783C7FF81CBFEFF4380C6FFF8F1
          ECFF3E51CDFF6F84F0FF3C4FCCFFF7F0ECFFFBF8F6FFB98271FFBF8C7AFFFEFC
          FBFFD7B58EFFCA9C69FFD5B18AFFF9F4F0FF508CCDFF89CFFEFF4D88CBFFF9F3
          EFFF4357D0FF7A8DF3FF4155CFFFF9F2EEFFFCFAF9FFBB8675FFBF8C7AFFFEFC
          FBFFFAF5F1FFFAF4F0FFF9F4F0FFF9F4F0FF5A94D3FF93D4FEFF5690D0FFF9F3
          EFFF475BD2FF8497F4FF465AD1FFF9F2EEFFFCFAF9FFBB8675FFC08D7DFFFEFD
          FCFFFAF5F2FFFAF5F1FFFAF5F1FFFAF5F1FF629AD7FFB0E2FFFF5E98D5FFFAF4
          F0FF4A5FD4FF8FA1F7FF4A5ED3FFF9F3F0FFFDFBFAFFBC8877FFC28F7EFFFEFD
          FDFFFBF6F3FFFAF6F3FFFAF6F2FFFBF6F3FF8FB7E1FF679FDAFF8CB5DFFFF9F5
          F2FF4D62D5FF98A9F8FF4C61D5FFFAF4F0FFFDFCFAFFBE8A79FFC39281FFFEFE
          FDFFFBF7F4FFFAF7F4FFFAF7F4FFFBF7F4FFFAF7F4FFFAF6F3FFFAF6F2FFFAF6
          F2FF4F64D6FFB5C3FBFF4E63D6FFF9F6F2FFFDFCFCFFBF8D7BFFC49483FFFFFE
          FEFFFBF8F5FFFCF8F5FFFBF7F5FFFBF7F5FFFBF7F5FFFBF7F4FFFBF6F4FFFBF6
          F4FF7B8ADEFF4F64D7FF7B8ADEFFFAF6F3FFFEFDFCFFC08F7EFFC59584FFFFFE
          FEFFFCF8F7FFFBF8F6FFFCF8F6FFFBF8F5FFFBF8F6FFFBF8F5FFFCF8F5FFFCF8
          F5FFFBF8F5FFFBF7F4FFFBF7F4FFFBF7F4FFFEFDFDFFC2907FFFC69786FFFFFF
          FEFFFFFFFEFFFFFEFEFFFFFFFEFFFFFEFEFFFFFEFEFFFFFEFEFFFFFEFEFFFFFE
          FEFFFEFEFEFFFEFEFEFFFFFEFEFFFFFEFDFFFEFEFDFFC39281FF997A6EBDCFA4
          94FFCFA393FFCEA393FFCEA393FFCEA292FFCEA292FFCDA292FFCDA191FFCDA1
          91FFCDA090FFCCA090FFCC9F8FFFCB9E8EFFCB9E8EFF967469BD}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000100000005040F0B2F104531A51A68
          49EB1B7350FF196849EB104530A6030F0A310000000600000001000000000000
          000000000000000000000000000000000005082117531D7352F5299F75FF31BD
          8DFF54D9B0FF2EBD8BFF279D73FF1B7050F50820175600000006000000000000
          000000000000000000000000000204100C2C237958F536BF90FF3FD2A2FF5ADC
          B4FF135C3BFF57DBB1FF3AD09DFF30BC8CFF217655F5040F0B2F000000000000
          0000000000000000000000000004164C389F3AAE86FF41D5A5FF3AC597FF1560
          3FFF15603FFF155F3EFF40C599FF38D09DFF31A980FF154A35A1000000000000
          000000000001000000030000000925785AE746CEA3FF47D8AAFF45D6A8FF53DB
          B0FF63DFB9FF176442FF2FA67CFF3DD2A1FF3BC898FF227355E7000000000000
          0003060C16281D39669F1B3560972D8968FD55DBB3FF4DDAAEFF4FDBAFFF44AA
          87FF196B47FF2C966EFF45D7A8FF43D6A6FF48D6A8FF298262FA000000020E1B
          304C3462A7F45091CDFF5B859EB0368F71F66BDBBAFF52DDB3FF4ACFA4FF1C70
          4DFF68DCB9FF6CE3C1FF52DBB1FF49D8AAFF60D7B2FF287C5EE7070E1726396A
          ADF46AB5E3FF8BCEF0FF315387C5236365D863C0A3FF66E3BFFF55DFB6FF1E75
          51FF1E7550FF1E7550FF42C097FF5DDFB9FF5CBC9EFF1C533F9A25426D9A67A6
          D9FF80D0F2FF5E8DC5FF315F9DE040788DAE399677FA7FDAC1FF7CE9CCFF64E3
          BFFF207954FF62E2BDFF77E7C8FF79D8BDFF338D6DF106110D253D6AABE586CD
          EFFFBBEAFAFF2E58A8FF98CAE9FF8AB3C5CB42838BBB3A9A79FB64C1A4FF86DF
          C7FF97EFD9FF84DFC6FF62C0A2FF379272F40F29204B000000024779BEF99CE0
          F7FF1B48A3FF1A47A2FF1A46A2FF18439AF661A1BFCB478093AD3E8D83D93B97
          7BF43A9C79FF348A6BE3235D489D07120E2200000002000000004573B4E5A9DE
          F4FF9BE1F9FF3060B3FF96CCECFF8DD7F5FFA3DEF7FF78B9D5E05B9BB9C55B88
          A2B028456D9200000004000000020000000100000000000000002F4D77958EBB
          E4FFADEAFAFF5F9AD4FF5286C9FFB0DDF4FF4E82C6FF6BB1E2FF8BD4F3FF7BAF
          DFFF2C4A729600000002000000000000000000000000000000000A1019205184
          C5F0B2DBF2FFB7E9F9FF528ACEFF255AB7FF4D85CBFFA3DDF6FF9FD0EEFF4D7E
          C0F1090F18220000000000000000000000000000000000000000000000011826
          39475488CAF38EBCE6FFB7DFF4FFCAF1FCFFB0DCF4FF87B7E3FF5284C6F31725
          3848000000010000000000000000000000000000000000000000000000000000
          00010B11192035557E974F7FBCE15990D5FF4F7EBBE23556809B0A1119200000
          0001000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000070000
          000B0000000C0000000C0000000C0000000C0000000C0000000C0000000D0000
          000D0000000D0000000D0000000D0000000D0000000C0000000882594DBEB47C
          6AFFB37C69FFB37B6AFFB37B69FFB37B68FFB37B68FFB37A69FFB37A68FFB37A
          68FFB37A68FFB27968FFB37967FFB27967FFB27967FF7F574ABFB67F6DFFF9F2
          EEFFF9F2EEFFF9F2EDFFF9F1EDFFC69A8CFFF9F2ECFFF9F1EDFFF9F1ECFFF9F1
          ECFFC5998BFFF8F0EBFFF8F0EBFFF8F0EBFFF8F0EBFFB37B69FFB78170FFF9F3
          EFFFF7EEE9FFF8EEE8FFF8EEE9FFC79C8DFFF7EEE9FFF8EEE8FFF8EEE9FFF7EE
          E8FFC69A8BFFF8EEE8FFF7EDE8FFF7EEE8FFF9F1ECFFB57D6BFFBA8472FFFAF4
          F0FFF8EFEAFFF8F0EAFFF8EFE9FFC99D8FFFF8EFEAFFF8EFEAFFF7EEEAFFF8EF
          EAFFC89D8EFFF7EFE9FFF8EFE9FFF7EFE9FFF9F1EDFFB7806EFFBB8776FFCCA2
          95FFCAA193FFCBA193FFCAA193FFCAA192FFCBA192FFCAA091FFCA9F91FFCA9F
          91FFC99F91FFC99F90FFC99E90FFC99E8FFFC99F90FFB88371FFBD8A79FFFBF6
          F2FFF9F1ECFFF9F1ECFFF9F0EDFFCCA295FFF8F1ECFFF9F1EBFFF9F0ECFFF9F1
          ECFFCAA194FFF9F0ECFFF9F1EBFFF8F0EBFFF9F3F0FFBA8574FFC08E7CFFFBF6
          F3FFF9F2EEFFFAF2EEFFF9F1EDFFCEA697FFF9F1EDFFF9F1EDFFF9F2EDFFF9F2
          ECFFCDA496FFF9F1EDFFF9F1ECFFF9F1EDFFFBF5F1FFBC8977FFC29180FFFCF8
          F5FFF9F2EFFFFAF3EFFFFAF2EFFFCFA89AFFFAF3EEFFF9F2EEFFF9F2EEFFFAF3
          EEFFCEA799FFF9F2EEFFF9F2EEFFF9F2EDFFFBF5F2FFBF8C7AFFC88E5AFFD9AD
          80FFCE9964FFCD9763FFCD9660FFCD9460FFCB935EFFCA925DFFC9905BFFC88F
          5AFFC88E59FFC88D57FFC88C56FFC78B55FFD19D6CFFBB7943FFC98F5AFFF1DA
          B5FFE7C28DFFE7C18DFFE7C18CFFCD9660FFE6BF89FFE5BE89FFE5BD87FFE5BC
          87FFC98E59FFE4BA84FFE4BA83FFE4B983FFECCDA3FFBC7A44FFCA905CFFF1DB
          B8FFE8C28EFFE7C28DFFE7C18CFFCD9662FFE6BF8BFFE6BE89FFE5BD88FFE5BC
          87FFC98F5AFFE4BB86FFE4BB84FFE4BA83FFEDCEA5FFBD7B45FFCA915EFFF2DD
          B9FFF1DAB9FFF1DAB7FFF1D9B6FFD8AC7EFFF0D8B3FFF0D6B2FFF0D6B1FFEFD5
          AFFFD5A576FFEFD2ACFFEED2A9FFEDD1A9FFEDD1A8FFBE7B45FF976C47BECA91
          5DFFCA8F5BFFC88E5AFFC78C58FFC68B57FFC58955FFC48852FFC48651FFC284
          50FFC2834EFFC0814CFFC0804BFFBF7F49FFBE7D48FF8D5D34BF000000010000
          0002000000020000000200000002000000020000000200000002000000020000
          0003000000030000000300000003000000030000000300000002000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000010000000500000007000000080000
          0008000000080000000800000008000000080000000500000001000000000000
          0000000000000000000000000000000000047B5144BCAB715EFFAC705DFFAB70
          5CFFAB6F5DFFAB6E5CFFAA6E5BFFAA6E5BFF7A4F41BC00000005000000000000
          000000000000000000000000000000000006AF7461FFF9F3EEFFF8F3EEFFF8F2
          EEFFF8F2EEFFF8F2EDFFF8F2EDFFF7F2EDFFAB6F5DFF00000007000000000000
          000000000000000000000000000000000006B17764FFF9F3EFFF082886FF0828
          85FF082885FF082885FF082885FFF8F2EEFFAE715FFF00000007000000050000
          00080000000900000009000000090000000EB27A67FFFAF5F1FFBDC1D3FF092A
          88FFBDC1D3FFF6EFEAFFF6EFEAFFF9F3F0FFAF7461FF000000068F6C5EBCC795
          83FFC79583FFC79583FFC79482FFD8B9AFFFB47D6BFFFAF6F3FFF6F1ECFF939E
          C2FF092D89FFF7F0EAFFF7EFEAFFFAF5F2FFB17764FF00000006C79785FFFFFF
          FFFFFFFFFFFFFFFFFFFFD7B9AEFFFBFBFBFFB8816EFFFBF7F4FFBEC2D5FF0A2F
          8CFFBEC3D4FFF6F1ECFFF6F0EBFFFAF6F3FFB27A67FF00000005C89786FFFFFF
          FFFFFFFFFFFFFFFFFFFFD8BAAFFFFBFBFBFFBA8472FFFBF8F6FF0B348EFF0B34
          8EFF0B338EFF0B318DFF0A318DFFFBF7F5FFB57D6BFF00000005C99887FFDDC4
          BBFFD8BBB0FFD8BBB0FFD8BBAFFFE4D2CBFFBB8775FFFCFAF7FFFCF9F7FFFCF9
          F7FFFCF9F7FFFBF9F7FFFBF9F7FFFBF9F6FFB8816FFF00000004CA9A88FFFFFF
          FFFFFFFFFFFFFFFFFFFFD8BCB1FFFDFDFDFFCCA799FFBD8977FFBD8876FFBC88
          75FFBB8875FFBB8774FFBB8674FFBA8573FF8A6255BF00000002CB9B89FFFFFF
          FFFFFFFFFFFFFFFFFFFFD9BCB1FFFFFFFFFFFEFEFEFFFDFDFDFFE4D2CCFFFCFC
          FCFFFCFCFCFFFCFCFCFFDABDB1FF000000090000000200000001C9905DFFDFBA
          92FFD7A775FFD6A673FFD6A471FFD6A470FFD6A26EFFD6A16DFFD59F6CFFD59F
          6AFFD59D69FFDCAD83FFBB7842FF000000050000000000000000C9905DFFEED4
          AEFFE9C693FFE9C591FFD7A572FFE7C18CFFE6C08AFFE5BE87FFD5A16CFFE4BA
          83FFE3B881FFE8C599FFBA7842FF000000050000000000000000C9905EFFEFD7
          B1FFEFD5B0FFEFD4ADFFE0BC97FFEDD1AAFFEDD1A8FFEBCFA6FFDFB890FFEBCB
          A1FFEAC99FFFE9C89DFFBB7943FF0000000400000000000000009B7550BED19D
          6AFFD09C68FFCE9A66FFCD9864FFCD9662FFCB935FFFCA915DFFC78E5AFFC68B
          57FFC48955FFC38752FF8F613ABF000000020000000000000000000000010000
          0002000000020000000200000002000000020000000200000002000000020000
          0002000000030000000300000002000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0001000000040000000900000007000000020000000000000000000000000000
          0002000000080000000900000004000000010000000000000000000000010000
          00050E090727724A3DBA54362D910000000D0000000200000000000000020000
          000D54362C926F483BBB0E090729000000050000000100000000000000041810
          0D359B6959EBD2B3A8FFCFAFA3FF55382E920000000C000000030000000D5437
          2E93CEADA2FFD0AFA5FF986453EC100A082B00000004000000000201010F8C5D
          4ED6DBC2B8FFF4E5D8FFF7F1EBFFD0B1A6FF553930920000001655382F92CFB0
          A4FFF7F0EBFFDDD9E7FFD7BBB1FF724A3EBB0000000D00000001402C256CC195
          87FFF6E9DFFFE2A669FFECC8A6FFF8F1ECFFD2B2A7FFAC7260FFD2B2A6FFF7F0
          ECFF9A9EE2FF4A57D9FFE1DDEAFFBC8F7FFF39252069000000057D5449BDE2CB
          C5FFEAC49FFFE1A365FFE7BA8DFFF8F1EDFFF7F1EDFFF8F1EDFFF7F1EDFFF8F1
          ECFF7E85E0FF4351DAFF8F94E1FFDDC4BBFF795043BF0000000AA57363EDF4EC
          E8FFE5AF78FFE3A669FFF4E5D7FFF8F2EEFF8D807CFF2C1C16FF8C807CFFF8F2
          EDFFD9D5EAFF4554DBFF5865DEFFF0E4DEFF9E6A59EB0000000BB6806EFFFAF7
          F5FFE5AE73FFE5AB6FFFF9F1ECFFF9F3EFFF523B33FF674E47FF362420FFF8F2
          EFFFF5EFEEFF4956DDFF4D5CDDFFF6F0ECFFAC7564FA0000000BA97769ECF6EE
          EBFFE8B781FFE6AF74FFF6E8DBFFF9F4F0FFA1918BFF4E342DFF48332DFFC8BD
          B9FFEEEAEEFF707BE1FF5F6BE0FFF2E7E3FFA27060EA00000009845F52BAE5D2
          CBFFF0D1AEFFE8B379FFEDC69CFFF9F4F1FFF9F3F1FFF9F4F1FFDAD1CCFF624B
          42FFAFA7B6FFC2C1E9FFACAEE9FFE2CDC5FF7F5A4DBC0000000743312B62CCA5
          97FFFBF4ECFFEBBB85FFEAB67FFFEEC89FFFF6E7D8FFF9F3F0FFF8EFE9FFD7C5
          B7FF634A44FFA69FB6FFF4F0F3FFC79D91FF3E2C266200000003020201089971
          62CFE3CEC5FFF8EADAFFEDBD88FFEBBA84FFECBC83FFECBC83FFEEC292FFF4DF
          CBFFD7C2B1FF675047FFE1C9C1FF7F5B4FB40000000600000001000000011B14
          1229B18575E9DEC4BAFFFBF2ECFFF3D7B7FFEFC595FFEDBD88FFEEC595FFF4D9
          BCFFFAF4F1FFDCC1B8FFAC7D6FE9120D0B1F0000000100000000000000000000
          0001100C0B1A856457B1CDA596FFEAD9D3FFF8F1EFFFFEFDFDFFF8F1EFFFEAD9
          D2FFCBA292FF836154B2100C0A1B000000020000000000000000000000000000
          000000000001000000033A2C264F73564C9A9A7465CBBE8F7DF99A7365CC7256
          4B9A392B25500000000400000001000000000000000000000000000000000000
          0000000000000000000000000001000000020000000200000003000000030000
          0002000000010000000000000000000000000000000000000000}
      end>
  end
  object dsHistorico: TDataSource
    Left = 32
    Top = 432
  end
  object dsJogos: TDataSource
    Left = 64
    Top = 432
  end
  object gpmHistorico: TcxGridPopupMenu
    Grid = grdHistorico
    PopupMenus = <>
    Left = 32
    Top = 400
  end
  object gpmJogos: TcxGridPopupMenu
    Grid = grdJogos
    PopupMenus = <>
    Left = 64
    Top = 400
  end
  object dsComparativo: TDataSource
    Left = 128
    Top = 432
  end
  object dsAcertos: TDataSource
    Left = 160
    Top = 432
  end
  object gpmSimulador: TcxGridPopupMenu
    Grid = grdResultadoSimulador
    PopupMenus = <>
    Left = 96
    Top = 400
  end
  object dsSimulador: TDataSource
    Left = 96
    Top = 432
  end
end
