program LotteryAnalyzer;

uses
  MidasLib,
  Forms,
  U_LotteryAnalyzer in 'U_LotteryAnalyzer.pas',
  U_MainForm in 'U_MainForm.pas' {MainForm},
  U_TextAndTableFrame in 'U_TextAndTableFrame.pas' {TextAndTableFrame: TFrame},
  U_TextFrame in 'U_TextFrame.pas' {TextFrame: TFrame},
  U_TableFrame in 'U_TableFrame.pas' {TableFrame: TFrame};

{$R *.res}

begin
  Application.Title := 'Lottery Analyzer - Analisador e gerador de apostas';
  TLoteria.RunApplication(TMainForm, nil);
end.
