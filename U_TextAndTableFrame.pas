//=============================================================================
// U_TextAndTableFrame
// Lottery Analyzer
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_TextAndTableFrame;

interface

uses
  Classes,
  Controls,
  Forms,
  ImgList,
  StdCtrls,
  cxControls,
  cxGraphics,
  cxLookAndFeelPainters,
  cxLookAndFeels,
  cxPC,
  dxBarBuiltInMenu,
  dxSkinOffice2010Silver,
  dxSkinsCore,
  dxSkinscxPCPainter,
  U_LotteryAnalyzer,
  U_TextFrame,
  U_TableFrame;

type
  // TTextoTabela
  TTextAndTableFrame = class(TFrame)
    pcTextoTabela: TcxPageControl;
    tsTexto: TcxTabSheet;
    tsTabela: TcxTabSheet;
  private
    FTexto: TTextFrame;
    FTabela: TTableFrame;
  protected
    procedure Preencher; override;
  public
    constructor Create(AOwner: TComponent); override;
  end;

implementation

{$R *.dfm}

constructor TTextAndTableFrame.Create(AOwner: TComponent);
begin
  inherited;
  FTexto := TTextFrame.Create(Self);
  FTexto.Parent := tsTexto;
  FTexto.Align := alClient;
  FTexto.AlignWithMargins := True;
  FTexto.Margins.Left := 5;
  FTexto.Margins.Top := 5;
  FTexto.Margins.Right := 5;
  FTexto.Margins.Bottom := 5;
  FTabela := TTableFrame.Create(Self);
  FTabela.Parent := tsTabela;
  FTabela.Align := alClient;
  FTabela.AlignWithMargins := True;
  FTabela.Margins.Left := 5;
  FTabela.Margins.Top := 5;
  FTabela.Margins.Right := 5;
  FTabela.Margins.Bottom := 5;
end;

procedure TTextAndTableFrame.Preencher;
begin
  FTexto.Estatistica := Estatistica;
  FTexto.Mostrar;
  FTabela.Estatistica := Estatistica;
  FTabela.Mostrar;
end;

end.
