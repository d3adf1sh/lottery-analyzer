//=============================================================================
// U_TableFrame
// Lottery Analyzer
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_TableFrame;

interface

uses
  SysUtils,
  Classes,
  Controls,
  DB,
  cxClasses,
  cxControls,
  cxCustomData,
  cxData,
  cxDataStorage,
  cxDBData,
  cxEdit,
  cxFilter,
  cxGraphics,
  cxGrid,
  cxGridCustomPopupMenu,
  cxGridCustomTableView,
  cxGridCustomView,
  cxGridDBTableView,
  cxGridLevel,
  cxGridPopupMenu,
  cxGridTableView,
  cxLookAndFeelPainters,
  cxLookAndFeels,
  cxNavigator,
  cxStyles,
  dxSkinOffice2010Silver,
  dxSkinsCore,
  dxSkinscxPCPainter,
  U_LotteryAnalyzer;

type
  // TTabela
  TTableFrame = class(TFrame)
    dsDados: TDataSource;
    gpmDados: TcxGridPopupMenu;
    grdDados: TcxGrid;
    grdDadosView: TcxGridDBTableView;
    grdDadosLevel: TcxGridLevel;
    procedure grdDadosViewEditKeyDown(Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word; Shift: TShiftState);
  private
    FDataSet: TDataSet;
    function GetTabela: ITabela;
  protected
    procedure Preencher; override;
  public
    destructor Destroy; override;
    property Tabela: ITabela read GetTabela;
  end;

implementation

{$R *.dfm}

destructor TTableFrame.Destroy;
begin
  if Assigned(FDataSet) then
    FreeAndNil(FDataSet);
  inherited;
end;

procedure TTableFrame.grdDadosViewEditKeyDown(
  Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem;
  AEdit: TcxCustomEdit; var Key: Word; Shift: TShiftState);
begin
  if (UpCase(Chr(Key)) = 'C') and (ssCtrl in Shift) then
    grdDadosView.CopyToClipboard(True);
end;

function TTableFrame.GetTabela: ITabela;
begin
  Estatistica.GetInterface(ITabela, Result);
end;

procedure TTableFrame.Preencher;
begin
  if Assigned(Tabela) then
  begin
    grdDadosView.BeginUpdate;
    try
      FDataSet := Tabela.CriarDataSet;
      dsDados.DataSet := FDataSet;
      grdDadosView.DataController.CreateAllItems(True);
    finally
      grdDadosView.EndUpdate;
    end;

    grdDadosView.ApplyBestFit;
  end;
end;

end.
