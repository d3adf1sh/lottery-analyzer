object TextAndTableFrame: TTextAndTableFrame
  Left = 0
  Top = 0
  Width = 500
  Height = 300
  TabOrder = 0
  object pcTextoTabela: TcxPageControl
    Left = 0
    Top = 0
    Width = 500
    Height = 300
    Align = alClient
    TabOrder = 0
    Properties.ActivePage = tsTexto
    Properties.CustomButtons.Buttons = <>
    LookAndFeel.Kind = lfUltraFlat
    ClientRectBottom = 294
    ClientRectLeft = 2
    ClientRectRight = 494
    ClientRectTop = 27
    object tsTexto: TcxTabSheet
      Caption = 'Texto'
      ImageIndex = 0
      ExplicitLeft = 0
      ExplicitTop = 24
      ExplicitWidth = 500
      ExplicitHeight = 276
    end
    object tsTabela: TcxTabSheet
      Caption = 'Tabela'
      ImageIndex = 1
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
    end
  end
end
