//=============================================================================
// U_TextFrame
// Lottery Analyzer
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_TextFrame;

interface

uses
  Classes,
  Controls,
  cxContainer,
  cxControls,
  cxEdit,
  cxGraphics,
  cxLookAndFeelPainters,
  cxLookAndFeels,
  cxMemo,
  cxTextEdit,
  dxSkinOffice2010Silver,
  dxSkinsCore,
  U_LotteryAnalyzer;

type
  // TTexto
  TTextFrame = class(TFrame)
    mDados: TcxMemo;
  private
    function GetTexto: ITexto;
  protected
    procedure Preencher; override;
  public
    property Texto: ITexto read GetTexto;
  end;

implementation

{$R *.dfm}

function TTextFrame.GetTexto: ITexto;
begin
  Estatistica.GetInterface(ITexto, Result);
end;

procedure TTextFrame.Preencher;
begin
  if Assigned(Texto) then
    Texto.GerarString(mDados.Lines);
end;

end.
