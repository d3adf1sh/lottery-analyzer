object TableFrame: TTableFrame
  Left = 0
  Top = 0
  Width = 500
  Height = 300
  TabOrder = 0
  object grdDados: TcxGrid
    Left = 0
    Top = 0
    Width = 500
    Height = 300
    Align = alClient
    TabOrder = 0
    object grdDadosView: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = dsDados
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Deleting = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.MultiSelect = True
      OptionsSelection.CellMultiSelect = True
      OptionsSelection.InvertSelect = False
    end
    object grdDadosLevel: TcxGridLevel
      GridView = grdDadosView
    end
  end
  object dsDados: TDataSource
    Left = 64
    Top = 240
  end
  object gpmDados: TcxGridPopupMenu
    Grid = grdDados
    PopupMenus = <>
    Left = 96
    Top = 240
  end
end
